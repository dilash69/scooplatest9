import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

// Import Social Icons
// import './icons';
import { AppUtility } from './app/common/app.utility';

if (environment.production) {
  enableProdMode();
}

//added for TransferState API to work
document.addEventListener('DOMContentLoaded', () => {
  platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err => AppUtility.log(err));
}); 

/* platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => AppUtility.log(err));  */
