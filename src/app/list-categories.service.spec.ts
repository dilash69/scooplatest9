import { TestBed, inject } from '@angular/core/testing';

import { ListCategoriesService } from './list-categories.service';

describe('ListCategoriesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListCategoriesService]
    });
  });

  it('should be created', inject([ListCategoriesService], (service: ListCategoriesService) => {
    expect(service).toBeTruthy();
  }));
});
