import { AppUtility } from './../common/app.utility';
import { AppSettings } from './../common/app-settings';
export class ShpProductsCO 
{
	id: string;
	productTypeId: string;
	defaultFormatId: string;
	languageId: string;
	subCategoryId: string;
	title: string;
	regularPrice: string;
	salePrice: string;
	mpd: string;
	defaultImage: string;
	status: string;
	taxStatus: string;
	organizationBy: string;
	avgRate: string;
	totalRating: string;
	totalEnroll: string;
	createdBy: string;
	createdDate: string;
	modifiedBy: string;
    modifiedDate: string; 

    productDiscount()
    {
        let priceDifference = parseInt(this.regularPrice) - parseInt(this.salePrice);
		return ((priceDifference/parseInt(this.regularPrice))*100).toFixed();
	}
	
	getProductUrl()
	{
		let searchSpecialSymbolsArray: string[] = [' - ', '--', '(', ')', '{', '}', '[', ']', '  ', '!', '@', '#', '$', '%', '^', '&', '*', '=', '?', '>', '<', '/', '|', ' '];
		//let replace1: string[] 	= ['-', '-', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '-'];
		let productSlug:string = this.title.toLowerCase();
		productSlug =productSlug.replace(/\s[-]/g, '-');
		productSlug =productSlug.replace(/\s/g, '-');
	 	productSlug =productSlug.replace('--', '-');
		productSlug =productSlug.replace(/[^a-zA-Z-]/g, "");
		
		let productUrl = AppSettings.MAIN_WEBSITE_HOME.concat('/').concat(encodeURIComponent(productSlug)).concat('/').concat(this.id);
		return productUrl;
	}

	getTrimmedPostTitle()
	{
		return AppUtility.getTrimmedReadableString(this.title, 100);
	}
		
}