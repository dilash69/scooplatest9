import { CatObjectCategoryRelationCO } from "./catObjectCategoryRelationCO";
import { CmnContactUsQueriesCO } from "./contactUsQueryCO";
import { UserCategoryRelationCO } from "./userCategoryRelationCO";

export class UserRegistrationDetailCO
{
    mobileNumber: string;
    message: string
    isTutorRegistration: string;
    emailId: string;
    leadFromId: string;
    //catObjectCategoryRelationCO: CatObjectCategoryRelationCO = new CatObjectCategoryRelationCO();
    userCategoryRelationCO:UserCategoryRelationCO = new UserCategoryRelationCO();
    name: string;
    cmnContactUsQueriesCO:CmnContactUsQueriesCO ;
    redirectUrl:string;
    isForgotPassword:boolean=false;

    firstFilterName:String;
    firstFilterValue:String;
    categoryName:string;
    isWhatsAppNotifChecked:string;
}

 

