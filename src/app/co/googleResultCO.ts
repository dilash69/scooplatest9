import { MetaDataCO } from './metaDataCO';
import { Type } from 'serializer.ts/Decorators';
import { GooglePlusShareCO } from './googlePlus-shareCO';

export class GoogleResultShareCO
{
    public id :String ;

    @Type(() => GooglePlusShareCO)
    result:GooglePlusShareCO;
}