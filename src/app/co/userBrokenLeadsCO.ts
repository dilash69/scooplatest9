import { AppConstants } from "../common/app.constants";

export class UserBrokenLeadsCO
{
    id: string;
    ipAddr: string
    mobile: string;
    url: string;
    action: string;
    visitorId: string;
    platform: string;
    deviceId: string;
    fcmId: string;
    createdOn: string;

    static getAutoBrokenLeadCO(mobileNumber:any,action:string):UserBrokenLeadsCO
    {
        let userBrokenLeadsCO: UserBrokenLeadsCO  = new UserBrokenLeadsCO();
        userBrokenLeadsCO.action = action;
        userBrokenLeadsCO.mobile = mobileNumber;
        userBrokenLeadsCO.platform = "scoop";
        userBrokenLeadsCO.url = window.location.href;
        return userBrokenLeadsCO;
    }

}


