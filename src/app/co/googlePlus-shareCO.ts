import { MetaDataCO } from './metaDataCO';
import { Type } from 'serializer.ts/Decorators';
export class GooglePlusShareCO
{
    public kind :String ;
    public id :String ;
    public isSetByViewer:Boolean;

    @Type(() => MetaDataCO)
    metadata:MetaDataCO;
}