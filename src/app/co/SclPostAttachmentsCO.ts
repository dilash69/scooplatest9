export class SclPostAttachmentsCO
{
    id: string;
    objectType: string;
    objectId: string;
    userId: string;
    name: string;
    title: string;
    url: string;
    type: string;
    fileSize: string;
    previewImage: string;
    pageCounts: string;
    createdDate: string;
    attachmentHeight: string;
    attachmentWidth: string;
    isRandomImage: string;

    public isPortrait(): boolean 
    {
        let isPortrait: boolean = false;
        let  height:number = +this.attachmentHeight;
        let  width:number = +this.attachmentWidth;
        if (height > width) {
            isPortrait = true;
        }
        
        return isPortrait;
    } 


}