export class CatStreamsCO
{
    public id: string;
    public title:string;
    public description: string;
    public status:string;
    public createdBy: string;
    public createdDate:string;
    public modifiedBy: string;
    public modifiedDate: string;

    //UI specific variable
    isSelected:boolean = false;
}
