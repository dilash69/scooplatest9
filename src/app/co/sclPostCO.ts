import { SclPostsSeoCO } from './sclPostsSeoCO';
import { CatCategoriesCO } from './categoryCO';
import { CmsTagsCO } from './cmsTagCO';
import { SclPostArticlesCO } from './sclPostArticlesCO';
import { UserCO } from './userCO';
import { Type } from 'serializer.ts/Decorators';
import { AppUtility } from './../common/app.utility';
import { SclPostQuestionCO } from './sclPostQuestionCO';
import { SclPostPollCO } from './sclPostPollCO';
import { SclPostCommentCO } from './commentCO';
import { SclPostAttachmentsCO } from './SclPostAttachmentsCO';
import { SclPostLabelsCO } from './sclPostLabelsCO';
import { SclQuizCO } from './sclQuizCO';
export class SclPostCO
{
    id: string;
    type: string;
    typeId: string;
    userId: string;
    defaultCategoryId: string;
    status: string;
    totalLikes: string;
    totalComments: any;
    totalShares: string;
    totalViewed: string;
    postViews: string;
    weightage: string;
    slug: string;
    visibility: string;
    avgReadTime: string;
    seoScore: string;
    createdBy: string;
    createdDate: string;
    modifiedBy: string;
    modifiedDate: string;
    pendingCommentCount: string;
    hasUserLiked:string;
    totalReplies: string;
    fbShareCount: string;
    categoryName: string;
    subjectName : string;
    isMathEquationTitle:string;
    isMathEquationDescription:string;
    canonicalUrl: string; 

    @Type(() => SclPostsSeoCO)
    sclPostsSeoCO:SclPostsSeoCO;

    @Type(() => SclPostArticlesCO)
    sclPostArticlesCO:SclPostArticlesCO;

    @Type(() => UserCO)
    userCO:UserCO;

    @Type(() => CmsTagsCO)
    cmsTagsCOList:CmsTagsCO[];

    @Type(() => CatCategoriesCO)
    catCategoriesCOList:CatCategoriesCO[];

    @Type(() => SclPostQuestionCO)
    sclPostQuestionCO:SclPostQuestionCO;

    @Type(() => SclPostPollCO)
    sclPostPollCO:SclPostPollCO;

    @Type(() => SclPostCommentCO)
    mostPopularCommentCO:SclPostCommentCO;

    @Type(() => SclPostAttachmentsCO)
    sclPostAttachmentsCOList:SclPostAttachmentsCO[];

    @Type(() => UserCO)
    profileImageOfLastLikerList:UserCO[];

    @Type(() => SclPostLabelsCO)
    sclPostLabelCO:SclPostLabelsCO;

    @Type(() => SclPostAttachmentsCO)
    sclVideoPostAttachmentsCO:SclPostAttachmentsCO;

    @Type(() => SclQuizCO)
    sclQuizCO:SclQuizCO;

    getDateStringFromMilliseconds(): string {
        return AppUtility.getDateStringFromMilliseconds(Number(this.createdDate));
    }

    getTimeStringFromMilliseconds(): String {
        return AppUtility.getTimeStringFromMilliseconds(Number(this.createdDate));
    }

    isMathematicalEquation():boolean
    {
        if(this.isMathEquationTitle === "1" || this.isMathEquationDescription === "1"  || (this.mostPopularCommentCO && this.mostPopularCommentCO.isMathEquation === "1"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public separateVideoAttachmentCO():void
	{
			if(this.sclPostAttachmentsCOList.length>0)
			{
				let videoIndex=-1;
				for(let i=0;i<this.sclPostAttachmentsCOList.length;i++)
				{
					if(this.sclPostAttachmentsCOList[i].type==="DCP_VIDEO")
					{
						videoIndex=i;
						this.sclVideoPostAttachmentsCO=this.sclPostAttachmentsCOList[i];
					}
				}
				if(videoIndex!=-1)
				{
					this.sclPostAttachmentsCOList.splice(videoIndex);
				}
			}
	}
    
}