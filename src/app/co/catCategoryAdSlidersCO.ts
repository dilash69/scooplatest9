export class CatCategoryAdSlidersCO 
{
	id: string;
	categoryId: string;
	imageName: string;
	libraryTitle: string;
	redirectUrl: string;
	adType: string;
	sequence: string;
	createdBy: string;
	createdDate: string;
} 