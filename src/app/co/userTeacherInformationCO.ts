export class UserTeacherInformationCO
{
    id: String;
    userId: String;
    status: String ;
    location:String
    highestQualification: String;
    specialization: String;
    yearsOfExperience: String;
    teachExamId: String;
    teachPrimarySubject: String;
    teachSecondarySubject: String;
    teachBoards: String;
    currentOccupation: String;
    leadFromInfo: String;
    resume: String;
} 
