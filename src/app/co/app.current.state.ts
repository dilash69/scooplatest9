import { SclPostCommentCO } from './commentCO';
import { UserCO } from "./userCO";
import { Type } from 'serializer.ts/Decorators';

export class AppCurrentState
{
    isUserLoggedIn: boolean = false;
    isServerSideRendering: boolean = true;
/*     processExternalLogout: boolean = false; */
    isBasicMandatoryInitializationsDone: boolean = false;

    @Type(() => UserCO)
    userCO: UserCO;
}