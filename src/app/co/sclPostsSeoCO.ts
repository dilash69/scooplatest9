export class SclPostsSeoCO
{

 	id: string; 
 	postId: string;
 	metaTitle: string;
 	metaDescription: string;
 	metaKeyword: string;
 	facebookTitle: string;
 	facebookDescription: string;
 	facebookImage: string;
 	twitterTitle: string;
 	twitterDescription: string;
 	twitterImage: string;
 	metaNoIndex: string;
 	metaNoFollow: string;
 	focusKeyword: string;
    keywordsList: string[]; 
}