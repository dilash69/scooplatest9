//import { DialogCO } from "./dialogCO";

//import { UserCO } from './scl-userCO';
import { DialogCO } from './dialogCO';
import { Type } from 'serializer.ts/Decorators';
export class ServerInstructionCO
{
	public static FORCE_LOGOUT_TYPE:string = "FORCE_LOGOUT";
	public static RESET_CLIENT_DB_TYPE:string = "RESET_CLIENT_DB";
	
	type:string;
	data:string;
	@Type(() => DialogCO)
	dialogCO:DialogCO;
}