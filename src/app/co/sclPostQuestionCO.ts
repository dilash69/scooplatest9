import { Type } from "serializer.ts/Decorators";
import { SclSubjectiveQuesCO } from "./sclSubjectiveQuesCO";
import { SclPostMcqCO } from "./sclPostMcqCO";

export class SclPostQuestionCO
{
    id: string;
    type: string;
    typeId: string;
    orgId: string;
    categoryId: string;
    subjectId: string;
    firstFilterType: string;
    firstFilterId: string;
    points: string;
    isPremium: string;
    claimedBy: string;
    tat: string;
    questionStatus: string;
    isOrgGaveup: string;
    isResolvedInTat: string;
    isAnsweredByStudent: string;
    isAnsweredByClaimer: string;
    claimerTimeToAnswer: string;
    claimedTime: string;
    
    @Type(() => SclSubjectiveQuesCO)
    sclSubjectiveQuesCO:SclSubjectiveQuesCO;

    @Type(() => SclPostMcqCO)
    sclPostMcqCO:SclPostMcqCO;
}



