export class SclPollOptionCO
{
    id: string;
    pollId: string;
    description: string;
    totalVotes: string;
    bulletTitle: string;
    sequence: string;

    //ui specific variable
    isAnsweredOption:boolean= false;
}
