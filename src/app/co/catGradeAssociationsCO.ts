export class CatGradeAssociationsCO
{
    public id: string;
    public gradeId: string;
    public type: string;
    public typeId: string;
}