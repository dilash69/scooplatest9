import { Type } from 'serializer.ts/Decorators';
import { CatObjectCategoryRelationCO } from './catObjectCategoryRelationCO';
import { CatSubjectsCO } from './catSubjectsCO';
import { CatCategoryFiltersCO } from './catCategoryFiltersCO';
import { CategoryExamCO } from './categoryExamCO';

export class Step3CO 
{
	@Type(() => CatObjectCategoryRelationCO)
    catObjectCategoryRelationCO:CatObjectCategoryRelationCO;
    
    @Type(() => CatSubjectsCO)
    catSubjectCOList:CatSubjectsCO[];

    @Type(() => CatCategoryFiltersCO)
    catCategoryFiltersCOList:CatCategoryFiltersCO[];
    
    @Type(() => CategoryExamCO)
    categoryExamCO:CategoryExamCO;
}