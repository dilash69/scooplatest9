import { SclUserPollOptionCO } from "./sclUserPollOptionCO";
import { Type } from 'serializer.ts/Decorators';
import { SclPollOptionCO } from "./SclPollOptionCO";

export class SclPostPollCO
{
    id: string;
    title: string;
    description: string;
    totalVotes: string;

    @Type(() => SclUserPollOptionCO)
    sclUserPollOptionCO:SclUserPollOptionCO;

    @Type(() => SclPollOptionCO)
    sclPollOptionCOList:SclPollOptionCO[];
}


