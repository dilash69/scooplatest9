import { Type } from "serializer.ts/Decorators";
import { AppConstants } from "../common/app.constants";
import { FilterValueCO } from "./filterValueCO";

export class CatCategoryFiltersCO
{
	public id: string;
    public categoryId:string;
    public filterType: string;
    public isMandatory:string;
    public sequence:string;
    public dependentFilterType: string;
    public createdBy: string;
    public createdDate:string;
	public modifiedBy:string;
    public modifiedDate: string;

    public isSingleSelection: boolean = true;
    public validationMessage:string = ""; 
    public filterTypeTitle:String

    @Type(() => FilterValueCO)
    selectedFilterValueCOArray: FilterValueCO[];

    @Type(() => FilterValueCO)
    dropDownFilterValueCOList: FilterValueCO[];

    public getSelectedFilterTitleForDropDown():string
    {
        let selectedFilterTitleForDropDown:string = "";
        if(this.selectedFilterValueCOArray && this.selectedFilterValueCOArray.length > 0)
        {
            for(let filtervalueCO of this.selectedFilterValueCOArray)
            {
                if(this.isSingleSelection)
                {
                    selectedFilterTitleForDropDown = filtervalueCO.filterTypeTitle;
                    break;
                }
                else
                {
                    if(selectedFilterTitleForDropDown.length>0)
                    {
                        selectedFilterTitleForDropDown = selectedFilterTitleForDropDown + ", " + filtervalueCO.filterTypeTitle;
                    }
                    else
                    {
                        selectedFilterTitleForDropDown = filtervalueCO.filterTypeTitle;
                    }
                }                
            }
        }

        if(!selectedFilterTitleForDropDown)
        {
            selectedFilterTitleForDropDown = AppConstants.NONE;
        }

        return selectedFilterTitleForDropDown;
    }

    public populateFilterType()
    {
        if(this.dropDownFilterValueCOList)
        {
            for(let filterValueCO of this.dropDownFilterValueCOList)
            {
                filterValueCO.filterType = this.filterType;
            }
        }
    }
}
