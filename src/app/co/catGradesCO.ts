export class CatGradesCO 
{
    public id: string;
    public name: string;
    public nextFilterType: string;

    public getGradeFirstLetter():string
    {
        let gradeFirstLetter:string="";
        if(this.name==="Class 1-5")
        {
            gradeFirstLetter="1-5";
        }
        else if(this.name==="Class 6-8")
        {
            gradeFirstLetter="6-8";
        }
        else if(this.name==="Class 9-10")
        {
            gradeFirstLetter="9-10";
        }
        else if(this.name==="Class 11-12(Regular)")
        {
            gradeFirstLetter="11-12";
        }
        else
        {
            gradeFirstLetter= this.name.charAt(0);
        }
        return gradeFirstLetter;
    }
}
