export class SclMcqOptionCO
{
    id: string;
    mcqId: string;
    bulletTitle: string;
    description: string;
    sequence: string;
    isCorrect: string;
    totalAttempts: string;

    //ui specific variable
    isAnsweredOption:boolean = false;
}
