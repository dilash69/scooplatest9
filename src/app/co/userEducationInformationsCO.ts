export class UserEducationInformationsCO
{
    id:string;
	userId:string;
	education:string;
	boardUniversity:string;
	tenureTo:string;
	tenureFrom:string;
	fieldOfStudy:string;
	createdBy:string;
	createdDate:string;
	modifiedBy:string;
	modifiedDate:string; 
}