import { CmsCategoryMetaDataCO } from './categoryMetaDataCO';
//import { CatCategorySlidersCO } from './categorySliderCO';
import { Type } from 'serializer.ts/Decorators';
import { CatExamsCO } from './catExamsCO';
import { AppUtility } from '../common/app.utility';

export class CatCategoriesCO 
{
	id: string;
	master_category_id: string;
	title: string;
	description: string;
	slug: string;
	status: string;
	bgColor: string;
	bgImage: string;
	displayLogo: string;
	featuredImage: string;
	articleCount: string;
	productCount: string;
	isArticleCat:string;
	createdBy: string;
	createdDate: string;
	modifiedBy: string;
	modifiedDate: string;
	isUserJoin: boolean;
	totalMemberCount: string;
	totalShareCount: string;
	canonicalUrl: string; 
	pageUrl: string;
	fbShareCount: string;


	
	@Type(() => CatExamsCO)
	catExamsCOList:CatExamsCO[];
	
	// @Type(() => CatCategorySlidersCO)
	// catCategorySlidersCOList:CatCategorySlidersCO[];

	@Type(() => CmsCategoryMetaDataCO)
	cmsCategoryMetaDataCO:CmsCategoryMetaDataCO;

	//ui specific variable
	categoryTitleColor:any = "bgcolor"+ AppUtility.getRandomBgColor();
	isActive:boolean= false;
	
} 