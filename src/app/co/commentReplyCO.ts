import { UserCO } from './userCO';
import { Type } from 'serializer.ts/Decorators';
import { SclPostAttachmentsCO } from './SclPostAttachmentsCO';
export class SclCommentReplyCO
{

    id: string;
	userId: string;
	commentId: string;
	reply: string;
	totalLikes: string;
	createdBy: string;
	createdDate: string;
	modifiedBy: string;
	modifiedDate: string;
    hasUserLiked: boolean;
    
    @Type(() => UserCO)
    repliedUserCO:UserCO;

    @Type(() => SclPostAttachmentsCO)
    sclPostAttachmentsCOList:SclPostAttachmentsCO[];

    getLikeCount()
    {
        if(this.totalLikes === null)
        {
            return 0;
        }
        else
        {
            return this.totalLikes;
        }
    }
   
}