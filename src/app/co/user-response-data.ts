export class UserResponseData
{
	public userResponseCode:string;
	public userMessageList:string[];
	public responseValues:any;
}