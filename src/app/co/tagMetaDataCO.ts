export class CmsTagMetaDataCO {
	
	id: string;
	tagId: string;
	metaTitle: string;
	metaDescription: string;
	metaKeywords: string;
	focusKeyword: string;
	metaNoindex: string;
	metaNofollow: string;
    keywordsList: string[]; 
}