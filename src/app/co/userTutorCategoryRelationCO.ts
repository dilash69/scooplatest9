export class UserTutorCategoryRelationCO
{
    id: string;
    userId: string
    categoryId: string;
    subjectId: string;
    examId: string;
    streamId: string;
    stageId: string;
    mediumLanguageId: string;
    topicId: string;
    yearId: string;
    boardId: string;
    zoneId: string;
    createdDate: string;
}