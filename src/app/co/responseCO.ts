// import { UserResponseData } from './user-response-data';
// import { Type } from "@angular/compiler/src/core";

import { Type } from "serializer.ts/Decorators";
import { ServerInstructionCO } from "./server-instructionCO";
import { UserResponseData } from "./user-response-data";
import { AppConstants } from "../common/app.constants";

export class ResponseCO
{
	serverResponseCode:string;
	serverMessage:string;
	isCacheable:string = AppConstants.NO;
	expirationTime:string;
	@Type(() => ServerInstructionCO)
	serverInstructionCO:ServerInstructionCO;
	@Type(() => UserResponseData)
	userResponseData:UserResponseData = new UserResponseData();

	//extra fields on client
	clientErrorCode:string;
	isUserError:boolean = false;

	public isSuccessResponse()
	{
		let isSuccessResponse:boolean = false;
		if(this.serverResponseCode === AppConstants.SUCCESS_REQUEST)
		{
			if(this.userResponseData)
			{
				if(this.userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST)
				{
					isSuccessResponse = true;
				}
			}
			else
			{
				isSuccessResponse = true;
			}
		}

		return isSuccessResponse;
	}
}