import { AppConstants } from "../common/app.constants";
import { AppUtility } from "../common/app.utility";

export class FilterValueCO
{    
    filterTypeTitle:string;
    categoryId:string;
    filterType:string;
    filterTypeId:string;
    isOther:string;
    sequence:number;

    dependentBoardId:string;
    dependentExamId:string;
    dependentMediumLanguageId:string;
    dependentStageId:string;
    dependentStreamId:string;
    dependentSubjectId:string;
    dependentTopicId:string;
    dependentYearId:string;
    dependentZoneId:string;

    // ui specific
    isActive:boolean = false;
    isSearchFiltered:boolean = true;
    filterValueColor:any = "bgcolor"+ AppUtility.getRandomBgColor();

/*     getDummyCatCategoryFiltersCO(categoryId:string)
    {
        let catCategoryFiltersCO:CatCategoryFiltersCO = new CatCategoryFiltersCO();
        catCategoryFiltersCO.categoryId = categoryId;
        catCategoryFiltersCO.dependentFilterType = this.dependentFilterType;
        catCategoryFiltersCO.filterType = this.filterType;
        catCategoryFiltersCO.isMandatory = '0';        

        return catCategoryFiltersCO;
    } */

    cloneObject(filterType:string) : FilterValueCO
    {
        let newFilterValueCO:FilterValueCO = new FilterValueCO();

        newFilterValueCO.categoryId = this.categoryId;
        newFilterValueCO.filterType = filterType;
        newFilterValueCO.dependentBoardId = this.dependentBoardId;
        newFilterValueCO.dependentExamId = this.dependentExamId;
        newFilterValueCO.dependentMediumLanguageId = this.dependentMediumLanguageId;
        newFilterValueCO.dependentStageId = this.dependentStageId;
        newFilterValueCO.dependentStreamId = this.dependentStreamId;
        newFilterValueCO.dependentSubjectId = this.dependentSubjectId;
        newFilterValueCO.dependentTopicId = this.dependentTopicId;
        newFilterValueCO.dependentYearId = this.dependentYearId;
        newFilterValueCO.dependentZoneId = this.dependentZoneId;

        if(this.filterType===AppConstants.EXAM)
        {
        newFilterValueCO.dependentExamId = this.filterTypeId;
        }
        else if(this.filterType===AppConstants.STREAM)
        {
        newFilterValueCO.dependentStreamId = this.filterTypeId;
        }
        else if(this.filterType===AppConstants.STAGE)
        {
        newFilterValueCO.dependentStageId = this.filterTypeId;
        }
        else if(this.filterType===AppConstants.SUBJECT)
        {
        newFilterValueCO.dependentSubjectId = this.filterTypeId;
        }
        else if(this.filterType===AppConstants.TOPIC)
        {
        newFilterValueCO.dependentTopicId = this.filterTypeId;
        }
        else if(this.filterType===AppConstants.YEAR)
        {
        newFilterValueCO.dependentYearId = this.filterTypeId;
        }
        else if(this.filterType===AppConstants.BOARD)
        {
        newFilterValueCO.dependentBoardId = this.filterTypeId;
        }
        else if(this.filterType===AppConstants.MEDIUM_LANGUAGE)
        {
        newFilterValueCO.dependentMediumLanguageId = this.filterTypeId;
        }
        else if(this.filterType===AppConstants.ZONE)
        {
        newFilterValueCO.dependentZoneId = this.filterTypeId;
        }

        return newFilterValueCO;
    }
} 