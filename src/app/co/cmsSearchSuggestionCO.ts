import { Type } from 'serializer.ts/Decorators';

export class CmsSearchSuggestionCO 
{
	subjectId : string;
	subjectTitle: string;
	categoryId: string;
	categoryTitle: string;
}