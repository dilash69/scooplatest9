import { AppConstants } from "../common/app.constants";

export class UserCO
{
    id: string;
    organizationId: string
    leadFormId: string;
    defaultCategoryId: string;
    email: string;
    mobile: string;
    firstName: string;
    middleName: string;
    lastName: string;
    profileImage: string;
    slug: string;
    tokenId: string;
    dob: string;
    about: string;
    isStudent: string;
    isTutor: string;
    lastLogin: string;
    lastLoginIp: string;
    facebookId: string;
    googleId: string;
    status: string;
    createdBy: string;
    createdDate: string;
    modifiedBy: string;
    modifiedDate: string;
    mobileNoVerified: string;
    totalShareCount: string;
    canonicalUrl: string; 
    pageUrl: string;
    fbShareCount: string;
    organisationName: string;
    feedbackQuestionDatetime:string;
    isCategoryRequired:string;
    city:string;
    userType:string;
    tutorBadge:string;
    whatsappNotification:string

    //UI specific variable
    newAccessTokenRequired:string;
    password:string;
    redirectUrl:string;
    isAdmin:string;
    isTutorAutoVerifyOtp:string;
    

    
    public fullName() 
    {
        let  fullName  =  this.firstName;
        if(this.middleName  !=  null  &&  this.lastName  !=  null) 
        {
            fullName  =  this.firstName  +  ' '  +  this.middleName  +  ' '  +  this.lastName ;
        }
        else if(this.middleName  !=  null) 
        {
            fullName  =  this.firstName  +  ' '  +  this.middleName;
        }
        else if(this.lastName  !=  null) 
        {
            fullName  =  this.firstName  +  ' '  +  this.lastName;
        }
        else if(!fullName)
        {
            fullName = "Unknown";
        }
        return  fullName.charAt(0).toUpperCase() + fullName.slice(1);
    }

    isBlockedUser():boolean
    {
        let blockedUserIdList:string[]=  this.getBlockedUserIdList();
        if(blockedUserIdList)
        {
            if(blockedUserIdList.indexOf(this.id) == -1)
         {
             return false;
         }
         else
         {
             return true;
         }
        }
        else
        {
            return false;
        }
    }

    getBlockedUserIdList():string[]
    {
        return JSON.parse(localStorage.getItem(AppConstants.dirtyList));
    }
}