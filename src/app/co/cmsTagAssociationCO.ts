export class CmsTagAssociationCO
{
    tagId: string;
    title: string;
    id: string;
    objectType: string;
    objectId: string;
    objectSubtype: string;
    createdBy: string;
    createdDate: string;
    modifiedBy: string;
    modifiedDate: string;
}