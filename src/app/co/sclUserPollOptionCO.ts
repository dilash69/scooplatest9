export class SclUserPollOptionCO
{
    id: string;
    pollId: string;
    userId: string;
    pollOptionId: string;
    createdOn: string;
}