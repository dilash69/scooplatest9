import { AppConstants } from './../common/app.constants';


export class DialogCO
{
	public static ALERT_DIALOG_TYPE:string = "ALERT";
	public static MESSAGE_DIALOG_TYPE:string = "MESSAGE";
	public static POSITIVE_DIALOG_TYPE:string = "POSITIVE";

	public static LINK_ACTION_TYPE:string = "LINK";
	
	public title:string;
	public description:string;
	public dialogCancelable:string;
	public type:string;

	public okButtonTitle:string;
	public okButtonActionType:string;
	public okButtonActionJsonData:string;
	
	public negativeButtonRequired:string = AppConstants.NO;
	public negativeButtonTitle:string;
	public negativeButtonActionType:string;
	public negativeButtonActionJsonData:string;
}