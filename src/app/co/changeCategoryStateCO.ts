
import { Type } from 'serializer.ts/Decorators';
import { CatCategoriesCO } from './categoryCO';

export class ChangeCategoryStateCO 
{
    purpose:string;
    isUserLoggedIn: boolean = true;
    currentStep: number;

    @Type(() => CatCategoriesCO)
    catCategoriesCOList:CatCategoriesCO[];

    isUserhasSelectedcategories():boolean
    {
        let isUserhasSelectedcategories:boolean = false;
        if(this.catCategoriesCOList && this.catCategoriesCOList.length>0)
        {
            isUserhasSelectedcategories = true;
        }

        return isUserhasSelectedcategories;
    }
}