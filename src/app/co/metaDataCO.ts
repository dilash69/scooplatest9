import { GlobalCountsCO } from './globalCountsCO';
import { Type } from 'serializer.ts/Decorators';
export class MetaDataCO
{
    public type:String;

    @Type(() => GlobalCountsCO)
    globalCounts:GlobalCountsCO;
    
}