import { Type } from 'serializer.ts/Decorators';
import { CatSubjectsCO } from './catSubjectsCO';

export class CatExamsCO 
{
      id : string;
	  title : string;
	  description : string;
	  status : string;
      createdBy : string;
	  createdDate : string;
	  modifiedBy : string;
	  modifiedDate : string;
	  courseCount : string;
	  testCount : string;
}