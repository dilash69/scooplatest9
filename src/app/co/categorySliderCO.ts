export class CatCategorySlidersCO 
{
	id: string;
	categoryId: string;
	fileName: string;
	libraryTitle: string;
	redirectUrl: string;
	sequence: string;
	createdBy: string;
    createdDate: string; 
}