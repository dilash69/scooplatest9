import { SclPostCO } from './sclPostCO';
import { CatCategoriesCO } from './categoryCO';
import { Type } from 'serializer.ts/Decorators';
export class GetDetailedCategoryForHomeCO 
{
    @Type(() => CatCategoriesCO)
    catCategoriesCO:CatCategoriesCO;

    @Type(() => SclPostCO)
    sclPostCOList:SclPostCO[];
} 
 
