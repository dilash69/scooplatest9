export class SclPostLabelsCO
{
    id:string;
	labelName:string;
	title:string;
	description:string;
	shortDescription:string;
	featuredImage: string;
	dcpVideoId: string;
	viewType:string;
}
