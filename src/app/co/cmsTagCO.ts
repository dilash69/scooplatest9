import { CmsTagMetaDataCO } from './tagMetaDataCO';
import { Type } from 'serializer.ts/Decorators';
export class CmsTagsCO 
{
	id: string;
	title: string;
	slug: string;
	createdBy: string;
	createdDate: string;
	modifiedBy: string;
	modifiedDate: string;
	totalArticles: string; 
	summary: string; 
	totalShareCount: string;
	canonicalUrl: string; 
	pageUrl: string;
	fbShareCount: string;
	

	@Type(() => CmsTagMetaDataCO)
	cmsTagMetaDataCO:CmsTagMetaDataCO;
	
}
