
import { Type } from 'serializer.ts/Decorators';
import { CatCategoryFiltersCO } from './catCategoryFiltersCO';
import { CatObjectCategoryRelationCO } from './catObjectCategoryRelationCO';
import { CatCategoriesCO } from './categoryCO';

export class ChangeCategoryUserSelectionCO 
{
    //categoryId:string;
    catCategoryCO:CatCategoriesCO = new CatCategoriesCO();
    message:string;

    @Type(() => CatCategoryFiltersCO)
    catCategoryFiltersCOList: CatCategoryFiltersCO[];

    @Type(() => CatCategoriesCO)
    allCatCategoriesCOList: CatCategoriesCO[];

    getCatObjectCategoryRelationCOList()
    {
      let catObjectCategoryRelationCOList:CatObjectCategoryRelationCO[] = new Array();
      let catObjectCategoryRelationCO: CatObjectCategoryRelationCO = new CatObjectCategoryRelationCO();
      catObjectCategoryRelationCO.categoryId = this.catCategoryCO.id;
      let isSingleSelection:boolean = true;

      for (let catCategoryFiltersCO of this.catCategoryFiltersCOList)
      {        
        if(catCategoryFiltersCO.isSingleSelection)
        {
            if(catCategoryFiltersCO.selectedFilterValueCOArray && catCategoryFiltersCO.selectedFilterValueCOArray.length > 0)
            {
                catObjectCategoryRelationCO.populateCatObjectCategoryRelationCO(catCategoryFiltersCO.selectedFilterValueCOArray[0]);
            }          
        }
      }

      for (let catCategoryFiltersCO of this.catCategoryFiltersCOList)
      {        
        if(!catCategoryFiltersCO.isSingleSelection)
        {
          isSingleSelection = false;
          if(catCategoryFiltersCO.selectedFilterValueCOArray && catCategoryFiltersCO.selectedFilterValueCOArray.length > 0)
          {
            for(let filterValueCO of catCategoryFiltersCO.selectedFilterValueCOArray)
            {
              let catObjectCategoryRelationCOTemp: CatObjectCategoryRelationCO = catObjectCategoryRelationCO.getDuplicateCO();
              catObjectCategoryRelationCOTemp.populateCatObjectCategoryRelationCO(filterValueCO);
              catObjectCategoryRelationCOList.push(catObjectCategoryRelationCOTemp);
            }
          }
        }
      }

      if(isSingleSelection)
      {
        catObjectCategoryRelationCOList.push(catObjectCategoryRelationCO);
      }

      return catObjectCategoryRelationCOList;
    }

/*     getSelectedCategoryExamCO()
    {
        if(this.catCategoryFiltersCOList)
        {
            for(let catCategoryFiltersCO of this.catCategoryFiltersCOList)
            {
                if(catCategoryFiltersCO.filterType === AppConstants.EXAM)
                {
                    if(catCategoryFiltersCO.selectedFilterValueCOArray && catCategoryFiltersCO.selectedFilterValueCOArray.length > 0)                    
                    {
                        return catCategoryFiltersCO.selectedFilterValueCOArray[0];
                    }
                }
            }
        }
    } */

/*     examSelected(categoryExamCO:CategoryExamCO)
    {
        this.categoryId = categoryExamCO.categoryId;
        this.selectedCategoryExamCO = categoryExamCO;
    } */

/*     isExamLastSelectedFilter():boolean
    {
        if(this.catCategoryFiltersCOList)
        {
            for(let catCategoryFiltersCO of this.catCategoryFiltersCOList)
            {
                if(catCategoryFiltersCO.filterType !== AppConstants.EXAM)
                {
                    if(catCategoryFiltersCO.selectedFilterValueCOArray && catCategoryFiltersCO.selectedFilterValueCOArray.length > 0)                    
                    {
                        return false;
                    }
                }
            }
        }

        return true;
    } */
}