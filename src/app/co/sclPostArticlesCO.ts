import { AppUtility } from "../common/app.utility";

export class SclPostArticlesCO
{
    id: string;
    title: string;
    slug: string;
    shortDecription: string;
    description: string;
    publishStatus: string;
    publishTime: string;
    publishDate: string;
    commentStatus: string;
    socialSharing: string;
    featuredPost: string;
    featuredImage: string;
    postPassword: string;
    readability: string;
    createdBy: string;
    createdDate: string;
    modifiedBy: string;
    modifiedDate: string;
    canonicalUrl: string; 
    pageUrl: string;
    displayDate:string;
    featuredImageThumbnail:string;
 



    public timeCalculate()
	  {
		let creationDate: any = this.publishTime;
        let currentDate = Date.now();
        return Math.round((currentDate - creationDate)/3600000);
      }
      
      getDateStringFromMilliseconds(): String {
        return AppUtility.getDateStringFromMilliseconds(Number(this.displayDate));
    }

    getTimeStringFromMilliseconds(): String {
        return AppUtility.getTimeStringFromMilliseconds(Number(this.displayDate));
    }

    public limitedTextDescription(limit:number)
    {
        return AppUtility.getTrimmedReadableString(this.shortDecription, limit);
    }

    public limitedTextTitle(limit:number)
    {
        return AppUtility.getTrimmedReadableString(this.title, limit);
    }
    
}