export class CatCategoryQuickLinksCO
{
	id: string;
	categoryId: string;
	linkTitle: string;
	sequence: string;
	redirectToLink: string;
	modifiedBy: string;
	modifiedOn: string;
	isExternalUrl: boolean;
	icon:string;
} 