export class CmsCategoryMetaDataCO 
{
	id: string;
	categoryId: string;
	metaTitle: string;
	metaDescription: string;
	metaKeywords: string;
	focusKeyword: string;
	metaNoindex: string;
    metaNofollow: string; 
}