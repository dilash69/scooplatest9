import { FilterValueCO } from "./filterValueCO";
import { AppConstants } from "../common/app.constants";

export class CategoryExamCO {
    examId: string;
    examTitle: string;
    categoryId: string;

    // ui specific variable
    isActive: boolean = false;
    examTitleColor:any;

    public getFilterValueCO():FilterValueCO
    {
        let filterValueCO:FilterValueCO = new FilterValueCO();
        filterValueCO.filterTypeId = this.examId;
        filterValueCO.filterTypeTitle = this.examTitle;
        filterValueCO.filterType = AppConstants.EXAM;
        filterValueCO.categoryId = this.categoryId;

        return filterValueCO;
    }
}