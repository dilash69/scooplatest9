import { Type } from 'serializer.ts/Decorators';
import { CatExamsCO } from './catExamsCO';
import { FilterValueCO } from './filterValueCO';
import { AppConstants } from '../common/app.constants';
import { AppSettings } from '../common/app-settings';

export class CoursesHeaderCO
{
	public categoryId: string;
	public categoryTitle: string;
	public filterType :string;
	@Type(() => FilterValueCO)
	filterValueCOList:FilterValueCO[];
	
	//ui specific variable
	public showCategoryDownFaIcon: boolean = false;
	public filterUrl:string;
	

	public toggleCategoryDownFaIcon()
	{
		this.showCategoryDownFaIcon = !this.showCategoryDownFaIcon
	}

	updateCategoryDownFaIconClass(): any 
	{
		return this.showCategoryDownFaIcon ? 'show' : '';
	}

	getFilterUrl(filterTypeId:string):string
	{
		if(this.filterType === AppConstants.SUBJECT)
		{
			this.filterUrl = AppSettings.MAIN_WEBSITE_HOME + "/store/search?sortby=prchtl&cat_id="+this.categoryId +"&category="+this.categoryTitle+"&subject_ids%5B%5D="+filterTypeId;
		}
		else
		{
			this.filterUrl = AppSettings.MAIN_WEBSITE_HOME + "/store/search?sortby=prchtl&cat_id="+this.categoryId +"&category="+this.categoryTitle;
		}
		return this.filterUrl;
	}
} 
