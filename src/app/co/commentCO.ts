import { UserCO } from './userCO';
import { SclCommentReplyCO } from './commentReplyCO';
import { Type } from 'serializer.ts/Decorators';
import { SclPostAttachmentsCO } from './SclPostAttachmentsCO';
export class SclPostCommentCO
{

    id: string;
	postId: string;
	userId: string; 
	comment: string;
	approved: string;
	replyCoJson: string;
	totalLikes: string;
	totalReply: string;
	createdBy: string;
	createdDate: string;
	modifiedBy: string;
    modifiedDate: string;
    answerScore: string;
    hasUserLiked: boolean;
    public isMathEquation:string;
    public selectedMcqOption:string;
    
    //ui specific veriables
    postCommentStatus: string;

    @Type(() => UserCO)
    userCO:UserCO;

    @Type(() => SclCommentReplyCO)
    sclCommentReplyCOList:SclCommentReplyCO[];

    @Type(() => SclPostAttachmentsCO)
    sclPostAttachmentsCOList:SclPostAttachmentsCO[];

    getLikeCount()
    {
        if(this.totalLikes === null)
        {
            return 0;
        }
        else
        {
            return this.totalLikes;
        }
    }
   
    getReplyCount()
    {
        if(this.totalReply === null)
        {
            return 0;
        }
        else
        {
            return this.totalReply;
        }
    }

}