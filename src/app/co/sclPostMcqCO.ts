import { Type } from "serializer.ts/Decorators";
import { SclUserMcqOptionCO } from "./sclUserMcqOptionCO";
import { SclMcqOptionCO } from "./sclMcqOptionCO";

export class SclPostMcqCO
{
    id: string;
    title: string;
    description: string;
    totalAttempts: string;

    @Type(() => SclUserMcqOptionCO)
    sclUserMcqOptionCOList:SclUserMcqOptionCO[];

    @Type(() => SclMcqOptionCO)
    sclMcqOptionCOList:SclMcqOptionCO[];
}
