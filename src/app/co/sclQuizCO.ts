export class SclQuizCO
{
 	id: string; 
 	title: string;
 	noOfQuestion: string;
 	points: string;
 	time: string;
 	totalMarks: string;
 	negativeMarksPercentage: string;
 	attemptLimit: string;
 	expireOn: string;
 	totalAttempts: string;
 	reportCounts: string;
 	pointDistributed: string;
 	featuredImage: string;
    createdOn: string;
    quizAttemptStatus:string;
    isHtmlTitle:string;
    isMathEquationTitle:string;
}



