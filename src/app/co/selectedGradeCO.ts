import { Type } from "serializer.ts/Decorators";
import { CatGradesCO } from "./catGradesCO";
import { CatObjectCategoryRelationCO } from "./catObjectCategoryRelationCO";
import { UserCategoryRelationCO } from "./userCategoryRelationCO";
import { UserTutorCategoryRelationCO } from "./userTutorCategoryRelationCO";

export class SelectedGradeCO
{
    @Type(() => CatGradesCO)
    catGradesCO:CatGradesCO;

    //public selectedSubjectTitleList:string[];

    //public selectedStreamTitleList:string[];
    public selectedSubjectTitleList:string[]= new Array();
    public selectedStreamTitleList:string[]= new Array();
    //public userCategoryRelationCOList:UserCategoryRelationCO[]= new Array();
    public userTutorCategoryRelationCOList:UserTutorCategoryRelationCO[] = new Array();

    /* public catObjectCategoryRelationCOList:CatObjectCategoryRelationCO[]= new Array();
 */

    /* @Type(() => CatObjectCategoryRelationCO)
    catObjectCategoryRelationCOList:CatObjectCategoryRelationCO[]; */
}