import { SclPostAttachmentsCO } from "./SclPostAttachmentsCO";

export class AppPopupCO
{
    isEntryPopUpShownOnce:boolean = false;
    isEntryPopUpRequired:boolean = true;
    isExitPopUpShownOnce:boolean = false;
    isExitPopUpRequired:boolean = true;
    
    isEntryPopUpOpen: boolean = false;
    isExitPopUpOpen: boolean = false;
    isOpenLoginPopUp:boolean = false;
    isSignUpPopUpOpen:boolean = false;
   

    isSwitchCategoryPopUpOpen : boolean = false;
    isConfirmUnFollowPopUpOpen : boolean =false;
    isOtpPopUpOpen : boolean = false;
    isDenyUnfollowPopUpOpen : boolean = false;
    isRequestACallBackPopUpOpen : boolean = false;
    isAskQuestionPopUpOpen : boolean = false;
    isReplyDeletePopUpOpen : boolean = false;
    isCommentDeletePopUpOpen : boolean = false;
    isUserQualificationPopUpOpen : boolean = false;
    isAndroidAppPopUpOpen : boolean = false;
    isTutorSuccessPopUpOpen : boolean = false;
    isImagePopUpOpen : boolean = false;
    sclPostAttachmentsCOList:SclPostAttachmentsCO[];
    sclPostAttachmentPosition : string;
    

    resetAll()
    {
      //this.openPopup = false;
      this.isEntryPopUpOpen = false;
      this.isOpenLoginPopUp = false;
      this.isSignUpPopUpOpen = false;
      this.isExitPopUpOpen = false;
      this.isSwitchCategoryPopUpOpen =false;
      this.isConfirmUnFollowPopUpOpen =false;
      this.isOtpPopUpOpen =false;
      this.isDenyUnfollowPopUpOpen =false;
      this.isRequestACallBackPopUpOpen =false;
      this.isAskQuestionPopUpOpen =false; 
      this.isReplyDeletePopUpOpen = false;     
      this.isCommentDeletePopUpOpen = false;
      this.isUserQualificationPopUpOpen = false;
      this.isAndroidAppPopUpOpen = false;
      this.isTutorSuccessPopUpOpen = false;
      this.isImagePopUpOpen=false;
      this.sclPostAttachmentsCOList=new Array(0);
      this.sclPostAttachmentPosition='';
      this.isEntryPopUpRequired = true;
      this.isExitPopUpRequired = true;
    }

    isAnyPopupOpen()
    {
        if(this.isEntryPopUpOpen)
        {
            console.log("popupOpen-isEntryPopUpOpen")
            return true;
        }
        else if(this.isExitPopUpOpen)
        {
            console.log("popupOpen-isExitPopUpOpen")
            return true;
        }
        else if(this.isOpenLoginPopUp)
        {
            console.log("popupOpen-isExitPopUpOpen")
            return true;
        }
        else if(this.isSignUpPopUpOpen)
        {
            console.log("popupOpen-isSignUpPopUpOpen")
            return true;
        }
      
        else if(this.isSwitchCategoryPopUpOpen)
        {
            console.log("popupOpen-isSwitchCategoryPopUpOpen")
            return true;
        }
        else if(this.isConfirmUnFollowPopUpOpen)
        {
            console.log("popupOpen-isConfirmUnFollowPopUpOpen")
            return true;
        }
        else if(this.isOtpPopUpOpen)
        {
            console.log("popupOpen-isOtpPopUpOpen")
            return true;
        }
        else if(this.isDenyUnfollowPopUpOpen)
        {
            console.log("popupOpen-isDenyUnfollowPopUpOpen")
            return true;
        }
        else if(this.isRequestACallBackPopUpOpen)
        {
            console.log("popupOpen-isRequestACallBackPopUpOpen")
            return true;
        }
        else if(this.isAskQuestionPopUpOpen)
        {
            console.log("popupOpen-isAskQuestionPopUpOpen")
            return true;
        }
        else if(this.isReplyDeletePopUpOpen)
        {
            console.log("popupOpen-isReplyDeletePopUpOpen")
            return true;
        }
        else if(this.isCommentDeletePopUpOpen)
        {
            console.log("popupOpen-isCommentDeletePopUpOpen")
            return true;
        }
        else if(this.isUserQualificationPopUpOpen)
        {
            console.log("popupOpen-isUserQualificationPopUpOpen")
            return true;
        }
        else if(this.isAndroidAppPopUpOpen)
        {
            console.log("popupOpen-isAndroidAppPopUpOpen")
            return true;
        }
        else if(this.isTutorSuccessPopUpOpen)
        {
            console.log("popupOpen-isTutorSuccessPopUpOpen")
            return true;
        }
        else if(this.isImagePopUpOpen)
        {
            console.log("popupOpen-isImagePopUpOpen")
            return true;
        }
        else
        {
            return false;
        }
    }
}