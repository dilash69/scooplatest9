import { AppConstants } from "../common/app.constants";
import { FilterValueCO } from "./filterValueCO";

export class CatObjectCategoryRelationCO
{
	public id: string;
    public objectId:string;
    public objectType: string;
    public objectSubType:string;
    public categoryId: string;
    public examId:string;
    public streamId: string;
    public stageId:string;
    public mediumLanguageId: string;
    public subjectId:string;
    public topicId: string;
    public yearId:string;
    public boardId:string;
    public zoneId:string;
    public createdBy: string;
    public createdDate:string;
	public modifiedBy:string;
    public modifiedDate: string;

    //ui specific variables
    public breadCrumbs: String[]= new Array();
    public radioIdbreadCrumbs : String;
    public isChecked:boolean = false;
    //public isSingleSubject:boolean = true;
    //public subjectFilterValueCOArray:FilterValueCO[];

    getDuplicateCO()
    {
        let catObjectCategoryRelationCO:CatObjectCategoryRelationCO = new CatObjectCategoryRelationCO();
        catObjectCategoryRelationCO.categoryId = this.categoryId;
        catObjectCategoryRelationCO.examId = this.examId;
        catObjectCategoryRelationCO.streamId = this.streamId;
        catObjectCategoryRelationCO.stageId = this.stageId;
        catObjectCategoryRelationCO.subjectId = this.subjectId;
        catObjectCategoryRelationCO.topicId = this.topicId;
        catObjectCategoryRelationCO.yearId = this.yearId;
        catObjectCategoryRelationCO.boardId = this.boardId;
        catObjectCategoryRelationCO.mediumLanguageId = this.mediumLanguageId;
        catObjectCategoryRelationCO.zoneId = this.zoneId;

        return catObjectCategoryRelationCO;
    }

    populateCatObjectCategoryRelationCO(filterValueCO:FilterValueCO)
    {
        if(filterValueCO.filterType===AppConstants.EXAM)
        {
            this.examId = filterValueCO.filterTypeId
        }
        else if(filterValueCO.filterType===AppConstants.STREAM)
        {
            this.streamId = filterValueCO.filterTypeId
        }
        else if(filterValueCO.filterType===AppConstants.STAGE)
        {
        this.stageId = filterValueCO.filterTypeId
        }
        else if(filterValueCO.filterType===AppConstants.SUBJECT)
        {
            this.subjectId = filterValueCO.filterTypeId
        }
        else if(filterValueCO.filterType===AppConstants.TOPIC)
        {
        this.topicId = filterValueCO.filterTypeId
        }
        else if(filterValueCO.filterType===AppConstants.YEAR)
        {
        this.yearId = filterValueCO.filterTypeId
        }
        else if(filterValueCO.filterType===AppConstants.BOARD)
        {
        this.boardId = filterValueCO.filterTypeId
        }
        else if(filterValueCO.filterType===AppConstants.MEDIUM_LANGUAGE)
        {
        this.mediumLanguageId = filterValueCO.filterTypeId
        }
        else if(filterValueCO.filterType===AppConstants.ZONE)
        {
        this.zoneId = filterValueCO.filterTypeId
        }
  }
}