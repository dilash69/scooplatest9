import { UserCO } from './userCO';
import { Type } from 'serializer.ts/Decorators';
export class AccessCheckResponseCO
{
    isUserLogin: string;
    access_token: string;

    @Type(() => UserCO)
    userCO: UserCO;  
}