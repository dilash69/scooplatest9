export class CmnContactUsQueriesCO
{
	public id: string;
	public userId:string;
	public name: string;
	public mobile: string;
	public email: string;
	public message: string;
	public status: string;
	public remark: string;
	public createdBy: string;
	public createdDate: string;
	public modifiedBy: string;
	public modifiedDate: string;

} 