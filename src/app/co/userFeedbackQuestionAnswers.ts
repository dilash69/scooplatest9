export class UserFeedbackQuestionAnswers
{
    id:string;
	userId:string;
	questionId:string;
	answer:string;
	createdDate:string;
	modifiedDate:string; 
} 