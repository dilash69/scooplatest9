import { Injectable } from '@angular/core';
import { CatCategoriesCO } from './co/categoryCO';
import { BehaviorSubject, Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ListCategoriesService {
  catCategoriesCOArray:Observable<CatCategoriesCO[]>;
  private catCategoriesCO = new BehaviorSubject<CatCategoriesCO[]>(null);
  
  constructor() 
  {    
    let categories:CatCategoriesCO[] = new Array();
    this.catCategoriesCOArray = new BehaviorSubject<CatCategoriesCO[]>(null);
    this.catCategoriesCO = <BehaviorSubject<CatCategoriesCO[]>>new BehaviorSubject(categories);
    this.catCategoriesCOArray = this.catCategoriesCO.asObservable();    
  }

   updatedCategories(catCategoriesCOArray: CatCategoriesCO[])
   {
      this.catCategoriesCO.next(catCategoriesCOArray);
   }
}
