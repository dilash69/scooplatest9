import { HttpRequest, HttpResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiCacheMap } from '../common/api.cache.map';
import { AppUtility } from '../common/app.utility';

//https://fullstack-developer.academy/caching-http-requests-with-angular/
var maxAge = 10000;//10 seconds
@Injectable()
export class RequestCache  
{
  apiCacheMap = new ApiCacheMap();

  get(req: HttpRequest<any>): HttpResponse<any> | undefined {
    //const url = req.urlWithParams;
    const cacheKey = RequestCache.getCacheKey(req);
    console.log("cache key",JSON.stringify(cacheKey))
    const cached = this.apiCacheMap.getCachedData(cacheKey);

    if (!cached) {
      return undefined;
    }

    //console.log("Returning cache response for ", req.body);
    //console.log("Available cache ", this.apiCacheMap);

    const isExpired = cached.lastRead < (Date.now() - maxAge);
    const expired = isExpired ? 'expired ' : 'not expired';
  //  console.log("cached.lastRead ", cached.lastRead);
  //  console.log("Date.now() ", Date.now());
  //  console.log("Date.now() - maxAge ", Date.now() - maxAge);
  //  console.log(expired);
    return cached.response;
  }

  put(req: HttpRequest<any>, response: HttpResponse<any>): void {
  //  const url = req.url;
    const entry = { response, lastRead: Date.now() };

    const cacheKey = RequestCache.getCacheKey(req);

    this.apiCacheMap.addApiInCache(cacheKey, entry);

/*     const expired = Date.now() - maxAge;
    this.cache.forEach(expiredEntry => {
      if (expiredEntry.lastRead < expired) {
        this.cache.delete(expiredEntry.url);
      }
    }); */
  }

  set(cacheKey: string, response: HttpResponse<any>): void 
  {
    const entry = { response, lastRead: Date.now() };
    this.apiCacheMap.addApiInCache(cacheKey, entry);
  }

  public static getCacheKey(req: HttpRequest<any>)
  {
    let cacheKey;
    if(req.method === 'POST')
    {
      cacheKey = req.body;
    }
    else
    {//GET request
      let paramMap = AppUtility.sort(AppUtility.getRequestMapFromUrl(req.urlWithParams));
      console.log("obj : ", JSON.stringify(paramMap));
      cacheKey = JSON.stringify(paramMap);
    }

    return cacheKey;
  }
}