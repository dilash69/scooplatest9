import { isPlatformBrowser } from "@angular/common";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Inject, Injectable, PLATFORM_ID } from "@angular/core";
//import { Observable } from 'rxjs';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { deserialize } from 'serializer.ts/Serializer';
import { ResponseCO } from "../co/responseCO";
import { ApiActions } from "../common/api.actions";
import { ApiCall } from "../common/api.call";
import { AppSettings } from "../common/app-settings";
import { AppConstants } from "../common/app.constants";
import { RequestCache } from "./request-cache.service";

@Injectable()
export class CachingInterceptor implements HttpInterceptor {

  constructor(private requestCache: RequestCache, @Inject(PLATFORM_ID) private platformId: Object) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    //return this.sendRequest(req, next, this.cache);
    if (CachingInterceptor.isCacheable(req)) {

      const cachedResponse = this.requestCache.get(req);

      if (cachedResponse) 
      {
        //console.log('Current cache : ', this.requestCache.apiCacheMap);
        if(req.body)
        {
          console.log('Returning CachingInterceptor Cached response for : ' + req.body);
        }
        else
        {
          console.log('Returning CachingInterceptor Cached response for : ' + req.params);
        }

        return of(cachedResponse);
      }
      else 
      {
        return this.sendRequest(req, next, this.requestCache);
      }
    }
    else {
      // continue if not cachable.
      //console.log("333333" , req);
      //return next.handle(req);
      return this.sendRequest(req, next, this.requestCache);
    }
  }

  /**
 * Get server response observable by sending request to `next()`.
 * Will add the response to the cache on the way out.
 */
  private sendRequest(
    req: HttpRequest<any>,
    next: HttpHandler,
    cache: RequestCache): Observable<HttpEvent<any>> {

    // No headers allowed in npm search request
    /* const noHeaderReq = req.clone({ headers: new HttpHeaders() }); */

    return next.handle(req).pipe(
      tap(event => {
        // There may be other events besides the response.
        if (event instanceof HttpResponse) {

          this.processGetCompletePostDataBySlugResponse(req, event);

          if(isPlatformBrowser(this.platformId))
          {
            if (CachingInterceptor.isResponseCachable(event)) {
              cache.put(req, event); // Update the cache.
            }
            else {
              //do not cache failed responses
              /*
              AppUtility.log('Failed response for : ');
              AppUtility.log(event.body);
              */
            }
          }
        }
      })
    );
  }

  private processGetCompletePostDataBySlugResponse(req: HttpRequest<any>, event : HttpResponse<any>)
  {
/*     console.log("request detail : " , req.body);
    console.log("request detail 2 : " , req.urlWithParams);
    console.log("request detail 3 : " , this.getApiName(req)); */
    if(this.getApiName(req) === ApiActions.getCompletePostDataBySlug)
    {
      let responseCO:ResponseCO = event.body;
      if(responseCO.serverResponseCode)
      {
        if(responseCO.serverResponseCode === AppConstants.SUCCESS_REQUEST)
        {
          this.setGetObjectCOBySlugResponse(event);
          
          let newResponseValues = responseCO.userResponseData.responseValues['getPostInitialCommentList'];
          if(newResponseValues)
          {//if slug belongs to a post
            this.setGetCoursesForHeaderResponse(event);
            this.getCategoryDetailByCategoryId(event);
            this.getCategoryQuickLinks(event);
            this.getCategoryAdSlider(event);
            this.getPostInitialCommentList(event);
            this.getYouMightAlsoLikePost(event);
          }
        }
      }
    }
  }

  private getYouMightAlsoLikePost(event : HttpResponse<any>)
  {
      let cloneResponse : HttpResponse<any> = this.cloneHttpResponse(event);
      let responseCO:ResponseCO = cloneResponse.body;
      responseCO.isCacheable = AppConstants.YES;
      let newResponseValues = responseCO.userResponseData.responseValues['getYouMightAlsoLikePost'];

      let getYouMightAlsoLikePostApiCall: ApiCall = new ApiCall(ApiActions.getYouMightAlsoLikePost);
      getYouMightAlsoLikePostApiCall.addRequestParams('categoryId', responseCO.userResponseData.responseValues['categoryId']);
     // getCategoryDetailByCategoryIdApiCall.addRequestParams('postId', "270");

      let reqBody:string = getYouMightAlsoLikePostApiCall.constructPostBody(null);

      responseCO.userResponseData.responseValues = newResponseValues;

      this.requestCache.set(reqBody, cloneResponse);
  }

  private getPostInitialCommentList(event : HttpResponse<any>)
  {
    //console.log("caching getPostInitialCommentList 11:" + JSON.stringify(event));
      let cloneResponse : HttpResponse<any> = this.cloneHttpResponse(event);
      let responseCO:ResponseCO = cloneResponse.body;
      responseCO.isCacheable = AppConstants.YES;
      let newResponseValues = responseCO.userResponseData.responseValues['getPostInitialCommentList'];

      let getPostInitialCommentListApiCall: ApiCall = new ApiCall(ApiActions.getPostInitialCommentList);
      getPostInitialCommentListApiCall.addRequestParams('postId', responseCO.userResponseData.responseValues['postId']);
     // getCategoryDetailByCategoryIdApiCall.addRequestParams('postId', "270");

      let reqBody:string = getPostInitialCommentListApiCall.constructPostBody(null);


      responseCO.userResponseData.responseValues = newResponseValues;

      //console.log("caching getPostInitialCommentList : ", JSON.stringify(cloneResponse));
      //console.log("caching getPostInitialCommentList : ", cloneResponse);
      this.requestCache.set(reqBody, cloneResponse);
  }

  private getCategoryAdSlider(event : HttpResponse<any>)
  {
      let cloneResponse : HttpResponse<any> = this.cloneHttpResponse(event);
      let responseCO:ResponseCO = cloneResponse.body;
      responseCO.isCacheable = AppConstants.YES;
      let newResponseValues = responseCO.userResponseData.responseValues['getCategoryAdSlider'];

      let getCategoryAdSliderApiCall: ApiCall = new ApiCall(ApiActions.getCategoryAdSlider);
      getCategoryAdSliderApiCall.addRequestParams('categoryId', responseCO.userResponseData.responseValues['categoryId']);
      getCategoryAdSliderApiCall.addRequestParams('adType', responseCO.userResponseData.responseValues['adType']);
     // getCategoryDetailByCategoryIdApiCall.addRequestParams('postId', "270");

      let reqBody:string = getCategoryAdSliderApiCall.constructPostBody(null);
      responseCO.userResponseData.responseValues = newResponseValues;

      this.requestCache.set(reqBody, cloneResponse);
  }

  private getCategoryQuickLinks(event : HttpResponse<any>)
  {
      let cloneResponse : HttpResponse<any> = this.cloneHttpResponse(event);
      let responseCO:ResponseCO = cloneResponse.body;
      responseCO.isCacheable = AppConstants.YES;
      let newResponseValues = responseCO.userResponseData.responseValues['getCategoryQuickLinks'];

      let getCategoryQuickLinksListApiCall: ApiCall = new ApiCall(ApiActions.getCategoryQuickLinks);
      getCategoryQuickLinksListApiCall.addRequestParams('categoryId', responseCO.userResponseData.responseValues['categoryId']);
     // getCategoryDetailByCategoryIdApiCall.addRequestParams('postId', "270");

      let reqBody:string = getCategoryQuickLinksListApiCall.constructPostBody(null);
      responseCO.userResponseData.responseValues = newResponseValues;

      this.requestCache.set(reqBody, cloneResponse);
  }

  private getCategoryDetailByCategoryId(event : HttpResponse<any>)
  {
      let cloneResponse : HttpResponse<any> = this.cloneHttpResponse(event);
      let responseCO:ResponseCO = cloneResponse.body;
      responseCO.isCacheable = AppConstants.YES;
      let newResponseValues = responseCO.userResponseData.responseValues['getCategoryDetailByCategoryId'];

      let getCategoryDetailByCategoryIdApiCall: ApiCall = new ApiCall(ApiActions.getCategoryDetailByCategoryId);
      getCategoryDetailByCategoryIdApiCall.addRequestParams('categoryId', responseCO.userResponseData.responseValues['categoryId']);
     // getCategoryDetailByCategoryIdApiCall.addRequestParams('postId', "270");

      let reqBody:string = getCategoryDetailByCategoryIdApiCall.constructPostBody(null);
      responseCO.userResponseData.responseValues = newResponseValues;

      this.requestCache.set(reqBody, cloneResponse);
  }

  private setGetCoursesForHeaderResponse(event : HttpResponse<any>)
  {
      let getCoursesForHeaderApiCall : ApiCall = new ApiCall(ApiActions.getCoursesForHeader);
      let reqBody:string = getCoursesForHeaderApiCall.constructPostBody(null);

      let cloneResponse : HttpResponse<any> = this.cloneHttpResponse(event);
      let responseCO:ResponseCO = cloneResponse.body;
      responseCO.isCacheable = AppConstants.YES;
      let responseValues = responseCO.userResponseData.responseValues['getCoursesForHeader'];
      responseCO.userResponseData.responseValues = responseValues;

      this.requestCache.set(reqBody, cloneResponse);
  }

  private setGetObjectCOBySlugResponse(event : HttpResponse<any>)
  {
      let cloneResponse : HttpResponse<any> = this.cloneHttpResponse(event);
      let responseCO:ResponseCO = cloneResponse.body;
      responseCO.isCacheable = AppConstants.YES;
      let newResponseValues = responseCO.userResponseData.responseValues['getObjectCOBySlug'];
      
      let getObjectCOBySlugApiCall: ApiCall = new ApiCall(ApiActions.getObjectCOBySlug);
      getObjectCOBySlugApiCall.showLoader = true;
      getObjectCOBySlugApiCall.addRequestParams('slug', responseCO.userResponseData.responseValues['slug']);
      let reqBody:string = getObjectCOBySlugApiCall.constructPostBody(null);

      responseCO.userResponseData.responseValues = newResponseValues;

      this.requestCache.set(reqBody, cloneResponse);
  }

  private cloneHttpResponse(httpResponse : HttpResponse<any>) : HttpResponse<any>
  {
    let httpResponseString : string = JSON.stringify(httpResponse);
    let clonedHttpResponse : HttpResponse<any> = JSON.parse(httpResponseString);
    clonedHttpResponse = deserialize<HttpResponse<any>>(HttpResponse, clonedHttpResponse);

    return clonedHttpResponse;
  }

  public static isCacheable(req: HttpRequest<any>) 
  {
    let isCacheable: boolean = false;
    

    if ((req.method === 'POST') && (req.url.includes(AppSettings.API_ENDPOINT))) 
    {//caching applicable for only API requestss
      //console.log("ddddddddddddddddd" + req.body);
     // console.log(req.body);
      isCacheable = true;
    }
    else if ((req.method === 'GET') && (req.url.includes(AppSettings.API_DOMAIN))) 
    {
      isCacheable = true;
    }

    if(CachingInterceptor.shouldSkipCache(req))
    {
      isCacheable = false;
    }

/*     if(!isCacheable)
    {
      console.log("not cacheable api : ", req);
      console.log("ffff" + req.method + req.url + AppSettings.API_ENDPOINT);
    } */
    
    return isCacheable;
  }

  public static isResponseCachable(httpResponse: HttpResponse<any>): boolean {
    let isResponseCachable: boolean = false;
    if (httpResponse.body) {
      //       AppUtility.log("Response in interceptor");
      //       AppUtility.log(httpResponse.body);
      let responseCO: ResponseCO = <ResponseCO>httpResponse.body;

      if (responseCO.serverResponseCode) {
        if (responseCO.serverResponseCode === AppConstants.SUCCESS_REQUEST
          && responseCO.isCacheable === AppConstants.YES) {
          //           AppUtility.log("Response is cachable");
          //            AppUtility.log(responseCO);
          isResponseCachable = true;
        }
      }
    }

    return isResponseCachable;
  }

  private isGoogleAnalyticReq(url: String): boolean {
    if (url.includes('https://www.google-analytics.com')
      || url.includes('https://www.googleadservices.com')
      || url.includes('https://www.googletagmanager.com')
      || url.includes('https://googleads.g.doubleclick.net')
    ) {
      return true;
    }
  }

  private getApiName(req: HttpRequest<any>): String 
  {
    if(req.url.includes("/fc") || req.url.includes("/gc"))
    {
      if(req.body)
      {
        //console.log("req body ",req.body);
        let reqMap = JSON.parse(req.body);
        let actionName = reqMap["ACTION"];
        return actionName;
      }
      else
      {
        let actionName = req.params.get("ACTION");
        return actionName;
      }
    }
  }

  public static shouldSkipCache(req: HttpRequest<any>): boolean 
  {
    //console.log("request1",req)
    let shouldSkipCache:boolean = false;
    if(req.url.includes("/fc") || req.url.includes("/gc"))
    {
      if(req.body)
      {
        let reqMap = JSON.parse(req.body);
        let invalidateCache = reqMap["invalidateCache"];
  //      console.log("shouldSkipCache", invalidateCache);
        if(invalidateCache && invalidateCache === "Y")
        {
            shouldSkipCache = true;
        }
      }
      else
      {
        let invalidateCache = req.params.get("invalidateCache");
        if(invalidateCache && invalidateCache === "Y")
        {
          shouldSkipCache = true;
        }
      }
    }
    else
    {
      shouldSkipCache = true;
    }

    return shouldSkipCache;
  }
}
