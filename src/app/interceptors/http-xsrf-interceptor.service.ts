import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpInterceptor, HttpXsrfTokenExtractor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';


 @Injectable()
export class HttpXsrfInterceptor implements  HttpInterceptor
 {
  private tokenExtractor: HttpXsrfTokenExtractor
//  private tokenExtractor: HttpXsrfTokenExtractor
  constructor( )
   {
     
  }
   intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> 
   {
   
    
   // const headerName = 'XSRF-TOKEN';
  //  const respHeaderName = 'X-XSRF-TOKEN';
 
   // let token = this.tokenExtractor.getToken() as string;

  //  let token = "SUHELKHAN!@#";
  //  token+"suhel";
  //  if (token !== null && !req.headers.has(headerName)) {
 
   //   req = req.clone({ headers: req.headers.set(respHeaderName, token) });
      
  //  } 
    return next.handle(req);
  }  
  
}



/* @Injectable()
export class HttpXsrfInterceptor implements HttpInterceptor {

   constructor(private tokenExtractor: HttpXsrfTokenExtractor) {}

   intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

     let requestMethod: string = req.method;
     requestMethod = requestMethod.toLowerCase();

     if (requestMethod && (requestMethod === 'post' || requestMethod === 'delete' || requestMethod === 'put' )) {
         const headerName = 'X-XSRF-TOKEN';
         let token = this.tokenExtractor.getToken() as string;
         if (token !== null && !req.headers.has(headerName)) {
           req = req.clone({ headers: req.headers.set(headerName, token) });
         }
      }

    return next.handle(req);
   }
} */