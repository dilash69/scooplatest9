import { isPlatformBrowser, isPlatformServer } from "@angular/common";
import { HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { deserialize } from "serializer.ts/Serializer";
import { ResponseCO } from "../co/responseCO";
import { AppSettings } from "../common/app-settings";
import { CachingInterceptor } from "./cachingInterceptor";
import { RequestCache } from "./request-cache.service";

@Injectable()
export class TransferStateInterceptor implements HttpInterceptor {

  constructor(private transferState:TransferState, @Inject(PLATFORM_ID) private platformId: Object) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) 
  { 
    //console.log("TRANSFER_STATE_KEY called ",req.urlWithParams);
   // return next.handle(req);
   if(AppSettings.ENABLE_TRANSFER_STATE && CachingInterceptor.isCacheable(req))
    {
/*       console.log('TransferStateInterceptor',req.body);
      console.log('TransferStateInterceptor JsonString', JSON.stringify(req.body));
      console.log('TransferStateInterceptor JsonString replaced', JSON.stringify(req.body).replace(/[^a-zA-Z1-9]/g, "")); */
 //     let reqBody:any = req.body;

/*       if(!(reqBody instanceof String))
      {
        return next.handle(req);
      } */
      
      let uniqueKey:string = RequestCache.getCacheKey(req);      
      uniqueKey = uniqueKey.replace(/[^a-zA-Z1-9]/g, "");
  //    console.log("uniqueKey ", uniqueKey);
      const TRANSFER_STATE_KEY = makeStateKey<ResponseCO>(uniqueKey);
    //  console.log("TRANSFER_STATE_KEY ", TRANSFER_STATE_KEY);
  
      if (this.transferState.hasKey(TRANSFER_STATE_KEY) && isPlatformBrowser(this.platformId)) 
      {        
        let responseCO:ResponseCO = this.transferState.get<ResponseCO>(TRANSFER_STATE_KEY, null);
        responseCO = deserialize<ResponseCO>(ResponseCO, responseCO);

        if(responseCO.isSuccessResponse())
        {
          console.log('returning TransferStateInterceptor cache for key : ' + TRANSFER_STATE_KEY);
          this.transferState.remove(TRANSFER_STATE_KEY);
    
          let httpResponse: HttpResponse<any> = new HttpResponse({'body':responseCO, 'headers' : req.headers, 'status' : 200, 'statusText' : "success", 'url' : req.url});
          return of(httpResponse);
        }
        else
        {
          return next.handle(req);
        }
      }
      else
      {
 //       AppUtility.log('else this.transferState.hasKey(TRANSFER_STATE_KEY)');
        return next.handle(req).pipe(
          tap(event => {
            // There may be other events besides the response.
            if (event instanceof HttpResponse) 
            { 
              if (isPlatformServer(this.platformId)) 
              {
                if(event.body)
                {
                    let responseCO:ResponseCO = <ResponseCO>event.body;
                    responseCO = deserialize<ResponseCO>(ResponseCO, responseCO);
                    if(responseCO.isSuccessResponse())
                    {
                      this.transferState.set(TRANSFER_STATE_KEY, responseCO);
                      //           console.log('Caching TransferStateInterceptor http response');
                      //           console.log(responseCO);
                    }
                }    
              }
            }
          })
        );
      }
    }
    else
    {
      return next.handle(req);
    }     
  }
}
