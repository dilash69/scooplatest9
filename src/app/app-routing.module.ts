import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageScoopComponent } from './blog-content/home-page-scoop/home-page-scoop.component';
import { SlugComponent } from './blog-content/slug/slug.component';
import { BlogRegisterFormComponent } from './blog-content/blog-register-form/blog-register-form.component';
 /* star */
 

import { SingleAuthorComponent } from './blog-content/single-author/single-author.component';
import { SingleTagComponent } from './blog-content/single-tag/single-tag.component';

import { SocialTimelineComponent } from './blog-content/social-timeline/social-timeline.component';

import { TutorRegistrationPopupComponent } from './blog-content/tutor-registration-popup/tutor-registration-popup.component';
import { SingleSocialTimelineComponent } from './blog-content/single-social-timeline/single-social-timeline.component';
import { ImageGridLayoutsComponent } from './blog-content/image-grid-layouts/image-grid-layouts.component';
import { ImageVideoSliderComponent } from './blog-content/image-video-slider/image-video-slider.component';
import { NotFoundComponent } from './blog-content/not-found/not-found.component';
 /* star */


const routes: Routes = [
    {
        path: '', component: HomePageScoopComponent
    },
    {
        path: ':slug', component: SlugComponent,
        children: [
            {
                path: 'blog-register', component: BlogRegisterFormComponent
            }
        ],
        
    },
     /* star */
     
    {
        path: 'new-tutor/registration', component: HomePageScoopComponent
    },
    {
        path: '404', component: NotFoundComponent
    },
    
    {
        path: 'unanswered/:slug', component: SlugComponent,
        children: [
            {
                path: 'blog-register', component: BlogRegisterFormComponent
            }
        ],
    },
    {
        path: 'author/:slug', component: SingleAuthorComponent,
        children: [
            {
                path: 'blog-register', component: BlogRegisterFormComponent
            }
        ]
    },
    {
        path: 'post/:postId', component: SingleSocialTimelineComponent,
        children: [
            {
                path: 'blog-register', component: BlogRegisterFormComponent
            }
        ]
    },
    {
        path: 'tag/:slug', component: SingleTagComponent,
        children: [
            {
                path: 'blog-register', component: BlogRegisterFormComponent
            }
        ]
    },
    
    {
        path: 'tutor/registration', component: TutorRegistrationPopupComponent
    }, 
    {
        path: 'abc/tutor-registration/:dbAccessToken', component: TutorRegistrationPopupComponent
    },
    {
        path: 'abc/images-grid-layout', component: ImageGridLayoutsComponent
      
    },
    {
        path: 'abc/image-video-slider', component: ImageVideoSliderComponent
      
    },
    {
        path: 'preview/:slug', component: SlugComponent,
    },
    {
        path: '**', redirectTo: '/404'
      } 
     /* star */
    
      

    // {
    //     // path: ':slug', component: SingleBlogComponent,
    //     path: ':slug', component: SlugComponent,
    //     children: [
    //         {
    //             path: 'blog-register', component: BlogRegisterFormComponent
    //         }
    //     ]
    // }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { enableTracing: true, initialNavigation: 'enabled' })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }