import { Component, OnInit } from '@angular/core';
import { AppCurrentState } from './co/app.current.state';
import { AppPopupCO } from './co/app.popup.co';
import { AppUtility } from './common/app.utility';
import { AppCurrentStateService } from './services/app.current.state.service';
import { ManagePopupService } from './services/manage-popup.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit
 {

  public appCurrentState : AppCurrentState = new AppCurrentState();
  public appPopupCO: AppPopupCO = new AppPopupCO();

  constructor(
    private appCurrentStateService :AppCurrentStateService,
    private managePopupService:ManagePopupService
       )
    {
        this.managePopupService.appPopupCO.subscribe(appPopupCO => 
            {
                this.appPopupCO = appPopupCO;    
            }); 
    }

    ngOnInit()
    {
      
        this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => 
        {
            this.appCurrentState = appCurrentState;
            if(this.appCurrentState.isBasicMandatoryInitializationsDone)
            {
                //AppUtility.log('loading the app now');
            }       
        });
    }

    mouseLeave(div : string)
    {        
        if(this.appPopupCO.isEntryPopUpShownOnce)
        {
            if(!this.appCurrentState.isUserLoggedIn)
            {
                this.managePopupService.showExitPopUp();
            }            
        }                
    }


    mouseEnter(div : string) 
    {
        //AppUtility.log("mouse enter : " + div);
     }

    
}
