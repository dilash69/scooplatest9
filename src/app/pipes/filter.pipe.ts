import { CatCategoriesCO } from './../co/categoryCO';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter'
})
export  class  FilterPipe  implements  PipeTransform 
{

    // transform(value: any, args: string): any {
    // let filter = args.toLocaleLowerCase(); 
    // return filter ? value.filter(category=> category.title.toLocaleLowerCase().indexOf(filter) != -1) : value; 
    // } 

    transform(categoryCOArray:  CatCategoriesCO[], searchText:  string):  any 
    {
        searchText  =  searchText.toLocaleLowerCase();
        if (searchText) 
        {
            categoryCOArray  =  categoryCOArray.filter(categoryCO  =>  categoryCO.title.toLocaleLowerCase().indexOf(searchText)  !=  -1);
        }

        if (categoryCOArray.length  ===  0) 
        {
            return  [-1];
        }
        else 
        {
            return  categoryCOArray;
        }
        //return filter ? value.filter(category=> category.title.toLocaleLowerCase().indexOf(filter) != -1) : value; 
    }
} 