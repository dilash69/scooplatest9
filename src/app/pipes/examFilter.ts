import { Pipe, PipeTransform } from '@angular/core';
import { CatExamsCO } from '../co/catExamsCO';

@Pipe({ 
  name: 'exam-filter'
}) 
export class ExamFilterPipe implements PipeTransform {

    // transform(value: any, args: string): any {
    //    let filter = args.toLocaleLowerCase(); 
    //    return filter ? value.filter(category=> category.title.toLocaleLowerCase().indexOf(filter) != -1) : value; 
    // } 

    transform(catExamsArray: CatExamsCO[], searchText: string): any
    {
      searchText = searchText.toLocaleLowerCase(); 

      if(searchText)
      {
        catExamsArray = catExamsArray.filter(categoryExamsCO => categoryExamsCO.title.toLocaleLowerCase().indexOf(searchText) != -1);
      }

      if(catExamsArray.length === 0)
      {
          return [-1];
      }
      else
      {
        return catExamsArray;
      }
    } 
}