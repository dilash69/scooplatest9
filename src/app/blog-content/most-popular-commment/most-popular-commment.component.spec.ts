import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostPopularCommmentComponent } from './most-popular-commment.component';

describe('MostPopularCommmentComponent', () => {
  let component: MostPopularCommmentComponent;
  let fixture: ComponentFixture<MostPopularCommmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostPopularCommmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostPopularCommmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
