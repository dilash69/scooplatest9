import { Component, OnInit,Input, Output } from '@angular/core';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { BlogModuleService } from './../../services/blog-module.service';
import { AppConstants } from './../../common/app.constants';
import { AdjustDrawerService } from './../../services/adjust-drawer.service';
import { AppCurrentStateService } from './../../services/app.current.state.service';


@Component({
  selector: 'app-promotion-banner',
  templateUrl: './promotion-banner.component.html',
  styleUrls: ['./promotion-banner.component.css']
})
export class PromotionBannerComponent implements OnInit {

  @Input() categoryId:string;
  public showBanner:boolean = false;
  public currentCategoryName:string;
  public showSuccessMessage:boolean = true;
  


  constructor(
    public blogModuleService:BlogModuleService,
    private adjustDrawerService:AdjustDrawerService,
    private appCurrentStateService:AppCurrentStateService) 
    {}

  ngOnInit() 
  {
    if(this.categoryId)
    {
      let getCategoryDetailByCategoryIdApiCall: ApiCall = new ApiCall(ApiActions.getCategoryDetailByCategoryId);
      getCategoryDetailByCategoryIdApiCall.addRequestParams('categoryId', this.categoryId);

      this.blogModuleService.getCategoryDetailByCategoryId(getCategoryDetailByCategoryIdApiCall).subscribe(response => {
        this.currentCategoryName = response.title;
      });
    }
  }

  addAppSubsciber(mobileNumber)
  {
    let saveAppSubscribersApiCall: ApiCall = new ApiCall(ApiActions.saveAppSubscribers);
    saveAppSubscribersApiCall.showLoader = true;
    saveAppSubscribersApiCall.addRequestParams('mobile', mobileNumber);
    saveAppSubscribersApiCall.addRequestParams('currentUrl',  window.location.href);
    if(localStorage.getItem("currentCategoryId"))
    {
      saveAppSubscribersApiCall.addRequestParams('categoryId',  localStorage.getItem("currentCategoryId"));
    }
    this.blogModuleService.saveAppSubscribers(saveAppSubscribersApiCall)
    .subscribe(responseValues =>
     {
       this.closeBanner();
       this.showSuccessAlert();
     });
  }

  closeBanner()
  {
    this.showBanner = false;
    if(this.appCurrentStateService.isBrowser())
    {
      localStorage.setItem(AppConstants.isPromotionBannerOpen,"0");
    }
    this.adjustDrawerService.updateData(true);
  }

  showSuccessAlert()
  {
    this.appCurrentStateService.updateAlertMessage(AppConstants.linkSentMobile,"success");
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

}
