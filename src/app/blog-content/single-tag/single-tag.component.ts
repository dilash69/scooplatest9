import { DOCUMENT } from "@angular/common";
import { Component, Inject, OnInit, ViewChild, HostListener } from "@angular/core";
import { Meta, Title } from "@angular/platform-browser";
import { ActivatedRoute, Router } from '@angular/router';
//import { ActivatedRoute } from "@angular/router";
import { deserialize } from "serializer.ts/Serializer";
import { AppCurrentState } from "./../../co/app.current.state";
import { CmsTagsCO } from "./../../co/cmsTagCO";
import { SclPostCO } from "./../../co/sclPostCO";
import { ApiActions } from "./../../common/api.actions";
import { ApiCall } from "./../../common/api.call";
import { AppConstants } from "./../../common/app.constants";
import { AppCurrentStateService } from "./../../services/app.current.state.service";
import { BlogModuleService } from "./../../services/blog-module.service";

@Component({
    selector: "app-single-tag",
    templateUrl: "./single-tag.component.html",
    styleUrls: ["./single-tag.component.css"]
})
export class SingleTagComponent implements OnInit {
    @ViewChild("fixAdvertisement",{static: true}) srcElement;
    //@ViewChild("hideSocialOnFooter",{static: true}) srcElement1;

    public tagId: any;
    public postIdList: String[];
    public sclPostCOArray: SclPostCO[];
    public cmsTagsCO: CmsTagsCO= new CmsTagsCO();
    public chunkSize: any;
    //public load: boolean = true;
    public load = false;
    public noPost = false;
    public pageUrl: string;
    public appCurrentState: AppCurrentState = new AppCurrentState();

    constructor(
        private blogModuleService: BlogModuleService,
        private route: ActivatedRoute,
        private meta: Meta,
        private title: Title,
        private appCurrentStateService: AppCurrentStateService,
        public router: Router,
        @Inject(DOCUMENT) private doc
    ) { }

    ngOnInit() 
    {
        this.appCurrentStateService.setCurrentPage(AppConstants.TAG_PAGE);

        if (this.appCurrentStateService.isBrowser()) 
        {
            localStorage.setItem(AppConstants.isPromotionBannerOpen,"0");
            localStorage.removeItem(AppConstants.currentCategoryName);
            //localStorage.removeItem("currentCategoryId");
            window.scrollTo(0, 0);
            this.pageUrl = window.location.href;
        }

     /*    this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
            this.appCurrentState = appCurrentState;
            if(this.appCurrentStateService.isLoggedInChecked && this.appCurrentState.isUserLoggedIn)
            {
                this.getTagArticleIdList();
            }
          }); */

        this.route.paramMap.subscribe(params => {
            this.tagId = params.get("slug");

            let getTagDetailBySlugApiCall: ApiCall = new ApiCall(ApiActions.getTagDetailBySlug);
            getTagDetailBySlugApiCall.showLoader = true;
            getTagDetailBySlugApiCall.addRequestParams("slug", this.tagId);

            this.blogModuleService.getTagDetailBySlug(getTagDetailBySlugApiCall).subscribe(responseValues => 
                {
                    if(responseValues['NO_SLUG_FOUND'])
                    {
                        this.router.navigate(['']);
                    }
                    else
                    {
                        this.cmsTagsCO = responseValues['cmsTagCO'];
                        this.cmsTagsCO = deserialize<CmsTagsCO>(CmsTagsCO, this.cmsTagsCO);
                        

                        if (this.cmsTagsCO.cmsTagMetaDataCO.metaTitle) 
                            {
                                this.title.setTitle(this.cmsTagsCO.cmsTagMetaDataCO.metaTitle);
                                if(this.cmsTagsCO.cmsTagMetaDataCO.metaKeywords)
                                {
                                    this.meta.addTags([
                                        {
                                            name: "description",
                                            content: this.cmsTagsCO.cmsTagMetaDataCO.metaDescription
                                        },
                                        {
                                            name: "keywords",
                                            content: JSON.parse(this.cmsTagsCO.cmsTagMetaDataCO.metaKeywords)
                                        },
                                        {
                                            property: "og:title",
                                            content: this.cmsTagsCO.cmsTagMetaDataCO.metaTitle
                                        },
                                        { property: "og:url", content: this.cmsTagsCO.pageUrl },
                                        { property: "og:site_name", content: "EduScoop" }
                                    ]);
                                }
                                else
                                {
                                    this.meta.addTags([
                                        {
                                            name: "description",
                                            content: this.cmsTagsCO.cmsTagMetaDataCO.metaDescription
                                        },
                                        {
                                            property: "og:title",
                                            content: this.cmsTagsCO.cmsTagMetaDataCO.metaTitle
                                        },
                                        { property: "og:url", content: this.cmsTagsCO.pageUrl },
                                        { property: "og:site_name", content: "EduScoop" }
                                    ]);
                                }
                                
                            }
                            let link: HTMLLinkElement = this.doc.createElement("link");
                            link.setAttribute("rel", "canonical");
                            this.doc.head.appendChild(link);
                            link.setAttribute("href", this.cmsTagsCO.canonicalUrl);

                           //new api
                            this.getTagArticleIdList();
                    }
                  });
        });

        // this.advertisementScrollEvent();
        // this.onWindowScrollBottom();
        this.hideElementsOnScroll()
    }

    @HostListener('window:scroll', ['$event'])
    hideElementsOnScroll() { // To hide social icon on footer
        if (this.appCurrentStateService.isBrowser())
        {
            if (window.pageYOffset > 900) 
            {
                if(this.srcElement)
                {
                    this.srcElement.nativeElement.classList.add("stickyAdvertisement");
                    this.srcElement.nativeElement.classList.add("animated");
                    this.srcElement.nativeElement.classList.add("fadeInDown");
                }
            } 
            else 
            {
                if(this.srcElement)
                {
                    this.srcElement.nativeElement.classList.remove("stickyAdvertisement");
                    this.srcElement.nativeElement.classList.remove("animated");
                    this.srcElement.nativeElement.classList.remove("fadeInDown");      
                }          
            }

            // REMOVE ADVERTISEMENT WHEN FOOTER START SHOWING
            if (window.scrollY >= (document.body.scrollHeight - 1800)) 
            {
                if(this.srcElement)
                {
                    this.srcElement.nativeElement.classList.remove("stickyAdvertisement");
                    this.srcElement.nativeElement.classList.remove("animated");
                    this.srcElement.nativeElement.classList.remove("fadeInDown");
                }
            }

            // HIDE SOCIAL SHARE ICONS
            // if (window.scrollY >= (document.body.scrollHeight - 1700)) 
            // {
            //     if(this.srcElement1)
            //     {
            //     this.srcElement1.nativeElement.classList.add("d-none");
            //     }
            // }
            // else
            // {
            //     if(this.srcElement1)
            //     {
            //     this.srcElement1.nativeElement.classList.remove("d-none");
            //     }
            // }
        }
    }

    getTagArticleIdList()
    {
        if(this.cmsTagsCO)
        {
            let getTagPostArticleIdListApiCall: ApiCall = new ApiCall(ApiActions.getTagPostArticleIdList);
            getTagPostArticleIdListApiCall.addRequestParams("tagId", this.cmsTagsCO.id);
    
                        this.blogModuleService
                            .getTagPostArticleIdList(getTagPostArticleIdListApiCall)
                            .subscribe(responseValues => {
                                this.postIdList = responseValues["sclPostIdList"];
                                this.sclPostCOArray = <SclPostCO[]>(responseValues["sclPostCOList"]);
    
                                this.sclPostCOArray = deserialize<SclPostCO[]>(SclPostCO,this.sclPostCOArray);
    
                                this.chunkSize = responseValues["chunkSize"];
    
                                if (this.sclPostCOArray.length == 0 ||this.postIdList.length == 0) 
                                {
                                    this.noPost = true;
                                } 
                                else if (this.sclPostCOArray.length < this.postIdList.length) 
                                {
                                    this.load = true;
                                }                            
                            });  
        }        
    }

    // Sticky Advertisement Code Start
    // advertisementScrollEvent() { // To stick advertisement
    //     if (this.appCurrentStateService.isBrowser()) {
    //         window.addEventListener("scroll", e => {
    //             if (window.pageYOffset > 100) {
    //                 this.srcElement.nativeElement.classList.add("stickyAdvertisement");
    //                 this.srcElement.nativeElement.classList.add("animated");
    //                 this.srcElement.nativeElement.classList.add("fadeInDown");
    //             } else {
    //                 this.srcElement.nativeElement.classList.remove("stickyAdvertisement");
    //                 this.srcElement.nativeElement.classList.remove("animated");
    //                 this.srcElement.nativeElement.classList.remove("fadeInDown");
    //             }
    //         });
    //     }
        
    // }

    // onWindowScrollBottom() { // To close advertisement
    //     if (this.appCurrentStateService.isBrowser()) {
    //         window.addEventListener("scroll", e => {
    //             if (
    //                 window.innerHeight + window.scrollY - 180 >=
    //                 document.body.offsetHeight
    //             ) {
    //                 this.srcElement.nativeElement.classList.remove("stickyAdvertisement");
    //             }
    //         });
    //     }

        
    // }
    // Sticky Advertisement Code End

    loadMorePosts() {
        if (this.sclPostCOArray.length <= this.postIdList.length) {
            let size = this.postIdList.length - this.sclPostCOArray.length;

            if (size >= parseInt(this.chunkSize)) {
                size = parseInt(this.chunkSize);
                this.load = true;
            } else {
                this.load = false;
            }

            let tempList = new Array();

            let msize = size + this.sclPostCOArray.length;
            for (let i = this.sclPostCOArray.length; i < msize; i++) {
                tempList.push(this.postIdList[i]);
            }
            let getArticleDetailListApiCall: ApiCall = new ApiCall(
                ApiActions.getPostDetailList
            );
            getArticleDetailListApiCall.addRequestParams("postIdList", tempList);

            this.blogModuleService
                .getPostCOList(getArticleDetailListApiCall)
                .subscribe(response => {
                    this.sclPostCOArray = this.sclPostCOArray.concat(response);
                });
        }
    }

    getPosition(): number {
        let postCount: number = this.sclPostCOArray.length; // input
        let AFTER_POST: number = 3;
        let afterPost = AppConstants.AFTER_POST;

        if (postCount <= afterPost) {
            let temp: number = postCount / afterPost;
            return temp < 1 ? 1 : temp;
        }
        return afterPost;
    }
}
