import { Component, OnInit,EventEmitter,Output } from '@angular/core';
import { ManagePopupService } from '../../services/manage-popup.service';

@Component({
  selector: 'app-delete-pop-up',
  templateUrl: './delete-pop-up.component.html',
  styleUrls: ['./delete-pop-up.component.css']
})
export class DeletePopUpComponent implements OnInit {
  @Output() onYesClick = new EventEmitter();
  public commentId: string;

  constructor(private managePopUpService : ManagePopupService) { }

  ngOnInit()
   {
   
  }

  delete()
  {
    this.onYesClick.emit();
    this.dismiss();
  }
  dismiss()
  {
   this.managePopUpService.closePopUp();
  }

}
