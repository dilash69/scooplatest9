import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProtectedBlogPopupComponent } from './protected-blog-popup.component';

describe('ProtectedBlogPopupComponent', () => {
  let component: ProtectedBlogPopupComponent;
  let fixture: ComponentFixture<ProtectedBlogPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProtectedBlogPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProtectedBlogPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
