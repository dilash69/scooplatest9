import { AppCurrentStateService } from './../../services/app.current.state.service';
import { deserialize } from 'serializer.ts/Serializer';
import { CatCategoriesCO } from './../../co/categoryCO';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { BlogModuleService } from './../../services/blog-module.service';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { SclPostCO } from '../../co/sclPostCO';
import { AppConstants } from '../../common/app.constants';
import {Location} from '@angular/common';
import { AppCurrentState } from './../../co/app.current.state';
import { Meta } from '@angular/platform-browser';
import { AppUtility } from '../../common/app.utility';

@Component({
  selector: 'app-slug',
  templateUrl: './slug.component.html',
  styleUrls: ['./slug.component.css']
})
export class SlugComponent implements OnInit 
{
  public slug: any;
  public catCategoriesCO:CatCategoriesCO;
  public appCurrentState : AppCurrentState = new AppCurrentState();
  private cachedMultiAPICalled:boolean = false;
  private unCachedMultiApiCalled:boolean = false;
  @Input() sclPostCO:SclPostCO;
  public token:string;
  
  @ViewChild("fixedHeader, fixAdvertisement",{static: false}) srcElement;

  constructor(private blogModuleService: BlogModuleService,
              private appCurrentStateService: AppCurrentStateService,
              private route: ActivatedRoute,
              public router: Router,
              public location: Location, 
              private activatedRoute: ActivatedRoute,
              private meta: Meta) 
              { 

              }

  ngOnInit() 
  {
    this.appCurrentStateService.setCurrentPage(AppConstants.SLUG_PAGE);

    this.activatedRoute.queryParams.subscribe(params => {
      
      this.token = params['token'];
      //alert(this.token)
    });

    this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
      this.appCurrentState = appCurrentState;
      
     

       if(this.appCurrentStateService.isLoggedInChecked && this.cachedMultiAPICalled)
       { 
         this.callUnCachedMultiAPI();
       } 
  });

    this.route.paramMap
      .subscribe(params => {
        this.slug = params.get('slug');

        if (this.appCurrentStateService.isBrowser()) 
        {
          let  currentUrl:string = window.location.href;
           if(currentUrl.includes("/preview/"))
           {
            this.meta.addTag({ name:'robots',content:"noindex, nofollow"});  
             if(!this.token)
             {
              this.router.navigate([""]);
             }
           }
           else if(currentUrl.includes("/privacyPolicy"))
           {
            window.location.href='https://www.eduncletest.com/privacy-policy';
           }
        }

        this.sclPostCO = null;
        this.catCategoriesCO = null;

        if(this.slug)
        {
          this.callCachedMultiAPI();
        }
      });
        this.bingScrollEvent();
  }

  callCachedMultiAPI()
  {
    let getCompletePostDataBySlug: ApiCall = new ApiCall(ApiActions.getCompletePostDataBySlug);
    getCompletePostDataBySlug.showLoader = true;
    getCompletePostDataBySlug.isGetRequest = true;
    getCompletePostDataBySlug.additionalUrlParameter = this.slug;
    if(this.token)
    {
      getCompletePostDataBySlug.addRequestParams('adminAccessToken', this.token);
    }

    getCompletePostDataBySlug.addRequestParams('slug', this.slug);
    this.blogModuleService.getCompletePostDataBySlug(getCompletePostDataBySlug)
    
    .subscribe(responseValues =>
     {
     
        this.cachedMultiAPICalled = true;
        this.callgetObjectCOBySlug(true);

        if(this.appCurrentStateService.isLoggedInChecked && this.cachedMultiAPICalled)
        {
          this.callUnCachedMultiAPI();
        }        
     });
  }

  callUnCachedMultiAPI()
  {
    if(this.unCachedMultiApiCalled)
    {
      return;
    }
    this.unCachedMultiApiCalled = true;
    let getCompletePostDataBySlug: ApiCall = new ApiCall(ApiActions.getCompletePostDataBySlug);
    getCompletePostDataBySlug.showLoader = false;
    getCompletePostDataBySlug.invalidateCache = true;
    if(this.token)
    {
      getCompletePostDataBySlug.addRequestParams('adminAccessToken', this.token);
    }

    getCompletePostDataBySlug.addRequestParams('slug', this.slug);
    this.blogModuleService.getCompletePostDataBySlug(getCompletePostDataBySlug)
    .subscribe(responseValues =>
     {
        this.callgetObjectCOBySlug(false);
     });
  }

  callgetObjectCOBySlug(showLoader:boolean)
  {
    let getObjectCOBySlugApiCall: ApiCall = new ApiCall(ApiActions.getObjectCOBySlug);
    getObjectCOBySlugApiCall.showLoader = showLoader;
    if(this.token)
    {
    getObjectCOBySlugApiCall.addRequestParams('adminAccessToken', this.token);
    }
    getObjectCOBySlugApiCall.addRequestParams('slug', this.slug);
    this.blogModuleService.getObjectBySlug(getObjectCOBySlugApiCall)
    .subscribe(responseValues =>
     {
        if(responseValues['NO_SLUG_FOUND'])
        {
            this.router.navigate(['']);
        }
        else
        {
            if(responseValues['sclPostCO'])
            {
                this.sclPostCO = responseValues['sclPostCO'];
                this.sclPostCO = deserialize<SclPostCO>(SclPostCO, this.sclPostCO);
                this.sclPostCO.separateVideoAttachmentCO();
              if (this.appCurrentStateService.isBrowser()) 
              {
                localStorage.setItem("currentCategoryId", this.sclPostCO.defaultCategoryId);
                localStorage.setItem("postType", this.sclPostCO.type);
                this.setPromotionalBannerState(this.sclPostCO.defaultCategoryId);
                this.replaceSlug();

                if (this.slug !== this.sclPostCO.slug) 
                {
                  this.location.replaceState(this.sclPostCO.slug)
                }
              }
                else if(this.appCurrentStateService.isServer())
                {
                  if (this.sclPostCO.type === AppConstants.QUESTION) 
                  {
                    if (this.router.url.includes('/unanswered/')) 
                    {
                      if (this.sclPostCO.sclPostQuestionCO.questionStatus) 
                      {
                        this.router.navigate([this.sclPostCO.slug]);
                      }
                    }
                    else if (!this.router.url.includes('/unanswered/')) 
                    {
                      if (!this.sclPostCO.sclPostQuestionCO.questionStatus) 
                      {
                        this.router.navigate(['404']);
                      }
                    }
                  }
                }
            }
            else
            {
                AppUtility.downloadMathJs();
               
                this.catCategoriesCO = responseValues['catCategoriesCO'];
                this.catCategoriesCO = deserialize<CatCategoriesCO>(CatCategoriesCO, this.catCategoriesCO);
                if(this.appCurrentStateService.isBrowser())
                {
                 localStorage.setItem("currentCategoryId",this.catCategoriesCO.id);
                 this.setPromotionalBannerState(this.catCategoriesCO.id);
                }
            }
            
        }
     });
  }

  bingScrollEvent() 
  {
    if(this.appCurrentStateService.isBrowser())
    {
      window.addEventListener('scroll', (e) => 
    {
      if(this.srcElement)
      {
        if (window.pageYOffset > 100) {
          //this.shouldStickHeader = true;
          this.srcElement.nativeElement.classList.add("stickyHeader");
          this.srcElement.nativeElement.classList.add("animated");
          this.srcElement.nativeElement.classList.add("fadeInDown");
        }
        else 
        {
            //this.shouldStickHeader = false;
            this.srcElement.nativeElement.classList.remove("stickyHeader");
            this.srcElement.nativeElement.classList.remove("animated");
            this.srcElement.nativeElement.classList.remove("fadeInDown");
        }
      }
    });
    }
    
 }


public replaceSlug()
{
  if(this.sclPostCO.type===AppConstants.QUESTION)
  {
      if (!this.sclPostCO.sclPostQuestionCO.questionStatus) 
      {
        if(!this.router.url.includes('/unanswered'))
        {
          this.location.replaceState("/unanswered/"+this.sclPostCO.slug);
        }
      }
      else
      {
        if(this.router.url.includes('unanswered'))
        {
          this.location.replaceState(this.sclPostCO.slug);
        }
      }
    }
   
  }

  setPromotionalBannerState(categoryId:string)
  {
    localStorage.setItem(AppConstants.isPromotionBannerOpen,"0");
    /* if(categoryId ==="2" || categoryId ==="3" || categoryId ==="4")
    {
      localStorage.setItem(AppConstants.isPromotionBannerOpen,"1");
    }
    else
    {
      localStorage.setItem(AppConstants.isPromotionBannerOpen,"0");
    } */
  }


}