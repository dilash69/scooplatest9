import { deserialize } from 'serializer.ts/Serializer';
import { CatCategoryFiltersCO } from './../../co/catCategoryFiltersCO';
import { AppConstants } from './../../common/app.constants';
import { CatObjectCategoryRelationCO } from './../../co/catObjectCategoryRelationCO';
import { CatCategoriesCO } from './../../co/categoryCO';
import { RegisterUserService } from './../../services/register-user.service';
import { CmnContactUsQueriesCO } from './../../co/contactUsQueryCO';
import { AppCurrentState } from './../../co/app.current.state';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { BlogModuleService } from './../../services/blog-module.service';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ManagePopupService } from '../../services/manage-popup.service';
import { UserRegistrationDetailCO } from './../../co/userRegistrationDetailCO';
import { UserCategoryRelationCO } from './../../co/userCategoryRelationCO';
import { UserBrokenLeadsCO } from './../../co/userBrokenLeadsCO';
import { debounceTime } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { FilterValueCO } from '../../co/filterValueCO';
declare var $ :any;
@Component({
  selector: 'app-request-a-call-back',
  templateUrl: './request-a-call-back.component.html',
  styleUrls: ['./request-a-call-back.component.css']
})
export class RequestACallBackComponent implements OnInit 
{

  public appCurrentState : AppCurrentState = new AppCurrentState();
  public otp:string;
  public validationError:string;
  public showValidationError:boolean=false;
  public loadCallBackOtpComponent : boolean = false;

  //catObjectCategoryRelationCO:CatObjectCategoryRelationCO = new CatObjectCategoryRelationCO();
  userCategoryRelationCO:UserCategoryRelationCO = new UserCategoryRelationCO();
  catCategoriesCOArray:CatCategoriesCO[] = new Array();
  selectedCategoryTitle :string= AppConstants.Category;
  catCategoryFiltersCOList:CatCategoryFiltersCO[];
  isCategorySelected:boolean = true;
  private saveBrokenLeadBS : BehaviorSubject<any> = new BehaviorSubject<string>(null);
  firstFilterName:String;
  firstFilterValue:String;

  sortedArray1 = new Array();
  sortedArray2 = new Array();
  isUgcNetSelected:boolean = false;

//   @Output() loadCallBackOtpComponent = new EventEmitter();

  constructor(private managePopupService : ManagePopupService,
    private blogModuleService:BlogModuleService,
    private appCurrentStateService:AppCurrentStateService,
    private registerUserService:RegisterUserService) 
    { 

    }

  ngOnInit() 
  {

    this.saveBrokenLeadBS.pipe(debounceTime(AppConstants.autoBrokenLeadApiHitTime))
    .subscribe(term => 
        { 
        if(term != null)
        {
          let saveUserBrokenLeadsApiCall: ApiCall = new ApiCall(ApiActions.saveUserBrokenLeads);
          saveUserBrokenLeadsApiCall.showLoader = false;
          saveUserBrokenLeadsApiCall.addRequestParams('userBrokenLeadsCO', UserBrokenLeadsCO.getAutoBrokenLeadCO(term.target.value,"AUTO_REQEST_CALLBACK_POP_UP"));
    
          this.blogModuleService.saveUserBrokenLeads(saveUserBrokenLeadsApiCall).subscribe(userResponseData => {
            
          });
        }
   });


    this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
      this.appCurrentState = appCurrentState; 
    });

    let getCategoryListApiCall: ApiCall = new ApiCall(ApiActions.getCategoryList);
    getCategoryListApiCall.showLoader = true;
    this.blogModuleService.getCategoryList(getCategoryListApiCall)
      .subscribe(response =>
      {
        this.catCategoriesCOArray = response;
        this.catCategoriesCOArray = deserialize<CatCategoriesCO[]>(CatCategoriesCO, this.catCategoriesCOArray);

        if(localStorage.getItem("currentCategoryId"))
        {
          for(let i = 0;i<this.catCategoriesCOArray.length;i++)
          {
            if(this.catCategoriesCOArray[i].id === localStorage.getItem("currentCategoryId"))
            {
              this.selectedCategoryTitle = this.catCategoriesCOArray[i].title;
              this.onCategorySelect(this.catCategoriesCOArray[i]);
              break;
            }
          }
        }
      });
  }
  dismiss()
  {
    this.managePopupService.closePopUp();
  }

  registerUser(name, mobileNumber, query) {
    if (this.checkIsCategorySelected()) 
    {
      if (this.checkIfAllFiltersAreSelected()) 
      {
        this.showValidationError = false;
        let cmnContactUsQueriesCO: CmnContactUsQueriesCO = new CmnContactUsQueriesCO();
        cmnContactUsQueriesCO.name = name;
        cmnContactUsQueriesCO.mobile = mobileNumber;
        cmnContactUsQueriesCO.message = query;

        let contactUsQueriesUserApiCall: ApiCall = new ApiCall(ApiActions.contactUsQueriesUser);
        contactUsQueriesUserApiCall.showLoader = true;
        contactUsQueriesUserApiCall.addRequestParams('cmnContactUsQueriesCO', cmnContactUsQueriesCO);
        contactUsQueriesUserApiCall.addRequestParams('leadFromId', "5");

        if (!this.appCurrentState.isUserLoggedIn) 
        {
          contactUsQueriesUserApiCall.addRequestParams('userCategoryRelationCO', this.userCategoryRelationCO);
        }

        this.blogModuleService.contactUsQueriesUser(contactUsQueriesUserApiCall).subscribe(responseValues => {
          if (responseValues['otp']) 
          {
            // this.loadCallBackOtpComponent.emit(true);
            this.loadCallBackOtpComponent = true;
            this.otp = responseValues['otp'];
            this.registerUserService.updatedUser(this.otp, mobileNumber, 'USER_QUERY', false, null);
            //alert(this.otp);
          }
          else if (responseValues['message']) 
          {
            this.managePopupService.closePopUp();
            $("#otpSuccessModal").modal("show");
          }
          else 
          {
            this.validationError = responseValues;
            this.showValidationError = true;
          }
        });
      }
    }
  }

  generateOtp(name,mobileNumber,query)
  {
    if (this.checkIsCategorySelected()) 
    {
      if (this.checkIfAllFiltersAreSelected()) 
      {
        this.showValidationError = false;
        if(this.appCurrentState.isUserLoggedIn)
        {
          this.registerUserQuery(name, mobileNumber, query);
        }
        else
        {
          let generateOtpApiCall: ApiCall = new ApiCall(ApiActions.generateOtp);
          generateOtpApiCall.showLoader = true;
          generateOtpApiCall.addRequestParams('mobileNumber',mobileNumber);
  
          this.blogModuleService.generateOtp(generateOtpApiCall).subscribe(responseValues => {
            if (responseValues.userResponseCode === AppConstants.SUCCESS_REQUEST) 
            {
              let userRegistrationDetailCO:UserRegistrationDetailCO = new UserRegistrationDetailCO();
              userRegistrationDetailCO.isTutorRegistration = "0";
              userRegistrationDetailCO.leadFromId = "5";
              userRegistrationDetailCO.name = name;
              userRegistrationDetailCO.mobileNumber =mobileNumber;
              userRegistrationDetailCO.message = 'USER_QUERY';
              userRegistrationDetailCO.userCategoryRelationCO = this.userCategoryRelationCO;
              
              userRegistrationDetailCO.cmnContactUsQueriesCO = new CmnContactUsQueriesCO;
              userRegistrationDetailCO.cmnContactUsQueriesCO.name = name;
              userRegistrationDetailCO.cmnContactUsQueriesCO.mobile = mobileNumber;
              userRegistrationDetailCO.cmnContactUsQueriesCO.message = query;
      
              this.loadCallBackOtpComponent=true;
              this.registerUserService.updatedUser1(userRegistrationDetailCO);
            }
            else 
            {
              this.showValidationError = true;
              this.validationError = responseValues.userMessageList[0];
            }
          });
        }
      }
    }
  }

  numberOnly(event): boolean 
  {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
        }
        return true;
  }

  // showCallBackOtpComponent(load:boolean)
  //   {
  //       this.loadCallBackOtpComponent=true;
  //   }

  onCategorySelect(catCategoriesCO:CatCategoriesCO)
  {
    this.assignCategory(catCategoriesCO.title,catCategoriesCO.id);
    let getCategoryFiltersForAdminApiCall: ApiCall = new ApiCall(ApiActions.getCategoryFiltersForAdmin);
    getCategoryFiltersForAdminApiCall.addRequestParams('categoryId', catCategoriesCO.id);
    this.blogModuleService.getCategoryFiltersForAdmin(getCategoryFiltersForAdminApiCall).subscribe(response => {
      this.catCategoryFiltersCOList = response;
      //this.findIndexOfSubjectFilter();
      for (let i = 0; i < 1; i++) 
      {
        this.catCategoryFiltersCOList[i].filterTypeTitle = this.catCategoryFiltersCOList[i].filterType;
      }
      this.getIndependentFilterValues(); 
    });
  }

  assignCategory(categoryTitle:string,categoryId:string)
  {
    this.selectedCategoryTitle = categoryTitle;
    this.userCategoryRelationCO.categoryId = categoryId;
    this.isCategorySelected =true;
  }

  getIndependentFilterValues() 
  {
    for (let i = 0; i < 1; i++) 
    {
      if (this.catCategoryFiltersCOList[i].dependentFilterType === "" || !this.catCategoryFiltersCOList[i].dependentFilterType) 
      {
          let getIndependentFilterValues: ApiCall = new ApiCall(ApiActions.getIndependentFilterValues);
          getIndependentFilterValues.addRequestParams("categoryId", this.catCategoryFiltersCOList[i].categoryId);
          getIndependentFilterValues.addRequestParams("filterType", this.catCategoryFiltersCOList[i].filterType);

          this.blogModuleService.getIndependentFilterValues(getIndependentFilterValues).subscribe(response => {
            this.catCategoryFiltersCOList[i].dropDownFilterValueCOList = response;
            this.isUgcNetSelected = false;
            if(this.catCategoryFiltersCOList[i].categoryId=='2')
            {
              this.catCategoryFiltersCOList[i].dropDownFilterValueCOList =this.filterListForUGCNET(response)
            }

          });
      }
    }
  }

  assignFiltersId(filterType:String,filterValueId,filterValueCO)
  {
    this.firstFilterName = filterType;
    this.firstFilterValue = filterValueCO.filterTypeTitle;
    this.catCategoryFiltersCOList[0].filterTypeTitle = filterValueCO.filterTypeTitle;
    if(filterType===AppConstants.EXAM)
    {
      this.userCategoryRelationCO.examId = filterValueId
    }
    else if(filterType===AppConstants.STREAM)
    {
      this.userCategoryRelationCO.streamId = filterValueId
    }
    else if(filterType===AppConstants.STAGE)
    {
      this.userCategoryRelationCO.stageId = filterValueId
    }
    else if(filterType===AppConstants.SUBJECT)
    {
      this.userCategoryRelationCO.subjectId = filterValueId
    }
    else if(filterType===AppConstants.TOPIC)
    {
      this.userCategoryRelationCO.topicId = filterValueId
    }
    else if(filterType===AppConstants.YEAR)
    {
      this.userCategoryRelationCO.yearId = filterValueId
    }
    else if(filterType===AppConstants.BOARD)
    {
      this.userCategoryRelationCO.boardId = filterValueId
    }
    else if(filterType===AppConstants.MEDIUM_LANGUAGE)
    {
      this.userCategoryRelationCO.mediumLanguageId = filterValueId
    }
    else if(filterType===AppConstants.ZONE)
    {
      this.userCategoryRelationCO.zoneId = filterValueId
    }
  }

    checkIsCategorySelected(): boolean {
        if (!this.appCurrentState.isUserLoggedIn) 
        {
            let isValid: boolean = true;
            if (this.userCategoryRelationCO) 
            {
                if (!this.userCategoryRelationCO.categoryId) 
                {
                    isValid = false;
                    this.isCategorySelected = false;
                }
            }
            return isValid;
        }
        else 
        {
            return true;
        }
    }

    checkIfAllFiltersAreSelected(): boolean 
    {
        if (!this.appCurrentState.isUserLoggedIn) 
        {
            let isValid: boolean = true;
            for (let i = 0; i < 1; i++)
            {
                isValid = this.checkForFilterValidation(this.catCategoryFiltersCOList[i].filterType, this.catCategoryFiltersCOList[i]);
            }
            return isValid;
        }
        else 
        {
            return true;
        }
    }

  checkForFilterValidation(filterType:string,catCategoryFiltersCO:CatCategoryFiltersCO)
  {
    let isValid:boolean = true;
    if(filterType === AppConstants.EXAM)
    {
      if(!this.userCategoryRelationCO.examId)
      {
        catCategoryFiltersCO.validationMessage = catCategoryFiltersCO.filterType +" " + "is required";
        return false;
      }
    }
    else if(filterType===AppConstants.STREAM)
    {
      if(!this.userCategoryRelationCO.streamId)
      {
        catCategoryFiltersCO.validationMessage = catCategoryFiltersCO.filterType +" " + "is required";
        return false;
      }
    }
    else if(filterType===AppConstants.STAGE)
    {
      if(!this.userCategoryRelationCO.stageId)
      {
        catCategoryFiltersCO.validationMessage = catCategoryFiltersCO.filterType +" " + "is required";
        return false;
      }
    }
    else if(filterType===AppConstants.SUBJECT)
    {
      if(!this.userCategoryRelationCO.subjectId)
      {
        catCategoryFiltersCO.validationMessage = catCategoryFiltersCO.filterType +" " + "is required";
        return false;
      }
    }
    return isValid;
  }

  registerUserQuery(name, mobileNumber, query)
  {
    let cmnContactUsQueriesCO: CmnContactUsQueriesCO = new CmnContactUsQueriesCO();
    cmnContactUsQueriesCO.name = name;
    cmnContactUsQueriesCO.mobile = mobileNumber;
    cmnContactUsQueriesCO.message = query;

    let contactUsQueriesUserApiCall: ApiCall = new ApiCall(ApiActions.contactUsQueriesUser);
    contactUsQueriesUserApiCall.showLoader = true;
    contactUsQueriesUserApiCall.addRequestParams('cmnContactUsQueriesCO', cmnContactUsQueriesCO);
    contactUsQueriesUserApiCall.addRequestParams('leadFromId', "5");

    this.blogModuleService.contactUsQueriesUser(contactUsQueriesUserApiCall).subscribe(responseValues => {
      if (responseValues['message']) 
      {
        this.managePopupService.closePopUp();
        $("#otpSuccessModal").modal("show");
      }
      else 
      {
        this.validationError = responseValues;
        this.showValidationError = true;
      }
    });
  }

  contactUsUserQueryNew(name, mobileNumber, query) 
  {
    if (this.checkIsCategorySelected()) 
    {
      if (this.checkIfAllFiltersAreSelected()) 
      {
        this.showValidationError = false;
        let cmnContactUsQueriesCO: CmnContactUsQueriesCO = new CmnContactUsQueriesCO();
        cmnContactUsQueriesCO.name = name;
        cmnContactUsQueriesCO.mobile = mobileNumber;
        cmnContactUsQueriesCO.message = query;

        let contactUsQueriesUserNewApiCall: ApiCall = new ApiCall(ApiActions.contactUsQueriesUserNew);
        contactUsQueriesUserNewApiCall.showLoader = true;

        contactUsQueriesUserNewApiCall.addRequestParams('leadFromId', "5");

        if (this.appCurrentState.isUserLoggedIn) 
        {
          contactUsQueriesUserNewApiCall.addRequestParams('isLoggedInUser', "1");
          cmnContactUsQueriesCO.userId = this.appCurrentState.userCO.id;
        }
        else 
        {
          contactUsQueriesUserNewApiCall.addRequestParams('isLoggedInUser', "0");
          contactUsQueriesUserNewApiCall.addRequestParams('userCategoryRelationCO',  this.userCategoryRelationCO);
        }
        contactUsQueriesUserNewApiCall.addRequestParams('cmnContactUsQueriesCO', cmnContactUsQueriesCO);

        this.blogModuleService.contactUsQueriesUserNew(contactUsQueriesUserNewApiCall).subscribe(userResponseData => 
          {
          if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
          {
            if (this.appCurrentState.isUserLoggedIn) 
            {
              this.managePopupService.closePopUp();
              $("#otpSuccessModal").modal("show");
            }
            else 
            {
              let userRegistrationDetailCO: UserRegistrationDetailCO = new UserRegistrationDetailCO();
              userRegistrationDetailCO.isTutorRegistration = "0";
              userRegistrationDetailCO.mobileNumber = mobileNumber;
              userRegistrationDetailCO.message = 'USER_QUERY';
              userRegistrationDetailCO.categoryName = this.selectedCategoryTitle;
              userRegistrationDetailCO.firstFilterName = this.firstFilterName;
              userRegistrationDetailCO.firstFilterValue = this.firstFilterValue;
              userRegistrationDetailCO.name = name;
              this.loadCallBackOtpComponent = true;
              this.registerUserService.updatedUser1(userRegistrationDetailCO);
            }
          }
          else 
          {
            this.validationError = userResponseData.userMessageList[0];
            this.showValidationError = true;
          }
        });
      }
    }
  }

  saveBrokenLeads(event)
  {
    if(event.target.value.length == 10)
    {
      this.saveBrokenLeadBS.next(event);
    }
  }

  filterListForUGCNET(filterValueCOList: FilterValueCO[]):FilterValueCO[]
  {

    this.isUgcNetSelected=true;
    
    let filterValueCOList1: FilterValueCO[];
    let filterValueCOList2: FilterValueCO[];

    filterValueCOList1=filterValueCOList.filter(t=>t.isOther==='0')
    filterValueCOList2=filterValueCOList.filter(t=>t.isOther==='1')

    this.sortedArray1 = filterValueCOList1.sort((a, b) => a.sequence - b.sequence);
    this.sortedArray2=filterValueCOList2.sort((a, b) => (a.filterTypeTitle > b.filterTypeTitle) ? 1 : (a.filterTypeTitle < b.filterTypeTitle) ? -1:0)
    return this.sortedArray1.concat(this.sortedArray2);
  
  }

}
