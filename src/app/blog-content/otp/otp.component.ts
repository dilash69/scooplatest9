import { RegisterUserService } from './../../services/register-user.service';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { Component, OnInit, Input, Output } from '@angular/core';
import { AppConstants } from '../../common/app.constants';
import { ApiCall } from '../../common/api.call';
import { ApiActions } from '../../common/api.actions';
import { BlogModuleService } from '../../services/blog-module.service';
import { UserCO } from '../../co/userCO';
import { Router } from '@angular/router';
import { AppUtility } from '../../common/app.utility';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css']
})
export class OtpComponent implements OnInit {

  @Input() phoneNo: string;
  @Input() username: string;
  @Input() email: string;
  @Input() otp: number;
  @Input() popUpName: string;


  public phoneNumberLast3Digits: string;
  public timeLeft: number;
  public timeLeftOrVerifyText: string;
  public interval;
  //public otp: number;
  public isValidOtp = false;
  public userCO: UserCO = null;
  public validationError:string;
  public showValidationError:boolean=false;

  constructor(private blogModuleService: BlogModuleService, private appCurrentStateService: AppCurrentStateService, private router: Router) { }

  ngOnInit() 
  {
  // console.log("FFFFFFFFF","ffffffffffffffffff") 
    this.phoneNumberLast3Digits = this.phoneNo;
    this.phoneNumberLast3Digits = this.phoneNumberLast3Digits.substring(this.phoneNumberLast3Digits.length - 3, this.phoneNumberLast3Digits.length);

    //this.doRegisterAndGenerateOtp();
    
    this.startTimer();
  }

//   doRegisterAndGenerateOtp() 
//   {
//     let registerUser: ApiCall = new ApiCall(ApiActions.registerUser);
//     registerUser.addRequestParams('name', this.username);
//     registerUser.addRequestParams('emailId', this.email);
//     registerUser.addRequestParams('mobileNumber', this.phoneNo);
//     registerUser.addRequestParams('leadFromId', "2");

//     this.blogModuleService.registerUser(registerUser).subscribe(responseValues => {
     
//       if(responseValues['otp'])
//       {
//         this.otp = responseValues['otp'];
//         alert(this.otp)
        
//       }
//       else
//       {
//         this.validationError=responseValues;
//         alert(this.validationError)
//       }
//     });
//   }

  startTimer() 
  {
    // initialize below variables with default time
    this.timeLeft = AppConstants.OTP_VALID_TIME;
    this.timeLeftOrVerifyText = "00:" + this.timeLeft;

    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
        this.timeLeftOrVerifyText = "00:" + ((this.timeLeft < 10) ? "0" + this.timeLeft : "" + this.timeLeft);
      } else {
        clearInterval(this.interval);
      }
    }, 1000)
  }

  reSendOtp() 
  {
    this.startTimer();
    /* let createAndGetMobileOtp: ApiCall = new ApiCall(ApiActions.createAndGetMobileOtp);
    createAndGetMobileOtp.showLoader = true;
    createAndGetMobileOtp.addRequestParams('mobileNumber', this.phoneNo);

    this.blogModuleService.registerUser(createAndGetMobileOtp).subscribe(responseValues => {
      //this.otp = response;
      this.otp = responseValues['otp'];
      //alert("reGeneratedOtp: " + this.otp);
    });  */

    let resendOtpApiCall: ApiCall = new ApiCall(ApiActions.resendOtp);
    resendOtpApiCall.showLoader = true;
    resendOtpApiCall.addRequestParams('mobileNumber', this.phoneNo);

    this.blogModuleService.resendOtp(resendOtpApiCall).subscribe(responseValues => {
     
      if(!responseValues['message'])
      {
        this.otp = responseValues['otp'];
      }
    });
  }

  varifingOtp(userOtp) 
  {
    if (userOtp && userOtp.length > 0) 
    {
      this.isValidOtp = userOtp == this.otp;

      // Stop timer and show Valid/Invalid OTP
      this.timeLeft = 0;
      clearInterval(this.interval);
      this.timeLeftOrVerifyText = (this.isValidOtp) ? "OTP Verified" : "Invalid otp";

     
      if (this.isValidOtp) 
      {
        let apiVarifingOtp: ApiCall = new ApiCall(ApiActions.verifiedOtp);
        apiVarifingOtp.addRequestParams('mobileNumber', this.phoneNo);

        this.blogModuleService.verifiedOtp(apiVarifingOtp).subscribe(response => {
          this.userCO = response;
          localStorage.setItem(AppConstants.USERCO_AFTER_OTP_VERIFIED, JSON.stringify(this.userCO).toString());
        });
      }
    } else {
      this.timeLeftOrVerifyText = "Please enter your OTP.";
    }
    AppUtility.log(this.isValidOtp);
  }

  submitPasswordForm(form) 
  {
    this.showValidationError = false;
    if(form.password.length < AppConstants.MIN_PASSWORD_LENGTH) 
    {
      //alert("Password should be " + AppConstants.MIN_PASSWORD_LENGTH + " characters long.");
      this.showValidationError = true;
      this.validationError = "Password should be " + AppConstants.MIN_PASSWORD_LENGTH + " characters long.";
      return;
    }

    if (form.password != form.confirmPassword) 
    {
      //alert("Password and confirm password should be same.");
      this.showValidationError = true;
      this.validationError = "Password and confirm password should be same.";
      return;
    }

    let createUserPassword: ApiCall = new ApiCall(ApiActions.createUserPassword);
    createUserPassword.showLoader = true;
    createUserPassword.addRequestParams(AppConstants.DB_ACCESS_TOKEN, localStorage.getItem(AppConstants.DB_ACCESS_TOKEN));
    createUserPassword.addRequestParams('password', form.password);

    this.blogModuleService.createUserPassword(createUserPassword).subscribe(response => {
      if (response === AppConstants.PASSWORD_CREATED_SUCCESSFULLY) 
      {
        //window.location.href = AppConstants.FREE_DOWNLOAD;
        //new start
        this.appCurrentStateService.login(AppUtility.sendToDashboard(), localStorage.getItem(AppConstants.DB_ACCESS_TOKEN), null);

/*         if(this.popUpName)
        {
          this.appCurrentStateService.login(AppUtility.sendToDashboard(), localStorage.getItem(AppConstants.DB_ACCESS_TOKEN), null, null, null);
        }
        else
        {
          this.appCurrentStateService.login(AppUtility.sendToDashboard(), localStorage.getItem(AppConstants.DB_ACCESS_TOKEN), null, null, null);
        } */
        //end
      } 
      else 
      {
        //alert('Somthing went wrong, please try again..')
        this.showValidationError = true;
        this.validationError = "Somthing went wrong, please try again..";
      }
    });
  }
}