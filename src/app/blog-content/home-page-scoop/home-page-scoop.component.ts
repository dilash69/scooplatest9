import { AppCurrentStateService } from './../../services/app.current.state.service';
import { UserCO } from './../../co/userCO';
import { AppConstants } from './../../common/app.constants';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { Component, OnInit } from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page-scoop',
  templateUrl: './home-page-scoop.component.html',
  styleUrls: ['./home-page-scoop.component.css']
})
export class HomePageScoopComponent implements OnInit 
{
  constructor(private meta: Meta,
              private title: Title, 
              private appCurrentStateService:AppCurrentStateService,
              private router: Router) { 
                this.title.setTitle('Edscoop – Exam Information and Expert Tips');
                this.meta.addTags([
                  {name: "description", content: '#EdScoop by Eduncle.com - A place to keep you updated with latest exam notification, important news, experts suggested tips & success mantras shared by toppers.'},
                  {name: "keywords", content: 'Eduncle, Eduncle.com, EduScoop, IIT JAM Information, UGC NET Blog, CSIR NET News, IIT JEE Notification, NEET Exam Updates, RRB Exam Dates, IBPS Calendar, SSC Exams Date Sheet, Preparation Tips by Experts, Toppers Success Tips'}
                ]);
    // title.setTitle('EduScoop | Educating You Continuously...');
    // meta.addTags([
    //   { name: 'author', content: 'Eduncle.com' }
    // ]);
  }

  ngOnInit() 
  {
    this.appCurrentStateService.setCurrentPage(AppConstants.SCOOP_HOME_PAGE);

    if(this.appCurrentStateService.isBrowser())
    {
      localStorage.setItem(AppConstants.isPromotionBannerOpen,"0");
      localStorage.removeItem(AppConstants.currentCategoryName);
     // localStorage.removeItem("currentCategoryId");
      window.scrollTo(0,0);
    }
  }
}
