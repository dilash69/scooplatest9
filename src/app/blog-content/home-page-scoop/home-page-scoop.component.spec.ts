import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomePageScoopComponent } from './home-page-scoop.component';

describe('HomePageScoopComponent', () => {
  let component: HomePageScoopComponent;
  let fixture: ComponentFixture<HomePageScoopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePageScoopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePageScoopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
