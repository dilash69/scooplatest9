import { SclPostCO } from './../../co/sclPostCO';
import { DataSharingService } from './../../services/data-sharing.service';
import { BehaviorSubject} from 'rxjs';
import { deserialize } from 'serializer.ts/Serializer';
import { ApiActions } from './../../common/api.actions';
import { BlogModuleService } from './../../services/blog-module.service';
import { ApiCall } from './../../common/api.call';
import { Component, OnInit, Input } from '@angular/core';
import { AppUtility } from './../../common/app.utility';
import { AppConstants } from './../../common/app.constants';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  public articleIdList: String[];
  public sclPostCOArray: SclPostCO[];
  public replyCommentCount:number=0;

  defaultImageSquare:String = AppConstants.DEFAULT_IMAGE_SQUARE;
  defaultImageCircle:String = AppConstants.DEFAULT_IMAGE_CIRCLE;

  private postCOBehaviorSubject = new BehaviorSubject<SclPostCO>(null);

  @Input()
  set postCO(value) 
  {
    this.postCOBehaviorSubject.next(value);
  };

  get postCO() 
  {
    return this.postCOBehaviorSubject.getValue();
  }
  constructor(private blogModuleService : BlogModuleService) { 

  }

  ngOnInit() 
  {
    this.postCOBehaviorSubject
      .subscribe(x => {
        if (this.postCO) 
        {
          this.replyCommentCount = parseInt(this.postCO.totalComments) + parseInt(this.postCO.totalReplies);
        }
      });
  }
  
  public getResponsiveImage(imagePath:string):string {
    return AppUtility.getResponsiveImage(imagePath);
   }
}