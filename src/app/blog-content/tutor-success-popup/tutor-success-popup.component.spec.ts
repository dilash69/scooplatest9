import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutorSuccessPopupComponent } from './tutor-success-popup.component';

describe('TutorSuccessPopupComponent', () => {
  let component: TutorSuccessPopupComponent;
  let fixture: ComponentFixture<TutorSuccessPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutorSuccessPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutorSuccessPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
