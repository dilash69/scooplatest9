import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeCategoriesParentComponent } from './change-categories-parent.component';

describe('ChangeCategoriesParentComponent', () => {
  let component: ChangeCategoriesParentComponent;
  let fixture: ComponentFixture<ChangeCategoriesParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeCategoriesParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeCategoriesParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
