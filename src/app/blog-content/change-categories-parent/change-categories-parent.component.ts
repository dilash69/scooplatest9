import { ManagePopupService } from './../../services/manage-popup.service';
import { AppConstants } from './../../common/app.constants';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { BlogModuleService } from './../../services/blog-module.service';
import { ChangeCategoryUserSelectionCO } from './../../co/changeCategoryUserSelectionCO';
import { ChangeCategoryStateCO } from './../../co/changeCategoryStateCO';
import { Component, OnInit, Input } from '@angular/core';
//import { ChangeCategoryStateCO } from 'src/app/co/changeCategoryStateCO';
//import { ChangeCategoryUserSelectionCO } from 'src/app/co/changeCategoryUserSelectionCO';
import { ChangeCategoryService } from './../../services/change-category.service';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { AppUtility } from './../../common/app.utility';
//import { ManagePopupService } from 'src/app/services/manage-popup.service';
// import { ApiCall } from 'src/app/common/api.call';
// import { ApiActions } from 'src/app/common/api.actions';
// import { BlogModuleService } from 'src/app/services/blog-module.service';
// import { AppConstants } from 'src/app/common/app.constants';

@Component({
  selector: 'app-change-categories-parent',
  templateUrl: './change-categories-parent.component.html',
  styleUrls: ['./change-categories-parent.component.css']
})
export class ChangeCategoriesParentComponent implements OnInit {

  public changeCategoryStateCO:ChangeCategoryStateCO;
//  public ocrFetched:boolean = false;
 
  

  constructor(private changeCategoryService: ChangeCategoryService,
    private managePopupService:ManagePopupService) 
  {
      AppUtility.log('ChangeCategoriesParentComponent constructor');
  }

  ngOnInit() 
  {
    this.changeCategoryService.changeCategoryStateCO.subscribe(changeCategoryStateCO => {
      console.log("ChangeCategoriesParentComponent")
        this.changeCategoryStateCO = changeCategoryStateCO;
        this.changeCategoryService.setStepByPurpose(this.changeCategoryStateCO);
    });
  }

  dismiss()
  {
    this.managePopupService.closePopUp();
  }

/*   getCategoryRelationObject()
  {
    let getCategoryRelationObjectByUserApiCall:ApiCall = new ApiCall(ApiActions.getCategoryRelationObjectByUser)
    getCategoryRelationObjectByUserApiCall.addRequestParams(AppConstants.ACCESS_TOKEN,"eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxNTMwOTcxIiwiaWF0IjoxNTQ5MDE1MjUwLCJpc3MiOiIxYjAwODE4YjE5NGM5Y2MwNDA0ODJiMDdhYjc1MDU3YyJ9.BzusrIoXjJRsT_EBT7IwWO6Svd8haiX3fFcKkAdHNQo");

    this.blogModuleService.getCategoryRelationObjectByUser(getCategoryRelationObjectByUserApiCall).subscribe(response => 
    {      
      this.changeCategoryStateCO.catCategoriesCOList = response;
      this.ocrFetched = true;
      this.changeCategoryStateCO.currentStep = AppConstants.STEP_3_VALUE;
           
       if(this.changeCategoryStateCO.catCategoriesCOList && this.changeCategoryStateCO.catCategoriesCOList.length > 0)
      {
        this.changeCategoryStateCO.currentStep = AppConstants.STEP_3_VALUE;
      }
      else
      {
        this.changeCategoryStateCO.currentStep = AppConstants.STEP_2_VALUE;
      } 

      this.changeCategoryService.updateChangeCategoryStateCO(this.changeCategoryStateCO);
    });
  } */
}

