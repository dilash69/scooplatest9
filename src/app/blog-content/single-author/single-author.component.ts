import { DOCUMENT } from "@angular/common";
import { Component, Inject, OnInit, ViewChild, HostListener } from "@angular/core";
import { Meta } from "@angular/platform-browser";
import { ActivatedRoute, Router } from '@angular/router';
import { deserialize } from "serializer.ts/Serializer";
import { AppCurrentState } from "./../../co/app.current.state";
import { CatCategoriesCO } from "./../../co/categoryCO";
import { SclPostCO } from "./../../co/sclPostCO";
import { UserCO } from "./../../co/userCO";
import { ApiActions } from "./../../common/api.actions";
import { ApiCall } from "./../../common/api.call";
import { AppConstants } from "./../../common/app.constants";
import { AppCurrentStateService } from "./../../services/app.current.state.service";
import { BlogModuleService } from "./../../services/blog-module.service";


@Component({
    selector: "app-single-author",
    templateUrl: "./single-author.component.html",
    styleUrls: ["./single-author.component.css"]
})
export class SingleAuthorComponent implements OnInit {
    @ViewChild("fixAdvertisement",{static: false}) srcElement;
    @ViewChild("hideSocialOnFooter",{static: false}) srcElement3;
    public authId: any;
    public userCO: UserCO;
    public sclPostCOArray: SclPostCO[];
    public postIdList: String[];
    public chunkSize: any;
    public load: boolean = false;
    public noPost = false;
    public catCategoryCO = new CatCategoriesCO();
    public pageUrl: string;
    public appCurrentState: AppCurrentState = new AppCurrentState();

    constructor(
        private blogModuleService: BlogModuleService,
        private route: ActivatedRoute,
        private appCurrentStateService: AppCurrentStateService,
        private meta: Meta,
        public router: Router,
        @Inject(DOCUMENT) private doc
    ) {}

    ngOnInit() {
        this.appCurrentStateService.setCurrentPage(AppConstants.AUTHOR_PAGE);

        if (this.appCurrentStateService.isBrowser()) 
        {
            localStorage.setItem(AppConstants.isPromotionBannerOpen,"0");
            localStorage.removeItem(AppConstants.currentCategoryName);
           // localStorage.removeItem("currentCategoryId");
            this.pageUrl = window.location.href;
        }
        if (this.appCurrentStateService.isBrowser()) {
            window.scrollTo(0, 0);
        }
        this.route.paramMap.subscribe(params => {
            this.authId = params.get("slug");
        });

        this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
            this.appCurrentState = appCurrentState;
            if(this.appCurrentStateService.isLoggedInChecked && this.appCurrentState.isUserLoggedIn)
            {
                this.getAuthorArticleIDList();
            }
        });

        let getAuthorDetail: ApiCall = new ApiCall(ApiActions.getUserDetailBySlug);
        getAuthorDetail.showLoader = true;
        getAuthorDetail.addRequestParams("slug", this.authId);

        this.blogModuleService
        .getUserDetailbySlug(getAuthorDetail)
        .subscribe(responseValues => {
            if(responseValues['NO_SLUG_FOUND'])
            {
                this.router.navigate(['']);
            }
            else
            {
                this.userCO = responseValues['userCO'];
                this.userCO = deserialize<UserCO>(UserCO, this.userCO);
                //this.userCO = response;
            }
            this.catCategoryCO.id = this.userCO.defaultCategoryId;
            this.getAuthorArticleIDList();
        });

        this.hideElementsOnScroll()
    }

    getAuthorArticleIDList()
    {
        if(this.userCO)
        {
            let getAuthorArticleIdListApiCall: ApiCall = new ApiCall(
                ApiActions.getUserPostArticleIdList
            );
            getAuthorArticleIdListApiCall.addRequestParams(
                "userId",
                this.userCO.id
            );
    
            this.blogModuleService
            .getAuthorArticleIdList(getAuthorArticleIdListApiCall)
            .subscribe(responseValues => {
                this.postIdList = responseValues["sclPostidList"];
                this.sclPostCOArray = <SclPostCO[]>responseValues["sclPostCOList"];
                this.sclPostCOArray = deserialize<SclPostCO[]>(
                    SclPostCO,
                    this.sclPostCOArray
                );
                this.chunkSize = responseValues["chunkSize"];

                if (
                    this.sclPostCOArray.length == 0 ||
                    this.postIdList.length == 0
                ) 
                {
                    this.noPost = true;
                } else if (this.sclPostCOArray.length < this.postIdList.length) {
                    this.load = true;
                }
                this.meta.addTags([
                    { property: "og:image", content: this.userCO.profileImage },
                    { property: "og:url", content: this.userCO.pageUrl },
                    { property: "og:site_name", content: "EduScoop" }
                ]);
                let link: HTMLLinkElement = this.doc.createElement("link");
                link.setAttribute("rel", "canonical");
                this.doc.head.appendChild(link);
                link.setAttribute("href", this.userCO.canonicalUrl);
            });
        }
    }


    // STICK ELEMENTS ON SCROLL CODE START
    @HostListener('window:scroll', ['$event'])
    hideElementsOnScroll() { // Method for sticky advertisement
        if(this.appCurrentStateService.isBrowser())
        {
            if (window.pageYOffset > 900 ) {
                if(this.srcElement)
                {
                    this.srcElement.nativeElement.classList.add("stickyAdvertisement");
                    this.srcElement.nativeElement.classList.add("animated");
                    this.srcElement.nativeElement.classList.add("fadeInDown");
                }
            } 
            else 
            {
                if(this.srcElement)
                {
                    this.srcElement.nativeElement.classList.remove("stickyAdvertisement");
                this.srcElement.nativeElement.classList.remove("animated");
                this.srcElement.nativeElement.classList.remove("fadeInDown");

                }
                
            }

            // REMOVE ADVERTISEMENT WHEN FOOTER START SHOWING
            if (window.scrollY >= (document.body.scrollHeight - 1800)) 
            {
                if(this.srcElement)
                {
                    this.srcElement.nativeElement.classList.remove("stickyAdvertisement");
                    this.srcElement.nativeElement.classList.remove("animated");
                    this.srcElement.nativeElement.classList.remove("fadeInDown");
                }
               
            }

            // SHOW AND HIDE SOCIAL SHARE ICONS
            if (window.scrollY >= (document.body.scrollHeight - 1700)) 
            {
                if(this.srcElement3)
                {
                    this.srcElement3.nativeElement.classList.add("d-none");
                }
               
            }
            else
            {
                if(this.srcElement3)
                {
                    this.srcElement3.nativeElement.classList.remove("d-none");
                }
               
            }
        }
    }
    // STICK ELEMENTS ON SCROLL CODE END


    getPosition(): number {
        let postCount: number = this.sclPostCOArray.length; // input
        let AFTER_POST: number = 3;
        let afterPost = AppConstants.AFTER_POST;

        if (postCount <= afterPost) {
            let temp: number = postCount / afterPost;
            return temp < 1 ? 1 : temp;
        }
        return afterPost;
    }

    loadMorePosts() {
        if (this.sclPostCOArray.length <= this.postIdList.length) {
            let size = this.postIdList.length - this.sclPostCOArray.length;

            if (size >= parseInt(this.chunkSize)) {
                size = parseInt(this.chunkSize);
                this.load = true;
            } else {
                this.load = false;
            }

            let tempList = new Array();

            let msize = size + this.sclPostCOArray.length;
            for (let i = this.sclPostCOArray.length; i < msize; i++) {
                tempList.push(this.postIdList[i]);
            }
            let getArticleDetailListApiCall: ApiCall = new ApiCall(
                ApiActions.getPostDetailList
            );
            getArticleDetailListApiCall.addRequestParams("postIdList", tempList);

            this.blogModuleService
            .getPostCOList(getArticleDetailListApiCall)
            .subscribe(response => {
                this.sclPostCOArray = this.sclPostCOArray.concat(response);
            });
        }
    }
}
