import { ChangeCategoryService } from './../../services/change-category.service';
import { CoursesHeaderCO } from './../../co/coursesHeaderCO';
import { SclPostCommentCO } from './../../co/commentCO';
import { Component, OnInit } from '@angular/core';
import { AppCurrentState } from '../../co/app.current.state';
import { CatCategoriesCO } from '../../co/categoryCO';
import { CmsSearchSuggestionCO } from '../../co/cmsSearchSuggestionCO';
import { CmsTagsCO } from '../../co/cmsTagCO';
import { ApiActions } from '../../common/api.actions';
import { ApiCall } from '../../common/api.call';
import { AppConstants } from '../../common/app.constants';
import { AppCurrentStateService } from '../../services/app.current.state.service';
import { BlogModuleService } from '../../services/blog-module.service';
import { AppSettings } from './../../common/app-settings';
import { AppPopupCO } from '../../co/app.popup.co';
import { ManagePopupService } from '../../services/manage-popup.service';
import { CmnFeedbackQuestionsCO } from '../../co/CmnFeedbackQuestionsCO';
import { AppUtility } from '../../common/app.utility';
import { ChangeCategoryStateCO } from '../../co/changeCategoryStateCO';
//import { ChangeCategoryService } from 'src/app/services/change-category.service';
//import { CoursesHeaderCO } from 'src/app/co/coursesHeaderCO';



/* declare var jQuery: any;
declare var $: any; */

@Component({
    selector: 'app-header-logged-in',
    templateUrl: './header-logged-in.component.html',
    styleUrls: ['./header-logged-in.component.css']
})
export class HeaderLoggedInComponent implements OnInit {
    public isCallUsDropdown: boolean = false;
    public siteUrl: string = AppSettings.MAIN_WEBSITE_HOME;
    public scoopSiteUrl: string = AppSettings.SCOOP_WEBSITE_HOME;
    private changeCategoryStateCO: ChangeCategoryStateCO = new ChangeCategoryStateCO();

    public showExamMobileMenu = false;
    public isCollapsed = true;
    public cartCount: string = "0";
    public appCurrentState: AppCurrentState = new AppCurrentState();
    public panelArray = AppConstants.PANEL_ARRAY;
    public cmsTagsCOList: CmsTagsCO[] = new Array();//suggested
    //public catCategoriesCOList: CatCategoriesCO[] = new Array();//scoop
    public cmsSearchSuggestionCOList: CmsSearchSuggestionCO[] = new Array();//store
    public selectedOptionName = AppConstants.ALL;

    public searchInputTextvalues: string;
    public isStore: boolean;
    public isSuggested: boolean;
    public isScoop: boolean;
    public isMenuShow: boolean;
    public showHideLearnMenu: boolean = false;
    public showHideBgNav: boolean = false;
    public scoopUrl: string = AppSettings.SCOOP_WEBSITE_HOME;
    public courseHeaderCOList: CoursesHeaderCO[] = new Array();
    public appPopupCO: AppPopupCO = new AppPopupCO();
    public textBackgroundUserProfile: String;

    //    public encodedUrl: string;

    //  public catCategoriesCOListForFIlterStream: CatCategoriesCO[] = new Array();

    constructor(
        private managePopupService: ManagePopupService,
        private blogModuleService: BlogModuleService,
        private appCurrentStateService: AppCurrentStateService,
        private changeCategoryService: ChangeCategoryService) {

        this.managePopupService.appPopupCO.subscribe(appPopupCO => {
            this.appPopupCO = appPopupCO;
        });
    }

    // @HostListener('document:click')
    // clickout() {
    //     this.isMenuShow = false;
    // }

    // onButtonClick(event: MouseEvent) {
    //     event.stopPropagation();

    // }


    showRequestCallBack() {
        this.managePopupService.showrequestACallBackPopUp();
    }

    openAskQuestionPopup() {
        let getFeedbackQuestions: ApiCall = new ApiCall(ApiActions.getFeedbackQuestions)
        getFeedbackQuestions.addRequestParams('status', "getquestions");
        this.blogModuleService.getFeedbackQuestions(getFeedbackQuestions).subscribe(response => {
            let cmnFeedbackQuestionsCOList: CmnFeedbackQuestionsCO[] = response;
            if (cmnFeedbackQuestionsCOList.length > 0) {
                let timeoutTime: number = 10000;
                setTimeout(() => {
                    if (!this.appPopupCO.isAnyPopupOpen()) {
                        this.managePopupService.showAskQuestionPopUp();
                    }
                    else {
                        timeoutTime = 3000;
                        this.openAskQuestionPopup();
                    }
                }, timeoutTime);
            }
            else {
                //    alert();
            }
        });
    }

    openUserQualificationPopup() {
        let userInfoPopUpShow: ApiCall = new ApiCall(ApiActions.userInfoPopUpShow)
        userInfoPopUpShow.addRequestParams('status', "dateNotUpdate");
        this.blogModuleService.userInfoPopUpShow(userInfoPopUpShow).subscribe(response => {
            if (response['isShowPopUp']) {
                let timeoutTime: number = 10000;
                setTimeout(() => {
                    if (!this.appPopupCO.isAnyPopupOpen()) {
                        this.managePopupService.showUserQualificationPopUp();
                    }
                    else {
                        timeoutTime = 3000;
                        this.openUserQualificationPopup();
                    }
                }, timeoutTime);
            }
            else {

            }

        });

    }

    ngOnInit() {

        // if (!this.appCurrentState.userCO) {
        //     let getMostPopularArticleListApiCall: ApiCall = new ApiCall(ApiActions.getFilterStream);
        //     this.blogModuleService.getFilteredStream(getMostPopularArticleListApiCall).subscribe(response => {

        //         this.catCategoriesCOListForFIlterStream = response;

        //     });
        // }




        this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
            this.appCurrentState = appCurrentState;
            console.log("header logged in")
            //this.textBackgroundUserProfile = this.appCurrentState.userCO.firstName.charAt(0).toUpperCase();

            if(!this.appCurrentState.isUserLoggedIn || AppUtility.isEmpty(this.appCurrentState.userCO.firstName))
            {
                this.textBackgroundUserProfile=AppConstants.DEFAULT_FIRST_CHARACTER;
            }
            else{
                this.textBackgroundUserProfile=this.appCurrentState.userCO.firstName.charAt(0).toUpperCase();
            }

            /*             if (this.appCurrentState.processExternalLogout) {
                            this.appExternalLogout();
                        } */

            if (this.appCurrentStateService.isBrowser()) {
                this.openAskQuestionPopup();
                this.openUserQualificationPopup();
                if (this.appCurrentState.isUserLoggedIn) {
                    let unSavedCommentCOString = localStorage.getItem(AppConstants.UNSAVED_COMMENT_CO);
                    if (unSavedCommentCOString) {
                        let newCommentApiCall: ApiCall = new ApiCall(ApiActions.savePostComment);
                        newCommentApiCall.addRequestParams('sclPostCommentCO', unSavedCommentCOString);

                        this.blogModuleService.writePostComment(newCommentApiCall).subscribe(response => {
                            let result = response;
                            //this.commentCO.emit(result); 
                        });
                    }
                    localStorage.removeItem(AppConstants.UNSAVED_COMMENT_CO);

                    let getUserConfigurationApiCall: ApiCall = new ApiCall(ApiActions.getUserConfiguration);
                    this.blogModuleService.getUserConfiguration(getUserConfigurationApiCall).subscribe(response => {
                        let blockUserIdList: string[] = new Array();
                        blockUserIdList = response.responseValues['blockUserIdList'];

                        if (blockUserIdList) {
                            if (blockUserIdList.length > 0) {
                                localStorage.setItem(AppConstants.dirtyList, JSON.stringify(blockUserIdList));
                            }
                        }
                        //this.commentCO.emit(result); 
                    });

                    if (localStorage.getItem(AppConstants.tutorFormFilled)) {
                        this.managePopupService.showTutorSuccessPopUp();
                        localStorage.removeItem(AppConstants.tutorFormFilled);
                    }
                }
            }
        });


        if (this.appCurrentStateService.isBrowser()) {
            /*             this.pageUrl = this.window.location.href;
                      this.encodedUrl = encodeURI( this.pageUrl); */

            let getProductsCartCount: ApiCall = new ApiCall(ApiActions.getProductsCartCount);

            this.blogModuleService.getProductsCartCount(getProductsCartCount).subscribe(response => {
                this.cartCount = response;
            });
        }

        this.changeCategoryService.changeCategoryStateCO.subscribe(changeCategoryStateCO => {
            this.changeCategoryStateCO = changeCategoryStateCO;
        });

        let getCoursesForHeaderApiCall: ApiCall = new ApiCall(ApiActions.getCoursesForHeader);

        this.blogModuleService.getCoursesForHeader(getCoursesForHeaderApiCall).subscribe(response => {
            this.courseHeaderCOList = response;
        });

        /* let getCategoryListApiCall: ApiCall = new ApiCall(ApiActions.getCategoryList);
        this.blogModuleService.getCategoryList(getCategoryListApiCall).subscribe(response => {
            this.catCategoriesCOList = response;
        }); */

        this.onClickOutside();
    }

    saveComment() {

    }

    closeMobileMenu() {
        this.showExamMobileMenu = false;
    }

    hideYouAreStudying(event) {
        this.showExamMobileMenu = false;
    }

    showHideCallUsDropdown() {
        event.stopPropagation();
        if (this.isCallUsDropdown == false) {
            this.isCallUsDropdown = true;
        } else {
            this.isCallUsDropdown = false;
        }
    }
    onClickOutside() { // Hide call us dropdown on click anywhere outside
        document.addEventListener("click", e => {
            this.isCallUsDropdown = false;
        });
    }
    isDropdownOpen() {
        event.stopPropagation();
    }

    preventClickHere() {
        event.stopPropagation();
    }

    subscribeExam() {
        this.changeCategoryService.subscribeCategory();
        this.managePopupService.showSwitchCategoryPopUp();
    }

    switchCategoryId() {
        this.changeCategoryService.switchCategory(this.changeCategoryStateCO.catCategoriesCOList);
        this.managePopupService.showSwitchCategoryPopUp();
    }




    appInternalLogout() {
        this.appCurrentStateService.logout();
        /*         let redirectUrl = window.location.href;
                window.location.href = AppSettings.MAIN_WEBSITE_HOME+"/include/misutils/ng-login?action=logout&redirect_url=" + encodeURI(redirectUrl); */
    }

    /*     appExternalLogout() 
        {       
            
            let pageUrl: string = window.location.href;
            let encodedUrl: string = encodeURI(pageUrl);
            //window.location.href = AppSettings.MAIN_WEBSITE_HOME + "/member/signout?redirect_url=" + encodeURI(pageUrl);
        } */




    // searchKeyWordEvent(event: any) {
    //     this.searchInputTextvalues = event.target.value;
    //     if (this.searchInputTextvalues.length != 0) {
    //         this.searchApiCall();
    //         this.isMenuShow = true;
    //         this.selectPanelOption();
    //     }
    //     else {
    //         this.isMenuShow = false;
    //     }
    // }

    // selectDropDown(panel: string) {
    //     this.selectedOptionName = panel;
    //     if (this.searchInputTextvalues.length != 0) {
    //         this.selectPanelOption();
    //     }
    // }



    // toggleLearnMenu() {
    //     if (this.showHideLearnMenu === true) {
    //         this.showHideLearnMenu = false;
    //         this.showHideBgNav = false;
    //         $("html,body").css("overflow", "auto");
    //     }
    //     else {
    //         this.showHideLearnMenu = true;
    //         this.showHideBgNav = true;
    //         $("html,body").css("overflow", "hidden");
    //     }
    // }

    // searchApiCall() {
    //     let newSearchApiCall: ApiCall = new ApiCall(ApiActions.searchKeywords);
    //     newSearchApiCall.addRequestParams('searchText', this.searchInputTextvalues);
    //     newSearchApiCall.addRequestParams('searchType', this.selectedOptionName);

    //     this.blogModuleService.getSearchedKeyWords(newSearchApiCall).subscribe(response => {
    //         this.cmsTagsCOList = response['cmsTagsCOList'];
    //         this.catCategoriesCOList = response['catCategoriesCOList'];
    //         this.cmsSearchSuggestionCOList = response['cmsSearchSuggestionCO'];
    //     });

    // }

    // selectPanelOption() {
    //     if (this.selectedOptionName === AppConstants.ALL) {
    //         this.isStore = true;
    //         this.isSuggested = true;
    //         this.isScoop = true;

    //         this.searchApiCall();
    //         this.isMenuShow = true;

    //     }
    //     else if (this.selectedOptionName === AppConstants.STORE) {
    //         this.isStore = true;
    //         this.isSuggested = true;
    //         this.isScoop = false;

    //         this.searchApiCall();
    //         this.isMenuShow = true;

    //     }
    //     else if (this.selectedOptionName === AppConstants.SCOOP) {
    //         this.isStore = false;
    //         this.isSuggested = false;
    //         this.isScoop = true;
    //         this.searchApiCall();
    //         this.isMenuShow = true;

    //     }

    // }

    // inAllClick() {
    //     window.location.href = AppSettings.MAIN_WEBSITE_HOME + "/store/search?sortby=ltst&location=" + encodeURI(this.selectedOptionName) + "&s=" + encodeURI(this.searchInputTextvalues) + "&cat_id=off&subject_ids%5B%5D=off&category=off&module=all";
    // }
    // inStoreClick() {
    //     window.location.href = AppSettings.MAIN_WEBSITE_HOME + "/store/search?sortby=ltst&location=" + this.selectedOptionName + "&s=" + this.searchInputTextvalues + "&cat_id=off&subject_ids%5B%5D=off&category=off&module=store";
    // }
    // onScoopClick() {
    //     window.location.href = AppSettings.SCOOP_WEBSITE_HOME + "/?location=" + this.selectedOptionName + "&s=" + this.searchInputTextvalues + "&module=scoop";
    // }

    // onSearchedStoreClick(cmsSuggestionCO: CmsSearchSuggestionCO) {
    //     window.location.href = AppSettings.MAIN_WEBSITE_HOME + "/store/search?sortby=ltst&location=" + encodeURI(this.selectedOptionName) + "&s=" + encodeURI(cmsSuggestionCO.subjectTitle) + "+%28BL%29&" + encodeURI(cmsSuggestionCO.categoryId) + "&subject_ids%5B%5D=" + encodeURI(cmsSuggestionCO.subjectId) + "&" + encodeURI(cmsSuggestionCO.categoryTitle) + "&module=store"
    // }
    // onSearchedScoopClick(catCategoriesCO: CatCategoriesCO) {
    //     window.location.href = AppSettings.SCOOP_WEBSITE_HOME + "/category/" + encodeURI(catCategoriesCO.title) + "?location=" + encodeURI(this.selectedOptionName) + "&module=scoop"
    // }
    // onSearchedSuggetedClick(cmsTagsCO: CmsTagsCO) {
    //     window.location.href = AppSettings.MAIN_WEBSITE_HOME + "/store/search?sortby=ltst&location=" + encodeURI(this.selectedOptionName) + "&s=" + encodeURI(cmsTagsCO.title) + "&cat_id=off&subject_ids%5B%5D=off&category=off&module=suggested+search"
    // }


    openAppPopUp() 
    {
        if (!this.appPopupCO.isAnyPopupOpen())
         {
            this.managePopupService.showAndroidAppPopUp();
        }
    }

    public getClassForTextBackGround(): String {
        return "user-profile-commonDesign " + AppUtility.textBackgroundColor(this.textBackgroundUserProfile) + " sm16 inlineBlock-profile";;
    }

    



}
