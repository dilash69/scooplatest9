import { AppConstants } from './../../common/app.constants';
import { Component, OnInit, Input, ContentChild, TemplateRef, Output, OnDestroy, ElementRef, Renderer2, Inject, PLATFORM_ID, EventEmitter } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
/* import { DeviceDetectorService } from 'ngx-device-detector'; */

@Component({
  selector: 'ngui-in-view',
  template: `
    <ng-container *ngIf="inView" [ngTemplateOutlet]="template">
    </ng-container>
  `,
  styles: [':host {display: block;}']
})
export class NguiInViewComponent implements OnInit, OnDestroy {
  observer: IntersectionObserver;
  inView: boolean = false;
  once50PctVisible: boolean = false;

  @ContentChild(TemplateRef) template: TemplateRef<any>;
  // @Input() options: any = { threshold: [.1, .2, .3, .4, .5, .6, .7, .8] };
  @Input() options: any = { rootMargin: '200px', threshold: [1] };
  @Output('inView') inView$: EventEmitter<any> = new EventEmitter();
  @Output('notInView') notInView$: EventEmitter<any> = new EventEmitter();

  constructor(
    public element: ElementRef,
    public renderer: Renderer2,
    @Inject(PLATFORM_ID) private platformId: any
    ) { }

  ngOnInit(): void {
    /* if(this.deviceService.getDeviceInfo().os !== AppConstants.iOS)
    {
      if (isPlatformBrowser(this.platformId)) 
      {
        this.observer = new IntersectionObserver(this.handleIntersect.bind(this), this.options);
        this.observer.observe(this.element.nativeElement);
      }
    } 
    else 
    {
      //console.log("this.deviceService.getDeviceInfo().os ", this.deviceService.getDeviceInfo().os);
      this.inView = true;
    } */
    
  }

  ngOnDestroy(): void {
    if (this.observer) {
      this.observer.disconnect();
    }
  }

  handleIntersect(entries, observer): void {
    entries.forEach((entry: IntersectionObserverEntry) => {
      if (entry.isIntersecting) {
        this.inView = true;
        this.inView$.emit(entry);
      } else {
        this.notInView$.emit(entry);
      }
    });
  }
}