import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-login-popup-left-part',
  templateUrl: './login-popup-left-part.component.html',
  styleUrls: ['./login-popup-left-part.component.css']
})
export class LoginPopupLeftPartComponent implements OnInit {
    //showNavigationArrows = true;
    showNavigationIndicators = true;

    constructor(config: NgbCarouselConfig) { 
        config.interval = 5000;
        config.wrap = true;
        config.keyboard = true;
        config.pauseOnHover = true;
        //config.showNavigationArrows = true;
        config.showNavigationIndicators = true;
    }

    ngOnInit() {
    }
}
