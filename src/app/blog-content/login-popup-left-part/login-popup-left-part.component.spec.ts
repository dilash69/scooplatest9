import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginPopupLeftPartComponent } from './login-popup-left-part.component';

describe('LoginPopupLeftPartComponent', () => {
  let component: LoginPopupLeftPartComponent;
  let fixture: ComponentFixture<LoginPopupLeftPartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginPopupLeftPartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPopupLeftPartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
