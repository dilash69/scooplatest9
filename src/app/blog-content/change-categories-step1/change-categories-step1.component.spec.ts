import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeCategoriesStep1Component } from './change-categories-step1.component';

describe('ChangeCategoriesStep1Component', () => {
  let component: ChangeCategoriesStep1Component;
  let fixture: ComponentFixture<ChangeCategoriesStep1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeCategoriesStep1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeCategoriesStep1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
