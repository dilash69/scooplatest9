import { CatCategoriesCO } from './../../co/categoryCO';
import { AppUtility } from './../../common/app.utility';
import { ChangeCategoryStateCO } from './../../co/changeCategoryStateCO';
import { ChangeCategoryUserSelectionCO } from './../../co/changeCategoryUserSelectionCO';
import { deserialize } from "serializer.ts/Serializer";
import { BlogModuleService } from './../../services/blog-module.service';
import { ChangeCategoryService } from './../../services/change-category.service';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { CategoryExamCO } from './../../co/categoryExamCO';
import { AppConstants } from './../../common/app.constants';
import { Component, OnInit, Input } from '@angular/core';
//import { CatCategoriesCO } from 'src/app/co/categoryCO';
//import { AppUtility } from 'src/app/common/app.utility';
//import { AppUtility } from 'src/app/common/app.utility';
// import { ChangeCategoryUserSelectionCO } from 'src/app/co/changeCategoryUserSelectionCO';
// import { ChangeCategoryStateCO } from 'src/app/co/changeCategoryStateCO';

@Component({
  selector: 'app-change-categories-step1',
  templateUrl: './change-categories-step1.component.html',
  styleUrls: ['./change-categories-step1.component.css']
})
export class ChangeCategoriesStep1Component implements OnInit 
{
  public changeCategoryStateCO:ChangeCategoryStateCO;
  public changeCategoryUserSelectionCO:ChangeCategoryUserSelectionCO;  
  public randomNumber: number;
  searchText:any
  
 
  constructor(private blogModuleService: BlogModuleService, 
              private changeCategoryService :ChangeCategoryService) { }

  ngOnInit() 
  {    
      this.changeCategoryService.changeCategoryStateCO.subscribe(changeCategoryStateCO => {
          this.changeCategoryStateCO = changeCategoryStateCO;
       });

      this.changeCategoryService.changeCategoryUserSelectionCO.subscribe(changeCategoryUserSelectionCO => {
        this.changeCategoryUserSelectionCO = changeCategoryUserSelectionCO;

        if(this.changeCategoryStateCO.currentStep === AppConstants.STEP_1_VALUE)
        {
          if(!this.changeCategoryUserSelectionCO.allCatCategoriesCOList)
          {
            let getCategoryListApiCall: ApiCall = new ApiCall(ApiActions.getCategoryList);
            
            this.blogModuleService.getCategoryList(getCategoryListApiCall)
              .subscribe(response => 
              {
                this.changeCategoryUserSelectionCO.allCatCategoriesCOList = response;
                AppUtility.log("this.changeCategoryUserSelectionCO.allCatCategoriesCOList",this.changeCategoryUserSelectionCO.allCatCategoriesCOList[1])
                //this.assignRandomColor();
              });
          } 
        }
       
      }); 
  }

  //-- step1
  getRandomBgColor() {
    let rand = Math.floor(Math.random() * 10);
    while (rand < 1) {
      rand = Math.floor(Math.random() * 10);
    }
    
    return rand.toString();
  }

  getFirstLetter(categoryName: string) {
    return categoryName.substring(0, 1);
  }

  selectCategory(catCategoryCO: CatCategoriesCO) 
  {
    for (let i = 0; i < this.changeCategoryUserSelectionCO.allCatCategoriesCOList.length; i++) 
    {
      this.changeCategoryUserSelectionCO.allCatCategoriesCOList[i].isActive = false;
    }
    
    catCategoryCO.isActive = true;
    this.changeCategoryUserSelectionCO.catCategoryCO=catCategoryCO;
    this.goToNextStep()
    //this.changeCategoryService.updateChangeCategoryUserSelectionCO(this.changeCategoryUserSelectionCO); 
  }

  updateActiveClass(isActive): any 
  {
    return isActive ? 'active' : '';
  }

  goToNextStep()
  {
    for(let i =0 ;i<this.changeCategoryUserSelectionCO.allCatCategoriesCOList.length;i++)
    {
      if(this.changeCategoryUserSelectionCO.allCatCategoriesCOList[i].isActive)
      {
       // this.changeCategoryService.followCategory(this.changeCategoryUserSelectionCO.allCatCategoriesCOList[i],AppConstants.FOLLOW_CATEGORY);

        this.changeCategoryStateCO.currentStep = AppConstants.STEP_2_VALUE;
        this.changeCategoryStateCO.purpose = AppConstants.FOLLOW_CATEGORY;
        this.changeCategoryService.updateChangeCategoryStateCO(this.changeCategoryStateCO);

        this.changeCategoryService.updateChangeCategoryUserSelectionCO(this.changeCategoryUserSelectionCO);
        break;
      }
      
    }
    
    /* this.changeCategoryStateCO.currentStep = AppConstants.STEP_2_VALUE;
    this.changeCategoryService.updateChangeCategoryStateCO(this.changeCategoryStateCO); */
  }

  /* assignRandomColor()
  {
    for(let i =0 ;i<this.changeCategoryUserSelectionCO.allCatCategoriesCOList.length;i++)
    {
      if(!(this.changeCategoryUserSelectionCO.allCatCategoriesCOList[i].categoryTitleColor===''))
      {
        this.changeCategoryUserSelectionCO.allCatCategoriesCOList[i].categoryTitleColor = "bgcolor"+ this.getRandomBgColor();
      }
    }
  } */

  getLimitedCategoryTitle(title:string):string
  {
    return AppUtility.getTrimmedReadableString(title,10)
  } 
}
