import { UserCO } from './../../co/userCO';
//import { BehaviorSubject } from 'rxjs';
import {BehaviorSubject} from 'rxjs';
import { BlogModuleService } from './../../services/blog-module.service';
import { Component, OnInit, Input } from '@angular/core';
import { AppUtility } from '../../common/app.utility';

@Component({
  selector: 'app-author-summary',
  templateUrl: './author-summary.component.html',
  styleUrls: ['./author-summary.component.css']
})
export class AuthorSummaryComponent implements OnInit {

  private userCOBehaviorSubject = new BehaviorSubject<UserCO>(null);
  public textbackground:String;

  @Input()
  set userCO(value) 
  {
    this.userCOBehaviorSubject.next(value);
  };

  get userCO() 
  {
    return this.userCOBehaviorSubject.getValue();
  }

  constructor(private blogModuleService : BlogModuleService) { 

  }

  ngOnInit() 
  {
    this.userCOBehaviorSubject
        .subscribe(x => {
            if (this.userCO) 
            {
               this.textbackground =this.userCO.firstName.charAt(0);
            }
        });
  }


  public getClassForTextBackGround():String
  {
    return "user-profile-commonDesign userProfile-96 "+AppUtility.textBackgroundColor(this.textbackground)+" sm20";;
  }
  
}
