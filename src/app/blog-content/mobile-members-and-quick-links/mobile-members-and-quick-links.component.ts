import { CategoryCOService } from './../../services/category-co.service';
import { SclPostCO } from './../../co/sclPostCO';
import { CatCategoryQuickLinksCO } from './../../co/catCategoryQuickLinksCO';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { AppSettings } from './../../common/app-settings';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { DataSharingService } from './../../services/data-sharing.service';
import { BlogModuleService } from './../../services/blog-module.service';
import { AppCurrentState } from './../../co/app.current.state';
import { BehaviorSubject} from 'rxjs';
import { CatCategoriesCO } from './../../co/categoryCO';
import { Component, OnInit, Input, Renderer2, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-mobile-members-and-quick-links',
    templateUrl: './mobile-members-and-quick-links.component.html',
    styleUrls: ['./mobile-members-and-quick-links.component.css']
})
export class MobileMembersAndQuickLinksComponent implements OnInit {
    showHideQuickLinks: boolean = false;
    showHideToggleIcons: boolean = false;

    private sclPostCOBehaviorSubject = new BehaviorSubject<SclPostCO>(null);
    public appCurrentState : AppCurrentState = new AppCurrentState();
    public pageEncodedUrl :string = "";
    public siteUrl: string;
    public catCategoryQuickLinksCOlist:CatCategoryQuickLinksCO[]
    public catCategoriesCO:CatCategoriesCO;
      
    @Input() categoryId:string;
    //@Input() sclPostCO:SclPostCO;
    @Output() closeCategoryProfileMobileHeader = new EventEmitter();
  
    constructor(private blogModuleService : BlogModuleService, 
      private dataSharingService: DataSharingService,
      private route: ActivatedRoute,
      private modalService: NgbModal,
      private appCurrentStateService: AppCurrentStateService,
      private categoryCOService:CategoryCOService,
      private renderer: Renderer2) { 
     
    }

    ngOnInit() 
    {
        if(this.appCurrentStateService.isBrowser())
        {
            this.pageEncodedUrl = encodeURIComponent(window.location.href);
        }
        
        this.siteUrl = AppSettings.MAIN_WEBSITE_HOME;

        this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => 
            {
                this.appCurrentState = appCurrentState;  
                this.categoryCOService.categoryCO.subscribe(categoryCO => 
                    {
                        this.catCategoriesCO = categoryCO;
            
                    });

                let getCategoryQuickLinksListApiCall: ApiCall = new ApiCall(ApiActions.getCategoryQuickLinks);
                getCategoryQuickLinksListApiCall.addRequestParams('categoryId', this.categoryId);

                this.blogModuleService.getCategoryQuickLinks(getCategoryQuickLinksListApiCall).subscribe(response => 
                    {
                        this.catCategoryQuickLinksCOlist = response;
                    });
            });
    }

    hideMembersAndQuickLinks() 
    {
        if(this.appCurrentStateService.isBrowser())
        {
            this.renderer.removeClass(document.body, 'modal-open');
            this.showHideQuickLinks = false;
            this.showHideToggleIcons = false;
            this.closeCategoryProfileMobileHeader.emit();
        }
    }
}
