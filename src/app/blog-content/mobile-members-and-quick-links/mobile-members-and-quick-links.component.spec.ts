import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileMembersAndQuickLinksComponent } from './mobile-members-and-quick-links.component';

describe('MobileMembersAndQuickLinksComponent', () => {
  let component: MobileMembersAndQuickLinksComponent;
  let fixture: ComponentFixture<MobileMembersAndQuickLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileMembersAndQuickLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileMembersAndQuickLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
