import { Component, OnInit, Input } from '@angular/core';
import { CatCategoryAdSlidersCO } from '../../co/catCategoryAdSlidersCO';
import { BehaviorSubject } from 'rxjs';
import { CatCategoriesCO } from '../../co/categoryCO';
import { BlogModuleService } from '../../services/blog-module.service';
import { ApiCall } from '../../common/api.call';
import { ApiActions } from '../../common/api.actions';

@Component({
  selector: 'app-eduncle-advertisement-half-slider',
  templateUrl: './eduncle-advertisement-half-slider.component.html',
  styleUrls: ['./eduncle-advertisement-half-slider.component.css']
})
export class EduncleAdvertisementHalfSliderComponent implements OnInit {
  public catCategoryAdSlidersCOList: CatCategoryAdSlidersCO[];
  @Input() tagId: string;
  private catCategoriesCOBehaviorSubject = new BehaviorSubject<CatCategoriesCO>(
      null
  );

  @Input()
  set catCategoriesCO(value) 
  {
      this.catCategoriesCOBehaviorSubject.next(value);
  }

  get catCategoriesCO() 
  {
      return this.catCategoriesCOBehaviorSubject.getValue();
  }

  constructor(private blogModuleService: BlogModuleService) 
  {
      /* config.max = 5;
          config.readonly = true;  */
  }

  ngOnInit() 
  {
      var vm = this;

      this.catCategoriesCOBehaviorSubject.subscribe(x => {
        console.log("EduncleAdvertisementHalfSliderComponent categoryCO subscriber")
          if (this.catCategoriesCO) 
          {
              let getCategoryAdSliderApiCall: ApiCall = new ApiCall(ApiActions.getCategoryAdSlider);
              getCategoryAdSliderApiCall.addRequestParams("categoryId",this.catCategoriesCO.id);
              getCategoryAdSliderApiCall.addRequestParams("adType", "IN_BETWEEN");

              this.blogModuleService.getCategoryAdSlider(getCategoryAdSliderApiCall).subscribe(response => {
                      this.catCategoryAdSlidersCOList = response;
                  });
          }
          if (this.tagId) 
          {
              let getTagAdSliderByTagIdApiCall: ApiCall = new ApiCall(ApiActions.getTagAdSliderByTagId);
              getTagAdSliderByTagIdApiCall.addRequestParams("tagId", this.tagId);
              getTagAdSliderByTagIdApiCall.addRequestParams("adType", "IN_BETWEEN");

              this.blogModuleService.getTagAdSliderByTagId(getTagAdSliderByTagIdApiCall).subscribe(response => {
                      this.catCategoryAdSlidersCOList = response;
                  });
          }
      });
  }

  customOptions: any = {
    loop: true,
    stagePadding: 100,
    margin:10,
    singleItem: true,
    items: 1,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 700,
    nav: true,
    navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
    responsive: {
      0: {
        items: 1,
        stagePadding: 0
      },
      400: {
        items: 1,
        stagePadding: 100
      },
      740: {
        items: 1,
        stagePadding: 150
      },
      940: {
        items: 1,
        stagePadding: 150
      }
    },
}
}
