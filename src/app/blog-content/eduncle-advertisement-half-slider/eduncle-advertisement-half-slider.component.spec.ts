import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EduncleAdvertisementHalfSliderComponent } from './eduncle-advertisement-half-slider.component';

describe('EduncleAdvertisementHalfSliderComponent', () => {
  let component: EduncleAdvertisementHalfSliderComponent;
  let fixture: ComponentFixture<EduncleAdvertisementHalfSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EduncleAdvertisementHalfSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EduncleAdvertisementHalfSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
