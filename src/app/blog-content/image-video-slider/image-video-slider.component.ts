import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { NgbModal, NgbCarouselConfig, NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { SclPostAttachmentsCO } from '../../co/SclPostAttachmentsCO';
import { ManagePopupService } from './../../services/manage-popup.service';

@Component({
  selector: 'app-image-video-slider',
  templateUrl: './image-video-slider.component.html',
  styleUrls: ['./image-video-slider.component.css'],
})
export class ImageVideoSliderComponent implements OnInit {
  @Input() sclPostAttachmentsCOList:SclPostAttachmentsCO[];
  @Input() sclPostAttachmentPosition:string;
  @ViewChild('myCarousel') myCarousel: NgbCarousel;
  
  constructor(private modalService: NgbModal, config: NgbCarouselConfig,
    private managePopupService:ManagePopupService) {
    config.showNavigationArrows = true;  
    config.showNavigationIndicators = false;
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg', centered: true });
  }


  ngAfterViewInit(){
    this.myCarousel.activeId=this.sclPostAttachmentPosition;
}

  ngOnInit() 
  {
    
   
  }
  
  dismiss()
  {
    this.managePopupService.closePopUp();
  }
}
