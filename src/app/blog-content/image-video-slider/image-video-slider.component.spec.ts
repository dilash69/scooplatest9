import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageVideoSliderComponent } from './image-video-slider.component';

describe('ImageVideoSliderComponent', () => {
  let component: ImageVideoSliderComponent;
  let fixture: ComponentFixture<ImageVideoSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageVideoSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageVideoSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
