import { ChangeCategoryUserSelectionCO } from './../../co/changeCategoryUserSelectionCO';
import { ChangeCategoryService } from './../../services/change-category.service';
import { ChangeCategoryStateCO } from './../../co/changeCategoryStateCO';
import { AppConstants } from './../../common/app.constants';
import { AppSettings } from './../../common/app-settings';
import { Inject } from '@angular/core';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AppCurrentState } from './../../co/app.current.state';
import { DataSharingService } from './../../services/data-sharing.service';
import { CatCategoriesCO } from './../../co/categoryCO';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { BlogModuleService } from './../../services/blog-module.service';
import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject} from 'rxjs';
import {ActivatedRoute} from "@angular/router";
import { deserialize } from 'serializer.ts/Serializer';
import { ManagePopupService } from '../../services/manage-popup.service';
import { TriggerJoinExamBlockService } from './../../services/trigger-join-exam-block.service';
import { UpdateCurrentCategoryDataService } from './../../services/update-current-category-data.service';
//import { ChangeCategoryUserSelectionCO } from 'src/app/co/changeCategoryUserSelectionCO';
// import { ChangeCategoryService } from 'src/app/services/change-category.service';
// import { ChangeCategoryStateCO } from 'src/app/co/changeCategoryStateCO';

//import { AppConstants } from 'src/app/common/app.constants';

@Component({
  selector: 'app-join-exam-block',
  templateUrl: './join-exam-block.component.html',
  styleUrls: ['./join-exam-block.component.css']
})
export class JoinExamBlockComponent implements OnInit {

  private catCategoriesCOBehaviorSubject = new BehaviorSubject<CatCategoriesCO>(null);
  private changeCategoryStateCO:ChangeCategoryStateCO = new ChangeCategoryStateCO();
  public changeCategoryUserSelectionCO:ChangeCategoryUserSelectionCO = new ChangeCategoryUserSelectionCO();
  
  public appCurrentState : AppCurrentState = new AppCurrentState();
   public pageEncodedUrl :string = "";
  public siteUrl: string;
/*  public scoopUrl: string; */
    

  @Input()
  set catCategoriesCO(value) 
  {
      this.catCategoriesCOBehaviorSubject.next(value);
  };

  get catCategoriesCO() 
  {
      return this.catCategoriesCOBehaviorSubject.getValue();
  }

  constructor(
    private managePopupService : ManagePopupService,
    private blogModuleService : BlogModuleService, 
    private dataSharingService: DataSharingService,
    private route: ActivatedRoute,
    private appCurrentStateService: AppCurrentStateService,
    private changeCategoryService :ChangeCategoryService,
    private triggerExamBlockService :TriggerJoinExamBlockService,
    private updateCurrentCategoryDataService: UpdateCurrentCategoryDataService) { 
   
  }
  ngOnInit() 
  {
    if(this.appCurrentStateService.isBrowser())
    {
      this.pageEncodedUrl = encodeURIComponent(window.location.href);
    }

    this.changeCategoryService.changeCategoryStateCO.subscribe(changeCategoryStateCO => 
    {
      this.changeCategoryStateCO = changeCategoryStateCO;
      this.checkIfCategoryIsFollowed();
    });

    this.triggerExamBlockService.triggerObservable.subscribe(triggerJoinExamBlock => 
      {
        if(triggerJoinExamBlock)
        {
          this.updateCurrentCategoryDataService.updateCurrentCategory(this.catCategoriesCO);
        }
      });
  


    this.changeCategoryService.changeCategoryUserSelectionCO.subscribe(changeCategoryUserSelectionCO => 
      {
        this.changeCategoryUserSelectionCO = changeCategoryUserSelectionCO;
      });
    
    this.siteUrl = AppSettings.MAIN_WEBSITE_HOME;
//    this.scoopUrl = AppSettings.SCOOP_WEBSITE_HOME;

    this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
      this.appCurrentState = appCurrentState;

       if(this.appCurrentStateService.isLoggedInChecked && this.appCurrentState.isUserLoggedIn)
      { 
        let getCategoryRelationObjectByUserApiCall:ApiCall = new ApiCall(ApiActions.getCategoryRelationObjectByUser)
        this.blogModuleService.getCategoryRelationObjectByUser(getCategoryRelationObjectByUserApiCall).subscribe(response => 
        {
          this.changeCategoryStateCO.catCategoriesCOList = response;   
          this.checkIfCategoryIsFollowed(); 
          //this.changeCategoryService.updateChangeCategoryStateCO(this.changeCategoryStateCO);  
        });
      } 
    });
    
    this.catCategoriesCOBehaviorSubject
    .subscribe(x => 
    {
      if(this.catCategoriesCO)
      {
       if(this.catCategoriesCO.title == null)
       {
        let getCategoryDetailByCategoryIdApiCall: ApiCall = new ApiCall(ApiActions.getCategoryDetailByCategoryId);
        getCategoryDetailByCategoryIdApiCall.addRequestParams('categoryId', this.catCategoriesCO.id);

        this.blogModuleService.getCategoryDetailByCategoryId(getCategoryDetailByCategoryIdApiCall).subscribe(response => {
          this.catCategoriesCO = response;
          this.checkIfCategoryIsFollowed();
          if(this.appCurrentStateService.isBrowser())
          {
            localStorage.setItem(AppConstants.currentCategoryName,this.catCategoriesCO.title);
          }
        });
       }
       else
       {
        if(this.appCurrentStateService.isBrowser())
        {
          localStorage.setItem(AppConstants.currentCategoryName,this.catCategoriesCO.title);
        }
       }
      }
    });
   }

   checkIfCategoryIsFollowed()
   {
     if(this.catCategoriesCO)
     {
      if(this.changeCategoryStateCO && this.changeCategoryStateCO.catCategoriesCOList)
      {
        this.catCategoriesCO.isUserJoin = false;
       for(let catCategoriesCOTemp of this.changeCategoryStateCO.catCategoriesCOList)
       {
           if(catCategoriesCOTemp.id === this.catCategoriesCO.id)
           {
             this.catCategoriesCO.isUserJoin = true;
           }
       }
      }
      else
      {
       this.catCategoriesCO.isUserJoin = false;
      }
     }
   }


   openDenyUnfollowPopUpOpen()
   {    
    this.changeCategoryService.unFollowCategory(this.catCategoriesCO);
    this.managePopupService.showDenyUnfollowPopUp();
   }

   openLoginPopUp(catCategoryId)
   {
    // this.managePopupService.showLoginPopUp(catCategoryId);
    localStorage.setItem(AppConstants.REGISTRATION_PROCESS, AppConstants.FOLLOW_CATEGORY_REGISTRATION_PROCESS);
    this.managePopupService.showSignPopUp(catCategoryId,AppConstants.REGISTRATION);
   }

   switchCategoryId()
   {
    this.changeCategoryService.switchCategory(this.changeCategoryStateCO.catCategoriesCOList);
    this.managePopupService.showSwitchCategoryPopUp();
   }

   followCategory()
   {
    this.changeCategoryService.followCategory(this.changeCategoryStateCO.catCategoriesCOList, this.catCategoriesCO);
    this.managePopupService.showSwitchCategoryPopUp();
   }

   public getRidirectUrl()
   {
      return AppConstants.REDIRECT_URL;
   }

   changeCategoryPopup() 
   {
    //this.modalReference = this.modalService.open(content, { centered: true });
/*      this.changeCategoryService.changeCategoryUserSelectionCO.subscribe(changeCategoryUserSelectionCO => {
      changeCategoryUserSelectionCO.categoryExamCOList = null;

    });  */
   }


closePopUp()
{
  //this.modalReference.close();
}




}