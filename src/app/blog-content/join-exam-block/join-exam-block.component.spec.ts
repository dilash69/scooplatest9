import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinExamBlockComponent } from './join-exam-block.component';

describe('JoinExamBlockComponent', () => {
  let component: JoinExamBlockComponent;
  let fixture: ComponentFixture<JoinExamBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinExamBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinExamBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
