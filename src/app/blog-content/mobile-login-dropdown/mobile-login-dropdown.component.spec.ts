import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileLoginDropdownComponent } from './mobile-login-dropdown.component';

describe('MobileLoginDropdownComponent', () => {
  let component: MobileLoginDropdownComponent;
  let fixture: ComponentFixture<MobileLoginDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileLoginDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileLoginDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
