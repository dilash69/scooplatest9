import { Component, OnInit } from '@angular/core';
import { UserCO } from '../../co/userCO';
import { AppSettings } from '../../common/app-settings';
import { AppConstants } from '../../common/app.constants';

@Component({
  selector: 'app-mobile-login-dropdown',
  templateUrl: './mobile-login-dropdown.component.html',
  styleUrls: ['./mobile-login-dropdown.component.css']
})
export class MobileLoginDropdownComponent implements OnInit {

  public loggedInUserCO: UserCO;
  siteUrl:string=AppSettings.MAIN_WEBSITE_HOME;
  constructor() { }

  ngOnInit() {
    this.loggedInUserCO = JSON.parse(localStorage.getItem(AppConstants.LOGGED_IN_USER_CO));
  }

}
