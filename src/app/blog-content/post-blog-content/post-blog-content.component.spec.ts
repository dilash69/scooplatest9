import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostBlogContentComponent } from './post-blog-content.component';

describe('PostBlogContentComponent', () => {
  let component: PostBlogContentComponent;
  let fixture: ComponentFixture<PostBlogContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostBlogContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostBlogContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
