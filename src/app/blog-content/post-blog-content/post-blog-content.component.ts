import { Component, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject } from 'rxjs';
import { deserialize } from 'serializer.ts/Serializer';
import { AppUtility } from '../../common/app.utility';
import { AppCurrentState } from './../../co/app.current.state';
import { AppPopupCO } from './../../co/app.popup.co';
import { CatCategoryAdSlidersCO } from './../../co/catCategoryAdSlidersCO';
import { CatCategoriesCO } from './../../co/categoryCO';
import { SclPostCommentCO } from './../../co/commentCO';
import { SclCommentReplyCO } from './../../co/commentReplyCO';
import { SclPostCO } from './../../co/sclPostCO';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { AppConstants } from './../../common/app.constants';
import { IitJamAchieverCourse2017 } from './../../common/iit-jam-achiever-course2017';
import { UgcNetAchieverCourse } from './../../common/ugc-net-achiever-course';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { BlogModuleService } from './../../services/blog-module.service';
import { ManagePopupService } from './../../services/manage-popup.service';

// import a plugin
import 'lazysizes';
import 'lazysizes/plugins/parent-fit/ls.parent-fit';


declare var $: any;



//import { CatCategoriesCO } from 'src/app/co/categoryCO';


@Component({
    selector: 'app-post-blog-content',
    templateUrl: './post-blog-content.component.html',
    styleUrls: ['./post-blog-content.component.css']
})
export class PostBlogContentComponent implements OnInit {
    public commentIdList: String[];
    public commentCOArray: SclPostCommentCO[] = new Array();
    public isAllCommentsLoaded: boolean = true;
    public chunkSize: string;
    public replyIdList: string[];
    public sclPostCOList: SclPostCO[];
    public catCategoryAdSlidersCOList: CatCategoryAdSlidersCO[];
    public appCurrentState: AppCurrentState = new AppCurrentState();
    public shareUrl: string;
    public facebookCount: number = 0;
    public replyCommentCount: number = 0;
    public postLikeStatus: boolean = false;
    public showCommentListing: boolean = false;
    public showInsertComment: boolean = false;
    public showYouMightAlsoLike: boolean = false;
    public showTag: boolean = false;

    private postCOBehaviorSubject = new BehaviorSubject<SclPostCO>(null);

    public catCategoryCO: CatCategoriesCO;
    // UI
    public isContentHasSlider: boolean = false;
    public innerHtmlContent1: any;
    public innerHtmlContent2: any;
    public count = 0;
    public commentId: string;
    public replyId: string;
    public jumpTagString: string;
    public showMyElement:boolean;

    public appPopupCO: AppPopupCO = new AppPopupCO();
    public fragment: string;

    public textbackground:String;
    public textBackgroundUserProfile:String;

    defaultImageSquare:String = AppConstants.DEFAULT_IMAGE_SQUARE;
    defaultImageCircle:String = AppConstants.DEFAULT_IMAGE_CIRCLE;
    

    @Output() deleteCommentFromList = new EventEmitter();
    @Input()
    set postCO(value) {
        this.postCOBehaviorSubject.next(value);
    };

    get postCO() {
        return this.postCOBehaviorSubject.getValue();
    }


    
    constructor(
        private renderer: Renderer2,
        private managePopupService: ManagePopupService,
        private blogModuleService: BlogModuleService,
        private modalService: NgbModal,
        private appCurrentStateService: AppCurrentStateService,
        private route: ActivatedRoute) {

    }

    openLoginPopUp(categoryId) {

        // this.managePopupService.showLoginPopUp(categoryId);
        localStorage.setItem(AppConstants.REGISTRATION_PROCESS, AppConstants.BLOG_ACTIVITY_REGISTRATION_PROCESS);
        this.managePopupService.showSignPopUp(categoryId,AppConstants.APP_PROMOTION);
    }

    ngOnInit() 
    {
        this.route.fragment.subscribe(fragment => { this.fragment = fragment; });
        this.managePopupService.appPopupCO.subscribe(appPopupCO => {
            console.log("PostBlogContentComponent popup subscriber")
            this.appPopupCO = appPopupCO;

            if (this.appCurrentStateService.isBrowser()) {
                if (this.appPopupCO.isAnyPopupOpen()) {
                    this.renderer.addClass(document.body, 'modal-open');
                }
                else {
                    this.renderer.removeClass(document.body, 'modal-open');
                }
            }
        });

        if (this.appCurrentStateService.isBrowser()) {
            this.shareUrl = window.location.href;
            this.jumpTagString = this.shareUrl.split('#')[1];
            this.blogModuleService.getFacebookShareCount(this.shareUrl)
                .subscribe(response => {
                   // console.log("res123",response)
                    //this.facebookCount = parseInt(response.share['share_count'])
                });
        }

        this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
            console.log("PostBlogContentComponent appcurrent subscriber")
            this.appCurrentState = appCurrentState;
 //           console.log("this.appCurrentState1 ", JSON.stringify(this.appCurrentState))
 //           console.log("this.appCurrentState", this.appCurrentState.userCO)
            //this.textBackgroundUserProfile=this.appCurrentState.userCO.firstName.charAt(0).toUpperCase();
             if(!this.appCurrentState.isUserLoggedIn || AppUtility.isEmpty(this.appCurrentState.userCO.firstName))
            {
                this.textBackgroundUserProfile=AppConstants.DEFAULT_FIRST_CHARACTER;
            }
            else
            {
                this.textBackgroundUserProfile=this.appCurrentState.userCO.firstName.charAt(0).toUpperCase();
            } 
           
           // console.log("isLoggedInChecked",this.appCurrentStateService.isLoggedInChecked);
            if(this.appCurrentStateService.isLoggedInChecked && this.appCurrentState.isUserLoggedIn)
            {
                //hasUserLiked
                if(this.postCO)
                {
                    let hasUserLikedPostApiCall: ApiCall = new ApiCall(ApiActions.hasUserLikedPost);
                    hasUserLikedPostApiCall.addRequestParams('postId', this.postCO.id);
            
                    this.blogModuleService.hasUserLikedPost(hasUserLikedPostApiCall).subscribe(responseValues => 
                    {
                        this.postLikeStatus = responseValues['isLiked'];
                        this.postCO.fbShareCount = responseValues['postTotalFbShares'];
                        this.postCO.totalReplies = responseValues['postTotalReplies'];
                        this.postCO.totalLikes = responseValues['postTotalLikes'];
                        this.replyCommentCount = parseInt(this.postCO.totalComments) + parseInt(this.postCO.totalReplies);
                    });
                    this.getInitialCommentList();
                }
            }
        });

        this.postCOBehaviorSubject
            .subscribe(x => {
                if (this.postCO) {
                    console.log("PostBlogContentComponent postCO subscriber")
                    //new start
                    //console.log("passed postco final " + JSON.stringify(this.postCO));

/*                     if(this.appCurrentStateService.isNativeLoadingSupported)
                    {
                      this.postCO.sclPostArticlesCO.description = this.postCO.sclPostArticlesCO.description.replace(new RegExp(' src="http', 'g'), ' class="lazyload" data-src="http');
                      console.log("this.sclPostCO.sclPostArticlesCO.description ", JSON.stringify(this.postCO.sclPostArticlesCO.description));
                    }  */

                    this.textbackground=this.postCO.userCO.firstName.charAt(0).toUpperCase()
                    if (this.appCurrentState.isUserLoggedIn) {
                        let userPostLikeApiCall: ApiCall = new ApiCall(ApiActions.userPostLike);
                        userPostLikeApiCall.addRequestParams('postId', this.postCO.id);

                        this.blogModuleService.userPostLike(userPostLikeApiCall).subscribe(response => {
                            this.postLikeStatus = response;
                        });
                    }
                    //end

                    this.replyCommentCount = parseInt(this.postCO.totalComments) + parseInt(this.postCO.totalReplies);
                    //alert("yo")

                    //you might also like
                    let getYouMightAlsoLikePostApiCall: ApiCall = new ApiCall(ApiActions.getYouMightAlsoLikePost);
                    getYouMightAlsoLikePostApiCall.addRequestParams('categoryId', this.postCO.defaultCategoryId);

                    this.blogModuleService.getYouMightAlsoLikePost(getYouMightAlsoLikePostApiCall).subscribe(response => {
                        this.sclPostCOList = response
                    });

                    //short code work, ad slider
                    this.postCO.sclPostArticlesCO.description = this.postCO.sclPostArticlesCO.description.replace('[ugc_net_pkg_tbl]', UgcNetAchieverCourse.getUgcNetAchieverCourse2018_19());
                    this.postCO.sclPostArticlesCO.description = this.postCO.sclPostArticlesCO.description.replace('[iit_jam_pkg_tbl]', IitJamAchieverCourse2017.getIitJamAchieverCourse());
                  


                    if (this.postCO.sclPostArticlesCO.description.indexOf('eduncle-ad-slider') !== -1) {
                        this.isContentHasSlider = true;
                        let innerHtmlContent = this.postCO.sclPostArticlesCO.description.split('eduncle-ad-slider')
                        this.innerHtmlContent1 = innerHtmlContent[0];
                        this.innerHtmlContent2 = innerHtmlContent[1];
                        this.catCategoryCO = new CatCategoriesCO();
                        this.catCategoryCO.id = this.postCO.defaultCategoryId;
                    }
                    else {
                        //alert("ffff2")
                    }

                    //get initial comment list
                    this.getInitialCommentList();
                }
            });

        this.bootstrapTabsActive();
        this.scheduleJumpTag(this.jumpTagString);
    }

    getInitialCommentList() {
        let getPostInitialCommentListApiCall: ApiCall = new ApiCall(ApiActions.getPostInitialCommentList);
        getPostInitialCommentListApiCall.addRequestParams('postId', this.postCO.id);

        this.blogModuleService.getPostInitialCommentList(getPostInitialCommentListApiCall).subscribe(responseValues => {
            this.commentIdList = responseValues['sclPostCommentIdList'];
            this.commentCOArray = <SclPostCommentCO[]>responseValues['sclPostCommentCOList'];
            this.chunkSize = responseValues['chunkSize'];
            //this.commentIdList = this.commentIdList;
            this.commentCOArray = deserialize<SclPostCommentCO[]>(SclPostCommentCO, this.commentCOArray);
            this.count = 0;
            for (let comment of this.commentCOArray) {
                comment.postCommentStatus = this.postCO.sclPostArticlesCO.commentStatus;
                this.count = this.count + parseInt(comment.totalReply);
            }

            this.count = this.count + this.commentCOArray.length;
            if (this.commentCOArray.length === this.commentIdList.length) {
                this.isAllCommentsLoaded = false;
            }
        });
    }

    // REMOVE ACTIVE CLASS INITIALLY FROM CKEDITOR TABS
    bootstrapTabsActive() {
        if (this.appCurrentStateService.isBrowser()) {
            $(document).ready(function () {
                $('.bootstrap-tabs .nav-item a:first').addClass('active');
                $("#eduncleAds").carousel();
            });
        }
    }


    likeReply(replyCO) {
        replyCO.hasUserLiked = true
        let count = parseInt(replyCO.totalLikes);
        count = count + 1;
        replyCO.totalLikes = count.toString();
        let likeReplyApiCall: ApiCall = new ApiCall(ApiActions.likeReply);
        likeReplyApiCall.addRequestParams('replyId', replyCO.id);

        this.blogModuleService.likeReply(likeReplyApiCall).subscribe(response => {
            //Do nothing
        });
    }

    unlikeReply(replyCO) {
        replyCO.hasUserLiked = false
        let count = parseInt(replyCO.totalLikes);
        count = count - 1;
        replyCO.totalLikes = count.toString();
        let unlikeReplyApiCall: ApiCall = new ApiCall(ApiActions.unlikeReply);
        unlikeReplyApiCall.addRequestParams('replyId', replyCO.id);

        this.blogModuleService.unlikeReply(unlikeReplyApiCall).subscribe(response => {
            //Do nothing
        });
    }

    loadMoreComments() {
        if (this.commentCOArray.length <= this.commentIdList.length) {
            let size = this.commentIdList.length - this.commentCOArray.length;
            if (size >= parseInt(this.chunkSize)) {
                size = parseInt(this.chunkSize);
            }
            else {
                this.isAllCommentsLoaded = false;
            }

            let tempList = new Array();

            let msize = size + this.commentCOArray.length;
            for (let i = this.commentCOArray.length; i < msize; i++) {
                tempList.push(this.commentIdList[i]);
            }
            let getCommentDetailListApiCall: ApiCall = new ApiCall(ApiActions.getCommentDetailList);
            getCommentDetailListApiCall.addRequestParams('commentIdList', tempList);

            this.blogModuleService.getCommentDetailList(getCommentDetailListApiCall).subscribe(response => {
                this.commentCOArray = this.commentCOArray.concat(response);
                this.count = 0;
                for (let comment of this.commentCOArray) {
                    this.count = this.count + parseInt(comment.totalReply);
                }
                this.count = this.count + this.commentCOArray.length;
            });
        }
    }

    loadMoreReplies(commentCO: SclPostCommentCO) {
        let getCommentReplyIdListApiCall: ApiCall = new ApiCall(ApiActions.getCommentReplyIdList);
        getCommentReplyIdListApiCall.addRequestParams('commentId', commentCO.id);

        this.blogModuleService.getCommentReplyIdList(getCommentReplyIdListApiCall).subscribe(responseValues => {
            this.replyIdList = responseValues['sclCommentReplyIdList'];
            commentCO.sclCommentReplyCOList = <SclCommentReplyCO[]>responseValues['sclCommentReplyCOList'];
            commentCO.sclCommentReplyCOList = deserialize<SclCommentReplyCO[]>(SclCommentReplyCO, commentCO.sclCommentReplyCOList);
        });
    }

    onCommentDeletion(commentId) {
        this.commentIdList.splice(this.commentIdList.indexOf(commentId), 1);;
        for (let i = 0; i < this.commentIdList.length; i++) {
            if (this.commentCOArray[i].id === commentId) {
                this.commentCOArray.splice(this.commentCOArray.indexOf(this.commentCOArray[i]), 1);
            }
        }
        this.decreaseCommentCountByOne()
    }

    decreaseCommentCountByOne() {
        let count = parseInt(this.postCO.totalComments);
        count = count - 1;
        this.postCO.totalComments = count.toString();
    }

    eduncleSlider(index: string, redirectUrl: string, imageName: string): string {
        if (index == '0') {
            return '<div class="carousel-item active"><a href="' + redirectUrl + '">' + '<img alt="" class="mx-auto d-block" src="' + imageName + '" alt="Random slide"/>' + '</a></div>';
        }
        else {
            return '<div class="carousel-item"><a href="' + redirectUrl + '">' + '<img alt="" class="mx-auto d-block" src="' + imageName + '" alt="Random slide"/>' + '</a></div>';
        }
    }
    circleList(index: string): string {
        if (index == '0') {
            return '<li data-target="#eduncleAds" data-slide-to="' + index + '" class="active"">';
        }
        else {
            return '<li data-target="#eduncleAds" data-slide-to="' + index + '">';
        }
    }

    changePosition() {
        if (this.appCurrentStateService.isBrowser()) {
            window.scrollTo(0, 0);
        }
    }

    changeImageSizes(featuredImage: string): string {
        /*  featuredImage = featuredImage.replace('.png', '-61X61.png');
         featuredImage = featuredImage.replace('.jpg', '-61X61.jpg'); */
         
        return featuredImage;
    }

    likePost() {
        //this.postCO.hasUserLiked = true;
        this.postLikeStatus = true;
        let count = parseInt(this.postCO.totalLikes);
        count = count + 1;
        this.postCO.totalLikes = count.toString();
        let likePostApiCall: ApiCall = new ApiCall(ApiActions.likePost);
        likePostApiCall.addRequestParams('postId', this.postCO.id);

        this.blogModuleService.likePost(likePostApiCall).subscribe(responseValues => {
            //Do nothing
        });
    }

    unlikePost() {
        //this.postCO.hasUserLiked = false;
        this.postLikeStatus = false;
        let count = parseInt(this.postCO.totalLikes);
        count = count - 1;
        this.postCO.totalLikes = count.toString();
        let unlikePostApiCall: ApiCall = new ApiCall(ApiActions.unlikePost);
        unlikePostApiCall.addRequestParams('postId', this.postCO.id);
        this.blogModuleService.unlikePost(unlikePostApiCall).subscribe(response => {
            //Do nothing
        });
    }

    onDeletePopupYesClick() {
        this.commentId = localStorage.getItem("commentId");
        this.replyId = localStorage.getItem("replyId");

        localStorage.removeItem("replyId");
        localStorage.removeItem("commentId");

        if (this.commentId != null) {
            this.deletePostComment();
        }
        else if (this.replyId != null) {
            this.deleteReply();
        }
        // this.deletePostComment();
    }

    deleteReply() {
        let sclPostReplyCO = new SclCommentReplyCO();
        let deleteReplyApiCall: ApiCall = new ApiCall(ApiActions.deleteReply);
        deleteReplyApiCall.addRequestParams('replyId', this.replyId);

        this.blogModuleService.deleteReply(deleteReplyApiCall).subscribe(response => {
            let result = response;
            sclPostReplyCO.id = this.replyId;
            //  this.deleteReplyFromList(sclPostReplyCO);
        });
    }

    // deleteReplyFromList(replyCO:SclCommentReplyCO)
    // {
    //     this.commentCO.sclCommentReplyCOList.splice(this.commentCO.sclCommentReplyCOList.indexOf(replyCO), 1);
    //     for (let i = 0; i < this.replyIdList.length; i++) 
    //     {
    //         this.replyIdList.splice(this.replyIdList.indexOf(replyCO.id), 1);
    //     } 
    //     this.decreaseReplyCountByOne()
    // }

    deletePostComment() {
        let deleteCommentApiCall: ApiCall = new ApiCall(ApiActions.deleteComment);
        deleteCommentApiCall.addRequestParams('commentId', this.commentId);

        this.blogModuleService.deleteComment(deleteCommentApiCall).subscribe(response => {
            let result = response;
            this.onCommentDeletion(this.commentId)
            //this.deleteCommentFromList.emit(this.commentCO);
        });
    }

    scheduleJumpTag(id: string) {
        setTimeout(() => {
            if (this.appCurrentStateService.isBrowser())
            {
                let elem: HTMLElement = document.getElementById(id);
               // console.log(elem);

                if(elem)
                {
                    //elem.scrollIntoView()
                    elem.scrollIntoView();
                }
            }

        }, 0);
    }

    ngAfterViewInit(): void {
        if (this.appCurrentStateService.isBrowser())
        {
            try {
                setTimeout(() => {
                    if(document.querySelector('#' + this.fragment))
                    {
                        document.querySelector('#' + this.fragment).scrollIntoView();
                    }                    
                }, 0);
            } catch (e) { }
        }
    }

    public getClassForTextBackGround(type:String): String {
        if(type==='post')
        {
            return "user-profile-commonDesign userProfile-35 " + AppUtility.textBackgroundColor(this.textbackground) + " sm16";;
        }
        else
        {
            return "user-profile-commonDesign userProfile-35 " + AppUtility.textBackgroundColor(this.textBackgroundUserProfile) + " sm16";;
        }
        

    }

public getResponsiveImage(imagePath:string):string {
    return AppUtility.getResponsiveImage(imagePath);
   }

   public getFeaturedImage(postCO:SclPostCO):string
   {
       if(postCO.sclPostArticlesCO.featuredImageThumbnail && postCO.sclPostArticlesCO.featuredImageThumbnail !== "")
       {
           return postCO.sclPostArticlesCO.featuredImageThumbnail;
       }
       else
       {
        return postCO.sclPostArticlesCO.featuredImage;
       }

   }

   increaseFbShareCount()
   {

    if(!this.appCurrentState.isUserLoggedIn)
    {
      this.openLoginPopUp(this.postCO.defaultCategoryId);
    }

   /*  let increaseFbShareCountApiCall: ApiCall = new ApiCall(ApiActions.scl_increaseFbShareCount);
    increaseFbShareCountApiCall.addRequestParams('postId', this.postCO.id);
    increaseFbShareCountApiCall.showLoader = true;

    this.blogModuleService.increaseFbShareCount(increaseFbShareCountApiCall).subscribe(response => {
        let result = response;
        if(this.postCO.fbShareCount)
        {
            let updatedFbCOunt = +(this.postCO.fbShareCount) + 1;
            this.postCO.fbShareCount = updatedFbCOunt.toString();
        }
        else{
            this.postCO.fbShareCount = "1";
        }
       
    }); */
   }

   onFooterCommentCountClick()
   {
    if(!this.appCurrentState.isUserLoggedIn)
    {
      this.openLoginPopUp(this.postCO.defaultCategoryId);
    }
   }

   onLikeUnlikeLabelClick()
   {
       if(this.appCurrentState.isUserLoggedIn)
       {//logged in user
        if(this.postLikeStatus)
        {//is already liked post
            this.unlikePost();
        }
        else
        {
            this.likePost();
        }
       }
       else
       {//non logged in user
        this.openLoginPopUp(this.postCO?.defaultCategoryId);
       }
   }

}