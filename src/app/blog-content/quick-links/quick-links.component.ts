import { CatCategoriesCO } from './../../co/categoryCO';
import { BehaviorSubject} from 'rxjs';
import { CatCategoryQuickLinksCO } from './../../co/catCategoryQuickLinksCO';
import { BlogModuleService } from './../../services/blog-module.service';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-quick-links',
    templateUrl: './quick-links.component.html',
    styleUrls: ['./quick-links.component.css']
})
export class QuickLinksComponent implements OnInit {
    private catCategoriesCOBehaviorSubject = new BehaviorSubject<CatCategoriesCO>(null);

    @Input()
    set catCategoriesCO(value) {
        this.catCategoriesCOBehaviorSubject.next(value);
    };

    get catCategoriesCO() {
        return this.catCategoriesCOBehaviorSubject.getValue();
    }
    public catCategoryQuickLinksCOlist: CatCategoryQuickLinksCO[];
    showQuickLinks = true;
    constructor(private blogModuleService: BlogModuleService) { }

    ngOnInit() {

        this.catCategoriesCOBehaviorSubject
            .subscribe(x => {
                if (this.catCategoriesCO) {
                    let getCategoryQuickLinksListApiCall: ApiCall = new ApiCall(ApiActions.getCategoryQuickLinks);
                    getCategoryQuickLinksListApiCall.addRequestParams('categoryId', this.catCategoriesCO.id);

                    this.blogModuleService.getCategoryQuickLinks(getCategoryQuickLinksListApiCall).subscribe(response => {
                        this.catCategoryQuickLinksCOlist = response;
                    });
                }
            });
    }

    // changePath(redirectToLink)
    // {alert
    //     window.location.href=redirectToLink;
    // }
}
