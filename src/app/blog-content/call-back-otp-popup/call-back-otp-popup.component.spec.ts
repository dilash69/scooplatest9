import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallBackOtpPopupComponent } from './call-back-otp-popup.component';

describe('CallBackOtpPopupComponent', () => {
  let component: CallBackOtpPopupComponent;
  let fixture: ComponentFixture<CallBackOtpPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallBackOtpPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallBackOtpPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
