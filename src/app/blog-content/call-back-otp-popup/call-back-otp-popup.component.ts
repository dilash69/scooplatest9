import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-call-back-otp-popup',
  templateUrl: './call-back-otp-popup.component.html',
  styleUrls: ['./call-back-otp-popup.component.css']
})
export class CallBackOtpPopupComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  closeRequestOtpModal(){
    $('#requestCallBackPopup').modal('toggle');
    $('body').addClass('modal-open modal-open');
  }
}
