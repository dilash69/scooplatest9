import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgbCarouselConfig, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DeferLoadModule } from '@trademe/ng-defer-load';
//import { CarouselModule } from 'ngx-owl-carousel-o';
import { NgxPageScrollModule } from 'ngx-page-scroll';
//import { NgImageSliderModule } from 'ng-image-slider';
//import { NgxSocialButtonModule } from "ngx-social-button";
import { AppShellNoRenderDirective } from '../directives/app-shell-no-render-directive';
import { AppShellRenderDirective } from '../directives/app-shell-render-directive';
import { RenderIfUserLoggedInDirective } from '../directives/render.if.user.logged.in';
import { ExamFilterPipe } from '../pipes/examFilter';
import { FilterPipe } from '../pipes/filter.pipe';
//import { ShareModule } from '@ngx-share/core';
//import { HttpClientModule } from 'node_modules/@angular/common/http';
//import { ShareButtonsOptions } from '@ngx-share/core';
//import { SafeHtmlPipe } from './../directives/safe.html-pipe';
//import { ShareButtonsModule } from '@ngx-share/buttons';
import { SafeHtmlPipe } from '../pipes/safe.html-pipe';
import { TruncatePipe } from '../pipes/truncate';
//import { FilterPipe } from './../directives/filter.pipe';
import { ExternalUrlDirective } from './../directives/external-url-directive';
import { PromotionBannerComponent } from './promotion-banner/promotion-banner.component';
import { ExitPopupComponent } from './exit-popup/exit-popup.component';
import { HeaderLoggedOutComponent } from './header-logged-out/header-logged-out.component';
import { HeaderComponent } from './header/header.component';
import { ChooseExamBlocksComponent } from './choose-exam-blocks/choose-exam-blocks.component';
import { MostPopularOnEduncleComponent } from './most-popular-on-eduncle/most-popular-on-eduncle.component';
import { FooterComponent } from './footer/footer.component';
import { HomePageScoopComponent } from './home-page-scoop/home-page-scoop.component';
import { EntryPopupComponent } from './entry-popup/entry-popup.component';
import { CategoryProfileMobileHeaderComponent } from './category-profile-mobile-header/category-profile-mobile-header.component';
import { JoinExamBlockComponent } from './join-exam-block/join-exam-block.component';
import { QuickLinksComponent } from './quick-links/quick-links.component';
import { RegisterFreeTodayComponent } from './register-free-today/register-free-today.component';
import { SocialTimelineComponent } from './social-timeline/social-timeline.component';
import { EduncleAdvertisementHorizontalComponent } from './eduncle-advertisement-horizontal/eduncle-advertisement-horizontal.component';
import { EduncleAdvertisementVerticalComponent } from './eduncle-advertisement-vertical/eduncle-advertisement-vertical.component';
import { PopularRecentTabsComponent } from './popular-recent-tabs/popular-recent-tabs.component';
import { SingleCategoryComponent } from './single-category/single-category.component';
import { SlugComponent } from './slug/slug.component';
import { BlogRegisterFormComponent } from './blog-register-form/blog-register-form.component';
import { PostViewSingleExceptArticleComponent } from './post-view-single-except-article/post-view-single-except-article.component';
import { SingleSocialTimelineComponent } from './single-social-timeline/single-social-timeline.component';
import { AuthorSummaryComponent } from './author-summary/author-summary.component';
import { MostPopularCommmentComponent } from './most-popular-commment/most-popular-commment.component';
import { ReadMoreComponent } from './read-more/read-more.component';
import { ShowReplyComponent } from './show-reply/show-reply.component';
import { SingleAuthorComponent } from './single-author/single-author.component';
import { PostBlogContentComponent } from './post-blog-content/post-blog-content.component';
import { MobileEmailScreenComponent } from './mobile-email-screen/mobile-email-screen.component';
import { LoginPopupComponent } from './login-popup/login-popup.component';
import { LoginPopupLeftPartComponent } from './login-popup-left-part/login-popup-left-part.component';
import { LoginPopupRightpartComponent } from './login-popup-rightpart/login-popup-rightpart.component';
import { RegistrationPopupComponent } from './registration-popup/registration-popup.component';
import { RegisterComponent } from './register/register.component';
import { InsertReplyComponent } from './insert-reply/insert-reply.component';
import { InsertCommentComponent } from './insert-comment/insert-comment.component';
import { RequestACallBackComponent } from './request-a-call-back/request-a-call-back.component';
import { PollSubmitActionComponent } from './poll-submit-action/poll-submit-action.component';
import { PollOptionDesignComponent } from './poll-option-design/poll-option-design.component';
import { McqOptionDesignComponent } from './mcq-option-design/mcq-option-design.component';
import { McqSubmitActionComponent } from './mcq-submit-action/mcq-submit-action.component';
import { ChangeCategoriesStep1Component } from './change-categories-step1/change-categories-step1.component';
import { ChangeCategoriesStep2Component } from './change-categories-step2/change-categories-step2.component';
import { ChangeCategoriesStep3Component } from './change-categories-step3/change-categories-step3.component';
import { HeaderLoggedInComponent } from './header-logged-in/header-logged-in.component';
import { ChangeCategoriesParentComponent } from './change-categories-parent/change-categories-parent.component';
import { ForgetPasswordPopupComponent } from './forget-password-popup/forget-password-popup.component';
import { TutorSuccessPopupComponent } from './tutor-success-popup/tutor-success-popup.component';
import { PostComponent } from './post/post.component';
import { OtpComponent } from './otp/otp.component';
import { OtpPopupComponent } from './otp-popup/otp-popup.component';
import { ChooseGradePopupComponent } from './choose-grade-popup/choose-grade-popup.component';
import { ChooseGradeFilterPopupComponent } from './choose-grade-filter-popup/choose-grade-filter-popup.component';
import { TutorRegistrationPopupComponent } from './tutor-registration-popup/tutor-registration-popup.component';
import { YouAreStudyingMobileMenuComponent } from './you-are-studying-mobile-menu/you-are-studying-mobile-menu.component';
import { HeaderBkComponent } from './header-bk/header-bk.component';
import { ShowCommentComponent } from './show-comment/show-comment.component';
import { UserQualificationInfoPopupComponent } from './user-qualification-info-popup/user-qualification-info-popup.component';
import { SubjectQuestionSubmitActionComponent } from './subject-question-submit-action/subject-question-submit-action.component';
import { MobileHeaderComponent } from './mobile-header/mobile-header.component';
import { LearnMobileMenuComponent } from './learn-mobile-menu/learn-mobile-menu.component';
import { MobileLoginDropdownComponent } from './mobile-login-dropdown/mobile-login-dropdown.component';
import { SingleTagComponent } from './single-tag/single-tag.component';
import { EduncleAppPopupComponent } from './eduncle-app-popup/eduncle-app-popup.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { DeletePopUpComponent } from './delete-pop-up/delete-pop-up.component';
import { RelatedCategoriesComponent } from './related-categories/related-categories.component';
import { MobileMembersAndQuickLinksComponent } from './mobile-members-and-quick-links/mobile-members-and-quick-links.component';
import { ChangeCategoriesStep3MobileComponent } from './change-categories-step3-mobile/change-categories-step3-mobile.component';
import { AskQuestionPopupComponent } from './ask-question-popup/ask-question-popup.component';
import { AskQuestionSubmittionPopupComponent } from './ask-question-submittion-popup/ask-question-submittion-popup.component';
import { DenyUnfollowComponent } from './deny-unfollow/deny-unfollow.component';
import { ConfirmUnfollowComponent } from './confirm-unfollow/confirm-unfollow.component';
import { MegaMenuComponent } from './mega-menu/mega-menu.component';
import { CallBackOtpPopupComponent } from './call-back-otp-popup/call-back-otp-popup.component';
import { ExamNameTagComponent } from './exam-name-tag/exam-name-tag.component';
import { EduncleAdvertisementHalfSliderComponent } from './eduncle-advertisement-half-slider/eduncle-advertisement-half-slider.component';
import { SingleBlogComponent } from './single-blog/single-blog.component';
import { SearchInBannerComponent } from './search-in-banner/search-in-banner.component';
import { ProtectedBlogPopupComponent } from './protected-blog-popup/protected-blog-popup.component';
import { ImageSliderBlogComponent } from './image-slider-blog/image-slider-blog.component';
import { NguiInViewComponent } from './ngui-in-view/ngui-in-view.component';
import { ImageGridLayoutsComponent } from './image-grid-layouts/image-grid-layouts.component';
import { LoginFirstComponent } from './login-first/login-first.component';
import { LazyLoadImageModule, scrollPreset } from 'ng-lazyload-image';
//import { ImgFallbackDirective } from '../directives/ImgFallbackDirective';




/* star */
/* 













import { SocialShareHorizontalComponent } from './social-share-horizontal/social-share-horizontal.component';
import { SocialShareComponent } from './social-share/social-share.component';





 */
import { NgxSocialButtonModule } from 'ngx-social-button';
import { ImageVideoSliderComponent } from './image-video-slider/image-video-slider.component';
import { DeferLoadDirective } from '../directives/DeferLoadDirective';
import { QuizSubmitActionComponent } from './quiz-submit-action/quiz-submit-action.component';
import { ImgFallbackDirective } from '../directives/ImgFallbackDirective';
/* 
import { KatexModule } from 'ng-katex'; */





/* export function getAuthServiceConfigs() {
  return config;
} */

/* let config = new SocialServiceConfig([
  {
    provider: new GoogleLoginProvider(AppSettings.GOOGLE_APP_ID)
  },
  {
    provider: new FacebookLoginProvider(AppSettings.FACEBOOK_APP_ID)
  }
]); */

/* export function provideConfig() {
    return config;
  } */
 
/* const customOptions: ShareButtonsOptions = {
    include: ['facebook', 'whatsapp'],
    exclude: ['tumblr', 'stumble', 'vk', 'google', 'linkedin'],
    theme: 'circles-dark',   
    gaTracking: true,
    twitterAccount: 'dummyAccountName',
    title: 'THIS IS TITLE',
    description: 'mydescription'
}
 */

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        RouterModule,
        FormsModule,
        BrowserModule,
        //CarouselModule,
        BrowserAnimationsModule,
        //KatexModule,
        
        //ShareModule.forRoot(),
       // Daterangepicker,
       /*  SocialLoginModule.initialize(config), */
       // SocialLoginModule,
        NgxPageScrollModule,
        NgxSocialButtonModule,
        DeferLoadModule,
        LazyLoadImageModule
        //NgImageSliderModule,
       // ShareButtonsModule.forRoot({ options: customOptions })
    ],
    declarations: [
        AppShellNoRenderDirective,
        ExternalUrlDirective,
        AppShellRenderDirective,
        MostPopularOnEduncleComponent,
        QuickLinksComponent,
        RegisterFreeTodayComponent,
        EduncleAdvertisementHorizontalComponent,
        EduncleAdvertisementVerticalComponent,
        PopularRecentTabsComponent,
        SingleCategoryComponent,
        BlogRegisterFormComponent,
        AuthorSummaryComponent,
        ShowReplyComponent,
        SingleAuthorComponent,
        PostBlogContentComponent,
        InsertReplyComponent,
        InsertCommentComponent,
        ChangeCategoriesParentComponent,
        PostComponent,
        OtpComponent,
        HeaderBkComponent,
        ShowCommentComponent,
        MobileHeaderComponent,
        LearnMobileMenuComponent,
        MobileLoginDropdownComponent,
        SingleTagComponent,
        RelatedCategoriesComponent,
        MobileMembersAndQuickLinksComponent,
        MegaMenuComponent,
        ExamNameTagComponent,
        SingleBlogComponent,
        NguiInViewComponent,
        LoginFirstComponent,
        /* star */
     /*    SocialShareComponent,
        SocialShareHorizontalComponent,
         */
        /* star */
        RenderIfUserLoggedInDirective,
        FilterPipe,
        ExamFilterPipe,
        SafeHtmlPipe,
        TruncatePipe,
        PromotionBannerComponent,
        ExitPopupComponent,
        HeaderLoggedOutComponent,
        HeaderComponent,
        ChooseExamBlocksComponent,
        FooterComponent,
        HomePageScoopComponent,
        EntryPopupComponent,
        CategoryProfileMobileHeaderComponent,
        JoinExamBlockComponent,
        SocialTimelineComponent,
        SlugComponent,
        PostViewSingleExceptArticleComponent,
        SingleSocialTimelineComponent,
        MostPopularCommmentComponent,
        ReadMoreComponent,
        MobileEmailScreenComponent,
        LoginPopupComponent,
        LoginPopupLeftPartComponent,
        LoginPopupRightpartComponent,
        RegistrationPopupComponent,
        RegisterComponent,
        RequestACallBackComponent,
        PollSubmitActionComponent,
        PollOptionDesignComponent,
        McqOptionDesignComponent,
        McqSubmitActionComponent,
        ChangeCategoriesStep1Component,
        ChangeCategoriesStep2Component,
        ChangeCategoriesStep3Component,
        HeaderLoggedInComponent,
        ForgetPasswordPopupComponent,
        TutorSuccessPopupComponent,
        OtpPopupComponent,
        ChooseGradePopupComponent,
        ChooseGradeFilterPopupComponent,
        TutorRegistrationPopupComponent,
        YouAreStudyingMobileMenuComponent,
        UserQualificationInfoPopupComponent,
        SubjectQuestionSubmitActionComponent,
        EduncleAppPopupComponent,
        NotFoundComponent,
        DeletePopUpComponent,
        ChangeCategoriesStep3MobileComponent,
        AskQuestionPopupComponent,
        AskQuestionSubmittionPopupComponent,
        DenyUnfollowComponent,
        ConfirmUnfollowComponent,
        CallBackOtpPopupComponent,
        EduncleAdvertisementHalfSliderComponent,
        SearchInBannerComponent,
        ProtectedBlogPopupComponent,
        ImageSliderBlogComponent,
        ImageGridLayoutsComponent,
        ImageVideoSliderComponent,
        DeferLoadDirective,
        QuizSubmitActionComponent,
        ImgFallbackDirective
         
        
    ],
    exports: [
      
       ChooseExamBlocksComponent,
       JoinExamBlockComponent,
       RegisterFreeTodayComponent,
       PopularRecentTabsComponent,
       BlogRegisterFormComponent,
       PostComponent,
       DeferLoadDirective,
	     ImgFallbackDirective
    ],
    providers: [
        NgbCarouselConfig,
       /*  NgbModal,
        /* {
            provide: AuthServiceConfig,
            useFactory: provideConfig
        } */
    ]
})
export class BlogContentModule { }
