import { CmsTagAssociationCO } from './../../co/cmsTagAssociationCO';
import { Meta, Title } from '@angular/platform-browser';
import { BlogModuleService } from './../../services/blog-module.service';
import { BehaviorSubject} from 'rxjs';
import { Component, OnInit, Input } from '@angular/core';
import { CmsTagsCO } from  './../../co/cmsTagCO';

@Component({
  selector: 'app-exam-name-tag',
  templateUrl: './exam-name-tag.component.html',
  styleUrls: ['./exam-name-tag.component.css']
})
export class ExamNameTagComponent implements OnInit {

  private tagDetailCOBehaviorSubject = new BehaviorSubject<CmsTagsCO>(null);

    @Input()
    set tagDetailCO(value) {
        this.tagDetailCOBehaviorSubject.next(value);
    };

    get tagDetailCO() {
        return this.tagDetailCOBehaviorSubject.getValue();
    }

    constructor(private blogModuleService: BlogModuleService) {

  }

  ngOnInit() 
  {
    this.tagDetailCOBehaviorSubject
        .subscribe(x => {
            if (this.tagDetailCO) 
            {
                
            }
        });
    }

}
