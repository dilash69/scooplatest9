import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamNameTagComponent } from './exam-name-tag.component';

describe('ExamNameTagComponent', () => {
  let component: ExamNameTagComponent;
  let fixture: ComponentFixture<ExamNameTagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamNameTagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamNameTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
