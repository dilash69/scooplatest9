import { AppConstants } from './../../common/app.constants';
import { Input } from '@angular/core';
import { BlogModuleService } from './../../services/blog-module.service';
import { ApiCall } from './../../common/api.call';
import { ApiActions } from './../../common/api.actions';
import { CatCategoriesCO } from './../../co/categoryCO';
import { CoursesHeaderCO } from './../../co/coursesHeaderCO';
import { Component, OnInit, ViewChild, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AppSettings } from './../../common/app-settings';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { OpenTutorRegistrationPopupService } from './../../services/open-tutor-registration-popup.service';
import { AppCurrentState } from '../../co/app.current.state';
import { RegisterUserService } from '../../services/register-user.service';
import { Subscription, fromEvent } from 'rxjs';
import  { Renderer2 }  from  '@angular/core';
import { LoadExitService } from '../../services/load-exit.service';
import { ManagePopupService } from '../../services/manage-popup.service';
import { AppPopupCO } from '../../co/app.popup.co';
import { AppUtility } from '../../common/app.utility';
import { ActivatedRoute, Router } from '@angular/router';

declare var $: any;


@Component({
    selector: 'app-header-logged-out',
    templateUrl: './header-logged-out.component.html',
    styleUrls: ['./header-logged-out.component.css']
})
export class HeaderLoggedOutComponent implements OnInit {
    public isCallUsDropdown: boolean = false;
    public isCallUsDropdownMobile: boolean = false;
    showHideQuickLinks: boolean = false;
    showHideToggleIcons: boolean = false;
    toOpen: boolean = false;
    public isShowingModal: boolean;
    public siteUrl: string = AppSettings.MAIN_WEBSITE_HOME;
    public pageUrl: string = "";
    public encodedUrl: string = "";
    public scoopUrl: string = AppSettings.SCOOP_WEBSITE_HOME;
    public showExamMobileMenu: Boolean = false
    modalReference: NgbModalRef;
    public showCategoryHeader: Boolean = false
    public appCurrentState: AppCurrentState = new AppCurrentState();
    public courseHeaderCOList: CoursesHeaderCO[] = new Array();
    //public catCategoriesCOList: CatCategoriesCO[] = new Array();
    public otp: string;
    public validationError: string;
    public popupLoad = false;
    public isOpen;
    public loadOtpComponent = false;
    public appPopupCO: AppPopupCO = new AppPopupCO();

    
    @Input() defaultCategoryId:string;

    @ViewChild('content') public content;

    constructor( private managePopupService : ManagePopupService,
        private  appCurrentStateService:  AppCurrentStateService,
        private  modalService:  NgbModal,
        private  openTutorRegistrationPopupService : OpenTutorRegistrationPopupService,
        private  blogModuleService: BlogModuleService,
        private  renderer:  Renderer2,
        private loadExitService: LoadExitService,
        private router: Router) {
          
        if  (this.appCurrentStateService.isBrowser()) { }

        this.managePopupService.appPopupCO.subscribe(appPopupCO => 
            {
                this.appPopupCO = appPopupCO;
            });

    }

    showUserOtpComponent(modalReference: any) {
    
        this.popupLoad = true;
        this.loadOtpComponent = false; 
        modalReference.close();
    }

    // I open or close the modal window.
    public toggleModal(): void {

        // When we open the modal window, we want to prevent scrolling on the main
        // document. This way, if the user can scroll within the modal window, the
        // scroll will never bleed into the body context.
        if (this.appCurrentStateService.isBrowser()) {
            if (this.isShowingModal = !this.isShowingModal) {
            } else {
            }
        }
    }
    ngOnInit()
     {
        this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
            this.appCurrentState = appCurrentState;

            if (appCurrentState.isUserLoggedIn) {
                AppUtility.log('HeaderComponent:User logged in');
            }
        });
        if (this.appCurrentStateService.isBrowser()) 
        {
            this.openEntryPopup();
            this.pageUrl = window.location.href;
            this.encodedUrl = encodeURIComponent(this.pageUrl);

            if (!this.pageUrl.includes("tag")) 
            {
                this.showCategoryHeader = true;
            }
            else if (!this.pageUrl.includes("author")) {
                this.showCategoryHeader = true;
            }
            
            this.onClickOutside();
        }

        this.openTutorRegistrationPopupService.showHideObservable.subscribe(res => {
            this.toOpen = res;
            if (this.toOpen) {
                // alert(this.toOpen)     
                //this.openTutorRegForm1()
                $("#loginBtn").modal("hide");
                this.openTutorRegForm1(this.content)
            }

        });

        this.loadExitService.showHideObservable.subscribe(res => 
            {
            if (res) 
            { 
                this.loadOtpComponent = true;  
            }
        });


        let getCoursesForHeaderApiCall: ApiCall = new ApiCall(ApiActions.getCoursesForHeader);

        this.blogModuleService.getCoursesForHeader(getCoursesForHeaderApiCall).subscribe(response => {
            this.courseHeaderCOList = response;
        });

        /* let getCategoryListApiCall: ApiCall = new ApiCall(ApiActions.getCategoryList);
        this.blogModuleService.getCategoryList(getCategoryListApiCall).subscribe(response => {
            this.catCategoriesCOList = response;
        }); */


        // if (this.appCurrentStateService.isBrowser()) {
        //     this.headerDropdownMenu();
        // }

        
    }

    // FOR HEADER DROPDOWN MENU
    // headerDropdownMenu() {
    //     $('.navbar-collapse .dropdown i').on('click', function (event) {
    //         event.preventDefault();
    //         event.stopPropagation();
    //         $(this).parent().parent().siblings().removeClass('open');
    //         $(this).parent().parent().toggleClass('open');
    //     });
    // }


    showHideCallUsDropdownMobile() 
    { // Call us dropdown for mobile
        event.stopPropagation();
        if (this.isCallUsDropdownMobile == false) {
            this.isCallUsDropdownMobile = true;
        } else {
            this.isCallUsDropdownMobile = false;
        }
    }
    showHideCallUsDropdown(event) {
        event.stopPropagation();
        if (this.isCallUsDropdown == false) {
            this.isCallUsDropdown = true;
        } else {
            this.isCallUsDropdown = false;
        }
    }
    onClickOutside() { // Hide call us dropdown on click anywhere outside
        if (this.appCurrentStateService.isBrowser()) {
            document.addEventListener("click", e => {
                this.isCallUsDropdown = false;
                this.isCallUsDropdownMobile = false;
            });
        }
    }

    preventClickHere(){
        event.stopPropagation();
    }

    openTutorRegForm(content) {
        this.modalService.open(content, { size: 'lg' });
    }

    openTutorRegForm1(content) {
        this.modalReference = this.modalService.open(content, { centered: true, size: 'lg' });
    }

    showHideMembersAndQuickLinks() {
        if (this.showHideQuickLinks == false) {
            this.showHideQuickLinks = true;
            this.showHideToggleIcons = true;
            if (this.appCurrentStateService.isBrowser()) {

            }
        }
        else {
            this.showHideQuickLinks = false;
            this.showHideToggleIcons = false;
            if (this.appCurrentStateService.isBrowser()) {
            }
        }
    }
    showExamSidebarMenu() 
    {
        if (this.appCurrentStateService.isBrowser()) {
            if (this.showExamMobileMenu  ==  true) {
                this.showExamMobileMenu  = false;
                this.renderer.removeClass(document.body,  'modal-open');
            }
            else {
                this.showExamMobileMenu  =  true;
                this.renderer.addClass(document.body,  'modal-open');
            }
        }
    }

    closeMobileMenu() {
        if (this.appCurrentStateService.isBrowser()) {
            this.renderer.removeClass(document.body, 'modal-open');
            this.showExamMobileMenu = false;
        }
    }

    hideYouAreStudying(event) {
        this.showExamMobileMenu = false;
    }

    openLoginModal()
    {
        let lastChar = this.pageUrl.substr(this.pageUrl.length - 1);
        let count = (this.pageUrl.match(/\//g) || []).length;

        localStorage.setItem(AppConstants.REGISTRATION_PROCESS, AppConstants.SIMPLE_REGISTRATION_PROCESS);

        if(count == 3 && lastChar != '/')
        {
            this.managePopupService.showLoginPopUp(this.defaultCategoryId);
        }
        else
        {
            this.managePopupService.showLoginPopUp(null);
        }
        //alert(count)
        //this.managePopupService.showLoginPopUp();
    } 

    openSignupModal()
    {
        //this.renderer.addClass(document.body, 'modal-open');
        let lastChar = this.pageUrl.substr(this.pageUrl.length - 1);
        let count = (this.pageUrl.match(/\//g) || []).length;
        localStorage.setItem(AppConstants.REGISTRATION_PROCESS, AppConstants.SIMPLE_REGISTRATION_PROCESS);

        if(count == 3 && lastChar != '/')
        {
            this.managePopupService.showSignPopUp(this.defaultCategoryId,AppConstants.REGISTRATION);
        }
        else
        {
            this.managePopupService.showSignPopUp(null,AppConstants.REGISTRATION);
        }
        //this.managePopupService.showSignPopUp();
    } 

    showRequestCallBack()
    {
        this.isCallUsDropdown = false;
        this.managePopupService.showrequestACallBackPopUp();
    }

    // openLoginPopup(content1)
    // {
    //     this.modalReference = this.modalService.open(content1, { centered: true });
    // }
    // eduncleEntry(content)
    // {

    //     // this.modalService.open(content, { centered: true });
    //     this.modalService.open(content, { windowClass : 'entryPopupCustomModal', centered: true });
    // }



    // eduncleExit(contentExit)
    //  {
    //     // this.modalService.open(content, { centered: true });
    //     this.modalService.open(contentExit, { windowClass : 'sm', centered: true });
    // }
    eduncleAppPopup(content) {
        this.modalService.open(content, { centered: true , windowClass: 'searchBarPopupWidth'});
      }

    openEntryPopup()
    {

        
         let timeoutTime:number = 10000;
        setTimeout(() => 
        {
            if (this.appCurrentState.isUserLoggedIn) 
            {
               // console.log('user is logged in so not showing the entry popup');                
            }
            else
            {
                if(!this.appPopupCO.isAnyPopupOpen())
                {
                    this.managePopupService.showEntryPopUp();
                }
                else
                {
                    timeoutTime = 3000; 
                    this.openEntryPopup();
                } 
            }                  
        }, timeoutTime); 
    }


    openPopUp() {
        if (!this.appPopupCO.isAnyPopupOpen()) {

            if (this.router.url.toLowerCase().includes("csir-net")) {//csir net
                this.openLoginPopUp("4");
            }
            else if (this.router.url.toLowerCase().includes("iit-jam")) {//iit jam
                this.openLoginPopUp("3");
            }
            else if (this.router.url.toLowerCase().includes("ugc-net") || this.router.url === "/") 
            {// ugc net
                this.openLoginPopUp("4");
            }
            else{
                this.openLoginPopUp(null);
            }
            //this.managePopupService.showAndroidAppPopUp();
        }
    }

    openLoginPopUp(categoryId) {

        localStorage.setItem(AppConstants.REGISTRATION_PROCESS, AppConstants.BLOG_ACTIVITY_REGISTRATION_PROCESS);
        this.managePopupService.showSignPopUp(categoryId,AppConstants.APP_PROMOTION);
      }
}
