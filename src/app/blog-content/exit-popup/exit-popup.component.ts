import { Component, OnInit, Output, EventEmitter, ViewChild, Input } from '@angular/core';
import { RegisterUserService } from '../../services/register-user.service';
import { OpenTutorRegistrationPopupService } from '../../services/open-tutor-registration-popup.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { BlogModuleService } from '../../services/blog-module.service';
import { ApiCall } from '../../common/api.call';
import { ApiActions } from '../../common/api.actions';
import { UserCO } from '../../co/userCO';
// SocialUser, AuthService, FacebookLoginProvider, GoogleLoginProvider } from 'angular4-social-login';
import { AppCurrentStateService } from '../../services/app.current.state.service';
import { AppConstants } from '../../common/app.constants';
import { deserialize } from 'serializer.ts/Serializer';
import { ManagePopupService } from '../../services/manage-popup.service';
import { AppSettings } from '../../common/app-settings';
import { CatObjectCategoryRelationCO } from '../../co/catObjectCategoryRelationCO';
import { CatCategoriesCO } from '../../co/categoryCO';
import { CatCategoryFiltersCO } from '../../co/catCategoryFiltersCO';
import { UserRegistrationDetailCO } from './../../co/userRegistrationDetailCO';
import { UserCategoryRelationCO } from './../../co/userCategoryRelationCO';
import { UserBrokenLeadsCO } from './../../co/userBrokenLeadsCO';
import { BehaviorSubject } from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import { FilterValueCO } from '../../co/filterValueCO';

@Component({
  selector: 'app-exit-popup',
  templateUrl: './exit-popup.component.html',
  styleUrls: ['./exit-popup.component.css']
})
export class ExitPopupComponent implements OnInit 
{
  //public modalReference: any;
  public siteUrl: string;
  modalReference: NgbModalRef;
  public otp1:string;
  public validationError:string;
  public phoneNumber;
 // private user: SocialUser;
  private loggedIn: boolean;
  public userCO:UserCO;
  
  @Output() loadOtpComponent = new EventEmitter();
  @Output() loadExitComponent = new EventEmitter();
  public loadOtpExit : boolean= false;

  public isExitOpen : string;
  public isOtherPopupOpen : string;
  userCategoryRelationCO:UserCategoryRelationCO = new UserCategoryRelationCO();
  /* catObjectCategoryRelationCO:CatObjectCategoryRelationCO = new CatObjectCategoryRelationCO(); */
  catCategoriesCOArray:CatCategoriesCO[] = new Array();
  selectedCategoryTitle :string= AppConstants.Category;
  catCategoryFiltersCOList:CatCategoryFiltersCO[];
  isCategorySelected:boolean = true;
  isMandatoryTermsChecked:boolean= true;
  private saveBrokenLeadBS : BehaviorSubject<any> = new BehaviorSubject<string>(null);
  firstFilterName:String;
  firstFilterValue:String;
  isWhatappNotificationChecked:boolean = true;

  sortedArray1 = new Array();
  sortedArray2 = new Array();
  isUgcNetSelected:boolean = false;

  @ViewChild('contentExit') private content;
  constructor(
    private appCurrentStateService:AppCurrentStateService,
   // private authService: AuthService,
    private managePopupService:ManagePopupService,
    private blogModuleService : BlogModuleService,
    private registerUserService :RegisterUserService,
    private openTutorRegistrationPopupService : OpenTutorRegistrationPopupService) { }
    public showValidationError:boolean=false;

  ngOnInit()
   { 

    this.saveBrokenLeadBS.pipe(debounceTime(AppConstants.autoBrokenLeadApiHitTime))
    .subscribe(term => 
        { 
        if(term != null)
        {
          let saveUserBrokenLeadsApiCall: ApiCall = new ApiCall(ApiActions.saveUserBrokenLeads);
          saveUserBrokenLeadsApiCall.showLoader = false;
          saveUserBrokenLeadsApiCall.addRequestParams('userBrokenLeadsCO', UserBrokenLeadsCO.getAutoBrokenLeadCO(term.target.value,"AUTO_EXIT_POP_UP"));
    
          this.blogModuleService.saveUserBrokenLeads(saveUserBrokenLeadsApiCall).subscribe(userResponseData => {
            
          });
        }
   });
      
      this.siteUrl = AppSettings.MAIN_WEBSITE_HOME;

      let getCategoryListApiCall: ApiCall = new ApiCall(ApiActions.getCategoryList);
      getCategoryListApiCall.showLoader = true;
      this.blogModuleService.getCategoryList(getCategoryListApiCall)
        .subscribe(response =>
        {
          this.catCategoriesCOArray = response;
          this.catCategoriesCOArray = deserialize<CatCategoriesCO[]>(CatCategoriesCO, this.catCategoriesCOArray);

          if(localStorage.getItem("currentCategoryId"))
      {
        for(let i = 0;i<this.catCategoriesCOArray.length;i++)
        {
          if(this.catCategoriesCOArray[i].id === localStorage.getItem("currentCategoryId"))
          {
            this.selectedCategoryTitle = this.catCategoriesCOArray[i].title;
            this.onCategorySelect(this.catCategoriesCOArray[i]);
            break;
          }
        }
      }
        });
  }



  dismiss()
  {
    this.managePopupService.closePopUp();
  }

  registerUser(userRegestrationJsonObj: JSON) {
    if (this.checkIsCategorySelected()) 
    {
      if (this.checkIfAllFiltersAreSelected()) 
      {
        this.showValidationError = false;
        let name = userRegestrationJsonObj['name'];
        let phone = userRegestrationJsonObj['phone'];
        this.phoneNumber = phone;
        let userRegistrationFrontEndApiCall: ApiCall = new ApiCall(ApiActions.userRegistrationFrontEnd);
        userRegistrationFrontEndApiCall.showLoader = true;
        userRegistrationFrontEndApiCall.addRequestParams('name', name);
        userRegistrationFrontEndApiCall.addRequestParams('mobileNumber', phone);
        userRegistrationFrontEndApiCall.addRequestParams('leadFromId', "15");
        userRegistrationFrontEndApiCall.addRequestParams('catObjectCategoryRelationCO', this.userCategoryRelationCO);

        this.blogModuleService.userRegistrationFrontEnd(userRegistrationFrontEndApiCall).subscribe(responseValues => {
          if (responseValues['otp']) 
          {
            localStorage.setItem(AppConstants.REGISTRATION_PROCESS, AppConstants.EXIT_POPUP_REGISTRATION_PROCESS);
            this.otp1 = responseValues['otp'];
            this.registerUserService.updatedUser(this.otp1, phone, null,false,null);
            // this.loadOtpComponent.emit(this.modalReference);
            // localStorage.setItem('isExitOpen',"true");
            this.loadOtpExit = true;
            //this.modalReference.close();

          }
          else {
            this.validationError = responseValues;
            this.showValidationError = true;
          }
        });
      }
    }
  }

  generateOtp(userJsonObj: JSON)
  {
    if (this.checkIsCategorySelected()) 
    {
      if (this.checkIfAllFiltersAreSelected() && this.checkTermsAccepted()) 
      {
        this.showValidationError = false;

        let generateOtpApiCall: ApiCall = new ApiCall(ApiActions.generateOtp);
        generateOtpApiCall.showLoader = true;
        generateOtpApiCall.addRequestParams('mobileNumber', userJsonObj['phone']);

        this.blogModuleService.generateOtp(generateOtpApiCall).subscribe(responseValues => 
          {
          if (responseValues.userResponseCode === AppConstants.SUCCESS_REQUEST) 
          {
            localStorage.setItem(AppConstants.REGISTRATION_PROCESS, AppConstants.EXIT_POPUP_REGISTRATION_PROCESS);
            let userRegistrationDetailCO:UserRegistrationDetailCO = new UserRegistrationDetailCO();
            userRegistrationDetailCO.isTutorRegistration = "0";
            userRegistrationDetailCO.leadFromId = "15";
            userRegistrationDetailCO.name = userJsonObj['name'];
            userRegistrationDetailCO.mobileNumber =userJsonObj['phone'];
            userRegistrationDetailCO.userCategoryRelationCO = this.userCategoryRelationCO;
            
            this.loadOtpExit = true;
            this.registerUserService.updatedUser1(userRegistrationDetailCO);
          }
          else 
          {
            this.showValidationError = true;
            this.validationError = responseValues.userMessageList[0];
          }
        });
      }
    }
  }

  eduncleExit(content)
    {
/*       if(!this.managePopupService.isAnyPopupOpen())
      {
        this.modalReference=  this.managePopupService.showPopUp(content, 'sm');
      } */
        
    }

  numberOnly(event): boolean 
  {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  onExitLoad(){
    setTimeout(() => {

        this.eduncleExit(this.content);
    }, 100);
  }

 
  openLoginPopupExit()
  {
    this.managePopupService.closePopUp();
    this.managePopupService.showLoginPopUp(null);
  }

  onCategorySelect(catCategoriesCO:CatCategoriesCO)
  {
    this.assignCategory(catCategoriesCO.title,catCategoriesCO.id);
    let getCategoryFiltersForAdminApiCall: ApiCall = new ApiCall(ApiActions.getCategoryFiltersForAdmin);
    getCategoryFiltersForAdminApiCall.addRequestParams('categoryId', catCategoriesCO.id);
    this.blogModuleService.getCategoryFiltersForAdmin(getCategoryFiltersForAdminApiCall).subscribe(response => {
      this.catCategoryFiltersCOList = response;
      //this.findIndexOfSubjectFilter();
      for (let i = 0; i < 1; i++) 
      {
        this.catCategoryFiltersCOList[i].filterTypeTitle = this.catCategoryFiltersCOList[i].filterType;
      }
      this.getIndependentFilterValues(); 
    });
  }

  assignCategory(categoryTitle:string,categoryId:string)
  {
    this.selectedCategoryTitle = categoryTitle;
    this.userCategoryRelationCO.categoryId = categoryId;
    this.isCategorySelected =true;
  }

  getIndependentFilterValues() 
  {
    for (let i = 0; i < 1; i++) 
    {
      if (this.catCategoryFiltersCOList[i].dependentFilterType === "" || !this.catCategoryFiltersCOList[i].dependentFilterType) 
      {
          let getIndependentFilterValues: ApiCall = new ApiCall(ApiActions.getIndependentFilterValues);
          getIndependentFilterValues.addRequestParams("categoryId", this.catCategoryFiltersCOList[i].categoryId);
          getIndependentFilterValues.addRequestParams("filterType", this.catCategoryFiltersCOList[i].filterType);

          this.blogModuleService.getIndependentFilterValues(getIndependentFilterValues).subscribe(response => {
            this.catCategoryFiltersCOList[i].dropDownFilterValueCOList = response;
            this.isUgcNetSelected = false;
            if(this.catCategoryFiltersCOList[i].categoryId=='2')
            {
              this.catCategoryFiltersCOList[i].dropDownFilterValueCOList =this.filterListForUGCNET(response)
            }
          });
      }
    }
  }

  assignFiltersId(filterType:String,filterValueId,filterValueCO)
  {
    this.firstFilterName = filterType;
    this.firstFilterValue = filterValueCO.filterTypeTitle;
    this.catCategoryFiltersCOList[0].filterTypeTitle = filterValueCO.filterTypeTitle;
    if(filterType===AppConstants.EXAM)
    {
      this.userCategoryRelationCO.examId = filterValueId
    }
    else if(filterType===AppConstants.STREAM)
    {
      this.userCategoryRelationCO.streamId = filterValueId
    }
    else if(filterType===AppConstants.STAGE)
    {
      this.userCategoryRelationCO.stageId = filterValueId
    }
    else if(filterType===AppConstants.SUBJECT)
    {
      this.userCategoryRelationCO.subjectId = filterValueId
    }
    else if(filterType===AppConstants.TOPIC)
    {
      this.userCategoryRelationCO.topicId = filterValueId
    }
    else if(filterType===AppConstants.YEAR)
    {
      this.userCategoryRelationCO.yearId = filterValueId
    }
    else if(filterType===AppConstants.BOARD)
    {
      this.userCategoryRelationCO.boardId = filterValueId
    }
    else if(filterType===AppConstants.MEDIUM_LANGUAGE)
    {
      this.userCategoryRelationCO.mediumLanguageId = filterValueId
    }
    else if(filterType===AppConstants.ZONE)
    {
      this.userCategoryRelationCO.zoneId = filterValueId
    }
  }

  checkIsCategorySelected(): boolean 
  {
    let isValid: boolean = true;
    if (this.userCategoryRelationCO) 
    {
      if(!this.userCategoryRelationCO.categoryId)
      {
        isValid = false;
        this.isCategorySelected = false;
      }
    }
    return isValid;
  }

  checkIfAllFiltersAreSelected(): boolean
  {
    let isValid: boolean = true;
     for (let i = 0; i < 1; i++) 
      {
        isValid = this.checkForFilterValidation(this.catCategoryFiltersCOList[i].filterType,this.catCategoryFiltersCOList[i]);
      }
      return isValid;
  }

  checkForFilterValidation(filterType:string,catCategoryFiltersCO:CatCategoryFiltersCO)
  {
    let isValid:boolean = true;
    if(filterType === AppConstants.EXAM)
    {
      if(!this.userCategoryRelationCO.examId)
      {
        catCategoryFiltersCO.validationMessage = catCategoryFiltersCO.filterType +" " + "is required";
        return false;
      }
    }
    else if(filterType===AppConstants.STREAM)
    {
      if(!this.userCategoryRelationCO.streamId)
      {
        catCategoryFiltersCO.validationMessage = catCategoryFiltersCO.filterType +" " + "is required";
        return false;
      }
    }
    else if(filterType===AppConstants.STAGE)
    {
      if(!this.userCategoryRelationCO.stageId)
      {
        catCategoryFiltersCO.validationMessage = catCategoryFiltersCO.filterType +" " + "is required";
        return false;
      }
    }
    else if(filterType===AppConstants.SUBJECT)
    {
      if(!this.userCategoryRelationCO.subjectId)
      {
        catCategoryFiltersCO.validationMessage = catCategoryFiltersCO.filterType +" " + "is required";
        return false;
      }
    }
    return isValid;
  }

  checkTermsAccepted()
  {
    if(!this.isMandatoryTermsChecked)
    {
      alert("Please accept Terms and Conditions")
      return false;
    }
    return true;
  }

  checkUncheckTermsCheckBox()
  {
    this.isMandatoryTermsChecked = !this.isMandatoryTermsChecked;
  }

  checkUncheckWhatsAppNotification()
  {
    this.isWhatappNotificationChecked = !this.isWhatappNotificationChecked;
  }

  userRegistrationNew(userRegestrationJsonObj: JSON) 
  {
    if (this.checkIsCategorySelected()) 
    {
      if (this.checkIfAllFiltersAreSelected() && this.checkTermsAccepted()) 
      {
        this.showValidationError = false;
        let name = userRegestrationJsonObj['name'];
        let phone = userRegestrationJsonObj['phone'];
        this.phoneNumber = phone;
        let userRegistrationNewApiCall: ApiCall = new ApiCall(ApiActions.userRegistrationNew);
        userRegistrationNewApiCall.showLoader = true;
        userRegistrationNewApiCall.addRequestParams('name', name);
        userRegistrationNewApiCall.addRequestParams('mobileNumber', phone);
        userRegistrationNewApiCall.addRequestParams('leadFromId', "15");
        userRegistrationNewApiCall.addRequestParams('userCategoryRelationCO', this.userCategoryRelationCO);

        if(this.isWhatappNotificationChecked)
        {
          userRegistrationNewApiCall.addRequestParams('whatsappNotification', "1");
        }
        else
        {
          userRegistrationNewApiCall.addRequestParams('whatsappNotification', "0");
        }

        this.blogModuleService.userRegistrationNew(userRegistrationNewApiCall).subscribe(userResponseData => {
          if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
          {
            localStorage.setItem(AppConstants.REGISTRATION_PROCESS, AppConstants.EXIT_POPUP_REGISTRATION_PROCESS);
            let userRegistrationDetailCO:UserRegistrationDetailCO = new UserRegistrationDetailCO();
            userRegistrationDetailCO.isTutorRegistration = "0";
            userRegistrationDetailCO.mobileNumber =userRegestrationJsonObj['phone'];
            userRegistrationDetailCO.categoryName = this.selectedCategoryTitle;
            userRegistrationDetailCO.firstFilterName = this.firstFilterName;
            userRegistrationDetailCO.firstFilterValue = this.firstFilterValue;
            userRegistrationDetailCO.name = name;
            this.loadOtpExit = true;
            this.registerUserService.updatedUser1(userRegistrationDetailCO);
          } 
          else 
          {
            this.showValidationError = true;
            this.validationError = userResponseData.userMessageList[0];
          }
        });
      }
    }
  }

  saveBrokenLeads(event)
  {
    if(event.target.value.length == 10)
    {
      this.saveBrokenLeadBS.next(event);
    }
  }

  filterListForUGCNET(filterValueCOList: FilterValueCO[]):FilterValueCO[]
  {

    this.isUgcNetSelected=true;
    
    let filterValueCOList1: FilterValueCO[];
    let filterValueCOList2: FilterValueCO[];

    filterValueCOList1=filterValueCOList.filter(t=>t.isOther==='0')
    filterValueCOList2=filterValueCOList.filter(t=>t.isOther==='1')

    this.sortedArray1 = filterValueCOList1.sort((a, b) => a.sequence - b.sequence);
    this.sortedArray2=filterValueCOList2.sort((a, b) => (a.filterTypeTitle > b.filterTypeTitle) ? 1 : (a.filterTypeTitle < b.filterTypeTitle) ? -1:0)
    return this.sortedArray1.concat(this.sortedArray2);
  
  }

  
}
