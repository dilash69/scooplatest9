import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AskQuestionSubmittionPopupComponent } from './ask-question-submittion-popup.component';

describe('AskQuestionSubmittionPopupComponent', () => {
  let component: AskQuestionSubmittionPopupComponent;
  let fixture: ComponentFixture<AskQuestionSubmittionPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AskQuestionSubmittionPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AskQuestionSubmittionPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
