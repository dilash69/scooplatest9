import { ManagePopupService } from './../../services/manage-popup.service';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-ask-question-submittion-popup',
  templateUrl: './ask-question-submittion-popup.component.html',
  styleUrls: ['./ask-question-submittion-popup.component.css']
})
export class AskQuestionSubmittionPopupComponent implements OnInit {

  constructor(private managePopupService:ManagePopupService,public modalService: NgbModal) { }

  ngOnInit() {
  }

  dismiss() {
    this.managePopupService.closePopUp();
  } 

  openQuestionSubmittion(questionSubmitton){
    this.modalService.open(questionSubmitton, { centered: true ,size: 'lg' });
  }

}
