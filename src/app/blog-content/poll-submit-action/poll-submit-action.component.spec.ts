import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollSubmitActionComponent } from './poll-submit-action.component';

describe('PollSubmitActionComponent', () => {
  let component: PollSubmitActionComponent;
  let fixture: ComponentFixture<PollSubmitActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollSubmitActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollSubmitActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
