import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseExamBlocksComponent } from './choose-exam-blocks.component';

describe('ChooseExamBlocksComponent', () => {
  let component: ChooseExamBlocksComponent;
  let fixture: ComponentFixture<ChooseExamBlocksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseExamBlocksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseExamBlocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
