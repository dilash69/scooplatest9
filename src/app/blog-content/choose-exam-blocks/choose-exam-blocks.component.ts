import { FilterPipe } from './../../pipes/filter.pipe';
import { ListCategoriesService } from './../../list-categories.service';
import { FilterService } from './../../services/filter.service';
import { DataSharingService } from './../../services/data-sharing.service';
import { ApiActions } from './../../common/api.actions';
import { BlogModuleService } from './../../services/blog-module.service';
import { CatCategoriesCO } from './../../co/categoryCO';
import { AppConstants } from './../../common/app.constants';
import { ApiCall } from './../../common/api.call';
import { Component, OnInit } from '@angular/core';
import { AppUtility } from './../../common/app.utility';
import { deserialize } from 'serializer.ts/Serializer';



@Component({
  selector: 'app-choose-exam-blocks',
  templateUrl: './choose-exam-blocks.component.html',
  styleUrls: ['./choose-exam-blocks.component.css']
})
export class ChooseExamBlocksComponent implements OnInit 
{

 catCategoriesCOArray:CatCategoriesCO[] = new Array();
 searchText:any;
 defaultImageSquare:String = AppConstants.DEFAULT_IMAGE_SQUARE;
 defaultImageCircle:String = AppConstants.DEFAULT_IMAGE_CIRCLE;
 //filterPipe = new FilterPipe();
 
public mostPopularOnEduncleArticleId: string[]; 
  
  constructor(private filterService: FilterService,private blogModuleService: BlogModuleService,private dataSharingService:DataSharingService,private listCategoriesService:ListCategoriesService) { 

  }

  ngOnInit() 
  {
    this.filterService.searchText.subscribe(searchText=> {
      this.searchText=searchText;
      }); 

    this.filterService.catCategoriesCOList.subscribe(catCategoriesCOList=> {
      this.catCategoriesCOArray=catCategoriesCOList;
       }); 

    let getCategoryListApiCall: ApiCall = new ApiCall(ApiActions.getCategoryList);
    getCategoryListApiCall.showLoader = true;
    this.blogModuleService.getCategoryList(getCategoryListApiCall)
      .subscribe(response =>
      {
        this.catCategoriesCOArray = response;
        this.catCategoriesCOArray = deserialize<CatCategoriesCO[]>(CatCategoriesCO, this.catCategoriesCOArray);
        this.listCategoriesService.updatedCategories(this.catCategoriesCOArray);
      });
  }
	public getResponsiveImage(imagePath:string):string {
    return AppUtility.getResponsiveImage(imagePath);
   }
  // public searchFilter(param: any)
  // {
  
  
  //  this.filterArray= this.filterPipe.transform(this.catCategoriesCOArray,param);
  
  // }

  
}
