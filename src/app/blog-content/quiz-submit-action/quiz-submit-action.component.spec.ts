import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizSubmitActionComponent } from './quiz-submit-action.component';

describe('QuizSubmitActionComponent', () => {
  let component: QuizSubmitActionComponent;
  let fixture: ComponentFixture<QuizSubmitActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizSubmitActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizSubmitActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
