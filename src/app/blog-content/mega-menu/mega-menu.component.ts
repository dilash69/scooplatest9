import { Component, OnInit, Input } from '@angular/core';
import { BlogModuleService } from '../../services/blog-module.service';
import { ApiCall } from '../../common/api.call';
import { ApiActions } from '../../common/api.actions';
import { CatCategoriesCO } from '../../co/categoryCO';
import { AppCurrentStateService } from '../../services/app.current.state.service';
import { AppCurrentState } from '../../co/app.current.state';
import { CatExamsCO } from '../../co/catExamsCO';
import { AppUtility } from '../../common/app.utility';

@Component({
  selector: 'app-mega-menu',
  templateUrl: './mega-menu.component.html',
  styleUrls: ['./mega-menu.component.css']
})
export class MegaMenuComponent implements OnInit 
{
  public appCurrentState  : AppCurrentState= new AppCurrentState(); 
  public catCategoriesCO : CatCategoriesCO;
  public catExamsCO :  CatExamsCO ;
  @Input('catCategoriesCOListForFIlterStream') catCategoriesCOList;

  constructor(private blogModuleService : BlogModuleService, private appCurrentStateService : AppCurrentStateService)
   {

    }

  ngOnInit() 
  {

    this.catCategoriesCO = this.catCategoriesCOList[0];
    // this.catExamsCO = this.catCategoriesCO.catExamsCOList[0];

    
    // this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
    //   this.appCurrentState = appCurrentState; 
    //   }); 

    //   if(this.appCurrentState.userCO!=null) 
    //   {
    //     let getMostPopularArticleListApiCall: ApiCall = new ApiCall(ApiActions.getFilterStream);
       
    //     this.blogModuleService.getFilteredStream(getMostPopularArticleListApiCall).subscribe(response => 
    //     {
    //       this.catCategoriesCOList = response;
    //       AppUtility.log(this.catCategoriesCOList);
    //     });
    //   }
  }

  mouseEnterCategory(div : CatCategoriesCO)
  {
    this.catCategoriesCO = div;
    this.catExamsCO = div.catExamsCOList[0];
    // alert('1')
  
    //AppUtility.log("mouse enter : " + div);
 }

 mouseEnterExam(div : CatExamsCO)
  {
    this.catExamsCO = div;
  
    //AppUtility.log("mouse enter : " + div);
 }


 mouseLeaveCategory(div : string){
  
  AppUtility.log('mouse leave :' + div);
 }

}
