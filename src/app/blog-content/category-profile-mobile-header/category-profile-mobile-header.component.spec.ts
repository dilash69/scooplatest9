import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryProfileMobileHeaderComponent } from './category-profile-mobile-header.component';

describe('CategoryProfileMobileHeaderComponent', () => {
  let component: CategoryProfileMobileHeaderComponent;
  let fixture: ComponentFixture<CategoryProfileMobileHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryProfileMobileHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryProfileMobileHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
