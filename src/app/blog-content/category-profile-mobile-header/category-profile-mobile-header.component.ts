import { Component, Input, OnInit, Renderer2 } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject } from 'rxjs';
import { AppCurrentState } from './../../co/app.current.state';
import { CatCategoriesCO } from './../../co/categoryCO';
import { ChangeCategoryStateCO } from './../../co/changeCategoryStateCO';
import { SclPostCO } from './../../co/sclPostCO';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { BlogModuleService } from './../../services/blog-module.service';
import { CategoryCOService } from './../../services/category-co.service';
import { ChangeCategoryService } from './../../services/change-category.service';
import { ManagePopupService } from './../../services/manage-popup.service';
import { AppConstants } from './../../common/app.constants';
import { AppUtility } from './../../common/app.utility';

@Component({
    selector: 'app-category-profile-mobile-header',
    templateUrl: './category-profile-mobile-header.component.html',
    styleUrls: ['./category-profile-mobile-header.component.css']
})
export class CategoryProfileMobileHeaderComponent implements OnInit 
{
    showHideQuickLinks: boolean = false;
    showHideToggleIcons: boolean = false;
    public isShowingModal: boolean;
    //public catCategoriesCO: CatCategoriesCO = new CatCategoriesCO();
    public appCurrentState : AppCurrentState = new AppCurrentState();
    public catCategoryCOList:CatCategoriesCO[] = new Array();
    public slug:string;
    public fetchedCategoryDetails:boolean = false;
    public changeCategoryStateCO:ChangeCategoryStateCO = new ChangeCategoryStateCO();
    private sclPostCOBehaviorSubject = new BehaviorSubject<SclPostCO>(null);

    modalReference: NgbModalRef;
    @Input() sclPostCO:SclPostCO;
    @Input() catCategoriesCO:CatCategoriesCO;
 /*  @Input()
  set sclPostCO(value) 
  {
    this.sclPostCOBehaviorSubject.next(value);
  };

  get sclPostCO() 
  {
    return this.sclPostCOBehaviorSubject.getValue();
  } */
    constructor(private blogModuleService : BlogModuleService,
        private categoryCOService:CategoryCOService,
        private appCurrentStateService: AppCurrentStateService,
        private route: ActivatedRoute,
        private renderer: Renderer2,
        private changeCategoryService :ChangeCategoryService,
        private modalService: NgbModal,
        private managePopupService: ManagePopupService) 
    { 
        
    }
    
            
    ngOnInit() 
    {
        if(this.catCategoriesCO)
        {
            this.categoryCOService.updatecategoryCO(this.catCategoriesCO);
            this.checkIfCategoryIsFollowed();
        }
        else
        {
             let getCategoryDetailByCategoryIdApiCall: ApiCall = new ApiCall(ApiActions.getCategoryDetailByCategoryId);
                getCategoryDetailByCategoryIdApiCall.addRequestParams('categoryId', this.sclPostCO.defaultCategoryId);
                       
                this.blogModuleService.getCategoryDetailByCategoryId(getCategoryDetailByCategoryIdApiCall).subscribe(response => {
                this.catCategoriesCO = response;
                this.categoryCOService.updatecategoryCO(this.catCategoriesCO);
                this.checkIfCategoryIsFollowed();
            }); 
        }

      this.changeCategoryService.changeCategoryStateCO.subscribe(changeCategoryStateCO => 
        {
          this.changeCategoryStateCO = changeCategoryStateCO;
          this.checkIfCategoryIsFollowed();
        });
        
        this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
            this.appCurrentState = appCurrentState;

             if(appCurrentState.isUserLoggedIn)
             { 
                let getCategoryRelationObjectByUserApiCall: ApiCall = new ApiCall(ApiActions.getCategoryRelationObjectByUser)
                this.blogModuleService.getCategoryRelationObjectByUser(getCategoryRelationObjectByUserApiCall).subscribe(response => {
                    this.changeCategoryStateCO.catCategoriesCOList = response;
                    this.checkIfCategoryIsFollowed(); 
                });
             } 
        });
          
    }

    showMembersAndQuickLinks() 
    {
        if(this.appCurrentStateService.isBrowser())
        {
            this.renderer.addClass(document.body, 'modal-open');
            this.showHideQuickLinks = true;
            this.showHideToggleIcons = true;
        }
    }

    hideMembersAndQuickLinks(event) 
    {
        if(this.appCurrentStateService.isBrowser())
        {
            this.renderer.removeClass(document.body, 'modal-open');
            this.showHideQuickLinks = false;
            this.showHideToggleIcons = false;
        }
    }

    followCategory()
   {
    this.changeCategoryService.followCategory(this.changeCategoryStateCO.catCategoriesCOList, this.catCategoriesCO);
    this.managePopupService.showSwitchCategoryPopUp();
   }

   switchCategoryId()
   {
    this.changeCategoryService.switchCategory(this.changeCategoryStateCO.catCategoriesCOList);
    this.managePopupService.showSwitchCategoryPopUp();
   }

   checkIfCategoryIsFollowed()
   {
    if(this.catCategoriesCO)
    {
     if(this.changeCategoryStateCO && this.changeCategoryStateCO.catCategoriesCOList)
     {
       this.catCategoriesCO.isUserJoin = false;
      for(let catCategoriesCOTemp of this.changeCategoryStateCO.catCategoriesCOList)
      {
          if(catCategoriesCOTemp.id === this.catCategoriesCO.id)
          {
            this.catCategoriesCO.isUserJoin = true;
          }
      }
     }
     else
     {
      this.catCategoriesCO.isUserJoin = false;
     }
   }
}

closePopUp()
    {
    this.modalReference.close();
    }
    eduncleExit(contentExit)
    {
        //this.modalService.open(contentExit, { windowClass : 'sm', centered: true });
    this.modalReference =  this.modalService.open(contentExit, { windowClass : 'sm', centered: true });
    }

    openDenyUnfollowPopUpOpen()
   {    
    this.changeCategoryService.unFollowCategory(this.catCategoriesCO);
    this.managePopupService.showDenyUnfollowPopUp();
   }

   openLoginPopUp(catCategoryId)
   {
    //localStorage.setItem("defaultCategoryId",catCategoryId);
    this.managePopupService.showSignPopUp(catCategoryId,AppConstants.REGISTRATION);
   }
  public getResponsiveImage(imagePath:string):string {
    return AppUtility.getResponsiveImage(imagePath);
   } 
}
