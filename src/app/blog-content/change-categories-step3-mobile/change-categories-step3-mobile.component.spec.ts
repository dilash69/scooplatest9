import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeCategoriesStep3MobileComponent } from './change-categories-step3-mobile.component';

describe('ChangeCategoriesStep3MobileComponent', () => {
  let component: ChangeCategoriesStep3MobileComponent;
  let fixture: ComponentFixture<ChangeCategoriesStep3MobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeCategoriesStep3MobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeCategoriesStep3MobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
