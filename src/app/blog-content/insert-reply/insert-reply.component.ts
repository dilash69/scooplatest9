import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AppConstants } from '../../common/app.constants';
import { AppUtility } from '../../common/app.utility';
import { AppCurrentState } from './../../co/app.current.state';
import { SclPostCommentCO } from './../../co/commentCO';
import { SclCommentReplyCO } from './../../co/commentReplyCO';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { BlogModuleService } from './../../services/blog-module.service';

@Component({
  selector: 'app-insert-reply',
  templateUrl: './insert-reply.component.html',
  styleUrls: ['./insert-reply.component.css']
})
export class InsertReplyComponent implements OnInit {
  public replyToggleVariable: boolean = false;
  public appCurrentState : AppCurrentState = new AppCurrentState();
  public spaceCheck: boolean=true;
  @Output() replyInserted = new EventEmitter();
  @Output() hideReplyBox = new EventEmitter();
  public textbackground:String;

  private commentCOBehaviorSubject = new BehaviorSubject<SclPostCommentCO>(null);

  @Input()
  set commentCO(value) 
  {
      this.commentCOBehaviorSubject.next(value);
  };

  get commentCO() 
  {
      return this.commentCOBehaviorSubject.getValue();
  }
  constructor(private blogModuleService: BlogModuleService,
    private appCurrentStateService: AppCurrentStateService) 
  {

  }

  ngOnInit() 
  {
    this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
      this.appCurrentState = appCurrentState;  
    //  this.textbackground =this.appCurrentState.userCO.firstName.charAt(0).toUpperCase();
      if(!this.appCurrentState.isUserLoggedIn || AppUtility.isEmpty(this.appCurrentState.userCO.firstName))
      {
          this.textbackground=AppConstants.DEFAULT_FIRST_CHARACTER;
      }
      else{
          this.textbackground=this.appCurrentState.userCO.firstName.charAt(0).toUpperCase();
      }
    });
  }

  insertReply(replyJsonObj: JSON)
  {
    this.commentCO.totalReply = String(parseInt(this.commentCO.totalReply)+1);

    let sclCommentReplyCO:SclCommentReplyCO = new SclCommentReplyCO();
    sclCommentReplyCO.reply = replyJsonObj['comment'];
    
    if (sclCommentReplyCO.reply.trim().length === 0) 
    {
      alert("Enter Something");
    }
    else
    {
      sclCommentReplyCO.commentId = this.commentCO.id;

      let replyCommentApiCall: ApiCall = new ApiCall(ApiActions.replyComment);
      replyCommentApiCall.addRequestParams('sclReplyCommentCO', sclCommentReplyCO);
     
      this.blogModuleService.replyComment(replyCommentApiCall).subscribe(response => {
        let result = response;
        //this.replyInserted.emit(result);
        this.hideReplyBox.emit();
      });
    }
  }

  checkSpace(event: string)
  {
    if(this.noWhitespaceValidator(event)) 
    {
        this.spaceCheck=false;
    }
    else
    {
        this.spaceCheck=true;
    }
  }
//for whitespace in comment
  public noWhitespaceValidator(control:string) 
  {
        const isWhitespace = (control|| '').trim().length === 0;
        const isValid = !isWhitespace;
        return isValid ? null : true;
  } 
  public getClassForTextBackGround():String
  {
    return "user-profile-commonDesign userProfile-35 "+AppUtility.textBackgroundColor(this.textbackground)+" sm16";;
      
  }
}
