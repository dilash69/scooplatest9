import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertReplyComponent } from './insert-reply.component';

describe('InsertReplyComponent', () => {
  let component: InsertReplyComponent;
  let fixture: ComponentFixture<InsertReplyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertReplyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertReplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
