import { AppSettings } from './../../common/app-settings';
import { AppCurrentState } from './../../co/app.current.state';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { BehaviorSubject } from "rxjs";
import { CatCategoriesCO } from "./../../co/categoryCO";
import { CatCategoryAdSlidersCO } from "./../../co/catCategoryAdSlidersCO";
import { ShpProductsCO } from "./../../co/ShpProductsCO ";
import { ApiActions } from "./../../common/api.actions";
import { ApiCall } from "./../../common/api.call";
import { BlogModuleService } from "./../../services/blog-module.service";
import { Component, OnInit, Input, ViewChild, HostListener } from "@angular/core";
import { NgbRatingConfig, NgbCarouselConfig } from "@ng-bootstrap/ng-bootstrap";
import { ManagePopupService } from '../../services/manage-popup.service';
import { AppConstants } from '../../common/app.constants';
import { AppUtility } from '../../common/app.utility';
declare global 
{
    interface Window { MyNamespace: any; }
}

@Component({
    selector: "app-eduncle-advertisement-vertical",
    templateUrl: "./eduncle-advertisement-vertical.component.html",
    styleUrls: ["./eduncle-advertisement-vertical.component.css"]
})
export class EduncleAdvertisementVerticalComponent implements OnInit 
{
    public adRedirectUrl:string;
    public catCategoryAdSlidersCOList: CatCategoryAdSlidersCO[];
    public appCurrentState : AppCurrentState = new AppCurrentState();
    public siteUrl: string;
    public showMyElement:boolean;
    @Input() tagId: string;
    private catCategoriesCOBehaviorSubject = new BehaviorSubject<CatCategoriesCO>(
        null
    );

    @Input()
    set catCategoriesCO(value) 
    {
        this.catCategoriesCOBehaviorSubject.next(value);
    }

    get catCategoriesCO() 
    {
        return this.catCategoriesCOBehaviorSubject.getValue();
    }

    constructor(
        private managePopUpService: ManagePopupService,
        private blogModuleService: BlogModuleService,
        private appCurrentStateService: AppCurrentStateService) 
    {
        /* config.max = 5;
            config.readonly = true;  */
    }

    ngOnInit() 
    {
        this.siteUrl = AppSettings.MAIN_WEBSITE_HOME;
        this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
            this.appCurrentState = appCurrentState;
          });
        var vm = this;

        this.catCategoriesCOBehaviorSubject.subscribe(x => {
            if (this.catCategoriesCO) 
            {
                let getCategoryAdSliderApiCall: ApiCall = new ApiCall(ApiActions.getCategoryAdSlider);
                getCategoryAdSliderApiCall.addRequestParams("categoryId",this.catCategoriesCO.id);
                getCategoryAdSliderApiCall.addRequestParams("adType", "IN_SIDE");

                this.blogModuleService.getCategoryAdSlider(getCategoryAdSliderApiCall).subscribe(response => {
                        this.catCategoryAdSlidersCOList = response;
                    });
            }
            if (this.tagId) 
            {
                let getTagAdSliderByTagIdApiCall: ApiCall = new ApiCall(ApiActions.getTagAdSliderByTagId);
                getTagAdSliderByTagIdApiCall.addRequestParams("tagId", this.tagId);
                getTagAdSliderByTagIdApiCall.addRequestParams("adType", "IN_SIDE");

                this.blogModuleService.getTagAdSliderByTagId(getTagAdSliderByTagIdApiCall).subscribe(response => {
                        this.catCategoryAdSlidersCOList = response;
                    });
            }
        });
    }

    openLoginPopUp(categoryId,redirectUrl)
    {
        //localStorage.setItem("defaultCategoryId",categoryId);
        localStorage.setItem("redirectUrl",redirectUrl)
        localStorage.setItem(AppConstants.REGISTRATION_PROCESS, AppConstants.ADVERTISEMENT_REGISTRATION_PROCESS);
        this.managePopUpService.showSignPopUp(categoryId,AppConstants.REGISTRATION) 
    }

    redirectUrl(redirectUrl)
    {
        this.adRedirectUrl=redirectUrl;
    }

    public getResponsiveImage(imagePath:string):string {
        return AppUtility.getResponsiveImage(imagePath);
       }
}