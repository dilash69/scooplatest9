import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollOptionDesignComponent } from './poll-option-design.component';

describe('PollOptionDesignComponent', () => {
  let component: PollOptionDesignComponent;
  let fixture: ComponentFixture<PollOptionDesignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollOptionDesignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollOptionDesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
