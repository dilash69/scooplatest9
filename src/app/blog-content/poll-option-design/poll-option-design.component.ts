import { Component, OnInit, Input } from '@angular/core';
import { SclPollOptionCO } from '../../co/SclPollOptionCO';

@Component({
  selector: 'app-poll-option-design',
  templateUrl: './poll-option-design.component.html',
  styleUrls: ['./poll-option-design.component.css']
})
export class PollOptionDesignComponent implements OnInit {
  @Input() sclPollOptionCO:SclPollOptionCO;
  @Input() pollTotalAttempts:string;
  public optionAttemptPercent:number;

  constructor() { }

  ngOnInit() 
  {
   // console.log("yo1",this.sclPollOptionCO)
    if(this.sclPollOptionCO.isAnsweredOption)
    {
      this.optionAttemptPercent = (100 * +this.sclPollOptionCO.totalVotes / +this.pollTotalAttempts);
    }
  }

}
