import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostViewSingleExceptArticleComponent } from './post-view-single-except-article.component';

describe('PostViewSingleExceptArticleComponent', () => {
  let component: PostViewSingleExceptArticleComponent;
  let fixture: ComponentFixture<PostViewSingleExceptArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostViewSingleExceptArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostViewSingleExceptArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
