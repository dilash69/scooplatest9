import { AppPopupCO } from './../../co/app.popup.co';
import { AppSettings } from './../../common/app-settings';
import { SclCommentReplyCO } from './../../co/commentReplyCO';
import { AppCurrentState } from './../../co/app.current.state';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { UserCO } from './../../co/userCO';
import { AppConstants } from './../../common/app.constants';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BlogModuleService } from './../../services/blog-module.service';
import { BehaviorSubject } from 'rxjs';
import { SclPostCommentCO } from './../../co/commentCO';
import { Component, OnInit, Input, Output, EventEmitter, Inject, Renderer2 } from '@angular/core';
import { deserialize } from 'serializer.ts/Serializer';
import { ManagePopupService } from '../../services/manage-popup.service';
import { AppUtility } from '../../common/app.utility';

@Component({
    selector: 'app-show-comment',
    templateUrl: './show-comment.component.html',
    styleUrls: ['./show-comment.component.css']
})
export class ShowCommentComponent implements OnInit {
    public siteUrl: string;
    public scoopUrl: string;
    public encodedUrl: string;
    public pageUrl: string;
 //public commentLikeStatuus: boolean = false;

    showReplyBox: boolean = false;

    private sclPostCommentCOBehaviorSubject = new BehaviorSubject<SclPostCommentCO>(null);
    public appCurrentState: AppCurrentState = new AppCurrentState();
    replyIdList: string[] = new Array();
    public appPopupCO: AppPopupCO = new AppPopupCO();
    //@Output() deleteCommentFromList = new EventEmitter();
    public replyId: string;

    public textBackgroundUserProfile: String;
    text:string;
    public commentText: String;
    maxLength: number = 220;
    currentText: string;
    hideToggle: boolean = true;

    defaultImageCircle:String = AppConstants.DEFAULT_IMAGE_CIRCLE;
    public isCollapsed: boolean = true;


    @Input() categoryId: string;
    @Input()
    set commentCO(value) {
        this.sclPostCommentCOBehaviorSubject.next(value);
    };

    get commentCO() {
        return this.sclPostCommentCOBehaviorSubject.getValue();
    }
    constructor(
        private blogModuleService: BlogModuleService,
        private modalService: NgbModal,
        private appCurrentStateService: AppCurrentStateService,
        private managePopupService: ManagePopupService,
        private renderer: Renderer2) {

    }

    ngOnInit() {
        this.managePopupService.appPopupCO.subscribe(appPopupCO => {
            console.log("ShowCommentComponent popup subscriber")
            this.appPopupCO = appPopupCO;

            if (this.appCurrentStateService.isBrowser()) {
                if (this.appPopupCO.isAnyPopupOpen()) {
                    this.renderer.addClass(document.body, 'modal-open');
                }
                else {
                    this.renderer.removeClass(document.body, 'modal-open');
                }
            }

        });
        // this.pageUrl = window.location.href;
        // this.encodedUrl = encodeURIComponent( this.pageUrl);

        this.siteUrl = AppSettings.MAIN_WEBSITE_HOME;
        this.scoopUrl = AppSettings.SCOOP_WEBSITE_HOME;
        //this.commentCO.sclCommentReplyCOList = new Array();
        this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
            this.appCurrentState = appCurrentState;
        });

        this.sclPostCommentCOBehaviorSubject
            .subscribe(x => {
                console.log("ShowCommentComponent commentCO subscriber")
                if (this.commentCO) {
                    this.text=this.commentCO.comment;
                    this.determineView(this.commentCO.comment);

                    this.textBackgroundUserProfile = this.commentCO.userCO.firstName.charAt(0).toUpperCase();
                   /*  if (this.appCurrentState.isUserLoggedIn) 
                    {
                        let userCommentLikeApiCall: ApiCall = new ApiCall(ApiActions.userCommentLike);
                        userCommentLikeApiCall.addRequestParams('commentId', this.commentCO.id);

                        this.blogModuleService.userCommentLike(userCommentLikeApiCall).subscribe(response => {
                            this.commentLikeStatuus = response;
                        });s
                    } */
                }
            });


    }



    //   onDeletePopupYesClick()
    //   {
    //     this.replyId = localStorage.getItem("replyId");
    //     localStorage.removeItem("replyId");
    //       this.deleteReply();
    //   }

    //   deleteReply()
    //   {
    //     let sclPostReplyCO=new SclCommentReplyCO();
    //       let deleteReplyApiCall: ApiCall = new ApiCall(ApiActions.deleteReply);
    //       deleteReplyApiCall.addRequestParams('replyId', this.replyId);

    //       this.blogModuleService.deleteReply(deleteReplyApiCall).subscribe(response => 
    //       {
    //           let result = response;
    //           sclPostReplyCO.id= this.replyId;
    //           this.deleteReplyFromList(sclPostReplyCO);

    //       });
    //   }

    openLoginPopUp(categoryId) {
        //localStorage.setItem("defaultCategoryId",categoryId)
        // this.managePopUpService.showSignPopUp(categoryId);
        localStorage.setItem(AppConstants.REGISTRATION_PROCESS, AppConstants.BLOG_ACTIVITY_REGISTRATION_PROCESS);
        this.managePopupService.showSignPopUp(categoryId,AppConstants.APP_PROMOTION)
    }

    likeComment(commentCO: SclPostCommentCO) {
        if (this.appCurrentState.isUserLoggedIn) {
            //commentCO.hasUserLiked = true;
            this.commentCO.hasUserLiked = true;
            let count = parseInt(commentCO.totalLikes);
            count = count + 1;
            commentCO.totalLikes = count.toString();

            let likeCommentApiCall: ApiCall = new ApiCall(ApiActions.likeComment);
            likeCommentApiCall.addRequestParams('commentId', commentCO.id);

            this.blogModuleService.likeComment(likeCommentApiCall).subscribe(response => {
                //Do nothing
            });
        }
        else {
            this.openLoginPopUp(this.categoryId)
        }
    }

    unlikeComment(commentCO: SclPostCommentCO) {
        if (this.appCurrentState.isUserLoggedIn) {
            this.commentCO.hasUserLiked = false;
            //commentCO.hasUserLiked = false;
            let count = parseInt(commentCO.totalLikes);
            count = count - 1;
            commentCO.totalLikes = count.toString();

            let unlikeCommentApiCall: ApiCall = new ApiCall(ApiActions.unlikeComment);
            unlikeCommentApiCall.addRequestParams('commentId', commentCO.id);

            this.blogModuleService.unlikeComment(unlikeCommentApiCall).subscribe(response => {
                //Do nothing
            });
        }
        else {
            this.openLoginPopUp(this.categoryId)
        }
    }

    // deletePostComment() 
    // {
    //     let deleteCommentApiCall: ApiCall = new ApiCall(ApiActions.deleteComment);
    //     deleteCommentApiCall.addRequestParams('commentId', this.commentCO.id);

    //     this.blogModuleService.deleteComment(deleteCommentApiCall).subscribe(response => {
    //         let result = response;
    //         console.log("12345",result);
    //         this.deleteCommentFromList.emit(this.commentCO);
    //     });
    // }

    loadMoreReplies(commentCO) {
        if (this.replyIdList.length > 0 && this.commentCO.sclCommentReplyCOList.length >= 10) {
            let tempReplyIdList: string[] = new Array();
            for (let i = this.commentCO.sclCommentReplyCOList.length; i < this.commentCO.sclCommentReplyCOList.length + 10; i++) {
                if (this.replyIdList[i]) {
                    tempReplyIdList.push(this.replyIdList[i])
                }
                else {
                    break;
                }
            }
            let getCommentReplyByIdList: ApiCall = new ApiCall(ApiActions.getCommentReplyByIdList);
            getCommentReplyByIdList.addRequestParams('replyIdList', tempReplyIdList);

            this.blogModuleService.getCommentReplyByIdList(getCommentReplyByIdList).subscribe(responseValues => {
                let sclCommentReplyCOList: SclCommentReplyCO[] = new Array();

                sclCommentReplyCOList = <SclCommentReplyCO[]>responseValues['sclCommentReplyCOList'];
                sclCommentReplyCOList = deserialize<SclCommentReplyCO[]>(SclCommentReplyCO, sclCommentReplyCOList);

                this.commentCO.sclCommentReplyCOList = this.commentCO.sclCommentReplyCOList.concat(sclCommentReplyCOList)
            });
        }
        else {
            let getCommentReplyIdListApiCall: ApiCall = new ApiCall(ApiActions.getCommentReplyIdList);
            getCommentReplyIdListApiCall.addRequestParams('commentId', commentCO.id);

            this.blogModuleService.getCommentReplyIdList(getCommentReplyIdListApiCall).subscribe(responseValues => {
                this.replyIdList = responseValues['sclCommentReplyIdList'];
                this.commentCO.sclCommentReplyCOList = <SclCommentReplyCO[]>responseValues['sclCommentReplyCOList'];
                this.commentCO.sclCommentReplyCOList = deserialize<SclCommentReplyCO[]>(SclCommentReplyCO, this.commentCO.sclCommentReplyCOList);
            });
        }
    }

    replyInserted(sclCommentReplyCO: SclCommentReplyCO) {
        if (!this.commentCO.sclCommentReplyCOList) {
            this.commentCO.sclCommentReplyCOList = new Array();
            this.commentCO.sclCommentReplyCOList.push(sclCommentReplyCO);
        }
        else {
            this.commentCO.sclCommentReplyCOList.push(sclCommentReplyCO);
        }

    }

    hideReplyBox(event) {
        this.showReplyBox = false
    }

    postPopup(contents) {
        this.modalService.open(contents, { size: 'lg' });
    }

    // deleteReplyFromList(replyCO:SclCommentReplyCO)
    // {
    //     this.commentCO.sclCommentReplyCOList.splice(this.commentCO.sclCommentReplyCOList.indexOf(replyCO), 1);
    //     for (let i = 0; i < this.replyIdList.length; i++) 
    //        {
    //             this.replyIdList.splice(this.replyIdList.indexOf(replyCO.id), 1);
    //        } 
    //     this.decreaseReplyCountByOne()

    // }

    decreaseReplyCountByOne() {
        let count = parseInt(this.commentCO.totalReply);
        count = count - 1;
        this.commentCO.totalReply = count.toString();
    }

    // onDeletePopupYesClick()
    // {
    //     this.deletePostComment();
    // }

    showInsertReplyBox() {
        if (this.appCurrentState.userCO) {
            this.showReplyBox = true;
        }
        else {
            this.managePopupService.showSignPopUp(this.categoryId,AppConstants.REGISTRATION);
        }
    }


    openCommentDeletePopup(commentId) {
        localStorage.setItem("commentId", commentId);
        this.managePopupService.showCommentDeletePopUpOpen();
    }

    public getClassForTextBackGround(): String {
        return "user-profile-commonDesign userProfile-35 " + AppUtility.textBackgroundColor(this.textBackgroundUserProfile) + " sm16";;


    }

    toggleView() {
        this.isCollapsed = !this.isCollapsed;
        this.determineView(this.text);
    }

    determineView(text:string) {
        if (!text || text.length <= this.maxLength) {
            this.currentText = text;
            this.isCollapsed = false;
            this.hideToggle = true;
            return;
        }
        this.hideToggle = false;
        if (this.isCollapsed == true) {
            this.currentText = text.substring(0, this.maxLength) + "...";
        } else if (this.isCollapsed == false) {
            this.currentText = text;
        }

    }
	
	public getResponsiveImage(imagePath:string):string {
        return AppUtility.getResponsiveImage(imagePath);
       }

    /* ngOnChanges() {
        this.determineView();
    } */
}
