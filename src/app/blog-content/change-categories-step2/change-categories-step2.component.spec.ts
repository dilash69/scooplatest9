import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeCategoriesStep2Component } from './change-categories-step2.component';

describe('ChangeCategoriesStep2Component', () => {
  let component: ChangeCategoriesStep2Component;
  let fixture: ComponentFixture<ChangeCategoriesStep2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeCategoriesStep2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeCategoriesStep2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
