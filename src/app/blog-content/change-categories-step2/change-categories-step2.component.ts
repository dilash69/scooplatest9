import { ManagePopupService } from './../../services/manage-popup.service';
import { CatCategoriesCO } from './../../co/categoryCO';
import { AppUtility } from './../../common/app.utility';
import { CatObjectCategoryRelationCO } from './../../co/catObjectCategoryRelationCO';
import { AppConstants } from './../../common/app.constants';
import { ChangeCategoryUserSelectionCO } from './../../co/changeCategoryUserSelectionCO';
import { ChangeCategoryStateCO } from './../../co/changeCategoryStateCO';
import { Component, OnInit } from '@angular/core';
import { deserialize } from "serializer.ts/Serializer";
import { FilterValueCO } from '../../co/filterValueCO';
import { CatCategoryFiltersCO } from './../../co/catCategoryFiltersCO';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { BlogModuleService } from './../../services/blog-module.service';
import { ChangeCategoryService } from './../../services/change-category.service';
import {  Router } from '@angular/router';
//import { ManagePopupService } from 'src/app/services/manage-popup.service';
//import { CatCategoriesCO } from 'src/app/co/categoryCO';
//import { AppUtility } from 'src/app/common/app.utility';
declare var $ :any;

@Component({
  selector: 'app-change-categories-step2',
  templateUrl: './change-categories-step2.component.html',
  styleUrls: ['./change-categories-step2.component.css']
})
export class ChangeCategoriesStep2Component implements OnInit 
{
  public changeCategoryStateCO:ChangeCategoryStateCO = new ChangeCategoryStateCO();
  public changeCategoryUserSelectionCO:ChangeCategoryUserSelectionCO = new ChangeCategoryUserSelectionCO();

  public IsDropdownCheckboxShow:boolean = false;
  public catCategoryFiltersCOList: CatCategoryFiltersCO[];
  public currentCatCategoryDependentFiltersCO:CatCategoryFiltersCO;
//  public isSingleSubject:string;  
  public isValid:boolean= true;
  public mandatoryFilterName:string = "";
//  catSubjectCOList:CatSubjectsCO[]= new Array();
  searchText:string = "";

  constructor(private blogModuleService :BlogModuleService,
              private changeCategoryService: ChangeCategoryService,
              private managePopupService: ManagePopupService
              ) 
  { 

  }

  ngOnInit() 
  {
    this.changeCategoryService.changeCategoryStateCO.subscribe(changeCategoryStateCO => {
      this.changeCategoryStateCO = changeCategoryStateCO;
    });

    this.changeCategoryService.changeCategoryUserSelectionCO.subscribe(changeCategoryUserSelectionCO => 
      {
        this.changeCategoryUserSelectionCO = changeCategoryUserSelectionCO;

        if(this.changeCategoryStateCO.currentStep === AppConstants.STEP_2_VALUE)
        {
          if (this.changeCategoryUserSelectionCO) 
          {
            if(this.changeCategoryStateCO.purpose === AppConstants.EDIT_CATEGORY)
            {
                for(let catCategoryFiltersCO of this.changeCategoryUserSelectionCO.catCategoryFiltersCOList)
                {
                  if(catCategoryFiltersCO.sequence === "1")
                  {
                    this.currentCatCategoryDependentFiltersCO = catCategoryFiltersCO;
                    break;
                  }
                }
  
                this.catCategoryFiltersCOList = this.changeCategoryUserSelectionCO.catCategoryFiltersCOList;
                //this.assignRandomColor();
            }
            else if(this.changeCategoryUserSelectionCO.catCategoryCO.id)
            {
              this.currentCatCategoryDependentFiltersCO = null;
              let getCategoryFiltersForCustomerApiCall: ApiCall = new ApiCall(ApiActions.getCategoryFiltersForCustomer);
              getCategoryFiltersForCustomerApiCall.addRequestParams('categoryId', this.changeCategoryUserSelectionCO.catCategoryCO.id);
    
              this.blogModuleService.getCategoryFiltersForCustomer(getCategoryFiltersForCustomerApiCall).subscribe(response => 
              {
                this.catCategoryFiltersCOList = response['catCategoryFiltersCOList'];
                this.catCategoryFiltersCOList = deserialize<CatCategoryFiltersCO[]>(CatCategoryFiltersCO, this.catCategoryFiltersCOList);
    
                 for(let catCategoryFiltersCO of this.catCategoryFiltersCOList)
                {
                    if(catCategoryFiltersCO.sequence === "1")
                    {
                      this.currentCatCategoryDependentFiltersCO = catCategoryFiltersCO;
                      break;
                    }
                } 
  
                this.changeCategoryUserSelectionCO.catCategoryFiltersCOList = this.catCategoryFiltersCOList;
              
                if(response['isSingleSubject'] === '1')
                {
                    
                }
                else
                {                
                  for(let catCategoryFiltersCO of this.catCategoryFiltersCOList)
                  {
                      if(catCategoryFiltersCO.filterType === AppConstants.SUBJECT)
                      {
                        catCategoryFiltersCO.isSingleSelection = false;
                      }
                  }
                }
    
                //this.getFilterOnSequenceOne();
                this.getIndependentFilterValues();
              });
            }
          }
        }
        
    });   
  }

/*   getFilterDependentOnExam()
  {
    let filterValueCO:FilterValueCO = this.changeCategoryUserSelectionCO.selectedCategoryExamCO.getFilterValueCO();
    filterValueCO.filterType = AppConstants.EXAM;
    this.getFilterDependentOnMe(filterValueCO);
  } */

  getFirstLetter(examName: string) {
    return examName.substring(0, 1);
  }

  goToStep1()
  {
    this.changeCategoryService.subscribeCategory();
    this.managePopupService.showSwitchCategoryPopUp();
  }

  isCheckboxDropdownGroupShow()
  {
    if(this.IsDropdownCheckboxShow === false){
      this.IsDropdownCheckboxShow = true
    }else{
      this.IsDropdownCheckboxShow = false
    }
  }

  updateActiveClass(isActive): any {
    return isActive ? 'active checkedActive' : '';
  } 

  updateCheckedStatus(isActive:boolean): string {
    return isActive ? 'checked' : '';
  }

  getIndependentFilterValues()
  {
    for (let i = 0; i < this.catCategoryFiltersCOList.length; i++) 
    {
      if (!this.catCategoryFiltersCOList[i].dependentFilterType || this.catCategoryFiltersCOList[i].dependentFilterType==="") 
      {
        //this.catCategoryFiltersCOList[i].filterTypeTitle = this.catCategoryFiltersCOList[i].filterType;
        let getIndependentFilterValues: ApiCall = new ApiCall(ApiActions.getIndependentFilterValues);
        getIndependentFilterValues.addRequestParams("categoryId", this.catCategoryFiltersCOList[i].categoryId);
        getIndependentFilterValues.addRequestParams("filterType", this.catCategoryFiltersCOList[i].filterType);

        this.blogModuleService.getIndependentFilterValues(getIndependentFilterValues).subscribe(response => 
          {
              this.catCategoryFiltersCOList[i].dropDownFilterValueCOList = response;
              this.catCategoryFiltersCOList[i].populateFilterType();
              this.changeCategoryUserSelectionCO.catCategoryFiltersCOList = this.catCategoryFiltersCOList;
              //this.markEnglishMediumLanguageAsSelected(this.catCategoryFiltersCOList[i].dropDownFilterValueCOList)
              //this.assignRandomColor();              
          });
      }
    }
  }

  onFilterClick(filterValueCO: FilterValueCO, catCategoryFiltersCO:CatCategoryFiltersCO)
  {
      catCategoryFiltersCO.selectedFilterValueCOArray = new Array();

      AppUtility.log(catCategoryFiltersCO);
      AppUtility.log(filterValueCO);
      if(catCategoryFiltersCO.isSingleSelection)
      {
        //remove all selections
        for (let i = 0; i < catCategoryFiltersCO.dropDownFilterValueCOList.length; i++) 
        {
          catCategoryFiltersCO.dropDownFilterValueCOList[i].isActive = false;
        } 

        filterValueCO.isActive = true;
        catCategoryFiltersCO.selectedFilterValueCOArray.push(filterValueCO);
      }
      else
      {
          filterValueCO.isActive = !filterValueCO.isActive;
          catCategoryFiltersCO.selectedFilterValueCOArray = new Array();

          for(let filterValueCO of catCategoryFiltersCO.dropDownFilterValueCOList)
          {
              if(filterValueCO.isActive)
              {
                catCategoryFiltersCO.selectedFilterValueCOArray.push(filterValueCO);
              }
          }
      }

      this.isValid = true;
      this.getFilterDependentOnMe(filterValueCO);
      this.changeCategoryUserSelectionCO.catCategoryFiltersCOList = this.catCategoryFiltersCOList;
  }

  getFilterDependentOnMe(filterValueCO:FilterValueCO)
  {
    for(let i = 0;i<this.catCategoryFiltersCOList.length; i++)
    {
      if(this.catCategoryFiltersCOList[i].dependentFilterType === filterValueCO.filterType)
      {
        let getDependentFilterValues: ApiCall = new ApiCall(ApiActions.getDependentFilterValues);
        getDependentFilterValues.addRequestParams("filterValueCO", filterValueCO.cloneObject(this.catCategoryFiltersCOList[i].filterType));
/*         
        getDependentFilterValues.addRequestParams("categoryId", this.catCategoryFiltersCOList[i].categoryId);
        getDependentFilterValues.addRequestParams("filterType", this.catCategoryFiltersCOList[i].filterType);
        getDependentFilterValues.addRequestParams("dependentFilterTypeId", filterValueCO.filterTypeId);
        getDependentFilterValues.addRequestParams("dependentFilterType", filterValueCO.filterType); */

        this.blogModuleService.getDependentFilterValues(getDependentFilterValues).subscribe(response => 
        {
          this.catCategoryFiltersCOList[i].dropDownFilterValueCOList = response;
          this.catCategoryFiltersCOList[i].populateFilterType();
          this.currentCatCategoryDependentFiltersCO = this.catCategoryFiltersCOList[i];
         // this.assignRandomColor();
          this.searchText = "";
          this.performSearch();
          this.changeCategoryUserSelectionCO.catCategoryFiltersCOList = this.catCategoryFiltersCOList;
        });
      }
    }
  }

  goToStep3()
  {
    if(this.changeCategoryStateCO.isUserLoggedIn)
    {
      this.changeCategoryStateCO.currentStep = AppConstants.STEP_3_VALUE;
      this.changeCategoryService.updateChangeCategoryStateCO(this.changeCategoryStateCO);
    }
    else
    {

    }
    
  } 

  checkIsMandatoryFilterIsSelected()
  {
    for (let catCategoryFiltersCO of this.catCategoryFiltersCOList) 
      {
          if(catCategoryFiltersCO.isMandatory === "1")
          {
              if(catCategoryFiltersCO.selectedFilterValueCOArray && catCategoryFiltersCO.selectedFilterValueCOArray.length > 0)
              {

              }
              else
              {
                this.mandatoryFilterName = catCategoryFiltersCO.filterType;
                this.isValid = false;
                break;
              }
          }
      }
  }

  continue()
  {
    this.changeCategoryUserSelectionCO.catCategoryFiltersCOList = this.catCategoryFiltersCOList;
    this.isValid=true;
    this.checkIsMandatoryFilterIsSelected();
    if (this.isValid) 
    {
      if(this.changeCategoryStateCO.isUserLoggedIn)
      {
        let catObjectCategoryRelationCOList:CatObjectCategoryRelationCO[] = this.changeCategoryUserSelectionCO.getCatObjectCategoryRelationCOList();

        let updateUserCategoryRelationObjectApicall: ApiCall = new ApiCall(ApiActions.updateUserCategoryRelationObject);
        updateUserCategoryRelationObjectApicall.addRequestParams("catObjectCategoryRelationCOStringList", catObjectCategoryRelationCOList);

        this.blogModuleService.updateUserCategoryRelationObject(updateUserCategoryRelationObjectApicall).subscribe(response1 => 
          {
            AppUtility.log("yo",response1['catCategoryCOList'])
            let catCategoryCOList: CatCategoriesCO[] = response1['catCategoryCOList'];
              catCategoryCOList = deserialize<CatCategoriesCO[]>(CatCategoriesCO, catCategoryCOList);
              this.changeCategoryStateCO.catCategoriesCOList = catCategoryCOList;
              

          if(this.changeCategoryStateCO.purpose === AppConstants.SWITCH_CATEGORY)
          {
            let getCategoryRelationObjectByUserApiCall:ApiCall = new ApiCall(ApiActions.getCategoryRelationObjectByUser)

            this.blogModuleService.getCategoryRelationObjectByUser(getCategoryRelationObjectByUserApiCall).subscribe(response => 
              {      
                //this.changeCategoryStateCO.catCategoriesCOList = response;
                this.changeCategoryStateCO.currentStep = AppConstants.STEP_3_VALUE;
                this.changeCategoryService.updateChangeCategoryStateCO(this.changeCategoryStateCO);
               // this.changeCategoryService.updateChangeCategoryStateCO(this.changeCategoryStateCO);
              });
          }
          else if(this.changeCategoryStateCO.purpose === AppConstants.FOLLOW_CATEGORY)
          {
            this.changeCategoryService.updateChangeCategoryStateCO(this.changeCategoryStateCO);
            this.managePopupService.closePopUp()          
          }
          else if(this.changeCategoryStateCO.purpose === AppConstants.EDIT_CATEGORY)
          {
            this.changeCategoryStateCO.currentStep = AppConstants.STEP_3_VALUE;
            this.changeCategoryStateCO.purpose = AppConstants.SWITCH_CATEGORY;
            this.changeCategoryService.updateChangeCategoryStateCO(this.changeCategoryStateCO);
          }
        });
      }
/*       else
      {
        let getCategoryDetailByCategoryIdApiCall: ApiCall = new ApiCall(ApiActions.getCategoryDetailByCategoryId);
            getCategoryDetailByCategoryIdApiCall.addRequestParams('categoryId', this.changeCategoryUserSelectionCO.selectedCategoryExamCO.categoryId);
           
        this.blogModuleService.getCategoryDetailByCategoryId(getCategoryDetailByCategoryIdApiCall).subscribe(response => {
          this.categoryCOService.updatecategoryCO(response);
          this.router.navigate([response.slug]);
        });
      } */
    }
  }

/*   onBackClicked()
  {
      this.changeCategoryStateCO.currentStep = AppConstants.STEP_1_VALUE;
      this.changeCategoryService.updateChangeCategoryStateCO(this.changeCategoryStateCO);
  } */

  onBreadCrumbItemClicked(catCategoryFiltersCO:CatCategoryFiltersCO)
  {
      this.resetFiltersBelowCurrent(catCategoryFiltersCO);
      this.changeCategoryService.updateChangeCategoryUserSelectionCO(this.changeCategoryUserSelectionCO);
  }

  resetFiltersBelowCurrent(catCategoryFiltersCO:CatCategoryFiltersCO)
  {
      if(this.catCategoryFiltersCOList)
      {
          let resetNow:boolean = false;
          let parentFilterTypeArray:string[] = new Array();
          for(let catCategoryFiltersCOTemp of this.catCategoryFiltersCOList)
          {
              let resetCurrentEntry:boolean = false;
              if(resetNow)
              {
                  if(catCategoryFiltersCOTemp.dependentFilterType)
                  {
                      if(parentFilterTypeArray)
                      {
                          for(let parentFilterType of parentFilterTypeArray)
                          {
                              if(parentFilterType === catCategoryFiltersCOTemp.dependentFilterType)
                              {
                                resetCurrentEntry = true;
                              }
                          }
                      }

                      if(resetCurrentEntry)
                      {
                        catCategoryFiltersCOTemp.selectedFilterValueCOArray = new Array();
                        catCategoryFiltersCOTemp.dropDownFilterValueCOList = new Array();
                      }
                  }
                  else
                  {
                      //skip
                  }                  
              }

              if(catCategoryFiltersCOTemp.filterType === catCategoryFiltersCO.filterType)
              {
                  parentFilterTypeArray.push(catCategoryFiltersCOTemp.filterType);
                  resetNow = true;
              }
          }
      }

      this.changeCategoryUserSelectionCO.catCategoryFiltersCOList = this.catCategoryFiltersCOList;
      this.onFilterClick(catCategoryFiltersCO.selectedFilterValueCOArray[0], catCategoryFiltersCO)
  }

  getFilteredFilterValueCOList()
  {
    let fIlteredFilterValueCOList:FilterValueCO[] = new Array();
    if(this.currentCatCategoryDependentFiltersCO)
    {
      if(this.currentCatCategoryDependentFiltersCO.dropDownFilterValueCOList)
      {
        for(let dropDownFilterValueCO of this.currentCatCategoryDependentFiltersCO.dropDownFilterValueCOList)
        {
            if(dropDownFilterValueCO.isSearchFiltered)
            {
              fIlteredFilterValueCOList.push(dropDownFilterValueCO);
            }
        }
      }
    }

      return fIlteredFilterValueCOList;
  }

  onSearchFilter(event:any)
  {
    this.searchText = <string>event.target.value;
    this.performSearch();
  }

  performSearch()
  {
    if(this.searchText)
    {
      if(this.currentCatCategoryDependentFiltersCO.dropDownFilterValueCOList)
      {
        for(let dropDownFilterValueCO of this.currentCatCategoryDependentFiltersCO.dropDownFilterValueCOList)
        {
            if(dropDownFilterValueCO.filterTypeTitle.toLowerCase().indexOf(this.searchText.toLowerCase()) === -1)
            {
              dropDownFilterValueCO.isSearchFiltered = false;
            }
            else
            {
              dropDownFilterValueCO.isSearchFiltered = true;
            }
        }
      }
    }
    else
    {
      if(this.currentCatCategoryDependentFiltersCO.dropDownFilterValueCOList)
      {
        for(let dropDownFilterValueCO of this.currentCatCategoryDependentFiltersCO.dropDownFilterValueCOList)
        {
          dropDownFilterValueCO.isSearchFiltered = true;
        }
      }
    }
  }

  getLimitedDependentFilterTitle(title:string):string
  {
    return AppUtility.getTrimmedReadableString(title,15)
  }

  /* assignRandomColor()
  {
    for(let i =0 ;i<this.getFilteredFilterValueCOList().length;i++)
    {
      if(!(this.getFilteredFilterValueCOList()[i].filterValueColor===''))
      {
        this.getFilteredFilterValueCOList()[i].filterValueColor = "bgcolor"+ this.getRandomBgColor();
      }
    }
  } */

  getRandomBgColor() {
    let rand = Math.floor(Math.random() * 10);
    while (rand < 1) {
      rand = Math.floor(Math.random() * 10);
    }
    return rand.toString();
  }

  markEnglishMediumLanguageAsSelected(filterValueCOList:FilterValueCO[])
  {
    if (filterValueCOList) 
    {
      for (let filterValueCO of filterValueCOList) 
      {
        if (filterValueCO.filterType === AppConstants.MEDIUM_LANGUAGE)
        {
          if(filterValueCO.filterTypeId === AppConstants.ENGLISH_MEDIUM_LANGUAGE_ID)
          {
            filterValueCO.isActive = true
            break;
          }
        }
        else
        {
          break;
        }
      }
    }
  }   

  
}
