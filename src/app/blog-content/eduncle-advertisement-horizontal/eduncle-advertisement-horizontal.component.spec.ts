import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EduncleAdvertisementHorizontalComponent } from './eduncle-advertisement-horizontal.component';

describe('EduncleAdvertisementHorizontalComponent', () => {
  let component: EduncleAdvertisementHorizontalComponent;
  let fixture: ComponentFixture<EduncleAdvertisementHorizontalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EduncleAdvertisementHorizontalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EduncleAdvertisementHorizontalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
