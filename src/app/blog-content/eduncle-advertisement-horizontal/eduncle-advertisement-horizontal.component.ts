import { AppSettings } from './../../common/app-settings';
import { AppCurrentState } from './../../co/app.current.state';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { CatCategoriesCO } from './../../co/categoryCO';
import { BehaviorSubject } from 'rxjs';
import { CatCategoryAdSlidersCO } from './../../co/catCategoryAdSlidersCO';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { NgbRatingConfig, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { BlogModuleService } from './../../services/blog-module.service';
import { ShpProductsCO } from './../../co/ShpProductsCO ';
import { Component, OnInit, Input } from '@angular/core';
//import { SlidesOutputData } from 'ngx-owl-carousel-o';
import { ManagePopupService } from '../../services/manage-popup.service';
import { AppConstants } from '../../common/app.constants';
import { AppUtility } from '../../common/app.utility';
/* declare var $ :any; */

@Component({
    selector: 'app-eduncle-advertisement-horizontal',
    templateUrl: './eduncle-advertisement-horizontal.component.html',
    styleUrls: ['./eduncle-advertisement-horizontal.component.css']
})
export class EduncleAdvertisementHorizontalComponent implements OnInit {

    public showNavigationArrows =false;
    public showNavigationIndicators =false;
    public appCurrentState : AppCurrentState = new AppCurrentState();
    public adRedirectUrl:string;
    public siteUrl: string;
    public catCategoryAdSlidersCOList: CatCategoryAdSlidersCO[];
    public showMyElement:boolean;

    images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);

    private catCategoriesCOBehaviorSubject = new BehaviorSubject<CatCategoriesCO>(null);
    @Input() tagId: string;

    @Input()
    set catCategoriesCO(value) {
        this.catCategoriesCOBehaviorSubject.next(value);
    };

    get catCategoriesCO() {
        return this.catCategoriesCOBehaviorSubject.getValue();
    }

    constructor(private managePopUpService : ManagePopupService,
        private blogModuleService: BlogModuleService,
        config: NgbRatingConfig,
        configCarousel: NgbCarouselConfig,
        private appCurrentStateService: AppCurrentStateService) 
        {
            config.max = 5;
            config.readonly = true;

            // customize default values of carousels used by this component tree
            //configCarousel.interval = 1000;
            //configCarousel.wrap = false;
           // configCarousel.keyboard = false;
            //configCarousel.pauseOnHover = false;
            //configCarousel.showNavigationArrows = true;
            configCarousel.showNavigationIndicators = false;
        }

    ngOnInit() 
    {
        this.siteUrl = AppSettings.MAIN_WEBSITE_HOME;

        this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
            this.appCurrentState = appCurrentState;
          });
        

        this.catCategoriesCOBehaviorSubject
        .subscribe(x => {
            if (this.catCategoriesCO) 
            {
                let getCategoryAdSliderApiCall: ApiCall = new ApiCall(ApiActions.getCategoryAdSlider);
                 getCategoryAdSliderApiCall.addRequestParams('categoryId', this.catCategoriesCO.id);
                 getCategoryAdSliderApiCall.addRequestParams('adType', 'IN_BETWEEN');

                 this.blogModuleService.getCategoryAdSlider(getCategoryAdSliderApiCall).subscribe(response => {
                    this.catCategoryAdSlidersCOList = response;
                    //this.catCategoryAdSlidersCOList[0].imageName
                });
            }

            if(this.tagId)
            {
              let getTagAdSliderByTagIdApiCall: ApiCall = new ApiCall(ApiActions.getTagAdSliderByTagId);
              getTagAdSliderByTagIdApiCall.addRequestParams('tagId', this.tagId);
              getTagAdSliderByTagIdApiCall.addRequestParams('adType', 'IN_BETWEEN');
              
              this.blogModuleService.getTagAdSliderByTagId(getTagAdSliderByTagIdApiCall).subscribe(response => 
                {
                  this.catCategoryAdSlidersCOList = response;
                });
            }
        });
    }

    openLoginPopUp(categoryId,redirectUrl)
    {
       // localStorage.setItem("defaultCategoryId",categoryId);
        localStorage.setItem("redirectUrl",redirectUrl)
        localStorage.setItem(AppConstants.REGISTRATION_PROCESS, AppConstants.ADVERTISEMENT_REGISTRATION_PROCESS);
        this.managePopUpService.showSignPopUp(categoryId,AppConstants.REGISTRATION) 
    }

    // customOptions: any = {
    //     loop: false,
    //     //stagePadding: 100,
    //     //margin:10,
    //     singleItem: true,
    //     items: 1,
    //     mouseDrag: true,
    //     touchDrag: true,
    //     pullDrag: true,
    //     dots: false,
    //     navSpeed: 700,
    //     nav: true,
    //     navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
    //     responsive: {
    //       0: {
    //         items: 1
    //       },
    //       400: {
    //         items: 1
    //       },
    //       740: {
    //         items: 1
    //       },
    //       940: {
    //         items: 1
    //       }
    //     },
    // }
 


    //Slick Slider
    // slides = [
    //     { img: "https://image.ibb.co/gkogwV/Untitled-1.jpg" },
    //     { img: "https://image.ibb.co/k03QOA/1212.jpg" },
    //     { img: "https://image.ibb.co/gkogwV/Untitled-1.jpg" },
    //     { img: "https://image.ibb.co/k03QOA/1212.jpg" }
    // ];
    // slideConfig = {
    //     slidesToShow: 1,
    //     slidesToScroll: 1,
    //     centerMode: true,
    //     centerPadding: '20px',
    //     speed: 100,
    //     autoplay: false,
    //     arrows: true,
    //     dots: false
    // };

    // bootstrapTabsActive() 
    // {
    //     if (this.appCurrentStateService.isBrowser()) {
    //         $(document).ready(function() {
    //             $('.carousel').carousel({
    //               interval: 1200
    //             })
    //           });
    //     } 
    // }
    redirectUrl(redirectUrl)
    {
        this.adRedirectUrl=redirectUrl;
    }

    public getResponsiveImage(imagePath:string):string {
      return AppUtility.getResponsiveImage(imagePath);
     }
}
