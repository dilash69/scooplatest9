import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeCategoriesStep3Component } from './change-categories-step3.component';

describe('ChangeCategoriesStep3Component', () => {
  let component: ChangeCategoriesStep3Component;
  let fixture: ComponentFixture<ChangeCategoriesStep3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeCategoriesStep3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeCategoriesStep3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
