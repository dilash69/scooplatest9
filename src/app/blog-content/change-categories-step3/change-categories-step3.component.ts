import { ManagePopupService } from './../../services/manage-popup.service';
import { AppCurrentState } from './../../co/app.current.state';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { CatCategoriesCO } from './../../co/categoryCO';
import { CatCategoryFiltersCO } from './../../co/catCategoryFiltersCO';
import { CategoryExamCO } from './../../co/categoryExamCO';
import { CatObjectCategoryRelationCO } from './../../co/catObjectCategoryRelationCO';
import { AppConstants } from './../../common/app.constants';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { BlogModuleService } from './../../services/blog-module.service';
import { ChangeCategoryService } from './../../services/change-category.service';
import { ChangeCategoryUserSelectionCO } from './../../co/changeCategoryUserSelectionCO';
import { ChangeCategoryStateCO } from './../../co/changeCategoryStateCO';
import { Component, OnInit } from '@angular/core';
import { deserialize } from 'serializer.ts/Serializer';
import {  Router } from '@angular/router';
import { AppUtility } from './../../common/app.utility';
import { AppSettings } from './../../common/app-settings';
declare const setAttribute: any;

@Component({
  selector: 'app-change-categories-step3',
  templateUrl: './change-categories-step3.component.html',
  styleUrls: ['./change-categories-step3.component.css']
})
export class ChangeCategoriesStep3Component implements OnInit 
{
  public changeCategoryStateCO:ChangeCategoryStateCO;
  public changeCategoryUserSelectionCO:ChangeCategoryUserSelectionCO;
  public selectedCategoryId:string
  public appCurrentState: AppCurrentState = new AppCurrentState();
   
  constructor(private changeCategoryService:ChangeCategoryService,
    private blogModuleService :BlogModuleService,
    private router: Router,
    private appCurrentStateService: AppCurrentStateService,
    private managePopupService:ManagePopupService) 
    { }

  ngOnInit() 
  {
     this.changeCategoryService.changeCategoryStateCO.subscribe(changeCategoryStateCO => 
        {
        this.changeCategoryStateCO = changeCategoryStateCO;

/*         if (this.changeCategoryStateCO.catCategoriesCOList && this.changeCategoryStateCO.catCategoriesCOList.length > 0)
        {
            this.assignRandomColor();
        } */

        if(this.changeCategoryStateCO.currentStep === AppConstants.STEP_3_VALUE)
        {
            if (!this.changeCategoryStateCO.catCategoriesCOList || (this.changeCategoryStateCO.catCategoriesCOList && this.changeCategoryStateCO.catCategoriesCOList.length === 0)) 
            {
                let getCategoryRelationObjectByUserApiCall: ApiCall = new ApiCall(ApiActions.getCategoryRelationObjectByUser)
   
                this.blogModuleService.getCategoryRelationObjectByUser(getCategoryRelationObjectByUserApiCall).subscribe(response => {
                    this.changeCategoryStateCO.catCategoriesCOList = response
           //         this.assignRandomColor();
   
   /*                    if (this.changeCategoryStateCO.catCategoriesCOList && this.changeCategoryStateCO.catCategoriesCOList.length > 0) 
                       {
                           this.changeCategoryService.updateChangeCategoryStateCO(changeCategoryStateCO);
                       }  */                                  
                
               });
                
            }

            
        }

         
   });

   this.changeCategoryService.changeCategoryUserSelectionCO.subscribe(changeCategoryUserSelectionCO => {
    this.changeCategoryUserSelectionCO = changeCategoryUserSelectionCO;
  });

  this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
    this.appCurrentState = appCurrentState;
    this.selectedCategoryId = this.appCurrentState.userCO.defaultCategoryId;
  });

  }

   deleteBreadCrumb(catCategoryCO:CatCategoriesCO)
  {
      if(this.appCurrentState.userCO.defaultCategoryId === catCategoryCO.id)
      {
        this.managePopupService.showConfirmUnfollowPopUp();
      }
      else
      {
        let deleteCategoryObjectRelationObjectApiCall:ApiCall = new ApiCall(ApiActions.deleteCategoryObjectRelationObject)
        deleteCategoryObjectRelationObjectApiCall.addRequestParams("categoryId", catCategoryCO.id);
    
        this.blogModuleService.deleteCategoryObjectRelationObject(deleteCategoryObjectRelationObjectApiCall).subscribe(response => 
          {  
            this.changeCategoryStateCO.catCategoriesCOList.splice(this.changeCategoryStateCO.catCategoriesCOList.indexOf(catCategoryCO),1);
            this.changeCategoryService.updateChangeCategoryStateCO(this.changeCategoryStateCO);
          });
      }
  } 

  chooseMoreExams()
  {
    this.changeCategoryService.subscribeCategory();
    this.managePopupService.showSwitchCategoryPopUp();
  } 

  onEditClick(catCategoryCO:CatCategoriesCO)
  {
    let editCategorySelectionApiCall:ApiCall = new ApiCall(ApiActions.editCategorySelection)
    editCategorySelectionApiCall.addRequestParams("catCategoryCO", catCategoryCO);

    this.blogModuleService.editCategorySelectionApiCall(editCategorySelectionApiCall).subscribe(userResponseData => 
      {      
/*         let categoryExamCOList: CategoryExamCO[] = userResponseData.responseValues['categoryExamCOList'];
        categoryExamCOList = deserialize<CategoryExamCO[]>(CategoryExamCO, categoryExamCOList); */

        let catObjectCategoryRelationCO: CatObjectCategoryRelationCO = userResponseData.responseValues['catObjectCategoryRelationCO'];
        catObjectCategoryRelationCO = deserialize<CatObjectCategoryRelationCO>(CatObjectCategoryRelationCO, catObjectCategoryRelationCO);

        let catCategoryFiltersCOList: CatCategoryFiltersCO[] = userResponseData.responseValues['catCategoryFiltersCOList'];
        catCategoryFiltersCOList = deserialize<CatCategoryFiltersCO[]>(CatCategoryFiltersCO, catCategoryFiltersCOList);

        if(userResponseData.responseValues['isSingleSubject'] === '0')
        {//multi subject
          for(let catCategoryFiltersCO of catCategoryFiltersCOList)
          {
              if(catCategoryFiltersCO.filterType === AppConstants.SUBJECT)
              {
                catCategoryFiltersCO.isSingleSelection = false;
              }
          }
        }
        
//        this.changeCategoryUserSelectionCO.categoryExamCOList = categoryExamCOList;
        this.changeCategoryUserSelectionCO.catCategoryFiltersCOList = catCategoryFiltersCOList;
        this.changeCategoryUserSelectionCO.catCategoryCO = catCategoryCO;
        this.changeCategoryStateCO.purpose= AppConstants.EDIT_CATEGORY;
        this.populateForEdit(catObjectCategoryRelationCO);
        this.changeCategoryService.updateChangeCategoryUserSelectionCO(this.changeCategoryUserSelectionCO);

        this.changeCategoryStateCO.currentStep = AppConstants.STEP_2_VALUE;
        this.changeCategoryService.updateChangeCategoryStateCO(this.changeCategoryStateCO);
      });
  }

  populateForEdit(catObjectCategoryRelationCO:CatObjectCategoryRelationCO)
    { 
        this.changeCategoryUserSelectionCO.catCategoryCO.id = catObjectCategoryRelationCO.categoryId;
        
/*         for(let categoryExamCO of this.changeCategoryUserSelectionCO.categoryExamCOList)
        {
            if(categoryExamCO.examId === catObjectCategoryRelationCO.examId)
            {
                categoryExamCO.isActive = true;
                this.changeCategoryUserSelectionCO.selectedCategoryExamCO = categoryExamCO;
                break;
            }
        } */

        for(let catCategoryFiltersCO of this.changeCategoryUserSelectionCO.catCategoryFiltersCOList)
        {            
            let targetIdToMatch:string;

            switch(catCategoryFiltersCO.filterType)
            {
                case AppConstants.EXAM:
                    targetIdToMatch = catObjectCategoryRelationCO.examId;
                    break;

                case AppConstants.MEDIUM_LANGUAGE:
                    targetIdToMatch = catObjectCategoryRelationCO.mediumLanguageId;
                    break;

                case AppConstants.BOARD:
                    targetIdToMatch = catObjectCategoryRelationCO.boardId;
                    break;

                case AppConstants.YEAR:
                    targetIdToMatch = catObjectCategoryRelationCO.yearId;
                    break;

                case AppConstants.TOPIC:
                    targetIdToMatch = catObjectCategoryRelationCO.topicId;
                    break;

                case AppConstants.SUBJECT:
                    targetIdToMatch = catObjectCategoryRelationCO.subjectId;
                    break;

                case AppConstants.STAGE:
                    targetIdToMatch = catObjectCategoryRelationCO.stageId;
                    break;

                case AppConstants.STREAM:
                    targetIdToMatch = catObjectCategoryRelationCO.streamId;
                    break;

                case AppConstants.ZONE:
                    targetIdToMatch = catObjectCategoryRelationCO.zoneId;
                    break;
            }

            AppUtility.log(catCategoryFiltersCO.filterType, targetIdToMatch);
            this.populateSelectedFilterValueCOArray(catCategoryFiltersCO, targetIdToMatch);
        }
    }

    populateSelectedFilterValueCOArray(catCategoryFiltersCO:CatCategoryFiltersCO, targetIdToMatch:string)
    {
        catCategoryFiltersCO.selectedFilterValueCOArray = new Array();
        for(let dropDownFilterValueCO of catCategoryFiltersCO.dropDownFilterValueCOList)
        {
            dropDownFilterValueCO.filterType = catCategoryFiltersCO.filterType;
            if(dropDownFilterValueCO.filterTypeId === targetIdToMatch)
            {
                dropDownFilterValueCO.isActive = true;
                catCategoryFiltersCO.selectedFilterValueCOArray.push(dropDownFilterValueCO);
            }
        }
    }

    continue()
    {
        /* let getCategoryDetailByCategoryIdApiCall: ApiCall = new ApiCall(ApiActions.getCategoryDetailByCategoryId);
        getCategoryDetailByCategoryIdApiCall.addRequestParams('categoryId', this.selectedCategoryId);
               
        this.blogModuleService.getCategoryDetailByCategoryId(getCategoryDetailByCategoryIdApiCall).subscribe(response => {
        this.router.navigate([response.slug]);
    }); */

    let updateDefaultCategoryIdOfUserApiCall: ApiCall = new ApiCall(ApiActions.updateDefaultCategoryIdOfUser);
        updateDefaultCategoryIdOfUserApiCall.addRequestParams('categoryId', this.selectedCategoryId);
               
        this.blogModuleService.updateDefaultCategoryIdOfUser(updateDefaultCategoryIdOfUserApiCall).subscribe(response => {
        this.callDCPScript(response);
        this.appCurrentState.userCO.defaultCategoryId = this.selectedCategoryId;
        this.appCurrentStateService.updatedLoggedInUserCurrentState(this.appCurrentState);
        this.managePopupService.closePopUp();
        if (this.appCurrentStateService.isBrowser()) 
        {
           window.location.href = AppSettings.MAIN_WEBSITE_HOME+"/member/student/";
        }
        
       
    });
    }

    assignSelectedCategoryId(categoryId:string)
    {
        this.selectedCategoryId=categoryId
    }

    assignRandomColor()
  {
      AppUtility.log("color")
      if(this.changeCategoryStateCO.catCategoriesCOList)
      {
        for(let i =0 ;i<this.changeCategoryStateCO.catCategoriesCOList.length;i++)
        {
            if(!this.changeCategoryStateCO.catCategoriesCOList[i].categoryTitleColor)
            {
                //AppUtility.log("assigning colors");
                /* if(this.changeCategoryStateCO.catCategoriesCOList[i].categoryTitleColor.length > 0)
                {
                    this.changeCategoryStateCO.catCategoriesCOList[i].categoryTitleColor = "bgcolor"+ this.getRandomBgColor();
                } */
                this.changeCategoryStateCO.catCategoriesCOList[i].categoryTitleColor = "bgcolor"+ this.getRandomBgColor();
            }
        }
      }
  }

  getRandomBgColor() {
    let rand = Math.floor(Math.random() * 10);
    while (rand < 1) {
      rand = Math.floor(Math.random() * 10);
    }
    
    return rand.toString();
  }

  getFirstLetter(categoryName: string) {
    return categoryName.substring(0, 1);
  }

  callDCPScript(response)
  {
    let firstFilterValue = response["firstFilterValue"];
    let firstFilterType = response["firstFilterType"];
    let selectedCategoryTitle;

    
    for(let i =0;i<this.changeCategoryStateCO.catCategoriesCOList.length;i++)
    {
      if(this.changeCategoryStateCO.catCategoriesCOList[i].id === this.selectedCategoryId)
      {
        selectedCategoryTitle = this.changeCategoryStateCO.catCategoriesCOList[i].title;
        break;
      }
    }

    if(firstFilterType===AppConstants.EXAM)
    {
      setAttribute({
        'phone':this.appCurrentState.userCO.mobile,
        'CategoryName':selectedCategoryTitle,
        'FirstFilterValue':firstFilterValue,
        'FirstFilterName':firstFilterType,
        'ExamName':firstFilterValue,
        'SubjectName':'-',
        'StreamName':'-',
        'ActivityType':AppConstants.CatUpdate
      });
    }
    else if(firstFilterType===AppConstants.SUBJECT)
    {
      setAttribute({
        'phone':this.appCurrentState.userCO.mobile,
        'CategoryName':selectedCategoryTitle,
        'FirstFilterValue':firstFilterValue,
        'FirstFilterName':firstFilterType,
        'SubjectName':firstFilterValue,
        'ExamName':'-',
        'StreamName':'-',
        'ActivityType':AppConstants.CatUpdate
      });
    }
    else if(firstFilterType===AppConstants.STREAM)
    {
      setAttribute({
        'phone':this.appCurrentState.userCO.mobile,
        'CategoryName':selectedCategoryTitle,
        'FirstFilterValue':firstFilterValue,
        'FirstFilterName':firstFilterType,
        'StreamName':firstFilterValue,
        'SubjectName':"-",
        'ExamName':'-',
        'ActivityType':AppConstants.CatUpdate
      });
    }
  }

}
