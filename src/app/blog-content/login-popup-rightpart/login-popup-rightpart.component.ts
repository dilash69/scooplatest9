import { AppUtility } from './../../common/app.utility';
import { Input } from '@angular/core';
import { SclPostCommentCO } from './../../co/commentCO';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { AppConstants } from './../../common/app.constants';
//import { AppConstants } from 'src/app/common/app.constants';
import { AppSettings } from './../../common/app-settings';
import { Router } from '@angular/router';
import { BlogModuleService } from './../../services/blog-module.service';
import { ApiCall } from './../../common/api.call';
import { ApiActions } from './../../common/api.actions';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
//import { AuthService, SocialUser } from "angular4-social-login";
//import { FacebookLoginProvider, GoogleLoginProvider } from "angular4-social-login";
import { UserCO } from '../../co/userCO'; 
import { deserialize } from 'serializer.ts/Serializer';
import { urlencoded } from 'express';
import { SclPostCO } from '../../co/sclPostCO';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

declare var $ :any;
declare const setAttribute: any;

@Component({
  selector: 'app-login-popup-rightpart',
  templateUrl: './login-popup-rightpart.component.html',
  styleUrls: ['./login-popup-rightpart.component.css']
})
export class LoginPopupRightpartComponent implements OnInit 
{
 // public user: SocialUser;
  private loggedIn: boolean;
  public userCO:UserCO;
  public validationError:string="";
  public showValidationError:boolean=false;
  @Input() adRedirectUrl:string;

   //@Input() categoryId:string;

  @Output() loadRegisterComponent = new EventEmitter();
  @Output() loadForgotPassComponent = new EventEmitter();
  @Output() loadLoginComponent1 = new EventEmitter();

  

  constructor(private appCurrentStateService:AppCurrentStateService,private blogModuleService : BlogModuleService,private router: Router,config: NgbModalConfig, private modalService: NgbModal)
   {
     config.backdrop = 'static';
     config.keyboard = false;
   }

  ngOnInit() {
   /*  this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
    }); */
  }
  
  login(userLoginCredientialJsonObj: JSON)
  {
    this.showValidationError = false;
    let phone = userLoginCredientialJsonObj['phone'];
    let password = userLoginCredientialJsonObj['password'];

    let isEmail: boolean = false;
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    isEmail = re.test(String(phone).toLowerCase());

    let loginApiCall: ApiCall = new ApiCall(ApiActions.login);
    loginApiCall.showLoader = true;
    if(isEmail)
    {
      loginApiCall.addRequestParams('emailId', phone);
    }
    else
    {
      loginApiCall.addRequestParams('mobileNumber', phone);
    }
    loginApiCall.addRequestParams('password', password);
    

      this.blogModuleService.login(loginApiCall)
      .subscribe(responseValues => {
          if(responseValues['USER'])
          {
            this.userCO = responseValues['USER'];
            this.userCO = deserialize<UserCO>(UserCO, this.userCO);
            let dbAccessToken = responseValues[AppConstants.DB_ACCESS_TOKEN];
            //this.appCurrentStateService.login(this.userCO, accessToken);

            if(isEmail)
            {
              this.callDCPScript(responseValues,null,phone);
            }
            else
            {
              this.callDCPScript(responseValues,phone,null);
            } 
            

            if(localStorage.getItem("comment"))
            {
                let postCO:SclPostCO = new SclPostCO();
                postCO = JSON.parse(localStorage.getItem("postCO"))
                postCO.totalComments = String(parseInt(postCO.totalComments)+1);
                let sclPostCommentCO:SclPostCommentCO = new SclPostCommentCO();
                sclPostCommentCO.comment = localStorage.getItem("comment");
                sclPostCommentCO.postId = postCO.id;
                let newCommentApiCall: ApiCall = new ApiCall(ApiActions.savePostComment);
                newCommentApiCall.addRequestParams('sclPostCommentCO', sclPostCommentCO);

                this.blogModuleService.writePostComment(newCommentApiCall).subscribe(response => {
                let result = response;
                    //this.commentCO.emit(result); 
                });
            }
            this.closeLoginPopup();

            this.appCurrentStateService.login(AppUtility.sendToDashboard(), dbAccessToken, null);

/*             if(this.userCO.defaultCategoryId)
            {  
              if(this.adRedirectUrl)
              {
                this.appCurrentStateService.login(true, dbAccessToken, this.adRedirectUrl, null, null);
              }  
              else
              {
                this.appCurrentStateService.login(AppUtility.sendToDashboard(), dbAccessToken, null, null, null);
              }          
              
            }
            else
            {
                if(this.adRedirectUrl)
                {
                    this.appCurrentStateService.login(true, dbAccessToken, this.adRedirectUrl, null, null);
                }
                else
                {
                    this.appCurrentStateService.login(AppUtility.sendToDashboard(), dbAccessToken, null, null, null);
                }
            } */
          }
          else
          {
            this.showValidationError = true;
            this.validationError=responseValues;
            //alert(this.validationError)
          }
      });
  }

  closeLoginPopup()
  {
    $("#loginBtn").modal("hide");
  }

  showUserRegistrationComponent()
  {
    this.loadRegisterComponent.emit(true);
    this.loadLoginComponent1.emit(false);
  }

  loadForgetPasswordComponent()
  {
    this.loadForgotPassComponent.emit(true);
    this.loadLoginComponent1.emit(false);
  }

  callDCPScript(response,phone,email)
  {
    let firstFilterValue = response["firstFilterValue"];
    let firstFilterType = response["firstFilterType"];
    let selectedCategoryTitle = response["categoryName"];

    
    if(firstFilterType===AppConstants.EXAM)
    {
      setAttribute({
        'phone':phone,
        'email':email,
        'CategoryName':selectedCategoryTitle,
        'FirstFilterValue':firstFilterValue,
        'FirstFilterName':firstFilterType,
        'ExamName':firstFilterValue,
        'SubjectName':'-',
        'StreamName':'-',
        'ActivityType':AppConstants.LOGIN
      });
    }
    else if(firstFilterType===AppConstants.SUBJECT)
    {
      setAttribute({
        'phone':phone,
        'email':email,
        'CategoryName':selectedCategoryTitle,
        'FirstFilterValue':firstFilterValue,
        'FirstFilterName':firstFilterType,
        'SubjectName':firstFilterValue,
        'StreamName':'-',
        'ExamName':'-',
        'ActivityType':AppConstants.LOGIN
      });
    }
    else if(firstFilterType===AppConstants.STREAM)
    {
      setAttribute({
        'phone':phone,
        'email':email,
        'CategoryName':selectedCategoryTitle,
        'FirstFilterValue':firstFilterValue,
        'FirstFilterName':firstFilterType,
        'SubjectName':'-',
        'StreamName':firstFilterValue,
        'ExamName':'-',
        'ActivityType':AppConstants.LOGIN
      });
    }
  }

 
 


    

}
