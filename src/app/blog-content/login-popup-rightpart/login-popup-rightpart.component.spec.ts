import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginPopupRightpartComponent } from './login-popup-rightpart.component';

describe('LoginPopupRightpartComponent', () => {
  let component: LoginPopupRightpartComponent;
  let fixture: ComponentFixture<LoginPopupRightpartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginPopupRightpartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPopupRightpartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
