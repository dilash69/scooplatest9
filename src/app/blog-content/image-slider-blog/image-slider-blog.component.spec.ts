import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageSliderBlogComponent } from './image-slider-blog.component';

describe('ImageSliderBlogComponent', () => {
  let component: ImageSliderBlogComponent;
  let fixture: ComponentFixture<ImageSliderBlogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageSliderBlogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageSliderBlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
