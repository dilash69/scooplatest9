import { ActivatedRoute } from '@angular/router';
import { BlogModuleService } from './../../services/blog-module.service';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { CatCategorySlidersCO } from './../../co/categorySliderCO';
import { CatCategoriesCO } from './../../co/categoryCO';
import { BehaviorSubject} from 'rxjs';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-image-slider-blog',
  templateUrl: './image-slider-blog.component.html',
  styleUrls: ['./image-slider-blog.component.css']
})
export class ImageSliderBlogComponent implements OnInit 
{
  private catCategoriesCOBehaviorSubject = new BehaviorSubject<CatCategoriesCO>(null);

  public catCategorySlidersCOArray: CatCategorySlidersCO[];
  @Input()
  set catCategoryCO(value) 
  {
    this.catCategoriesCOBehaviorSubject.next(value);
  };

  get catCategoryCO() 
  {
    return this.catCategoriesCOBehaviorSubject.getValue();
  }

  showNavigationArrows = false;

  constructor(private blogModuleService : BlogModuleService,private route: ActivatedRoute) 
  { 

  }
  
  ngOnInit() 
  {
    this.catCategoriesCOBehaviorSubject
    .subscribe(x => 
      {
        if (this.catCategoryCO) 
        {
          // if(this.catCategoryCO.catCategorySlidersCOList)
          // {
          //   this.catCategorySlidersCOArray = this.catCategoryCO.catCategorySlidersCOList;
          // }
          // else
          // {
          
          //   let getCategoryDetailApiCall: ApiCall = new ApiCall(ApiActions.getCategoryDetailByCategoryId);
          //   getCategoryDetailApiCall.addRequestParams('categoryId', this.catCategoryCO.id);
      
          //   this.blogModuleService.getCategoryDetailByCategoryId(getCategoryDetailApiCall).subscribe(response => {
          //     this.catCategoryCO = response;
          //     this.catCategorySlidersCOArray = this.catCategoryCO.catCategorySlidersCOList;
          //   });
          // }
        }
    });
  }
}
