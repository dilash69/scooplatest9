import { Component, OnInit, Optional, Inject } from '@angular/core';
import { RESPONSE } from '@nguniversal/express-engine/tokens';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})

export class NotFoundComponent implements OnInit {
  constructor(
      @Optional() @Inject(RESPONSE) private response: any
  ) {}

  ngOnInit() {
      this.response.statusCode = 404;
      this.response.statusMessage = '404 - Page Not Found';
  }
}
