import { AppCurrentState } from './../../co/app.current.state';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { BlogModuleService } from './../../services/blog-module.service';
import { CatCategoriesCO } from './../../co/categoryCO';
import { CoursesHeaderCO } from './../../co/coursesHeaderCO';
import { AppSettings } from './../../common/app-settings';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {  Renderer2 } from '@angular/core'; 
import { ManagePopupService } from '../../services/manage-popup.service';
import { AppUtility } from '../../common/app.utility';
import { AppConstants } from './../../common/app.constants';
import { AdjustDrawerService } from './../../services/adjust-drawer.service';



@Component({
  selector: 'app-you-are-studying-mobile-menu',
  templateUrl: './you-are-studying-mobile-menu.component.html',
  styleUrls: ['./you-are-studying-mobile-menu.component.css']
})
export class YouAreStudyingMobileMenuComponent implements OnInit 
{
  @Output() closeYouAreStudyingMobileMenu = new EventEmitter(); 
  public courseHeaderCOList : CoursesHeaderCO[] = new Array();
 // public catCategoriesCOList : CatCategoriesCO[] = new Array();
  public siteUrl: string = AppSettings.MAIN_WEBSITE_HOME;
  public appCurrentState: AppCurrentState = new AppCurrentState();
  public showDownFaIcon:boolean = false;
  public showDownScoopFaIcon:boolean = false;
  public showDownAboutFaIcon:boolean = false;
  public scoopUrl :string = AppSettings.SCOOP_WEBSITE_HOME;
  public showBanner:boolean= false;
  public isPromotionBannerOpen:string;
  

  constructor(
    private managePopupService : ManagePopupService,
    private blogModuleService:BlogModuleService,
    private appCurrentStateService: AppCurrentStateService,private renderer: Renderer2,
    private adjustDrawerService:AdjustDrawerService) 
    { 
        
    }

  ngOnInit() 
  {
    if(this.appCurrentStateService.isBrowser())
    {
      this.isPromotionBannerOpen = localStorage.getItem(AppConstants.isPromotionBannerOpen);
    }
    
    this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => 
    {
        this.appCurrentState = appCurrentState; 

        if(appCurrentState.isUserLoggedIn)
        {
          AppUtility.log('HeaderComponent:User logged in');
        }            
    });
    let getCoursesForHeaderApiCall: ApiCall = new ApiCall(ApiActions.getCoursesForHeader);
    
    this.blogModuleService.getCoursesForHeader(getCoursesForHeaderApiCall).subscribe(response => {
          this.courseHeaderCOList = response;
        });

    this.adjustDrawerService.showHideObservable.subscribe(res => {
      if (res) {
        if (this.appCurrentStateService.isBrowser()) {
          this.isPromotionBannerOpen = localStorage.getItem(AppConstants.isPromotionBannerOpen);
        }
      }
    });

    

   /*  let getCategoryListApiCall: ApiCall = new ApiCall(ApiActions.getCategoryList);
      this.blogModuleService.getCategoryList(getCategoryListApiCall).subscribe(response =>{
            this.catCategoriesCOList = response;
          }); */
  }

  openLoginModal()
  {
      //this.renderer.addClass(document.body, 'modal-open');
      // this.managePopupService.showLoginPopUp(null);
      this.managePopupService.showSignPopUp(null,AppConstants.REGISTRATION);
  } 


  closeStudyMobileMenu()
  {
    if (this.appCurrentStateService.isBrowser()) 
    {
      this.renderer.removeClass(document.body, 'modal-open');
      this.closeYouAreStudyingMobileMenu.emit();
    }
  }

  toggleCoursesIcon()
  {
    this.showDownFaIcon = !this.showDownFaIcon;
  }

  toggleScoopIcon()
  {
    this.showDownScoopFaIcon = !this.showDownScoopFaIcon;
  }

  toggleAboutIcon()
  {
    this.showDownAboutFaIcon = !this.showDownAboutFaIcon;
  }
  addBannerClass(): any 
  {
    return this.isPromotionBannerOpen ==="1" ? 'changeExamMobileMenu-PromoBanner' : 'changeExamMobileMenu';
  }
}
