import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YouAreStudyingMobileMenuComponent } from './you-are-studying-mobile-menu.component';

describe('YouAreStudyingMobileMenuComponent', () => {
  let component: YouAreStudyingMobileMenuComponent;
  let fixture: ComponentFixture<YouAreStudyingMobileMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YouAreStudyingMobileMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YouAreStudyingMobileMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
