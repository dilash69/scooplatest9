import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterFreeTodayComponent } from './register-free-today.component';

describe('RegisterFreeTodayComponent', () => {
  let component: RegisterFreeTodayComponent;
  let fixture: ComponentFixture<RegisterFreeTodayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterFreeTodayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterFreeTodayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
