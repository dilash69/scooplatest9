import { AppConstants } from './../../common/app.constants';
import { CatCategoryFiltersCO } from './../../co/catCategoryFiltersCO';
import { CatSubjectsCO } from './../../co/catSubjectsCO';
import { CatExamsCO } from './../../co/catExamsCO';
import { Component, OnInit, Input } from '@angular/core';
//import { Component, OnInit } from '@angular/core';
import { CatCategoriesCO } from './../../co/categoryCO';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { BlogModuleService } from './../../services/blog-module.service';
import { DataSharingService } from './../../services/data-sharing.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppUtility } from './../../common/app.utility';
import { FilterValueCO } from '../../co/filterValueCO';
import { deserialize } from 'serializer.ts/Serializer';
import { UserRegistrationDetailCO } from './../../co/userRegistrationDetailCO';
import { RegisterUserService } from './../../services/register-user.service';
import { UserCategoryRelationCO } from './../../co/userCategoryRelationCO';
import { UserBrokenLeadsCO } from './../../co/userBrokenLeadsCO';
import { BehaviorSubject } from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import { AppSettings } from './../../common/app-settings';



@Component({
  selector: 'app-register-free-today',
  templateUrl: './register-free-today.component.html',
  styleUrls: ['./register-free-today.component.css']
})
export class RegisterFreeTodayComponent implements OnInit {

  // For captcha..
  public captchaNumber1: number;
  public captchaNumber2: number;
  public isCaptchCorrect = false;
  public modalReference: any;
  public load:boolean=false;
  public otp: number;
  public validationError:string;
  public username:string;
  public emailId:string;
  public mobileNumber:string;
  public catExamsCOArray:CatExamsCO[];
  public catSubjectCOArray:CatSubjectsCO[];
  public filterValueCOList:FilterValueCO[];
  @Input() catCategoryId:string;
  public showValidationError:boolean=false;
  public independentFilterType:string;
  public examId:string;
  public subjectId:string;
  private saveBrokenLeadBS : BehaviorSubject<any> = new BehaviorSubject<string>(null);
  isMandatoryTermsChecked:boolean =true; 
  isWhatappNotificationChecked:boolean = true;
  public siteUrl: string;

  // optionValue is for radio button, true if checked else false;
  public optionValue: boolean = false;
  // optionName is for radio button's lable
  public optionName: string;

  //catObjectCategoryRelationCO:CatObjectCategoryRelationCO = new CatObjectCategoryRelationCO();
  userCategoryRelationCO:UserCategoryRelationCO = new UserCategoryRelationCO();
  catCategoriesCOArray:CatCategoriesCO[] = new Array();
  selectedCategoryTitle :string= AppConstants.Category;
  catCategoryFiltersCOList:CatCategoryFiltersCO[];
  isCategorySelected:boolean = true;
  firstFilterName:String;
  firstFilterValue:String;
  sortedArray1 = new Array();
  sortedArray2 = new Array();
  isUgcNetSelected:boolean = false;

  constructor(private modalService: NgbModal, private blogModuleService: BlogModuleService, private dataSharingService: DataSharingService,private registerUserService:RegisterUserService) {

  }

  ngOnInit() 
  {
    this.siteUrl = AppSettings.MAIN_WEBSITE_HOME;
    this.saveBrokenLeadBS.pipe(debounceTime(AppConstants.autoBrokenLeadApiHitTime))
    .subscribe(term => 
        { 
        if(term != null)
        {
          let saveUserBrokenLeadsApiCall: ApiCall = new ApiCall(ApiActions.saveUserBrokenLeads);
          saveUserBrokenLeadsApiCall.showLoader = false;
          saveUserBrokenLeadsApiCall.addRequestParams('userBrokenLeadsCO', UserBrokenLeadsCO.getAutoBrokenLeadCO(term.target.value,"AUTO_REGISTRATION_FREE_TODAY_POP_UP"));
    
          this.blogModuleService.saveUserBrokenLeads(saveUserBrokenLeadsApiCall).subscribe(userResponseData => {
            
          });
        }
   });

    // Returns a random integer between 0 and 9
    this.captchaNumber1 = Math.floor(Math.random() * 10);
    this.captchaNumber2 = Math.floor(Math.random() * 10);
//start
    let getCategoryFiltersForAdminApiCall: ApiCall = new ApiCall(ApiActions.getCategoryFiltersForAdmin);
    getCategoryFiltersForAdminApiCall.addRequestParams('categoryId', this.catCategoryId);
    this.blogModuleService.getCategoryFiltersForAdmin(getCategoryFiltersForAdminApiCall).subscribe(response => {
      this.catCategoryFiltersCOList = response;
      this.independentFilterType = this.catCategoryFiltersCOList[0].filterType;
      this.getIndependentFilterValues();
    });
//end
    let getExamsByCategoryIdApiCall: ApiCall = new ApiCall(ApiActions.getExamsByCategoryId);
    getExamsByCategoryIdApiCall.addRequestParams('categoryId', this.catCategoryId);
    this.blogModuleService.getExamsByCategoryId(getExamsByCategoryIdApiCall).subscribe(response => {
      this.catExamsCOArray = response;
    });

    let getCategoryListApiCall: ApiCall = new ApiCall(ApiActions.getCategoryList);
    getCategoryListApiCall.showLoader = true;
    this.blogModuleService.getCategoryList(getCategoryListApiCall)
      .subscribe(response =>
      {
        this.catCategoriesCOArray = response;
        this.catCategoriesCOArray = deserialize<CatCategoriesCO[]>(CatCategoriesCO, this.catCategoriesCOArray);

        if(localStorage.getItem("currentCategoryId"))
        {
          for(let i = 0;i<this.catCategoriesCOArray.length;i++)
          {
            if(this.catCategoriesCOArray[i].id === localStorage.getItem("currentCategoryId"))
            {
              this.selectedCategoryTitle = this.catCategoriesCOArray[i].title;
              this.onCategorySelect(this.catCategoriesCOArray[i]);
              break;
            }
          }
        }
      });
  }

  changeExam(exam) 
  {
    let examId = exam.value;
    this.catSubjectCOArray=new Array();
    if(examId!='')
    {
        let getSubjectByExamIdApiCall: ApiCall = new ApiCall(ApiActions.getSubjectByExamId);
        getSubjectByExamIdApiCall.addRequestParams('categoryId', this.catCategoryId);
        getSubjectByExamIdApiCall.addRequestParams('examId', examId);
        this.blogModuleService.getSubjectByExamId(getSubjectByExamIdApiCall).subscribe(response => {
        this.catSubjectCOArray = response;
        });
    }
    else
    {
        this.catSubjectCOArray =null;
    }
  }

  /**
   * Triggered when user click on category's radio button
   * @param optionName: Category label/name from radio button
   */
  checkOptionsValue(optionName) {
    this.optionValue = true;
    this.optionName = optionName;
    AppUtility.log("checkOptionsValue: optionValue: " + this.optionValue);
  }
  
  hideOptions() {
    this.optionValue = false;
  }

  checkCaptcha(captchaNumber) {
    this.isCaptchCorrect = (this.captchaNumber1 + this.captchaNumber2) == captchaNumber;
    AppUtility.log("isCaptchCorrect: " + this.isCaptchCorrect)
  }

  openModal(content) {
    AppUtility.log("content:" + content)
    AppUtility.log("this.isCaptchCorrect:" + this.isCaptchCorrect)
    if (this.isCaptchCorrect) {
      this.modalReference = this.modalService.open(content);
    }
  }

  closeModel() 
  {
    this.modalReference.close();
  }

  numberOnly(event): boolean 
  {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  

  doRegisterAndGenerateOtp(username: string, phoneNo: string) 
  {
    if (this.checkIsCategorySelected()) 
    {
      if (this.checkIfAllFiltersAreSelected()) 
      {
        this.showValidationError = false;
        this.username = username;
        this.mobileNumber = phoneNo;

        let userRegistrationFrontEndApiCall: ApiCall = new ApiCall(ApiActions.userRegistrationFrontEnd);
        userRegistrationFrontEndApiCall.showLoader = true;
        userRegistrationFrontEndApiCall.addRequestParams('name', username);
        userRegistrationFrontEndApiCall.addRequestParams('mobileNumber', phoneNo);
        userRegistrationFrontEndApiCall.addRequestParams('leadFromId', "18");
        userRegistrationFrontEndApiCall.addRequestParams("catObjectCategoryRelationCO",this.userCategoryRelationCO);

        this.blogModuleService.userRegistrationFrontEnd(userRegistrationFrontEndApiCall).subscribe(responseValues => {
          //this.otp = response;
          //alert("generatedOtp: " + this.otp);
          if (responseValues['otp']) {
            this.otp = responseValues['otp'];
            this.load = true;
            //this.registerUserService.updatedUser(this.otp,phone,null);
          }
          else {
            this.validationError = responseValues;
            this.showValidationError = true;
          }
        });
      }
    }
  }

  generateOtp(username: string, phoneNo: string)
  {
    if (this.checkIsCategorySelected()) 
    {
      if (this.checkIfAllFiltersAreSelected()) 
      {
        this.showValidationError = false;

        let generateOtpApiCall: ApiCall = new ApiCall(ApiActions.generateOtp);
        generateOtpApiCall.showLoader = true;
        generateOtpApiCall.addRequestParams('mobileNumber', phoneNo);

        this.blogModuleService.generateOtp(generateOtpApiCall).subscribe(responseValues => 
          {
          if (responseValues.userResponseCode === AppConstants.SUCCESS_REQUEST) 
          {
            let userRegistrationDetailCO:UserRegistrationDetailCO = new UserRegistrationDetailCO();
            userRegistrationDetailCO.isTutorRegistration = "0";
            userRegistrationDetailCO.leadFromId = "18";
            userRegistrationDetailCO.name =username;
            userRegistrationDetailCO.mobileNumber =phoneNo;
            userRegistrationDetailCO.userCategoryRelationCO = this.userCategoryRelationCO;
            
            this.load = true;
            this.registerUserService.updatedUser1(userRegistrationDetailCO);
          }
          else 
          {
            this.showValidationError = true;
            this.validationError = responseValues.userMessageList[0];
          }
        });
      }
    }
  }

  /* getIndependentFilterValues()
  {
        let getIndependentFilterValuesApiCall: ApiCall = new ApiCall(ApiActions.getIndependentFilterValues);
        getIndependentFilterValuesApiCall.addRequestParams('categoryId', this.catCategoryId);
        getIndependentFilterValuesApiCall.addRequestParams('filterType', this.catCategoryFiltersCOList[0].filterType);
        this.blogModuleService.getIndependentFilterValues(getIndependentFilterValuesApiCall).subscribe(response => {
            this.filterValueCOList = response;
        });
  } */

  submitExam(examId)
  {
    this.examId=examId
  }

  submitSubject(subjectId)
  {
    this.subjectId=subjectId
  }

  onCategorySelect(catCategoriesCO:CatCategoriesCO)
  {
    this.assignCategory(catCategoriesCO.title,catCategoriesCO.id);
    let getCategoryFiltersForAdminApiCall: ApiCall = new ApiCall(ApiActions.getCategoryFiltersForAdmin);
    getCategoryFiltersForAdminApiCall.addRequestParams('categoryId', catCategoriesCO.id);
    this.blogModuleService.getCategoryFiltersForAdmin(getCategoryFiltersForAdminApiCall).subscribe(response => {
      this.catCategoryFiltersCOList = response;
      //this.findIndexOfSubjectFilter();
      for (let i = 0; i < 1; i++) 
      {
        this.catCategoryFiltersCOList[i].filterTypeTitle = this.catCategoryFiltersCOList[i].filterType;
      }
      this.getIndependentFilterValues(); 
    });
  }

  assignCategory(categoryTitle:string,categoryId:string)
  {
    this.selectedCategoryTitle = categoryTitle;
    this.userCategoryRelationCO.categoryId = categoryId;
    this.isCategorySelected =true;
  }

  getIndependentFilterValues() 
  {
    for (let i = 0; i < 1; i++) 
    {
      if (this.catCategoryFiltersCOList[i].dependentFilterType === "" || !this.catCategoryFiltersCOList[i].dependentFilterType) 
      {
          let getIndependentFilterValues: ApiCall = new ApiCall(ApiActions.getIndependentFilterValues);
          getIndependentFilterValues.addRequestParams("categoryId", this.catCategoryFiltersCOList[i].categoryId);
          getIndependentFilterValues.addRequestParams("filterType", this.catCategoryFiltersCOList[i].filterType);

          this.blogModuleService.getIndependentFilterValues(getIndependentFilterValues).subscribe(response => {
            this.catCategoryFiltersCOList[i].dropDownFilterValueCOList = response;
            this.isUgcNetSelected = false;
            if(this.catCategoryFiltersCOList[i].categoryId=='2')
            {
              this.catCategoryFiltersCOList[i].dropDownFilterValueCOList =this.filterListForUGCNET(response)
            }
          });
      }
    }
  }

  assignFiltersId(filterType:String,filterValueId,filterValueCO)
  {
    this.firstFilterName = filterType;
    this.firstFilterValue = filterValueCO.filterTypeTitle;
    this.catCategoryFiltersCOList[0].filterTypeTitle = filterValueCO.filterTypeTitle;
    if(filterType===AppConstants.EXAM)
    {
      this.userCategoryRelationCO.examId = filterValueId
    }
    else if(filterType===AppConstants.STREAM)
    {
      this.userCategoryRelationCO.streamId = filterValueId
    }
    else if(filterType===AppConstants.STAGE)
    {
      this.userCategoryRelationCO.stageId = filterValueId
    }
    else if(filterType===AppConstants.SUBJECT)
    {
      this.userCategoryRelationCO.subjectId = filterValueId
    }
    else if(filterType===AppConstants.TOPIC)
    {
      this.userCategoryRelationCO.topicId = filterValueId
    }
    else if(filterType===AppConstants.YEAR)
    {
      this.userCategoryRelationCO.yearId = filterValueId
    }
    else if(filterType===AppConstants.BOARD)
    {
      this.userCategoryRelationCO.boardId = filterValueId
    }
    else if(filterType===AppConstants.MEDIUM_LANGUAGE)
    {
      this.userCategoryRelationCO.mediumLanguageId = filterValueId
    }
    else if(filterType===AppConstants.ZONE)
    {
      this.userCategoryRelationCO.zoneId = filterValueId
    }
  }

  checkIsCategorySelected(): boolean 
  {
    let isValid: boolean = true;
    if (this.userCategoryRelationCO) 
    {
      if(!this.userCategoryRelationCO.categoryId)
      {
        isValid = false;
        this.isCategorySelected = false;
      }
    }
    return isValid;
  }

  checkIfAllFiltersAreSelected(): boolean
  {
    let isValid: boolean = true;
     for (let i = 0; i < 1; i++) 
      {
        isValid = this.checkForFilterValidation(this.catCategoryFiltersCOList[i].filterType,this.catCategoryFiltersCOList[i]);
      }
      return isValid;
  }

  checkForFilterValidation(filterType:string,catCategoryFiltersCO:CatCategoryFiltersCO)
  {
    let isValid:boolean = true;
    if(filterType === AppConstants.EXAM)
    {
      if(!this.userCategoryRelationCO.examId)
      {
        catCategoryFiltersCO.validationMessage = catCategoryFiltersCO.filterType +" " + "is required";
        return false;
      }
    }
    else if(filterType===AppConstants.STREAM)
    {
      if(!this.userCategoryRelationCO.streamId)
      {
        catCategoryFiltersCO.validationMessage = catCategoryFiltersCO.filterType +" " + "is required";
        return false;
      }
    }
    else if(filterType===AppConstants.STAGE)
    {
      if(!this.userCategoryRelationCO.stageId)
      {
        catCategoryFiltersCO.validationMessage = catCategoryFiltersCO.filterType +" " + "is required";
        return false;
      }
    }
    else if(filterType===AppConstants.SUBJECT)
    {
      if(!this.userCategoryRelationCO.subjectId)
      {
        catCategoryFiltersCO.validationMessage = catCategoryFiltersCO.filterType +" " + "is required";
        return false;
      }
    }
    return isValid;
  }

  userRegistrationNew(username: string, phoneNo: string) 
  {
    if (this.checkIsCategorySelected()) 
    {
      if (this.checkIfAllFiltersAreSelected() && this.checkTermsAccepted()) 
      {
        this.showValidationError = false;
        let name = username;
        let phone = phoneNo;
        let userRegistrationNewApiCall: ApiCall = new ApiCall(ApiActions.userRegistrationNew);
        userRegistrationNewApiCall.showLoader = true;
        userRegistrationNewApiCall.addRequestParams('name', name);
        userRegistrationNewApiCall.addRequestParams('mobileNumber', phone);
        userRegistrationNewApiCall.addRequestParams('leadFromId', "18");
        userRegistrationNewApiCall.addRequestParams('userCategoryRelationCO', this.userCategoryRelationCO);

        if(this.isWhatappNotificationChecked)
        {
          userRegistrationNewApiCall.addRequestParams('whatsappNotification', "1");
        }
        else
        {
          userRegistrationNewApiCall.addRequestParams('whatsappNotification', "0");
        }

        this.blogModuleService.userRegistrationNew(userRegistrationNewApiCall).subscribe(userResponseData => {
          if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
          {
            let userRegistrationDetailCO:UserRegistrationDetailCO = new UserRegistrationDetailCO();
            userRegistrationDetailCO.isTutorRegistration = "0";
            userRegistrationDetailCO.mobileNumber =phoneNo;
            userRegistrationDetailCO.categoryName = this.selectedCategoryTitle;
            userRegistrationDetailCO.firstFilterName = this.firstFilterName;
            userRegistrationDetailCO.firstFilterValue = this.firstFilterValue;
            userRegistrationDetailCO.name = name;
            this.load = true;
            this.registerUserService.updatedUser1(userRegistrationDetailCO);
          } 
          else 
          {
            this.showValidationError = true;
            this.validationError = userResponseData.userMessageList[0];
          }
        });
      }
    }
  }

  saveBrokenLeads(event)
  {
    if(event.target.value.length == 10)
    {
      this.saveBrokenLeadBS.next(event);
    }
  }

  checkUncheckTermsCheckBox()
  {
    this.isMandatoryTermsChecked = !this.isMandatoryTermsChecked;
  }

  checkUncheckWhatsAppNotification()
  {
    this.isWhatappNotificationChecked = !this.isWhatappNotificationChecked;
  }

  checkTermsAccepted()
  {
    if(!this.isMandatoryTermsChecked)
    {
      alert("Please accept Terms and Conditions")
      return false;
    }
    return true;
  }

  filterListForUGCNET(filterValueCOList: FilterValueCO[]):FilterValueCO[]
  {

    this.isUgcNetSelected=true;
    
    let filterValueCOList1: FilterValueCO[];
    let filterValueCOList2: FilterValueCO[];

    filterValueCOList1=filterValueCOList.filter(t=>t.isOther==='0')
    filterValueCOList2=filterValueCOList.filter(t=>t.isOther==='1')

    this.sortedArray1 = filterValueCOList1.sort((a, b) => a.sequence - b.sequence);
    this.sortedArray2=filterValueCOList2.sort((a, b) => (a.filterTypeTitle > b.filterTypeTitle) ? 1 : (a.filterTypeTitle < b.filterTypeTitle) ? -1:0)
    return this.sortedArray1.concat(this.sortedArray2);
  
  }

}


