import { AppSettings } from './../../common/app-settings';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit 
{
  public siteUrl: string;
  public scoopUrl: string;
  public showMyElement:boolean;
  public showWhatsappBlock:boolean = false;
  public whatsAppUrl:string;
  public showWhatsAppIcon:boolean = false;

  ngOnInit() 
  {
    this.siteUrl = AppSettings.MAIN_WEBSITE_HOME;
    this.scoopUrl = AppSettings.SCOOP_WEBSITE_HOME;

    
 
    if(this.router.url.toLowerCase()==="/iit-jam" )
    {
      this.whatsAppUrl = "https://wa.me/918003552211?text=Hi Eduncle, I need info about IIT JAM Exam";
      this.showWhatsAppIcon = true;
    }
    else if (this.router.url.toLowerCase()==="/csir-net")
    {
      this.whatsAppUrl = "https://wa.me/918003552211?text=Hi Eduncle, I need info about CSIR NET Exam";
      this.showWhatsAppIcon = true;
    }
    else if (this.router.url.toLowerCase()==="/ugc-net")
    {
      this.whatsAppUrl = "https://wa.me/918003552211?text=Hi Eduncle, I need info about UGC NET Exam";
      this.showWhatsAppIcon = true;
    }
    else if(this.router.url === "/")
    {//home page
      this.whatsAppUrl = "https://wa.me/918003552211?text=Hi Eduncle, I need info about Exam";
      this.showWhatsAppIcon = true;
    }
    else if(!(this.router.url.toLowerCase() === "/csir-net") &&  !(this.router.url.toLowerCase() === "/iit-jam") && !(this.router.url.toLowerCase() === "/ugc-net")
    && !(this.router.url.toLowerCase() === "/engineering-entrance-jee-net")  && !(this.router.url.toLowerCase() === "/medical-entrance-neet")
    && !(this.router.url.toLowerCase() === "/ibps")  &&  !(this.router.url.toLowerCase() === "/ssc")
    &&  !(this.router.url.toLowerCase() === "rrb")  && ((localStorage.getItem("currentCategoryId") === "2") || (localStorage.getItem("currentCategoryId") === "3") || (localStorage.getItem("currentCategoryId") === "4")))
    {
      this.whatsAppUrl = "https://wa.me/918003552211?text=Hi Eduncle, I need more info";
      this.showWhatsAppIcon = true;
    }
    else
    {
      this.whatsAppUrl = "https://wa.me/918003552211?text=Hi Eduncle";
      this.showWhatsAppIcon = false;
    }
   
  }

  constructor(private router: Router) 
    {

    }

  openWhatsappPopup(){
    this.showWhatsappBlock = true;
  }
  closeWhatsappPopup(){
    this.showWhatsappBlock = false;
  }

}
