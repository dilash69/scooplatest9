import { Component, OnInit, Renderer2 } from '@angular/core';
import { ManagePopupService } from '../../services/manage-popup.service';
import { AppPopupCO } from '../../co/app.popup.co';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { AppConstants } from '../../common/app.constants';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit 
{

  public loadRegisterComponent=false;
  public loadForgotPassComponent=false;
  public loadOtpComponent=false;
  public loadLoginComponent1=false;
  public loadTutorComponent=false;
  public categoryId;
  public adRedirectUrl;
  public type:string =AppConstants.REGISTRATION;

    constructor(private managePopupService : ManagePopupService, config: NgbCarouselConfig) {
        config.interval = 5000;
        config.wrap = true;
        config.keyboard = false;
        config.pauseOnHover = false;
        config.showNavigationArrows = false;
    }

  ngOnInit() 
  {
  
        this.categoryId= localStorage.getItem("currentCategoryId");
        this.adRedirectUrl= localStorage.getItem("redirectUrl");
        //localStorage.removeItem('defaultCategoryId');
        localStorage.removeItem('redirectUrl'); 
       
        
  //  localStorage.setItem('isOtherPopupOpen',"true");
  }

  dismiss()
  {
this.managePopupService.closePopUp();
  }

  showUserRegistrationComponent(load:boolean)
  {
    this.type = AppConstants.REGISTRATION;
    /* this.loadRegisterComponent=load;
    this.loadForgotPassComponent=false; */
  }

  showForgotPasswordComponent(load:boolean)
  {
    this.type = AppConstants.FORGOT_PASSWORD;
    /* this.loadForgotPassComponent=load;
    this.loadRegisterComponent=false; */
  }

  showUserOtpComponent(load:boolean)
  {
    this.type = AppConstants.OTP;
    /* this.loadOtpComponent=load; */
  }

  showLoginComponent(load:boolean)
  {
    this.type = AppConstants.LOGIN_RIGHT_PART;
    /* this.loadLoginComponent1=load; */
  }

  showTutorPopup(load:boolean)
  {
   /*  this.loadTutorComponent=load */
  }

  hideTutorRegistration(load:boolean)
  { 
    this.loadTutorComponent=load
    this.loadOtpComponent = true
  }

  showMobileEmailPopUp(event)
  {
      this.type = AppConstants.MOBILE_EMAIL;
  }

  

}
