import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ManagePopupService } from '../../services/manage-popup.service';
import { NgbModalRef, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { AppConstants } from './../../common/app.constants';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-login-popup',
  templateUrl: './login-popup.component.html',
  styleUrls: ['./login-popup.component.css']
})
export class LoginPopupComponent implements OnInit 
{
    public loadRegisterComponent=false;
    public loadForgotPassComponent=false;
    public loadOtpComponent=false;
    public loadTutorComponent=false;
    public loadLoginComponent1=true;
    public categoryId;
    public adRedirectUrl;
    public type:string = AppConstants.LOGIN_RIGHT_PART;

    constructor(private managePopupService : ManagePopupService, 
        private route: ActivatedRoute,config: NgbCarouselConfig) 
    { 
        config.interval = 5000;
        config.wrap = true;
        config.keyboard = false;
        config.pauseOnHover = false;
        config.showNavigationArrows = false;
    }

    ngOnInit() 
    {
        this.categoryId= localStorage.getItem("currentCategoryId");
        this.adRedirectUrl= localStorage.getItem("redirectUrl");
        //localStorage.removeItem('defaultCategoryId');
        localStorage.removeItem('redirectUrl'); 

        //for php tutor form
        if(this.route.snapshot.queryParamMap.get(AppConstants.param_redirect_url))
        {
            this.type ="MOBILE_EMAIL";
        } 
       /*  this.route.paramMap
        .subscribe(params => 
            {
            if(params.get(AppConstants.param_redirect_url))
            {
                this.type ="MOBILE_EMAIL";
            }

        }); */
    }
  

    dismiss()
    {
        this.managePopupService.closePopUp();
    }

    showUserRegistrationComponent(load:boolean)
    {
        this.type = AppConstants.REGISTRATION;
        /* this.loadOtpComponent=false;
        this.loadTutorComponent=false;
        this.loadLoginComponent1=false;
        this.loadRegisterComponent=true;
        this.loadForgotPassComponent=false; */
    }

    showForgotPasswordComponent(load:boolean)
    {
        this.type = AppConstants.FORGOT_PASSWORD;
       /*  this.loadOtpComponent=false;
        this.loadTutorComponent=false;
        this.loadLoginComponent1=false;
        this.loadForgotPassComponent=true;
        this.loadRegisterComponent=false; */
    }

    showUserOtpComponent(load:boolean)
    {
        this.type = AppConstants.OTP;
        /* this.loadRegisterComponent=false;
        this.loadForgotPassComponent=false;
        this.loadTutorComponent=false;
        this.loadLoginComponent1=false;
        this.loadOtpComponent=true; */
    }

    showTutorPopup(load:boolean)
    {
        /* this.loadRegisterComponent=false;
        this.loadForgotPassComponent=false;
        this.loadLoginComponent1=false;
        this.loadTutorComponent=true
        this.loadOtpComponent=false; */
    }

    hideTutorRegistration(load:boolean)
    {
        this.loadTutorComponent=load
        this.loadOtpComponent = true
    }
    showLoginComponent(load:boolean)
    {
        this.type = AppConstants.LOGIN_RIGHT_PART;
        /* this.loadLoginComponent1=true;
        this.loadRegisterComponent=false;
        this.loadForgotPassComponent=false;
        this.loadTutorComponent=false
        this.loadOtpComponent=false; */
    }

    showMobileEmailPopUp(event)
    {
        this.type = AppConstants.MOBILE_EMAIL;
    }
}

