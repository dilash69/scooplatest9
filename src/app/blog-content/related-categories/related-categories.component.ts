import { SclPostCO } from './../../co/sclPostCO';
import { BlogModuleService } from './../../services/blog-module.service';
import { BehaviorSubject} from 'rxjs';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-related-categories',
  templateUrl: './related-categories.component.html',
  styleUrls: ['./related-categories.component.css']
})
export class RelatedCategoriesComponent implements OnInit 
{
  showRelatedCategory = true;
  private sclPostCOBehaviorSubject = new BehaviorSubject<SclPostCO>(null);

  @Input()
  set sclPostCO(value) 
  {
    this.sclPostCOBehaviorSubject.next(value);
  };

  get sclPostCO() 
  {
    return this.sclPostCOBehaviorSubject.getValue();
  }
  constructor(private blogModuleService : BlogModuleService) { 

  }

  ngOnInit() 
  {
    this.sclPostCOBehaviorSubject
      .subscribe(x => {
        if (this.sclPostCO) 
        {
          
        }
      });
  }

}
