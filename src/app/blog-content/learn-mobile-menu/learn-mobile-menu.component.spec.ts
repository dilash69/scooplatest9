import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LearnMobileMenuComponent } from './learn-mobile-menu.component';

describe('LearnMobileMenuComponent', () => {
  let component: LearnMobileMenuComponent;
  let fixture: ComponentFixture<LearnMobileMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LearnMobileMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LearnMobileMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
