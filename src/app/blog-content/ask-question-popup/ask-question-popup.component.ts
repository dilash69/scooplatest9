import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { BlogModuleService } from './../../services/blog-module.service';
import { CmnFeedbackQuestionsCO } from './../../co/cmnFeedbackQuestionsCO';
import { UserFeedbackQuestionAnswers } from './../../co/userFeedbackQuestionAnswers';
import { UserCO } from './../../co/userCO';
import { ManagePopupService } from './../../services/manage-popup.service';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModalConfig, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// import { ApiActions } from 'src/app/common/api.actions';
// import { ApiCall } from 'src/app/common/api.call';
// import { BlogModuleService } from 'src/app/services/blog-module.service';
// import { UserFeedbackQuestionAnswers } from 'src/app/co/userFeedbackQuestionAnswers';
// import { CmnFeedbackQuestionsCO } from 'src/app/co/CmnFeedbackQuestionsCO';
// import { UserCO } from 'src/app/co/userCO';
// import { AppConstants } from 'src/app/common/app.constants';
//import { ManagePopupService } from '../../services/manage-popup.service';

@Component({
  selector: 'app-ask-question-popup',
  templateUrl: './ask-question-popup.component.html',
  styleUrls: ['./ask-question-popup.component.css']
})
export class AskQuestionPopupComponent implements OnInit 
{
  public cmnFeedbackQuestionsCOList:CmnFeedbackQuestionsCO[] = new Array();
  public userFeedbackQuestionAnswersList:UserFeedbackQuestionAnswers[] = new Array();
  public loggedInUserCO: UserCO;

  constructor(  private managePopupService:ManagePopupService,
  private blogModuleService : BlogModuleService, config: NgbModalConfig) 
  {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit()
  {
     this.loadQuestions();
  }


  loadQuestions()
  {  
    let getFeedbackQuestions: ApiCall = new ApiCall(ApiActions.getFeedbackQuestions)
    getFeedbackQuestions.addRequestParams('status', "loadQuestions");
    this.blogModuleService.getFeedbackQuestions(getFeedbackQuestions).subscribe(response => {
        this.cmnFeedbackQuestionsCOList = response
    });
  }



  addQuestionSubmit()
  {
    let msg;
    for(let i=0;i<this.cmnFeedbackQuestionsCOList.length;i++)
    {
      
      let userFeedbackQuestionAnswers: UserFeedbackQuestionAnswers = new UserFeedbackQuestionAnswers();
      userFeedbackQuestionAnswers.answer=this.cmnFeedbackQuestionsCOList[i].answer;
      userFeedbackQuestionAnswers.questionId=this.cmnFeedbackQuestionsCOList[i].id;
      this.userFeedbackQuestionAnswersList.push(userFeedbackQuestionAnswers)
    }


    let setUserFeedbackQuestionAnswers: ApiCall = new ApiCall(ApiActions.setUserFeedbackQuestionAnswers)
    setUserFeedbackQuestionAnswers.addRequestParams('userFeedbackQuestionAnswersCOList', this.userFeedbackQuestionAnswersList);
    this.blogModuleService.setUserFeedbackQuestionAnswers(setUserFeedbackQuestionAnswers).subscribe(response => {

        if (response['message'] == "success") {
           this.dismiss();
        }
    });
    

  }


  dismiss() {
    this.managePopupService.closePopUp();
  } 

}
