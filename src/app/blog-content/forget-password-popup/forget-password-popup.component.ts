import { RegisterUserService } from './../../services/register-user.service';
import { ApiCall } from './../../common/api.call';
import { ApiActions } from './../../common/api.actions';
import { BlogModuleService } from './../../services/blog-module.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UserRegistrationDetailCO } from './../../co/userRegistrationDetailCO';
import { AppConstants } from './../../common/app.constants';

@Component({
    selector: 'app-forget-password-popup',
    templateUrl: './forget-password-popup.component.html',
    styleUrls: ['./forget-password-popup.component.css']
})
export class ForgetPasswordPopupComponent implements OnInit 
{

    public showValidationError:boolean=false;
    public validationError:string;
    @Output() loadOtpComponent = new EventEmitter();
    constructor(private blogModuleService: BlogModuleService,private registerUserService:RegisterUserService) { }

    ngOnInit() 
    {

    }

    forgotPassword(forgotPasswordJsonObj: JSON) 
    {
        let isEmail: boolean = false;
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        isEmail = re.test(String(forgotPasswordJsonObj['email']).toLowerCase());
        let email = forgotPasswordJsonObj['email'];

        let forgotPasswordScoopApicall: ApiCall = new ApiCall(ApiActions.forgotPasswordScoop);
        forgotPasswordScoopApicall.showLoader = true;
        if (isEmail) 
        {
            forgotPasswordScoopApicall.addRequestParams('email', email);
        }
        else 
        {
            forgotPasswordScoopApicall.addRequestParams('mobileNumber', email);
        }

        this.blogModuleService.forgotPasswordScoop(forgotPasswordScoopApicall).subscribe(response => {
            /* if(response['otp'])
            {
                let otp = response['otp'];
                this.loadOtpComponent.emit(true);
                this.registerUserService.updatedUser(otp,email,null,false,null);
            }
            else
            {
                this.showValidationError = true;
                this.validationError=response;
                //alert(this.validationError);
            } */

            if (response.userResponseCode === AppConstants.SUCCESS_REQUEST) 
            {
                let userRegistrationDetailCO:UserRegistrationDetailCO = new UserRegistrationDetailCO();
                userRegistrationDetailCO.isTutorRegistration = "0";
                userRegistrationDetailCO.mobileNumber =email;
                userRegistrationDetailCO.isForgotPassword = true;
                
                this.loadOtpComponent.emit(true);
                this.registerUserService.updatedUser1(userRegistrationDetailCO);
            }
            else
            {
                this.showValidationError = true;
                this.validationError = response.userMessageList[0];
            }
        });
    }

   

    numberOnly(event): boolean 
    {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
        }
        return true;
    }
}
