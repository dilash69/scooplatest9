import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BlogModuleService } from '../../services/blog-module.service';
import { AppUtility } from '../../common/app.utility';


@Component({
  selector: 'app-blog-register-form',
  templateUrl: './blog-register-form.component.html',
  styleUrls: ['./blog-register-form.component.css']
})
export class BlogRegisterFormComponent implements OnInit {

  // For captcha..
  public captchaNumber1: number;
  public captchaNumber2: number;
  public isCaptchCorrect = false;
  public modalReference: any;
  private modalService: NgbModal;
  private blogModuleService: BlogModuleService;

  constructor() {
  }

  ngOnInit() {
    // Returns a random integer between 0 and 9
    this.captchaNumber1 = Math.floor(Math.random() * 10);
    this.captchaNumber2 = Math.floor(Math.random() * 10);
  }

  checkCaptcha(captchaNumber) {
    this.isCaptchCorrect = (this.captchaNumber1 + this.captchaNumber2) == captchaNumber;
    AppUtility.log("isCaptchCorrect: " + this.isCaptchCorrect)
  }

  openModal(content) {
    if (this.isCaptchCorrect) {
      this.modalReference = this.modalService.open(content);
    }
  }

  closeModel() {
    this.modalReference.close();
  }
}
