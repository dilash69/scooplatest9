import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogRegisterFormComponent } from './blog-register-form.component';

describe('BlogRegisterFormComponent', () => {
  let component: BlogRegisterFormComponent;
  let fixture: ComponentFixture<BlogRegisterFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogRegisterFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogRegisterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
