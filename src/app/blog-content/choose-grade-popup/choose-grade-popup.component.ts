import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ApiCall } from './../../common/api.call';
import { ApiActions } from './../../common/api.actions';
import { BlogModuleService } from '../../services/blog-module.service';
import { CatGradesCO } from '../../co/catGradesCO';
import { SelectedGradeCO } from  '../../co/selectedGradeCO';
import { deserialize } from 'serializer.ts/Serializer';
import { CatGradeAssociationsCO } from  '../../co/CatGradeAssociationsCO';
import { FilterValueCO } from  '../../co/filterValueCO';
import { CatSubjectsCO } from    '../../co/catSubjectsCO';
import { CatStreamsCO }  from  '../../co/catStreamsCO';
import { AppConstants } from '../../common/app.constants';
//import { CatObjectCategoryRelationCO } from  '../../co/catObjectCategoryRelationCO';
import { UserTutorCategoryRelationCO } from './../../co/userTutorCategoryRelationCO';
//import { UserTutorCategoryRelationCO } from 'src/app/co/userTutorCategoryRelationCO';


@Component({
  selector: 'app-choose-grade-popup',
  templateUrl: './choose-grade-popup.component.html',
  styleUrls: ['./choose-grade-popup.component.css']
})
export class ChooseGradePopupComponent implements OnInit {
  @Input() modalReference;
  @Input() selectedGradeCOArrayList:SelectedGradeCO[];
  @Output() addGrade = new EventEmitter();
  public catGradesCOList:CatGradesCO[];
 //public selectedGradeCOArrayList:SelectedGradeCO[] = new Array();
  public catGradeAssociationsCOList:CatGradeAssociationsCO[] = new Array();
  public catCategoryFilterValuesCOList:FilterValueCO[] = new Array();
  public catSubjectsCOList:CatSubjectsCO[] = new Array();
  public catStreamsCOList:CatStreamsCO[] = new Array();
  public toShowGrade:boolean= true;
  public selectedGradeCO:SelectedGradeCO = new SelectedGradeCO();
  

  constructor(private blogModuleService:BlogModuleService) { }

  ngOnInit() 
  {
    let getCatGradesApiCall: ApiCall = new ApiCall(ApiActions.getCatGrades);

    this.blogModuleService.getCatGrades(getCatGradesApiCall).subscribe(response => {
      this.catGradesCOList = response;
    });
  }

  onGradeSelect(catGradesCO:CatGradesCO)
  {
    let isGradeAlreadySelected:boolean = false;

    for(let i = 0;i<this.selectedGradeCOArrayList.length;i++)
    {
        if(this.selectedGradeCOArrayList[i].catGradesCO.id===catGradesCO.id)
        {
            alert("This grade is already selected.");
            isGradeAlreadySelected = true;
            break;
        }
    }

    if (!isGradeAlreadySelected) 
    {
      this.selectedGradeCO.catGradesCO = catGradesCO;
      this.toShowGrade = false;
      let getFilterValuesByGradeIdApiCall: ApiCall = new ApiCall(ApiActions.getFilterValuesByGradeId);
      getFilterValuesByGradeIdApiCall.addRequestParams("gradeId",catGradesCO.id);

      this.blogModuleService.getFilterValuesByGradeId(getFilterValuesByGradeIdApiCall).subscribe(response =>{
        this.catGradeAssociationsCOList = <CatGradeAssociationsCO[]>response['catGradeAssociationsList'];
        this.catGradeAssociationsCOList = deserialize<CatGradeAssociationsCO[]>(CatGradeAssociationsCO, this.catGradeAssociationsCOList);

        this.catCategoryFilterValuesCOList = <FilterValueCO[]>response['catCategoryFilterValuesCO'];
        this.catCategoryFilterValuesCOList = deserialize<FilterValueCO[]>(FilterValueCO, this.catCategoryFilterValuesCOList);

        if(catGradesCO.nextFilterType === AppConstants.SUBJECT)
        {
          this.catSubjectsCOList = <CatSubjectsCO[]>response['catSubjectsCO'];
          this.catSubjectsCOList = deserialize<CatSubjectsCO[]>(CatSubjectsCO, this.catSubjectsCOList);
        }
        if(catGradesCO.nextFilterType === AppConstants.STREAM)
        {
          this.catStreamsCOList = <CatStreamsCO[]>response['catStreamsCO'];
          this.catStreamsCOList = deserialize<CatStreamsCO[]>(CatStreamsCO, this.catStreamsCOList);
        }
      });
    }

  }

  onCatSubjectClick(catSubjectsCO: CatSubjectsCO) 
  {
    if (!catSubjectsCO.isSelected) 
    {//when checked
      if (this.catGradeAssociationsCOList[0].type === AppConstants.EXAM) 
      {//EXAM Type
        for (let i = 0; i < this.catCategoryFilterValuesCOList.length; i++) 
        {
          if (catSubjectsCO.id === this.catCategoryFilterValuesCOList[i].filterTypeId) 
          {
            let userTutorCategoryRelationCO: UserTutorCategoryRelationCO = new UserTutorCategoryRelationCO();
            userTutorCategoryRelationCO.examId = this.catCategoryFilterValuesCOList[i].dependentExamId;
            userTutorCategoryRelationCO.categoryId = this.catCategoryFilterValuesCOList[i].categoryId;
            userTutorCategoryRelationCO.subjectId = catSubjectsCO.id;
            this.selectedGradeCO.userTutorCategoryRelationCOList.push(userTutorCategoryRelationCO);
          }
        }
      }
      else if (this.catGradeAssociationsCOList[0].type === AppConstants.CATEGORY) 
      {//CATEGORY TYPE
        for (let i = 0; i < this.catCategoryFilterValuesCOList.length; i++) 
        {
          if (catSubjectsCO.id === this.catCategoryFilterValuesCOList[i].filterTypeId) 
          {
            let userTutorCategoryRelationCO: UserTutorCategoryRelationCO = new UserTutorCategoryRelationCO();
            userTutorCategoryRelationCO.categoryId = this.catCategoryFilterValuesCOList[i].categoryId;
            userTutorCategoryRelationCO.subjectId = catSubjectsCO.id;

            if(this.catCategoryFilterValuesCOList[i].dependentBoardId)
            {
              userTutorCategoryRelationCO.boardId = this.catCategoryFilterValuesCOList[i].dependentBoardId;
            }
            if(this.catCategoryFilterValuesCOList[i].dependentExamId)
            {
              userTutorCategoryRelationCO.examId = this.catCategoryFilterValuesCOList[i].dependentExamId;
            }
            if(this.catCategoryFilterValuesCOList[i].dependentMediumLanguageId)
            {
              userTutorCategoryRelationCO.mediumLanguageId = this.catCategoryFilterValuesCOList[i].dependentMediumLanguageId;
            }
            if(this.catCategoryFilterValuesCOList[i].dependentStageId)
            {
              userTutorCategoryRelationCO.stageId = this.catCategoryFilterValuesCOList[i].dependentStageId;
            }
            if(this.catCategoryFilterValuesCOList[i].dependentStreamId)
            {
              userTutorCategoryRelationCO.streamId = this.catCategoryFilterValuesCOList[i].dependentStreamId;
            }
            if(this.catCategoryFilterValuesCOList[i].dependentTopicId)
            {
              userTutorCategoryRelationCO.topicId = this.catCategoryFilterValuesCOList[i].dependentTopicId;
            }
            if(this.catCategoryFilterValuesCOList[i].dependentYearId)
            {
              userTutorCategoryRelationCO.yearId = this.catCategoryFilterValuesCOList[i].dependentYearId;
            }
            if(this.catCategoryFilterValuesCOList[i].dependentZoneId)
            {
              userTutorCategoryRelationCO.zoneId = this.catCategoryFilterValuesCOList[i].dependentZoneId;
            }
            this.selectedGradeCO.userTutorCategoryRelationCOList.push(userTutorCategoryRelationCO);
          }
        }
      }
      this.selectedGradeCO.selectedSubjectTitleList.push(catSubjectsCO.title);
    }
    else
    {//when unchecked
      if(this.catGradeAssociationsCOList[0].type === AppConstants.EXAM)
      {
          for(let i =0;i<this.selectedGradeCO.userTutorCategoryRelationCOList.length;i++)
          {
              if(this.selectedGradeCO.userTutorCategoryRelationCOList[i].subjectId===catSubjectsCO.id)
              {
                  //this.selectedGradeCO.catObjectCategoryRelationCOList.remove(selectedGradeCO.catObjectCategoryRelationCOList.get(i));
                  this.selectedGradeCO.userTutorCategoryRelationCOList.splice(this.selectedGradeCO.userTutorCategoryRelationCOList.indexOf(this.selectedGradeCO.userTutorCategoryRelationCOList[i]),1);
                  i--;
              }
          }
      }
      else if(this.catGradeAssociationsCOList[0].type === AppConstants.CATEGORY)
      {
          for(let i =0;i<this.selectedGradeCO.userTutorCategoryRelationCOList.length;i++)
          {
              if(this.selectedGradeCO.userTutorCategoryRelationCOList[i].subjectId===catSubjectsCO.id)
              {
                this.selectedGradeCO.userTutorCategoryRelationCOList.splice(this.selectedGradeCO.userTutorCategoryRelationCOList.indexOf(this.selectedGradeCO.userTutorCategoryRelationCOList[i]),1);
                i--;
              }
          }
      }
      this.removeSelectedSubject(catSubjectsCO.title);
    }
  } 

  onCatStreamClick(catStreamsCO: CatStreamsCO) 
  {
    if (!catStreamsCO.isSelected) 
    {//when checked
      if (this.catGradeAssociationsCOList[0].type === AppConstants.CATEGORY) 
      {
        for (let i = 0; i < this.catCategoryFilterValuesCOList.length; i++) 
        {
          if (catStreamsCO.id === this.catCategoryFilterValuesCOList[i].filterTypeId) 
          {
            let userTutorCategoryRelationCO:UserTutorCategoryRelationCO = new UserTutorCategoryRelationCO();
            userTutorCategoryRelationCO.streamId = catStreamsCO.id;
            userTutorCategoryRelationCO.categoryId = this.catCategoryFilterValuesCOList[i].categoryId;

            if(this.catCategoryFilterValuesCOList[i].dependentBoardId)
            {
              userTutorCategoryRelationCO.boardId = this.catCategoryFilterValuesCOList[i].dependentBoardId;
            }
            if(this.catCategoryFilterValuesCOList[i].dependentExamId)
            {
              userTutorCategoryRelationCO.examId = this.catCategoryFilterValuesCOList[i].dependentExamId;
            }
            if(this.catCategoryFilterValuesCOList[i].dependentMediumLanguageId)
            {
              userTutorCategoryRelationCO.mediumLanguageId = this.catCategoryFilterValuesCOList[i].dependentMediumLanguageId;
            }
            if(this.catCategoryFilterValuesCOList[i].dependentStageId)
            {
              userTutorCategoryRelationCO.stageId = this.catCategoryFilterValuesCOList[i].dependentStageId;
            }
            if(this.catCategoryFilterValuesCOList[i].dependentSubjectId)
            {
              userTutorCategoryRelationCO.subjectId = this.catCategoryFilterValuesCOList[i].dependentSubjectId;
            }
            if(this.catCategoryFilterValuesCOList[i].dependentTopicId)
            {
              userTutorCategoryRelationCO.topicId = this.catCategoryFilterValuesCOList[i].dependentTopicId;
            }
            if(this.catCategoryFilterValuesCOList[i].dependentYearId)
            {
              userTutorCategoryRelationCO.yearId = this.catCategoryFilterValuesCOList[i].dependentYearId;
            }
            if(this.catCategoryFilterValuesCOList[i].dependentZoneId)
            {
              userTutorCategoryRelationCO.zoneId = this.catCategoryFilterValuesCOList[i].dependentZoneId;
            }
            this.selectedGradeCO.userTutorCategoryRelationCOList.push(userTutorCategoryRelationCO);
          }
        }
        this.selectedGradeCO.selectedStreamTitleList.push(catStreamsCO.title);
      }
    }
    else 
    {//when unchecked
      if (this.catGradeAssociationsCOList[0].type === AppConstants.CATEGORY) 
      {
        for (let i = 0; i < this.selectedGradeCO.userTutorCategoryRelationCOList.length; i++)
        {
          if (this.selectedGradeCO.userTutorCategoryRelationCOList[i].streamId===catStreamsCO.id) 
          {
            this.selectedGradeCO.userTutorCategoryRelationCOList.splice(this.selectedGradeCO.userTutorCategoryRelationCOList.indexOf(this.selectedGradeCO.userTutorCategoryRelationCOList[i]),1);
            i--;
          }
        }
        this.removeSelectedStream(catStreamsCO.title);
      }
    }
    console.log(this.selectedGradeCO)
  }

  removeSelectedSubject(subjectTitle: string) 
  {
    for (let i = 0; i < this.selectedGradeCO.selectedSubjectTitleList.length; i++)
    {
      if (this.selectedGradeCO.selectedSubjectTitleList[i]=== subjectTitle) 
      {
        this.selectedGradeCO.selectedSubjectTitleList.splice(this.selectedGradeCO.selectedSubjectTitleList.indexOf(this.selectedGradeCO.selectedSubjectTitleList[i]),1);
        //selectedGradeCO.selectedSubjectTitleList.remove(selectedGradeCO.selectedSubjectTitleList.get(i));
        break;
      }
    }
  }

  removeSelectedStream(streamTitle:string)
  {
    for(let i =0;i<this.selectedGradeCO.selectedStreamTitleList.length;i++)
    {
        if(this.selectedGradeCO.selectedStreamTitleList[i]===streamTitle)
        {
          this.selectedGradeCO.selectedStreamTitleList.splice(this.selectedGradeCO.selectedStreamTitleList.indexOf(this.selectedGradeCO.selectedStreamTitleList[i]),1);
            break;
        }
    }
  }

  addNewGrade()
  {
    if(this.selectedGradeCO.selectedSubjectTitleList.length == 0 && this.selectedGradeCO.selectedStreamTitleList.length == 0)
    {
        alert("Select atleast one filter");
    }
    else
    {
      this.addGrade.emit(this.selectedGradeCO);
      this.dismissPopUp();
    }
  }

  dismissPopUp()
  {
    this.modalReference.close();
  }

  getRandomBgColor() {
    let rand= Math.floor(Math.random() * 10) + 1  
   
    let colorClass:string= 'bgcolor'+rand.toString();
    return colorClass;
  }

  

}
