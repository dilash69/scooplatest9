import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseGradePopupComponent } from './choose-grade-popup.component';

describe('ChooseGradePopupComponent', () => {
  let component: ChooseGradePopupComponent;
  let fixture: ComponentFixture<ChooseGradePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseGradePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseGradePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
