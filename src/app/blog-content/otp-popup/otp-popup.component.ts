import { AppCurrentStateService } from './../../services/app.current.state.service';
import { AppSettings } from './../../common/app-settings';
import { UserCO } from './../../co/userCO';
import { AppConstants } from './../../common/app.constants';
import { ApiActions } from './../../common/api.actions';
import { RegisterUserService } from './../../services/register-user.service';
import { ApiCall } from './../../common/api.call';
import { BlogModuleService } from './../../services/blog-module.service';
import { Component, OnInit, Output, ViewChild, Input } from '@angular/core';
import { NgbModal, NgbModalConfig, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventEmitter } from 'events';
import { AppUtility } from './../../common/app.utility';
import { Router } from '@angular/router';
import { TutorDetailService } from './../../services/tutor-detail.service';
import { ManagePopupService } from './../../services/manage-popup.service';
import { UserRegistrationDetailCO } from './../../co/userRegistrationDetailCO';
import { UserBrokenLeadsCO } from './../../co/userBrokenLeadsCO';

declare const setAttribute: any;




@Component({
  selector: 'app-otp-popup',
  templateUrl: './otp-popup.component.html',
  styleUrls: ['./otp-popup.component.css']
})
export class OtpPopupComponent implements OnInit {

  public otp:string;
  //public userOtp:string;
  public userPhoneNo:string;
  public message:string;
  public phoneNumberLast3Digits: string;
  public timeLeft: number;
  public timeLeftOrVerifyText: string;
  public interval;
  public isValidOtp = false;
  public userCO: UserCO;
  public validationError:string;
  public showValidationError:boolean=false;
  public isTutorRegistration:string;
  public userRegistrationDetailCO:UserRegistrationDetailCO;
  public accessToken:string;
  public isResendClicked:boolean = false;
  public isTutorAutoVerifyOtp :boolean = false;
  public autoVerifyOtptime:string;
  public isAutoVerifyOtp = false;

  //public userId:string;
  

  // @Output() loadOtpComponent = new EventEmitter();
   @ViewChild('contentOtp',{static: false}) private content;
  modalReference: NgbModalRef;

  @Input() ack:any;

  constructor(private blogModuleService : BlogModuleService,
    private appCurrentStateService : AppCurrentStateService,
    private registerUserService:RegisterUserService,
    private  router:  Router,
    private tutorDetailService: TutorDetailService,
    private managePopUpService:ManagePopupService,
    config: NgbModalConfig, private modalService: NgbModal) {
      config.backdrop = 'static';
      config.keyboard = false;
     }

  ngOnInit() 
  {
    if(this.ack)
    {
      this.open(this.content);
    }
    //
   // this.openOtpPopUp(this.content);

    this.registerUserService.otp.subscribe(uotp=> {
      this.otp=uotp;
       this.registerUserService.phone.subscribe(uphone=> {
        this.userPhoneNo=uphone;
        
        if(this.userPhoneNo)
        {
          this.phoneNumberLast3Digits = this.userPhoneNo as string;
          this.phoneNumberLast3Digits = this.phoneNumberLast3Digits.substring(this.phoneNumberLast3Digits.length - 3, this.phoneNumberLast3Digits.length);
        
          this.startTimer();
        }
      });  
    });

     
      this.registerUserService.message.subscribe(message=> {
        this.message=message;
        });

        this.registerUserService.isTutorRegistration.subscribe(isTutorRegistration=> {
          this.isTutorRegistration=isTutorRegistration;
          });

         

            this.registerUserService.userRegistrationDetailObservable.subscribe(userRegistrationDetailCO=> {
              this.userRegistrationDetailCO=userRegistrationDetailCO;
              this.phoneNumberLast3Digits = this.userRegistrationDetailCO.mobileNumber;
              this.phoneNumberLast3Digits = this.phoneNumberLast3Digits.substring(this.phoneNumberLast3Digits.length - 3, this.phoneNumberLast3Digits.length);
              this.startTimer();
              this.isTutorRegistration = this.userRegistrationDetailCO.isTutorRegistration;
              this.message = this.userRegistrationDetailCO.message;

               if(this.userRegistrationDetailCO.isTutorRegistration==="0")
              {
                this.callDCPScript('0');
              } 
              });


  }

  public openOtpPopUp(content)
  {
      // this.modalService.open(content, { centered: true });
     //this.modalReference= this.modalService.open(content, { windowClass : 'entryPopupCustomModal', centered: true });
     
 
    }

  open(content) {
    this.modalService.open(content, { centered: true });
  }

  startTimer() 
  {
    // initialize below variables with default time
    this.timeLeft = AppConstants.OTP_VALID_TIME;
    this.timeLeftOrVerifyText = "00:" + this.timeLeft;

    this.interval = setInterval(() =>
     {
      if (this.timeLeft > 0) 
      {
        this.timeLeft--;
        this.timeLeftOrVerifyText = "00:" + ((this.timeLeft < 10) ? "0" + this.timeLeft : "" + this.timeLeft);
        
         if(this.timeLeft == this. getAutoVerifyOtpTime() && !this.userRegistrationDetailCO.isForgotPassword && this.isResendClicked)
        {
          this.isAutoVerifyOtp = true;
          this.verifyOtpNew(null,true);
        } 
      } 
      else {
        clearInterval(this.interval);
      }
    }, 1000)
  }


  submitPasswordForm(form: JSON) 
  {
    this.showValidationError = false;
    if(form['password'].length < AppConstants.MIN_PASSWORD_LENGTH) 
    {
      //alert("Password should be " + AppConstants.MIN_PASSWORD_LENGTH + " characters long.");
      this.showValidationError = true;
      this.validationError = "Password should be " + AppConstants.MIN_PASSWORD_LENGTH + " characters long.";
      return;
    }

    if (form['password'] != form['confirmPassword']) 
    {
      //alert("Password and confirm password should be same.");
      this.showValidationError = true;
      this.validationError = "Password and confirm password should be same.";
      return;
    }

    if (this.userRegistrationDetailCO.isTutorRegistration === '1') 
    {//tutor registration 
      this.managePopUpService.closePopUp();
      this.redirectToTutorForm(form['password'],null);
    }
    else
    {
      let createUserPassword: ApiCall = new ApiCall(ApiActions.createUserPassword);
      createUserPassword.showLoader = true;
      createUserPassword.addRequestParams(AppConstants.DB_ACCESS_TOKEN, localStorage.getItem(AppConstants.DB_ACCESS_TOKEN));
      createUserPassword.addRequestParams('password', form['password']);
      createUserPassword.addRequestParams(AppConstants.ACCESS_TOKEN, this.accessToken);
  
      this.blogModuleService.createUserPassword(createUserPassword).subscribe(response => 
        {
          if (response === AppConstants.PASSWORD_CREATED_SUCCESSFULLY) 
          {

            if(!AppUtility.sendToDashboard() && !this.isAutoVerifyOtp)
            {
              this.callDCPScript('1');
            }
            if('USER_QUERY' === this.userRegistrationDetailCO.message)
            {
              this.appCurrentStateService.login(true, localStorage.getItem(AppConstants.DB_ACCESS_TOKEN), "?action=query_popup");
            }
            else
            {
              this.appCurrentStateService.login(AppUtility.sendToDashboard(), localStorage.getItem(AppConstants.DB_ACCESS_TOKEN), null);
            }
            
            //window.location.href = AppSettings.MAIN_WEBSITE_HOME+"/email-otp-instruction?getdefaultcat";
          } else 
          {
            //alert('Somthing went wrong, please try again..')
            this.showValidationError = true;
            this.validationError = "Somthing went wrong, please try again..";
          }
      });
    }
  }

 
  regenerateOtp()
  {
    this.startTimer();
    this.isResendClicked = true;
    this.showValidationError = false;

    let userBrokenLeadsCO : UserBrokenLeadsCO = new UserBrokenLeadsCO();
    if(this.appCurrentStateService.isBrowser())
    {
      userBrokenLeadsCO.url = window.location.href;
    }
    
    let regenerateOtpApiCall: ApiCall = new ApiCall(ApiActions.regenerateOtp);
    regenerateOtpApiCall.showLoader = true;
    regenerateOtpApiCall.addRequestParams('mobileNumber', this.userRegistrationDetailCO.mobileNumber);
    regenerateOtpApiCall.addRequestParams('userBrokenLeadsCO', userBrokenLeadsCO);

    if(this.userRegistrationDetailCO.isTutorRegistration==="1")
    {
      regenerateOtpApiCall.addRequestParams('isForTutorRegistration', "1");
    }
    else
    {
      regenerateOtpApiCall.addRequestParams('isForTutorRegistration', "0");
    } 

        this.blogModuleService.regenerateOtp(regenerateOtpApiCall).subscribe(response => 
          {
            if (response.userResponseCode === AppConstants.SUCCESS_REQUEST) 
            {
              this.autoVerifyOtptime = response.responseValues["autoVerifyOtpTime"];
            }
          //localStorage.setItem(AppConstants.LOGGED_IN_USER_CO, JSON.stringify(this.userCO).toString());
        });
  }

  requestCallbackRegistration() 
  {
    let contactUsQueriesUserApiCall: ApiCall = new ApiCall(ApiActions.contactUsQueriesUser);
    contactUsQueriesUserApiCall.showLoader = true;
    contactUsQueriesUserApiCall.addRequestParams('cmnContactUsQueriesCO', this.userRegistrationDetailCO.cmnContactUsQueriesCO);
    contactUsQueriesUserApiCall.addRequestParams('leadFromId',  this.userRegistrationDetailCO.leadFromId );
    contactUsQueriesUserApiCall.addRequestParams('userCategoryRelationCO',  this.userRegistrationDetailCO.userCategoryRelationCO);
    

    this.blogModuleService.contactUsQueriesUser(contactUsQueriesUserApiCall).subscribe(responseValues => {
       this.accessToken=responseValues[AppConstants.ACCESS_TOKEN];
    });
  }

  verifyForgotPasswordOtp(userOtp:string)
  {
    let verifyOtpForgotPasswordApiCall: ApiCall = new ApiCall(ApiActions.verifyOtpForgotPassword);
    verifyOtpForgotPasswordApiCall.showLoader = true;
    verifyOtpForgotPasswordApiCall.addRequestParams('otp', userOtp);
    verifyOtpForgotPasswordApiCall.addRequestParams('mobileNumber', this.userRegistrationDetailCO.mobileNumber);
        this.blogModuleService.verifyOtpForgotPassword(verifyOtpForgotPasswordApiCall).subscribe(response => 
          {
          if (response.userResponseCode === AppConstants.SUCCESS_REQUEST) 
          {
            this.timeLeft = 0;
            clearInterval(this.interval);
            this.timeLeftOrVerifyText =  "OTP Verified" ;
            this.showValidationError = false;
            this.isValidOtp = true;
            this.accessToken=response.responseValues[AppConstants.ACCESS_TOKEN];
          }
            else 
            {
              this.showValidationError = true;
              this.validationError = response.userMessageList[0];
            }
            
        });
  }

  verifyOtpNew(userOtp:string,isAutoVerifyOtp:boolean)
  {
    if((userOtp && userOtp.length == 4) || isAutoVerifyOtp)
    { 
      if(this.userRegistrationDetailCO.isForgotPassword)
      {
        this.verifyForgotPasswordOtp(userOtp);
      }
      else if(this.userRegistrationDetailCO.isTutorRegistration === "0")
      {
        let verifyOtpApiNewCall: ApiCall = new ApiCall(ApiActions.verifyOtpNew);
        verifyOtpApiNewCall.showLoader = true;
        verifyOtpApiNewCall.addRequestParams('mobileNumber', this.userRegistrationDetailCO.mobileNumber);
        if(isAutoVerifyOtp)
        {
          verifyOtpApiNewCall.addRequestParams("isVerifyOtpNew","1");
        }
        else
        {
          verifyOtpApiNewCall.addRequestParams('otp', userOtp);
        }
        this.blogModuleService.verifyOtpNew(verifyOtpApiNewCall).subscribe(response => 
          {
          if (response.userResponseCode === AppConstants.SUCCESS_REQUEST) 
          {
            localStorage.setItem(AppConstants.DB_ACCESS_TOKEN, response.responseValues[AppConstants.DB_ACCESS_TOKEN]);
            this.timeLeft = 0;
            clearInterval(this.interval);
            this.timeLeftOrVerifyText =  "OTP Verified" ;
            this.showValidationError = false;
            this.isValidOtp = true;

            this.accessToken=response.responseValues[AppConstants.ACCESS_TOKEN]
  
            /* if(this.userRegistrationDetailCO.isTutorRegistration==='0' && this.userRegistrationDetailCO.cmnContactUsQueriesCO)
            {
              this.requestCallbackRegistration();
            } */
            
          }
            else 
            {
              this.showValidationError = true;
              this.validationError = response.userMessageList[0];
            }
            
        });
      }
      else if (this.userRegistrationDetailCO.isTutorRegistration === "1") 
      {
        let verifyOtpApiCall: ApiCall = new ApiCall(ApiActions.verifyOtp);
        verifyOtpApiCall.showLoader = true;
        verifyOtpApiCall.addRequestParams('mobileNumber', this.userRegistrationDetailCO.mobileNumber);
        if(isAutoVerifyOtp)
        {
          verifyOtpApiCall.addRequestParams("isVerifyOtpNew","1");
          this.isTutorAutoVerifyOtp = true;
        }
        else
        {
          verifyOtpApiCall.addRequestParams('otp', userOtp);
        }
        this.blogModuleService.verifyOtp(verifyOtpApiCall).subscribe(response => {
          if (response.userResponseCode === AppConstants.SUCCESS_REQUEST) 
          {
            this.timeLeft = 0;
            clearInterval(this.interval);
            this.timeLeftOrVerifyText = "OTP Verified";
            this.showValidationError = false;
            this.isValidOtp = true;

            if(response.responseValues["userId"])
            {
              this.managePopUpService.closePopUp();
              this.redirectToTutorForm(null,response.responseValues["userId"]);
            }
           
          }
          else {
            this.showValidationError = true;
            this.validationError = response.userMessageList[0];
          }
        });
      }
    }
    

  }

  redirectToTutorForm(password:string,userId:string)
  {
    let userCO: UserCO = new UserCO();
    userCO.mobile = this.userRegistrationDetailCO.mobileNumber;
    userCO.email = this.userRegistrationDetailCO.emailId;
    userCO.newAccessTokenRequired = "1";
    userCO.whatsappNotification= this.userRegistrationDetailCO.isWhatsAppNotifChecked;
    
    if(password)
    {
      userCO.password = password;
    }
    if(userId)
    {
      userCO.id = userId;
    }
    
    if(this.userRegistrationDetailCO.redirectUrl && this.userRegistrationDetailCO.redirectUrl.length>0)
    {
      userCO.redirectUrl = this.userRegistrationDetailCO.redirectUrl;
    }
    if(this.isTutorAutoVerifyOtp)
    {
      userCO.isTutorAutoVerifyOtp = "1";
    }
    
    this.tutorDetailService.updateTutorDetail(userCO);
    this.router.navigate(["tutor/registration"]); 
  }

  getAutoVerifyOtpTime():number
  {
    let verifyTime :number;
    if(this.autoVerifyOtptime)
    {
      verifyTime = 45 - (+this.autoVerifyOtptime);
    }
    else
    {
      verifyTime = 25;
    }
    return verifyTime;
  }

   callDCPScript(isMobileVerified)
  {
    console.log("engage",this.userRegistrationDetailCO.firstFilterName);
    if(this.userRegistrationDetailCO.firstFilterName===AppConstants.EXAM)
    {
      console.log("engage1",this.userRegistrationDetailCO.firstFilterName);
      setAttribute({
        'phone':this.userRegistrationDetailCO.mobileNumber,
        'name':this.userRegistrationDetailCO.name,
        'CategoryName':this.userRegistrationDetailCO.categoryName,
        'FirstFilterValue':this.userRegistrationDetailCO.firstFilterValue,
        'FirstFilterName':this.userRegistrationDetailCO.firstFilterName,
        'isMobileVerified':isMobileVerified,
        'ExamName':this.userRegistrationDetailCO.firstFilterValue,
        'ActivityType':AppConstants.REGISTRATION
      });
    }
    else if(this.userRegistrationDetailCO.firstFilterName===AppConstants.SUBJECT)
    {
      console.log("engage2",this.userRegistrationDetailCO.firstFilterName);
      setAttribute({
        'phone':this.userRegistrationDetailCO.mobileNumber,
        'name':this.userRegistrationDetailCO.name,
        'CategoryName':this.userRegistrationDetailCO.categoryName,
        'FirstFilterValue':this.userRegistrationDetailCO.firstFilterValue,
        'FirstFilterName':this.userRegistrationDetailCO.firstFilterName,
        'isMobileVerified':isMobileVerified,
        'SubjectName':this.userRegistrationDetailCO.firstFilterValue,
        'ActivityType':AppConstants.REGISTRATION
      });
    }
    else if(this.userRegistrationDetailCO.firstFilterName===AppConstants.STREAM)
    {
      console.log("engage3",this.userRegistrationDetailCO.firstFilterName);
      setAttribute({
        'phone':this.userRegistrationDetailCO.mobileNumber,
        'name':this.userRegistrationDetailCO.name,
        'CategoryName':this.userRegistrationDetailCO.categoryName,
        'FirstFilterValue':this.userRegistrationDetailCO.firstFilterValue,
        'FirstFilterName':this.userRegistrationDetailCO.firstFilterName,
        'isMobileVerified':isMobileVerified,
        'StreamName':this.userRegistrationDetailCO.firstFilterValue,
        'ActivityType':AppConstants.REGISTRATION
      });
    }
  }
}
