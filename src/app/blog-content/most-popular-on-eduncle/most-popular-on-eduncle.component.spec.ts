import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostPopularOnEduncleComponent } from './most-popular-on-eduncle.component';

describe('MostPopularOnEduncleComponent', () => {
  let component: MostPopularOnEduncleComponent;
  let fixture: ComponentFixture<MostPopularOnEduncleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostPopularOnEduncleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostPopularOnEduncleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
