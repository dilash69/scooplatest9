import { AppUtility } from './../../common/app.utility';
import { SclPostCO } from './../../co/sclPostCO';
import { ListCategoriesService } from './../../list-categories.service';
import { DetailedCategoryForHomeCO } from './../../co/detailedCategoryForHomeCO';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { BlogModuleService } from './../../services/blog-module.service';
import { Component, OnInit } from '@angular/core';
import { CatCategoriesCO } from '../../co/categoryCO';
import { AppConstants } from './../../common/app.constants';


@Component({
  selector: 'app-most-popular-on-eduncle',
  templateUrl: './most-popular-on-eduncle.component.html',
  styleUrls: ['./most-popular-on-eduncle.component.css']
})
export class MostPopularOnEduncleComponent implements OnInit 
{
  catCategoriesCOArray:CatCategoriesCO[] = new Array();
  sclPostCOList:SclPostCO[];

  detailedCategoriesArray=[];
  detailCategoryForHomeCO:DetailedCategoryForHomeCO;
  detailedCategoryForHomeCOList: DetailedCategoryForHomeCO[];
 
  defaultImageSquare:String = AppConstants.DEFAULT_IMAGE_SQUARE;
  defaultImageCircle:String = AppConstants.DEFAULT_IMAGE_CIRCLE;

  constructor(private blogModuleService : BlogModuleService,private listCategoriesService:ListCategoriesService) { }

  ngOnInit() 
  {
    this.listCategoriesService.catCategoriesCOArray.subscribe(res => 
    {
        this.catCategoriesCOArray = res;
        if(this.catCategoriesCOArray && this.catCategoriesCOArray.length > 0)
        {          
          for(let i=0;i<this.catCategoriesCOArray.length;i++)
          {
            this.detailedCategoriesArray.push(this.catCategoriesCOArray[i].id);  
          }
          
          let getDetailedCategoriesForHomeApiCall: ApiCall = new ApiCall(ApiActions.getDetailedCategoriesForHome);
          getDetailedCategoriesForHomeApiCall.addRequestParams('categoryIdList', JSON.stringify(this.detailedCategoriesArray));
          this.blogModuleService.getDetailedCategoriesForHome(getDetailedCategoriesForHomeApiCall).subscribe(response => {
            this.detailedCategoryForHomeCOList = response;
          });
        }
    });
  }

  getDateStringFromMilliseconds(displayDate:string): String 
  {
    return AppUtility.getDateStringFromMilliseconds(Number(displayDate));
  }
  
  public getResponsiveImage(imagePath:string):string {
    return AppUtility.getResponsiveImage(imagePath);
   }

  // changeImageSizes(featuredImage:string):string
  // {
  //   featuredImage = featuredImage.replace('.png','-113x91.png');
  //   featuredImage = featuredImage.replace('.jpg','-113x91.jpg');
  //   return featuredImage;
  // }
}