import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SclPostAttachmentsCO} from '../../co/SclPostAttachmentsCO';
import { NgbCarouselConfig, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AppPopupCO } from '../../co/app.popup.co';
import { ManagePopupService } from '../../services/manage-popup.service';
import { AppUtility } from '../../common/app.utility';
import { AppConstants } from '../../common/app.constants';

@Component({
  selector: 'app-image-grid-layouts',
  templateUrl: './image-grid-layouts.component.html',
  styleUrls: ['./image-grid-layouts.component.css']
})
export class ImageGridLayoutsComponent implements OnInit {
  @Input() sclPostAttachmentsCOList:SclPostAttachmentsCO[];
  @Input() postType:string;
  sclPostImageAttachmentsCOList:SclPostAttachmentsCO[] = new Array();
  modalReference: NgbModalRef;
  showNavigationArrows = false;
  showNavigationIndicators = false;
  moreImageCount : number = 0;
  images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);
  public appPopupCO: AppPopupCO = new AppPopupCO();
  defaultImageSquare:String = AppConstants.DEFAULT_IMAGE_SQUARE;

  constructor(public sanitizer: DomSanitizer,
    private modalService: NgbModal, config: NgbCarouselConfig,
    private managePopupService: ManagePopupService) 
    {
      config.showNavigationArrows = true;  
      config.showNavigationIndicators = false;
    }

  ngOnInit() 
  {    
    
    if(this.sclPostAttachmentsCOList.length > 3)
    {
      this.moreImageCount = this.sclPostAttachmentsCOList.length - 3;
    }

    for(let sclPostAttachmentsCO of this.sclPostAttachmentsCOList)
    {      
        if(sclPostAttachmentsCO.type && sclPostAttachmentsCO.type.includes('image'))
        {
          this.sclPostImageAttachmentsCOList.push(sclPostAttachmentsCO);
        }       
    }
  }

  showMoreImageCount() : boolean
  {
    if(this.sclPostAttachmentsCOList.length > 3)
    {
      return true;
    }
    else{
      return false;
    }
  }

  getImageSize() : number
  {
    let length:number = 0;
    for(let sclPostAttachmentsCO of this.sclPostAttachmentsCOList)
    {
      
        if(sclPostAttachmentsCO.type && sclPostAttachmentsCO.type.includes('image'))
        {
          length = length + 1;
        }
       
    }
   
    return length;
  }

  showGridType(gridType:string):boolean
  {
    let showGrid:boolean = true;
            
    switch (gridType) {
      case "TWO_PORTRAIT_IMAGES":
        for(let sclPostAttachmentsCO of this.sclPostImageAttachmentsCOList)
        {
            if(!sclPostAttachmentsCO.isPortrait())
            {
              showGrid = false;
              break;
            }
        }
        break;

        case "TWO_LANDSCAPE_IMAGES":
        for(let sclPostAttachmentsCO of this.sclPostImageAttachmentsCOList)
        {
            if(sclPostAttachmentsCO.isPortrait())
            {
              showGrid = false;
              break;
            }
        }
        break;

        case "TWO_RANDOM_IMAGES":
          showGrid = false;
          if(this.sclPostImageAttachmentsCOList.length > 1)
          {
            if(this.sclPostImageAttachmentsCOList[0].isPortrait())
            {
              if(!this.sclPostImageAttachmentsCOList[1].isPortrait())
              {
                showGrid = true;
                break;
              }
            }
            else
            {
              if(this.sclPostImageAttachmentsCOList[1].isPortrait())
              {
                showGrid = true;
                break;
              }
            }
          }
          break;

          case "FIRST_LANDSCAPE_TWO_RANDOM":
          showGrid = false;
            if(!this.sclPostImageAttachmentsCOList[0].isPortrait())
            {
                  showGrid = true;
                  //console.log("FIRST_LANDSCAPE_TWO_RANDOM");
                  break;
            }
            break;

          case "FIRST_PORTRAIT_TWO_RANDOM":
          showGrid = false;
            if(this.sclPostImageAttachmentsCOList[0].isPortrait())
            {
                  showGrid = true;
                // console.log("FIRST_PORTRAIT_TWO_RANDOM");
                  break;
            }
            break;

          default:
            showGrid = false;
    }

    return showGrid;
  }

  public getFileSize(sizeParam: string): string {
    let sizeAct;
    let unit;
    let size = +sizeParam;
    if (size < 1000) {
      sizeAct = size;
      unit = "bytes";
    } else if (size < 1000 * 1000) {
      sizeAct = size / 1000;
      unit = "kb";
    } else if (size < 1000 * 1000 * 1000) {
      sizeAct = size / 1000 / 1000;
      unit = "mb";
    } else {
      sizeAct = size / 1000 / 1000 / 1000;
      unit = "gb";
    }
    return sizeAct + " " + unit;
  }





  openLg(content) 
  {
    this.modalService.open(content, { size: 'lg' });
  }

  /* openImageSliderPopUp(content,sclPostAttachmentsCO:SclPostAttachmentsCO) {
    

    if(sclPostAttachmentsCO.type.includes('image'))
    {
      this.modalService.open(content, { size: 'lg', windowClass: 'modalCenter-custom' });
    }
    else
    {
      window.open(sclPostAttachmentsCO.url, '_blank');
    }

 //   this.modalReference = this.modalService.open(content, { centered: true });
  } */
  openImageSliderPopUp(position:string) 
  {
    if (!this.appPopupCO.isAnyPopupOpen())
     {
      
      this.managePopupService.showImagePopUp(this.sclPostImageAttachmentsCOList,position);
    }
  }

  openPdf(position:any)
  {
    window.open(this.sclPostAttachmentsCOList[position].url, '_blank');
  }

  getVideoUrl(url:string):string
  {
    return AppUtility.DcpVideoUrl(url)
  }
  
  public getResponsiveImage(imagePath:string):string {
    return AppUtility.getResponsiveImage(imagePath);
   }

}
