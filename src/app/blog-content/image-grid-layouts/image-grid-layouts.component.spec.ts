import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageGridLayoutsComponent } from './image-grid-layouts.component';

describe('ImageGridLayoutsComponent', () => {
  let component: ImageGridLayoutsComponent;
  let fixture: ComponentFixture<ImageGridLayoutsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageGridLayoutsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageGridLayoutsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
