import { Component, OnInit, ViewChild } from '@angular/core';
import { UserCO } from '../../co/userCO';
import { AppConstants } from '../../common/app.constants';
import { BlogModuleService } from '../../services/blog-module.service';
import { ApiCall } from '../../common/api.call';
import { ApiActions } from '../../common/api.actions';
import { CatCategoriesCO } from '../../co/categoryCO';
import { AppCurrentStateService } from '../../services/app.current.state.service';
import { AppCurrentState } from '../../co/app.current.state';
import { CmsTagsCO } from '../../co/cmsTagCO';
import { CmsSearchSuggestionCO } from '../../co/cmsSearchSuggestionCO';
import { AppSettings } from '../../common/app-settings';

@Component({
    selector: 'app-mobile-header',
    templateUrl: './mobile-header.component.html',
    styleUrls: ['./mobile-header.component.css']
})
export class MobileHeaderComponent implements OnInit
 {
    public catCategoriesCOList :CatCategoriesCO[];
    public appCurrentState : AppCurrentState = new AppCurrentState();
    public panelArray = AppConstants.PANEL_ARRAY;
    public cmsTagsCOList:CmsTagsCO[]= new Array();//suggested
    public catCategoriesCOListSearch:CatCategoriesCO[] = new Array();//scoop
    public cmsSearchSuggestionCOList:CmsSearchSuggestionCO[] =new Array();//store
    public selectedOptionName = AppConstants.ALL;
    public searchInputTextvalues : string;
    public isStore : boolean;
    public isSuggested : boolean;
    public isScoop : boolean;
    public isMenuShow : boolean;
    public scoopUrl: string=AppSettings.SCOOP_WEBSITE_HOME;

    public shouldShow: boolean = false;
    @ViewChild("myLabel") lab;

    public teachShow: boolean = false;
    @ViewChild("teachMenu") teachMenu;

    public showMobileMenu: boolean = false;
    @ViewChild("mobileMenu") mobileMenu;

    public showMobileSearchBar: boolean = false;
    @ViewChild("mobileSearchBar") mobileSearchBar;

    constructor(private blogModuleService : BlogModuleService,public appCurrentStateService: AppCurrentStateService) { }

    ngOnInit()
     {
        let getMostPopularArticleListApiCall: ApiCall = new ApiCall(ApiActions.getFilterStream);
       
        this.blogModuleService.getFilteredStream(getMostPopularArticleListApiCall).subscribe(response => 
        {
          this.catCategoriesCOList = response;
          //AppUtility.log(this.catCategoriesCOList);
        });
    }

    searchKeyWordEvent(event: any)
    {
        this.searchInputTextvalues = event.target.value;
        if(this.searchInputTextvalues.length != 0)
        {
            this.searchApiCall();
            this.isMenuShow = true;
        }
        else
        {
            this.isMenuShow = false;
        }
    }


    searchApiCall()
    {
        let newSearchApiCall: ApiCall = new ApiCall(ApiActions.searchKeywords);
        newSearchApiCall.addRequestParams('searchText', this.searchInputTextvalues);
        newSearchApiCall.addRequestParams('searchType', AppConstants.ALL);
    
          this.blogModuleService.getSearchedKeyWords(newSearchApiCall).subscribe(response => {
          this.cmsTagsCOList  = response['cmsTagsCOList'];
          this.catCategoriesCOList = response['catCategoriesCOList'];
          this.cmsSearchSuggestionCOList = response['cmsSearchSuggestionCO'];
        });

    }


    inAllClick() {
        window.location.href = AppSettings.MAIN_WEBSITE_HOME + "/store/search?sortby=ltst&location=" + encodeURI(this.selectedOptionName) + "&s=" + encodeURI(this.searchInputTextvalues) + "&cat_id=off&subject_ids%5B%5D=off&category=off&module=all";
    }
    inStoreClick() {
        window.location.href = AppSettings.MAIN_WEBSITE_HOME + "/store/search?sortby=ltst&location=" + this.selectedOptionName + "&s=" + this.searchInputTextvalues + "&cat_id=off&subject_ids%5B%5D=off&category=off&module=store";
    }
    onScoopClick() {
        window.location.href = AppSettings.SCOOP_WEBSITE_HOME + "/?location=" + this.selectedOptionName + "&s=" + this.searchInputTextvalues + "&module=scoop";
    }

    onSearchedStoreClick(cmsSuggestionCO: CmsSearchSuggestionCO) {
        window.location.href = AppSettings.MAIN_WEBSITE_HOME + "/store/search?sortby=ltst&location=" + encodeURI(this.selectedOptionName) + "&s=" + encodeURI(cmsSuggestionCO.subjectTitle) + "+%28BL%29&" + encodeURI(cmsSuggestionCO.categoryId) + "&subject_ids%5B%5D=" + encodeURI(cmsSuggestionCO.subjectId) + "&" + encodeURI(cmsSuggestionCO.categoryTitle) + "&module=store"
    }
    onSearchedScoopClick(catCategoriesCO: CatCategoriesCO) {
        window.location.href = AppSettings.SCOOP_WEBSITE_HOME + "/category/" + encodeURI(catCategoriesCO.title) + "?location=" + encodeURI(this.selectedOptionName) + "&module=scoop"
    }
    onSearchedSuggetedClick(cmsTagsCO: CmsTagsCO) {
        window.location.href = AppSettings.MAIN_WEBSITE_HOME + "/store/search?sortby=ltst&location=" + encodeURI(this.selectedOptionName) + "&s=" + encodeURI(cmsTagsCO.title) + "&cat_id=off&subject_ids%5B%5D=off&category=off&module=suggested+search"
    }

    mobileLearnMenu() 
    {
        this.shouldShow = !this.shouldShow;
        if (this.shouldShow) {
            this.lab.nativeElement.classList.add("show");
            this.lab.nativeElement.classList.remove("hide");
        }
        else {
            this.lab.nativeElement.classList.add("hide");
            this.lab.nativeElement.classList.remove("show");
        }
    }

    mobileTeachMenu() {
        this.teachShow = !this.teachShow;
        if (this.teachShow) {
            this.teachMenu.nativeElement.classList.add("show");
            this.teachMenu.nativeElement.classList.remove("hide");
        }
        else {
            this.teachMenu.nativeElement.classList.add("hide");
            this.teachMenu.nativeElement.classList.remove("show");
        }
    }

    mobileToggleMenu() {
        this.showMobileMenu = !this.showMobileMenu;
        if (this.showMobileMenu) {
            this.mobileMenu.nativeElement.classList.add("show");
            this.mobileMenu.nativeElement.classList.remove("hide");
        }
        else {
            this.mobileMenu.nativeElement.classList.add("hide");
            this.mobileMenu.nativeElement.classList.remove("show");
        }
    }

    mobileSearch() {
        this.showMobileSearchBar = !this.showMobileSearchBar;
        if (this.showMobileSearchBar) {
            this.mobileSearchBar.nativeElement.classList.add("show");
            this.mobileSearchBar.nativeElement.classList.remove("hide");
        }
        else {
            this.mobileSearchBar.nativeElement.classList.add("hide");
            this.mobileSearchBar.nativeElement.classList.remove("show");
        }
    }
}
