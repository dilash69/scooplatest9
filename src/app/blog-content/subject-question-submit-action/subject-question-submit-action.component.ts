import { Component, OnInit, Input, Renderer2 } from '@angular/core';
import { SclPostCO } from '../../co/sclPostCO';
import { AppPopupCO } from '../../co/app.popup.co';
import { ManagePopupService } from '../../services/manage-popup.service';
import { AppCurrentStateService } from '../../services/app.current.state.service';
import { AppConstants } from '../../common/app.constants';
import { AppCurrentState } from '../../co/app.current.state';

@Component({
  selector: 'app-subject-question-submit-action',
  templateUrl: './subject-question-submit-action.component.html',
  styleUrls: ['./subject-question-submit-action.component.css']
})
export class SubjectQuestionSubmitActionComponent implements OnInit {

  @Input() postCO:SclPostCO; 
  public appPopupCO: AppPopupCO = new AppPopupCO();
  public appCurrentState: AppCurrentState = new AppCurrentState();

  constructor(
    private renderer: Renderer2,
    private managePopupService: ManagePopupService,
    private appCurrentStateService: AppCurrentStateService) 
    {}


  ngOnInit()
  {
    this.managePopupService.appPopupCO.subscribe(appPopupCO => {
      this.appPopupCO = appPopupCO;

      if (this.appCurrentStateService.isBrowser()) {
        if (this.appPopupCO.isAnyPopupOpen()) {
          this.renderer.addClass(document.body, 'modal-open');
        }
        else {
          this.renderer.removeClass(document.body, 'modal-open');
        }
      }
    });

    this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => 
      {
          this.appCurrentState = appCurrentState; 

          if(appCurrentState.isUserLoggedIn)
          {
              //AppUtility.log('HeaderComponent:User logged in');
          }            
      });
  }

  openLoginPopUp(categoryId) 
  {
     
      // this.managePopupService.showLoginPopUp(categoryId);
      localStorage.setItem(AppConstants.REGISTRATION_PROCESS, AppConstants.BLOG_ACTIVITY_REGISTRATION_PROCESS);
      this.managePopupService.showSignPopUp(categoryId,AppConstants.APP_PROMOTION);
  }
  
  openAndroidAppPopUp()
  {
    this.managePopupService.showAndroidAppPopUp();
  }

  

}
