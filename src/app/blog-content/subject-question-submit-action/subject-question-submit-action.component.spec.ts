import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectQuestionSubmitActionComponent } from './subject-question-submit-action.component';

describe('SubjectQuestionSubmitActionComponent', () => {
  let component: SubjectQuestionSubmitActionComponent;
  let fixture: ComponentFixture<SubjectQuestionSubmitActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectQuestionSubmitActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectQuestionSubmitActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
