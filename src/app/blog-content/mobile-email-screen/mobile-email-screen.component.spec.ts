import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileEmailScreenComponent } from './mobile-email-screen.component';

describe('MobileEmailScreenComponent', () => {
  let component: MobileEmailScreenComponent;
  let fixture: ComponentFixture<MobileEmailScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileEmailScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileEmailScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
