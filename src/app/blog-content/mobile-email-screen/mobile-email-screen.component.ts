import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ApiActions } from './../../common/api.actions';
import { BlogModuleService } from '../../services/blog-module.service';
import { RegisterUserService } from '../../services/register-user.service';
import { AppConstants } from './../../common/app.constants';
import { AppSettings } from './../../common/app-settings';
import { UserRegistrationDetailCO } from './../../co/userRegistrationDetailCO';
import { ApiCall } from './../../common/api.call';
import { ManagePopupService } from './../../services/manage-popup.service';
import { ActivatedRoute } from '@angular/router';
import { UserBrokenLeadsCO } from './../../co/userBrokenLeadsCO';


@Component({
  selector: 'app-mobile-email-screen',
  templateUrl: './mobile-email-screen.component.html',
  styleUrls: ['./mobile-email-screen.component.css']
})
export class MobileEmailScreenComponent implements OnInit {
  @Output() showOtpComponent = new EventEmitter();
  public showValidationError:boolean = false;
  public validationError:string;
  public siteUrl:string;
  isMandatoryTermsChecked:boolean=true;
  isWhatappNotificationChecked:boolean = true;

  constructor(public blogModuleService:BlogModuleService,
              public registerUserService:RegisterUserService,
              public managePopUpService:ManagePopupService,
              private route: ActivatedRoute) 
              { }

  ngOnInit() {
    this.siteUrl = AppSettings.MAIN_WEBSITE_HOME;
  }

  submitMobileEmail(mobileEmailJsonObj: JSON)
  {
    if(this.checkTermsAccepted())
    {
      this.showValidationError = false;
    let checkMobileEmailAvailablityApiCall: ApiCall = new ApiCall(ApiActions.checkMobileEmailAvailablity);
    checkMobileEmailAvailablityApiCall.showLoader = true;
    checkMobileEmailAvailablityApiCall.addRequestParams('mobileNumber', mobileEmailJsonObj['phone']);
    checkMobileEmailAvailablityApiCall.addRequestParams('emailId', mobileEmailJsonObj['email']);
   

    this.blogModuleService.checkMobileEmailAvailablity(checkMobileEmailAvailablityApiCall).subscribe(responseValues => {
      if (responseValues.userResponseCode === AppConstants.SUCCESS_REQUEST) 
      {
        //this.registerUserService.updatedUser(responseValues['otp'], mobileEmailJsonObj['phone'], null,true,mobileEmailJsonObj['email']);
        //this.loadOtp = true;
        //this.showOtpComponent.emit(); 
        let userBrokenLeadsCO: UserBrokenLeadsCO = new UserBrokenLeadsCO();
        userBrokenLeadsCO.url = window.location.href;

        let reGenerateOtpApiCall: ApiCall = new ApiCall(ApiActions.regenerateOtp);
        reGenerateOtpApiCall.showLoader = true;
        reGenerateOtpApiCall.addRequestParams('mobileNumber', mobileEmailJsonObj['phone']);
        reGenerateOtpApiCall.addRequestParams('userBrokenLeadsCO', userBrokenLeadsCO);
        reGenerateOtpApiCall.addRequestParams('isForTutorRegistration', "1");

    
        this.blogModuleService.regenerateOtp(reGenerateOtpApiCall).subscribe(responseValues => {
          if (responseValues.userResponseCode === AppConstants.SUCCESS_REQUEST) 
          {
            let userRegistrationDetailCO: UserRegistrationDetailCO = new UserRegistrationDetailCO();
            userRegistrationDetailCO.isTutorRegistration = "1";
            userRegistrationDetailCO.emailId = mobileEmailJsonObj['email'];
            userRegistrationDetailCO.mobileNumber = mobileEmailJsonObj['phone'];

            if(this.isWhatappNotificationChecked)
            {
               userRegistrationDetailCO.isWhatsAppNotifChecked = "1";
            }
            else
            {
               userRegistrationDetailCO.isWhatsAppNotifChecked = "0";
            }
    

             //for php tutor form
            this.route.paramMap
            .subscribe(params => 
                {
                if(this.route.snapshot.queryParamMap.get(AppConstants.param_redirect_url))
                {
                  userRegistrationDetailCO.redirectUrl = this.route.snapshot.queryParamMap.get(AppConstants.param_redirect_url)
                }

            });
            this.showOtpComponent.emit();
            this.registerUserService.updatedUser1(userRegistrationDetailCO);
            
          }
          else {
            this.showValidationError = true;
            this.validationError = responseValues.userMessageList[0];
          }
        });
      }
      else 
      {
        this.validationError = responseValues.userMessageList[0];
        this.showValidationError = true;
      }
    });
    }
    
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;

}

checkTermsAccepted()
  {
    if(!this.isMandatoryTermsChecked)
    {
      alert("Please accept Terms and Conditions")
      return false;
    }
    return true;
  }

  checkUncheckTermsCheckBox()
  {
    this.isMandatoryTermsChecked = !this.isMandatoryTermsChecked;
  }

  checkUncheckWhatsAppNotification()
  {
    this.isWhatappNotificationChecked = !this.isWhatappNotificationChecked;
  }

  saveBrokenLeads(event)
  {
    if(event.target.value.length == 10)
    {
      let saveUserBrokenLeadsApiCall: ApiCall = new ApiCall(ApiActions.saveUserBrokenLeads);
      saveUserBrokenLeadsApiCall.showLoader = false;
      saveUserBrokenLeadsApiCall.addRequestParams('userBrokenLeadsCO', UserBrokenLeadsCO.getAutoBrokenLeadCO(event.target.value,"AUTO_MOBILE_EMAIL_POPUP"));

      this.blogModuleService.saveUserBrokenLeads(saveUserBrokenLeadsApiCall).subscribe(userResponseData => {
        
      });
    }
  }
}
