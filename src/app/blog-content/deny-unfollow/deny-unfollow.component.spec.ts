import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DenyUnfollowComponent } from './deny-unfollow.component';

describe('DenyUnfollowComponent', () => {
  let component: DenyUnfollowComponent;
  let fixture: ComponentFixture<DenyUnfollowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DenyUnfollowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DenyUnfollowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
