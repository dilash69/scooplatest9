import { ApiCall } from './../../common/api.call';
import { Component, OnInit } from '@angular/core';
import { ManagePopupService } from '../../services/manage-popup.service';
//import { ApiCall } from 'src/app/common/api.call';
import { ApiActions } from './../../common/api.actions';
import { BlogModuleService } from './../../services/blog-module.service';
import { CatCategoriesCO } from './../../co/categoryCO';
import { deserialize } from 'serializer.ts/Serializer';
import { ChangeCategoryService } from './../../services/change-category.service';
import { AppCurrentState } from './../../co/app.current.state';
import { ChangeCategoryStateCO } from './../../co/changeCategoryStateCO';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { AppUtility } from './../../common/app.utility';


@Component({
  selector: 'app-deny-unfollow',
  templateUrl: './deny-unfollow.component.html',
  styleUrls: ['./deny-unfollow.component.css']
})
export class DenyUnfollowComponent implements OnInit {

  public catCategoryCO:CatCategoriesCO ;
  private changeCategoryStateCO:ChangeCategoryStateCO = new ChangeCategoryStateCO();
  public appCurrentState : AppCurrentState = new AppCurrentState();

  constructor(private managePopupService : ManagePopupService,
    private blogModuleService:BlogModuleService,
    private changeCategoryService: ChangeCategoryService,
    private appCurrentStateService:AppCurrentStateService) { }

  ngOnInit() 
  {
    this.changeCategoryService.changeCategoryUserSelectionCO.subscribe(changeCategoryUserSelectionCO => {
      this.catCategoryCO = changeCategoryUserSelectionCO.catCategoryCO;
     AppUtility.log("this.catCategoryCO",this.catCategoryCO)
    });

    this.changeCategoryService.changeCategoryStateCO.subscribe(changeCategoryStateCO => 
      {
        this.changeCategoryStateCO = changeCategoryStateCO;
      });

      this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
        this.appCurrentState = appCurrentState;
      });
  }

  

  dismiss()
  {
    this.managePopupService.closePopUp();
  }

  unfollowCategory()
   {
    if(this.appCurrentState.userCO!=null)
     {
       if(this.appCurrentState.userCO.defaultCategoryId === this.catCategoryCO.id )
       {
        this.dismiss();
        this.managePopupService.showConfirmUnfollowPopUp();
       }
       else
       {
        this.catCategoryCO.isUserJoin = false;
        this.dismiss();
        let unFollowCategoryApiCall: ApiCall = new ApiCall(ApiActions.unFollowCategory);
        unFollowCategoryApiCall.addRequestParams('categoryId', this.catCategoryCO.id);
  
            this.blogModuleService.unFollowCategory(unFollowCategoryApiCall).subscribe(response => {
              let catCategoryCOList: CatCategoriesCO[] = response['catCategoryCOList'];
              catCategoryCOList = deserialize<CatCategoriesCO[]>(CatCategoriesCO, catCategoryCOList);
              this.changeCategoryStateCO.catCategoriesCOList = catCategoryCOList;
              this.changeCategoryService.updateChangeCategoryStateCO(this.changeCategoryStateCO);
            });
       }
       } 
   }
}
