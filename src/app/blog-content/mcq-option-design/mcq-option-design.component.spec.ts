import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McqOptionDesignComponent } from './mcq-option-design.component';

describe('McqOptionDesignComponent', () => {
  let component: McqOptionDesignComponent;
  let fixture: ComponentFixture<McqOptionDesignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McqOptionDesignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McqOptionDesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
