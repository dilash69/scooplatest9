import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserQualificationInfoPopupComponent } from './user-qualification-info-popup.component';

describe('UserQualificationInfoPopupComponent', () => {
  let component: UserQualificationInfoPopupComponent;
  let fixture: ComponentFixture<UserQualificationInfoPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserQualificationInfoPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserQualificationInfoPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
