import { ResponseCO } from './../../co/responseCO';
import { AppConstants } from './../../common/app.constants';
import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalConfig, NgbDate, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { BlogModuleService } from './../../services/blog-module.service';
import { ManagePopupService } from './../../services/manage-popup.service';
import { UserCO } from './../../co/userCO';
//import { AppConstants } from 'src/app/common/app.constants';
import { ApiCall } from './../../common/api.call';
import { ApiActions } from './../../common/api.actions';
import { UserEducationInformationsCO } from './../../co/userEducationInformationsCO';
import { deserialize } from 'serializer.ts/Serializer';
import { HttpResponse } from '@angular/common/http';
import { UploadFileService } from './../../services/upload-file.service';
//import { ResponseCO } from 'src/app/co/responseCO';
//import { AppUtility } from 'src/app/common/app.utility';

@Component({
  selector: 'app-user-qualification-info-popup',
  templateUrl: './user-qualification-info-popup.component.html',
  styleUrls: ['./user-qualification-info-popup.component.css']
})
export class UserQualificationInfoPopupComponent implements OnInit {
  public userImage: string="assets/images/profile-default.png";
  selectedFiles: FileList;
  currentFileUpload: File;
  currentFileName: string='assasa';

  public loggedInUserCO: UserCO;
  public showFields:boolean=false;
  dateFrom:NgbDate;
  dateTo:NgbDate;

  constructor(private managePopupService:ManagePopupService,
    private blogModuleService : BlogModuleService, config: NgbModalConfig,
    private uploadService: UploadFileService,
    private ngbDateParserFormatter: NgbDateParserFormatter) {
      config.backdrop = 'static';
      config.keyboard = false;
     }

  

  ngOnInit() 
  {
    //this.loggedInUserCO = JSON.parse(localStorage.getItem(AppConstants.LOGGED_IN_USER_CO));

    let userInfoPopUpShow: ApiCall = new ApiCall(ApiActions.userInfoPopUpShow);
    userInfoPopUpShow.addRequestParams('status', "dateUpdate");
    this.blogModuleService.userInfoPopUpShow(userInfoPopUpShow).subscribe(response => {
      if (response['userImage']) {
        this.userImage = response['userImage'];
      }
      else if (response['userEducationInformationCO']) {
        let userEducationInformationsCO: UserEducationInformationsCO = response['userEducationInformationsCO'];
        userEducationInformationsCO = deserialize<UserEducationInformationsCO>(UserEducationInformationsCO, userEducationInformationsCO);
      }
      /* if (response['userProfileImage']) {
      
      }
      if (response['userEducationInformationCO']) {
        
      } */

      
      
      //localStorage.setItem(AppConstants.LOGGED_IN_USER_CO, JSON.stringify(this.userCO).toString());
    });
   
  }
  

  dismiss() {
    this.managePopupService.closePopUp();
  } 


  selectFile(event) 
{
 
    this.selectedFiles = event.target.files;
    this.currentFileUpload = this.selectedFiles.item(0);
    this.currentFileName = this.currentFileUpload.name;

    this.uploadService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
        if (event instanceof HttpResponse) 
        {
            let responseCO: ResponseCO = JSON.parse("" + event.body);
            this.userImage = responseCO.userResponseData.responseValues['urlLink']
        }
    });
    this.selectedFiles = undefined;
}

addUserEduInformation(education,board,field,dateFrom,dateTo) 
{
  
  if(this.userImage=="assets/images/profile-default.png")
  {
      alert("Please select your profile image")
  }
  else if(dateFrom==null)
  {
    alert("Please select tenure From Date")
  }
  else if(dateTo==null)
  {
    alert("Please select tenure To Date")
  }
  else
  {
    let userEducationInformationsCO: UserEducationInformationsCO = new UserEducationInformationsCO();
    userEducationInformationsCO.education=education;
    userEducationInformationsCO.boardUniversity=board;
    userEducationInformationsCO.fieldOfStudy=field;
    userEducationInformationsCO.tenureFrom= new Date(this.ngbDateParserFormatter.format(dateFrom)).getTime().toString();
    userEducationInformationsCO.tenureTo=new Date(this.ngbDateParserFormatter.format(dateTo)).getTime().toString();
 
   
    let saveUserEducationInformations: ApiCall = new ApiCall(ApiActions.saveUserEducationInformations);
    saveUserEducationInformations.addRequestParams('userProfileImage', this.userImage);
    saveUserEducationInformations.addRequestParams('userEducationInformationCO', userEducationInformationsCO);
    this.blogModuleService.saveUserEducationInformations(saveUserEducationInformations).subscribe(response => {
          if(response['message']=='success')
          {
            this.dismiss()
          }
    });
    
  }



   

}



}
