import { AppConstants } from './../../common/app.constants';
import { AppSettings } from './../../common/app-settings';
import { Inject, Renderer2 } from '@angular/core';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { BlogModuleService } from './../../services/blog-module.service';
import { AppCurrentState } from './../../co/app.current.state';
import { SclCommentReplyCO } from './../../co/commentReplyCO';

import { BehaviorSubject } from 'rxjs';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ManagePopupService } from '../../services/manage-popup.service';
import { AppPopupCO } from '../../co/app.popup.co';
import { AppUtility } from '../../common/app.utility';
import { SclPostAttachmentsCO } from '../../co/SclPostAttachmentsCO';

@Component({
    selector: 'app-show-reply',
    templateUrl: './show-reply.component.html',
    styleUrls: ['./show-reply.component.css']
})
export class ShowReplyComponent implements OnInit {
    public siteUrl: string;
    public scoopUrl: string;
    public encodedUrl: string;
    public pageUrl: string;
    @Input() categoryId: string;

    private sclCommentReplyCOBehaviorSubject = new BehaviorSubject<SclCommentReplyCO>(null);
    //public loggedInUserCO:UserCO;
    public appCurrentState: AppCurrentState = new AppCurrentState();
    // @Output() deleteReplyFromList = new EventEmitter();
    public appPopupCO: AppPopupCO = new AppPopupCO();

    public textbackground:String;
    public textBackgroundUserProfile:String;
    public postType: string;
    public showMyElement:boolean;
    defaultImageCircle:String = AppConstants.DEFAULT_IMAGE_CIRCLE;



    // @Output() commentCO = new EventEmitter();  
    @Input()
    set replyCO(value) {
        this.sclCommentReplyCOBehaviorSubject.next(value);
    };

    get replyCO() {
        return this.sclCommentReplyCOBehaviorSubject.getValue();
    }
    constructor(
        private renderer: Renderer2,
        private managePopUpService: ManagePopupService,
        private blogModuleService: BlogModuleService,
        private appCurrentStateService: AppCurrentStateService,
        private managePopupService: ManagePopupService) {

    }


    ngOnInit() {
        // this.pageUrl = window.location.href;
        // this.encodedUrl = encodeURIComponent( this.pageUrl);

        this.managePopUpService.appPopupCO.subscribe(appPopupCO => {
            this.appPopupCO = appPopupCO;

            if (this.appCurrentStateService.isBrowser()) {
                if (this.appPopupCO.isAnyPopupOpen()) {
                    this.renderer.addClass(document.body, 'modal-open');
                }
                else {
                    this.renderer.removeClass(document.body, 'modal-open');
                }
            }
        });

        this.siteUrl = AppSettings.MAIN_WEBSITE_HOME;
        this.scoopUrl = AppSettings.SCOOP_WEBSITE_HOME;
        this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
            this.appCurrentState = appCurrentState;

            if(!this.appCurrentState.isUserLoggedIn || AppUtility.isEmpty(this.appCurrentState.userCO.firstName))
            {
                this.textBackgroundUserProfile=AppConstants.DEFAULT_FIRST_CHARACTER;
            }
            else{
                this.textBackgroundUserProfile=this.appCurrentState.userCO.firstName.charAt(0).toUpperCase();
            }
        });

        this.sclCommentReplyCOBehaviorSubject
            .subscribe(x => {
                if (this.replyCO) {
                    this.textbackground=this.replyCO.repliedUserCO.firstName.charAt(0).toUpperCase();
                    this.postType = localStorage.getItem("postType");

                    //this.loggedInUserCO = JSON.parse(localStorage.getItem(AppConstants.LOGGED_IN_USER_CO));
                }
            });
    }

    openDeleteReplyPopUp(replyId) {
        localStorage.setItem("replyId", replyId);
        this.managePopUpService.showReplyDeletePopUpOpen();
    }

    openLoginPopUp(categoryId) {
        //localStorage.setItem("defaultCategoryId",categoryId)
        localStorage.setItem(AppConstants.REGISTRATION_PROCESS, AppConstants.BLOG_ACTIVITY_REGISTRATION_PROCESS);
        this.managePopUpService.showSignPopUp(localStorage.getItem("currentCategoryId"),AppConstants.APP_PROMOTION);
    }



    //   deleteReply()
    //   {
    //       let deleteReplyApiCall: ApiCall = new ApiCall(ApiActions.deleteReply);
    //       deleteReplyApiCall.addRequestParams('replyId', this.replyCO.id);

    //       this.blogModuleService.deleteReply(deleteReplyApiCall).subscribe(response => 
    //       {
    //           let result = response;
    // this.deleteReplyFromList.emit(this.replyCO);
    // for(let i=0;i<this.commentCOArray.length;i++)
    // {
    //     for(let j=0;j<this.commentCOArray[i].sclCommentReplyCOList.length;j++)
    //     {
    //         if(replyCO.id === this.commentCOArray[i].sclCommentReplyCOList[j].id)
    //         {
    //             this.commentCOArray[i].sclCommentReplyCOList.splice(this.commentCOArray[i].sclCommentReplyCOList.indexOf(this.commentCOArray[i].sclCommentReplyCOList[j]),1);
    //         }
    //     }

    // }
    //       });
    //   }

    //   onDeletePopupYesClick()
    //   {
    //       this.deleteReply();
    //   }

    likeReply() {

        if (this.appCurrentState.isUserLoggedIn && this.postType === AppConstants.ARTICLE) 
        {
            this.replyCO.hasUserLiked = true;
            let count = parseInt(this.replyCO.totalLikes);
            count = count + 1;
            this.replyCO.totalLikes = count.toString();

            let likeReplyApiCall: ApiCall = new ApiCall(ApiActions.likeReply);
            likeReplyApiCall.addRequestParams('replyId', this.replyCO.id);

            this.blogModuleService.likeReply(likeReplyApiCall).subscribe(response => {
                //Do nothing
            });
        }
        else 
        {
            this.openPopUp();
        }
    }

    unlikeReply() 
    {
        if (this.appCurrentState.isUserLoggedIn && this.postType === AppConstants.ARTICLE) 
        {
            this.replyCO.hasUserLiked = false;
            let count = parseInt(this.replyCO.totalLikes);
            count = count - 1;
            this.replyCO.totalLikes = count.toString();
            let unlikeReplyApiCall: ApiCall = new ApiCall(ApiActions.unlikeReply);
            unlikeReplyApiCall.addRequestParams('replyId', this.replyCO.id);

            this.blogModuleService.unlikeReply(unlikeReplyApiCall).subscribe(response => {
                //Do nothing
            });
        }
        else 
        {
            this.openPopUp();
        }
    }


    public getClassForTextBackGround(type: string): String {
        if (type == "post") {
            return "user-profile-commonDesign userProfile-35 " + AppUtility.textBackgroundColor(this.textbackground) + " sm16";;
        }
        else {
            return "user-profile-commonDesign userProfile-35 " + AppUtility.textBackgroundColor(this.textBackgroundUserProfile) + " sm16";;
        }

    }

    openPopUp() 
    {
        if (this.appCurrentState.isUserLoggedIn) 
        {
            this.openAppPopUp();
        }
        else 
        {
            this.openLoginPopUp(this.categoryId);
        }
    }

  openAppPopUp() 
  {
    this.managePopUpService.showAndroidAppPopUp();
  }

  showImageOnReply(sclPostAttachmentsCO:SclPostAttachmentsCO):string
 {
      if(sclPostAttachmentsCO.type.includes('pdf'))
      {
        return this.getResponsiveImage(sclPostAttachmentsCO.previewImage);
      }
      else
      {
        return this.getResponsiveImage(sclPostAttachmentsCO.url);
      }
 }

 openImageSliderPopUp(position:string) 
  {
    if (!this.appPopupCO.isAnyPopupOpen()) {
      this.managePopupService.showImagePopUp(this.replyCO.sclPostAttachmentsCOList,position);
    }
  }

 public getResponsiveImage(imagePath:string):string {
    return AppUtility.getResponsiveImage(imagePath);
   }

}
