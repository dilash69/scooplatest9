import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SingleSocialTimelineComponent } from './single-social-timeline.component';



describe('SingleSocialTimelineComponent', () => {
  let component: SingleSocialTimelineComponent;
  let fixture: ComponentFixture<SingleSocialTimelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleSocialTimelineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleSocialTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
