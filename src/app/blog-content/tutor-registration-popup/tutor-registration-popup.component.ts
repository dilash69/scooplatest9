import { HttpResponse } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
//import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { NgbDate, NgbDateParserFormatter, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { deserialize } from 'serializer.ts/Serializer';
import { AppCurrentState } from '../../co/app.current.state';
import { SelectedGradeCO } from '../../co/selectedGradeCO';
import { UserTutorCategoryRelationCO } from '../../co/userTutorCategoryRelationCO';
import { AppConstants } from '../../common/app.constants';
import { AppUtility } from '../../common/app.utility';
import { CatExamsCO } from './../../co/catExamsCO';
import { ResponseCO } from './../../co/responseCO';
import { UserCO } from './../../co/userCO';
import { UserTeacherInformationCO } from './../../co/userTeacherInformationCO';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { AppSettings } from './../../common/app-settings';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { BlogModuleService } from './../../services/blog-module.service';
import { ManagePopupService } from './../../services/manage-popup.service';
import { RegisterUserService } from './../../services/register-user.service';
import { TutorDetailService } from './../../services/tutor-detail.service';
import { UploadFileService } from './../../services/upload-file.service';

declare const setAttribute: any;


@Component({
    selector: 'app-tutor-registration-popup',
    templateUrl: './tutor-registration-popup.component.html',
    styleUrls: ['./tutor-registration-popup.component.css']
})
export class TutorRegistrationPopupComponent implements OnInit {
    catExamsCOList: CatExamsCO[] = new Array();
    teachExamId: string;
    selectedFiles: FileList;
    currentFileUpload: File;
    currentFileName: string = 'assasa';
    resumeButtonTitle: string = AppConstants.Upload;
    resumeUploadedPath: string = "";
    dob: NgbDate;
    public daterange: any = {};
    @Output() hideTutorRegistration = new EventEmitter();
    validationError: string;
    modalReference: NgbModalRef;
    public selectedGradeCOArrayList: SelectedGradeCO[] = new Array();
    public tutorDetailCO:UserCO = new UserCO();
    public appCurrentState: AppCurrentState = new AppCurrentState();
    public showSuccessView:boolean =false;
    public tutorFormStatus :String;
    private getUserCurrentStateInfoCalled:boolean = false;
   // public formSubmitted :boolean = false;
   //DCP Script Variables
   public firstFilterName:String;
   public firstFilterValue:String;
   public categoryName:String;
   public isMobileVerified :String;
   public maxDate :Date;
   public currentYear;

    constructor(
        private managePopUpService: ManagePopupService,
        //private daterangepickerOptions: DaterangepickerConfig,
        private blogModuleService: BlogModuleService,
        private uploadService: UploadFileService,
        private registerUserService: RegisterUserService,
        private ngbDateParserFormatter: NgbDateParserFormatter,
        private modalService: NgbModal,
        private router:Router,
        private tutorDetailService:TutorDetailService,
        private appCurrentStateService:AppCurrentStateService,
        private route:ActivatedRoute)
         {
        /*   this.daterangepickerOptions.settings = {
              "singleDatePicker": true
          };
          this.daterangepickerOptions.skipCSS = true; */
    }

/*     dismiss() {
        this.managePopUpService.closePopUp();
    } */


    public selectedDate(value: any, datepicker?: any) {

        // any object can be passed to the selected event and it will be passed back here
        //alert()
        datepicker.start = value.start;
        datepicker.end = value.end;

        // or manupulat your own internal property
        this.daterange.start = value.start;
        this.daterange.end = value.end;
        this.daterange.label = value.label;

        let fromDate = new Date(value.start.format('YYYY-MM-DD'));
        //this.dob = fromDate.getTime().toString();
    }

    ngOnInit() {
       // this.dismiss();

       this.managePopUpService.appPopupCO.subscribe(appPopupCO => 
        {
            appPopupCO.isEntryPopUpRequired = false;
            appPopupCO.isExitPopUpRequired = false;
            if(!appPopupCO.isEntryPopUpRequired)
            {
                this.managePopUpService.updatedAppPopupCO(appPopupCO);
            }            
        }); 

        this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
            this.appCurrentState = appCurrentState;

            if (this.route.snapshot.queryParamMap.get('accessToken') && this.appCurrentStateService.isLoggedInChecked) 
            {
                this.getUserCurrentStateInfo(this.route.snapshot.queryParamMap.get('accessToken'));
            }
        });

            this.tutorDetailService.tutorDetailObservable.subscribe(res => 
            {
                this.tutorDetailCO = res
               if (!this.tutorDetailCO) 
               {
                   if (this.route.snapshot.queryParamMap.get('accessToken')) 
                   {
                       if (this.appCurrentStateService.isLoggedInChecked) 
                       {
                           this.getUserCurrentStateInfo(this.route.snapshot.queryParamMap.get('accessToken'));
                       }
                   }
                   else 
                   {
                       this.router.navigate([""]);
                   }
               }
            });   

        /* let getCatExamsApiCall: ApiCall = new ApiCall(ApiActions.getCatExams);

        this.blogModuleService.getCatExams(getCatExamsApiCall).subscribe(response => {
            this.catExamsCOList = response;
        }); */
        var date = new Date();
        this.currentYear = date.getFullYear();
        /*  let currentMonth = date.getMonth();
        let currentDate = date.getDate();
        this.maxDate=new Date(this.currentYear, currentMonth, currentDate)
        console.log("fafasfas",this.maxDate.toDateString)
        console.log("fafasfas",this.maxDate.getDate) */
    }

    addTutorInformation(name, emailId, mobile, dob, location, highestQualification, specialization, experience, boards, occupation, leadFromInfo) {
        /*  if(this.resumeUploadedPath==="")
         {
             alert("Please uplod your resume")
         } */
         if(this.selectedGradeCOArrayList.length == 0)
         {
            alert("Please select atleast one grade")
         }
        else if (!localStorage.getItem(AppConstants.RESUME_UPLOADED_PATH)) {
            alert("Please upload your resume")
        }
        else {
            let userCO: UserCO = new UserCO();
            userCO.email = emailId;
            userCO.mobile = mobile;
            userCO.dob = new Date(this.ngbDateParserFormatter.format(this.dob)).getTime().toString();
            userCO.firstName = name;

            if(this.tutorDetailCO)
            {
                userCO.whatsappNotification = this.tutorDetailCO.whatsappNotification;
                if(this.tutorDetailCO.id)
                {
                    userCO.id = this.tutorDetailCO.id;
                }

            }

            let userTeacherInformationCO: UserTeacherInformationCO = new UserTeacherInformationCO();
            userTeacherInformationCO.location = location;
            userTeacherInformationCO.highestQualification = highestQualification;
            userTeacherInformationCO.specialization = specialization;
            userTeacherInformationCO.yearsOfExperience = experience;
            /* userTeacherInformationCO.teachPrimarySubject = primarySubject;
            userTeacherInformationCO.teachSecondarySubject = secondarySubject; */
            userTeacherInformationCO.teachBoards = boards;
            userTeacherInformationCO.currentOccupation = occupation;
            userTeacherInformationCO.leadFromInfo = leadFromInfo;
            //userTeacherInformationCO.teachExamId = this.teachExamId;
            //userTeacherInformationCO.resume = this.resumeUploadedPath;
            userTeacherInformationCO.resume = localStorage.getItem(AppConstants.RESUME_UPLOADED_PATH);

            
            //let catObjectCategoryRelationCOList:CatObjectCategoryRelationCO[]= new Array();
           // let userCategoryRelationCOList:UserCategoryRelationCO[]= new Array();
           let userTutorCategoryRelationCOList:UserTutorCategoryRelationCO[]= new Array();

            for(let i =0;i<this.selectedGradeCOArrayList.length;i++)
            {
                userTutorCategoryRelationCOList=userTutorCategoryRelationCOList.concat(this.selectedGradeCOArrayList[i].userTutorCategoryRelationCOList);
            }

            let tutorRegistrationApiCall: ApiCall = new ApiCall(ApiActions.tutorRegistration);
            tutorRegistrationApiCall.addRequestParams('userCO', userCO);
            tutorRegistrationApiCall.addRequestParams('userTeacherInformationCO', userTeacherInformationCO);
            tutorRegistrationApiCall.addRequestParams('userTutorCategoryRelationCOList', userTutorCategoryRelationCOList);
            tutorRegistrationApiCall.addRequestParams(AppConstants.newAccessTokenRequired, this.tutorDetailCO.newAccessTokenRequired);
            if(this.tutorDetailCO.password !== '')
            {
                tutorRegistrationApiCall.addRequestParams('password', this.tutorDetailCO.password);
            }

            if(this.tutorDetailCO.isTutorAutoVerifyOtp && this.tutorDetailCO.isTutorAutoVerifyOtp !=='')
            {
                this.isMobileVerified = "0";
                tutorRegistrationApiCall.addRequestParams("isVerifyOtpNew", "1");
            }
            else
            {
                this.isMobileVerified = "1";
            }

            this.blogModuleService.tutorRegistration(tutorRegistrationApiCall).subscribe(response => 
                {
                    
                    this.appCurrentStateService.login(true, localStorage.getItem(AppConstants.DB_ACCESS_TOKEN), null);
                    this.callDCPScript(emailId,mobile,name);

                    if(this.tutorDetailCO.redirectUrl && this.tutorDetailCO.redirectUrl.length>0)
                    {//land from php as non logged in user
                        window.location.href = AppSettings.MAIN_WEBSITE_HOME+"/include/misutils/ng-login?access_token="+localStorage.getItem(AppConstants.DB_ACCESS_TOKEN)+"&redirect_url=" + encodeURIComponent(this.tutorDetailCO.redirectUrl);
                    }
                    else
                    {//land from scoop
                        localStorage.setItem(AppConstants.tutorFormFilled,"1");
                        window.location.href = AppSettings.MAIN_WEBSITE_HOME+"/include/misutils/ng-login?access_token="+localStorage.getItem(AppConstants.DB_ACCESS_TOKEN)+"&redirect_url=" + encodeURIComponent(AppSettings.SCOOP_WEBSITE_HOME);
                    }
            });
        } 

    }

    changeExam(exam) {
        this.teachExamId = exam.value;
    }

    selectFile(event) {
        this.resumeButtonTitle = AppConstants.Uploading;
        this.selectedFiles = event.target.files;
        this.currentFileUpload = this.selectedFiles.item(0);
        this.currentFileName = this.currentFileUpload.name;

        this.uploadService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
            
            if (event instanceof HttpResponse) 
            {
                let responseCO: ResponseCO = JSON.parse("" + event.body);
                this.resumeUploadedPath = responseCO.userResponseData.responseValues['urlLink']
                localStorage.setItem(AppConstants.RESUME_UPLOADED_PATH, this.resumeUploadedPath);
                this.resumeButtonTitle = AppConstants.Upload;
                AppUtility.log('File is completely uploaded!', this.resumeUploadedPath);
            }
        });
        this.selectedFiles = undefined;
    }



    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    openGradePopUp(content) {
        this.modalReference = this.modalService.open(content, { centered: true });
    }

    addGrade(selectedGradeCO: SelectedGradeCO) 
    {
       // selectedGradeCO.catGradesCO.
        this.selectedGradeCOArrayList.push(selectedGradeCO);
    }

    getGradeBreadCrumb(selectedGradeCO:SelectedGradeCO):string
    {
        let breadCrumbString:string="";
        if(selectedGradeCO.catGradesCO.nextFilterType === AppConstants.SUBJECT)
        {
            for(let i =0;i<selectedGradeCO.selectedSubjectTitleList.length;i++)
            {
                if(i == selectedGradeCO.selectedSubjectTitleList.length-1)
                {
                    breadCrumbString = breadCrumbString + selectedGradeCO.selectedSubjectTitleList[i];
                }
                else
                {
                    breadCrumbString = breadCrumbString + selectedGradeCO.selectedSubjectTitleList[i]+", ";
                }
            }
        }
        if(selectedGradeCO.catGradesCO.nextFilterType === AppConstants.STREAM)
        {
            for(let i =0;i<selectedGradeCO.selectedStreamTitleList.length;i++)
            {
                if(i == selectedGradeCO.selectedStreamTitleList.length-1)
                {
                    breadCrumbString = breadCrumbString + selectedGradeCO.selectedStreamTitleList[i];
                }
                else
                {
                    breadCrumbString = breadCrumbString + selectedGradeCO.selectedStreamTitleList[i]+", ";
                }
            }
        }
        return breadCrumbString;
    }

    deleteBreadCrumb(selectedGradeCO :SelectedGradeCO) 
    {
        for (let i = 0; i < this.selectedGradeCOArrayList.length; i++)
        {
            if (this.selectedGradeCOArrayList[i].catGradesCO.id === selectedGradeCO.catGradesCO.id) 
            {
                this.selectedGradeCOArrayList.splice(this.selectedGradeCOArrayList.indexOf(this.selectedGradeCOArrayList[i]),1);
                break;
            }
        }
    }

    disableInput(type: string): boolean 
    {
        if (this.tutorDetailCO) 
        {
            if (type === "email") 
            {
                if (this.tutorDetailCO.email != "") 
                {
                    return true;
                }
            }
            if (type === "mobile") 
            {
                if (this.tutorDetailCO.mobile != "") 
                {
                    return true;
                }
            }
            if(type === "name")
            {
                if(this.tutorDetailCO.firstName)
                {
                    if (this.tutorDetailCO.firstName != "") 
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        else
        {
            return false;
        }

    }

    getUserCurrentStateInfo(accessToken:string)
    {
        if (this.tutorDetailCO || this.getUserCurrentStateInfoCalled) 
        {
            return;
        }

        this.getUserCurrentStateInfoCalled = true;
        let getUserCurrentStateInfoApiCall: ApiCall = new ApiCall(ApiActions.getUserCurrentStateInfo);
        getUserCurrentStateInfoApiCall.addRequestParams("dbAccessToken",accessToken)
        this.blogModuleService.getUserCurrentStateInfo(getUserCurrentStateInfoApiCall).subscribe(response => 
            {
            if (response.userResponseCode === AppConstants.SUCCESS_REQUEST) 
            {
                let userCO: UserCO = response.responseValues['userCO'];
                userCO = deserialize<UserCO>(UserCO, userCO);

                let userTeacherInformationCO: UserTeacherInformationCO = response.responseValues['userTeacherInformationCO'];
                userTeacherInformationCO = deserialize<UserTeacherInformationCO>(UserTeacherInformationCO, userTeacherInformationCO);
                if(userCO.id === this.appCurrentState.userCO.id)
                {//valid user
                    this.tutorDetailCO = userCO;
                    this.tutorDetailCO.newAccessTokenRequired = "0";
                    this.tutorDetailCO.redirectUrl = this.route.snapshot.queryParamMap.get('redirect');
    
                    if(userTeacherInformationCO)
                    {
                        if(userTeacherInformationCO.status===AppConstants.PENDING || userTeacherInformationCO.status===AppConstants.SAVE_FOR_LATER || userTeacherInformationCO.status===AppConstants.TUTOR_FORM_REJECTED)
                        {
                            this.tutorFormStatus = userTeacherInformationCO.status;
                            this.showSuccessView = true;
                        }
                        else
                        {
                            this.showSuccessView = false;
                        }
                    }
                }
                else
                {//invalid user
                    this.router.navigate([""]); 
                }
            }
            else 
            {
                this.router.navigate([""]);
            }
        });
    }

    callDCPScript(emailId,mobile,name)
    {
        let selectedGradeCO:SelectedGradeCO=this.selectedGradeCOArrayList[0];
        
        if(selectedGradeCO.catGradesCO.nextFilterType === AppConstants.SUBJECT)
        {
            setAttribute({
                'phone':mobile,
                'name':name,
                'email':emailId,
                'CategoryName':selectedGradeCO.catGradesCO.name,
                'FirstFilterValue':selectedGradeCO.selectedSubjectTitleList[0],
                'FirstFilterName':selectedGradeCO.catGradesCO.nextFilterType,
                'isMobileVerified':this.isMobileVerified,
                'SubjectName':selectedGradeCO.selectedSubjectTitleList[0],
                'StreamName':'-',
                'ExamName':'-',
                'ActivityType':AppConstants.TUTOR_REGISTRATION
              });
        }
        else
        {
            setAttribute({
                'phone':mobile,
                'name':name,
                'email':emailId,
                'CategoryName':selectedGradeCO.catGradesCO.name,
                'FirstFilterValue':selectedGradeCO.selectedStreamTitleList[0],
                'FirstFilterName':selectedGradeCO.catGradesCO.nextFilterType,
                'isMobileVerified':this.isMobileVerified,
                'StreamName':selectedGradeCO.selectedStreamTitleList[0],
                'ExamName':'-',
                'SubjectName':'-',
                'ActivityType':AppConstants.TUTOR_REGISTRATION
              });
        }
    }

}
