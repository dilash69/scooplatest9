import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutorRegistrationPopupComponent } from './tutor-registration-popup.component';

describe('TutorRegistrationPopupComponent', () => {
  let component: TutorRegistrationPopupComponent;
  let fixture: ComponentFixture<TutorRegistrationPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutorRegistrationPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutorRegistrationPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
