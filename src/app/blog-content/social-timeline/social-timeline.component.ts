import { Location, DOCUMENT } from '@angular/common';
import { Component, Input, OnInit, Renderer2, ViewChild, Inject } from '@angular/core';
import { DomSanitizer, Title, Meta } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppPopupCO } from '../../co/app.popup.co';
import { AppConstants } from '../../common/app.constants';
import { AppUtility } from '../../common/app.utility';
import { AppCurrentState } from './../../co/app.current.state';
import { SclPostCO } from './../../co/sclPostCO';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { BlogModuleService } from './../../services/blog-module.service';
import { ManagePopupService } from './../../services/manage-popup.service';
import { BehaviorSubject } from 'rxjs';
import { AppSettings } from '../../common/app-settings';
import { SclPostCommentCO } from '../../co/commentCO';
import { deserialize } from 'serializer.ts/Serializer';



@Component({
  selector: 'app-social-timeline',
  templateUrl: './social-timeline.component.html',
  styleUrls: ['./social-timeline.component.css']
})
export class SocialTimelineComponent implements OnInit {
  //@Input() postCO: SclPostCO;
  private sclPostCOBehaviorSubject = new BehaviorSubject<SclPostCO>(null);
  public postTitle: string;
  public postDescription: string = "";
  public userFunction: string;
  public postType: string;
  public appPopupCO: AppPopupCO = new AppPopupCO();
  public appCurrentState: AppCurrentState = new AppCurrentState();
  public postLikeStatus: boolean = false;
  public writeCommentStatus: string;
  public replyCommentCount: number = 0;
  public loadMoreCount: number = 0;
  public textbackground:String;
  public textBackgroundUserProfile:String;
  public isMcqAnswered:Boolean;
  public limit: number = 200;;
  truncating = true;
  public commentIdList: String[];
  public commentCOArray: SclPostCommentCO[] = new Array();
  public isAllCommentsLoaded: boolean = true;
  public chunkSize: string;
  public count = 0;
  public postTitleRoutingLink:string;
  show = false;
  showClass:string='lg16 text-grey paraTextLines';
  //public shareUrl: string;
  defaultImageSquare:String = AppConstants.DEFAULT_IMAGE_SQUARE;
  defaultImageCircle:String = AppConstants.DEFAULT_IMAGE_CIRCLE;
  

  public jumpTagString: string;
  dataLength: boolean;
  @Input()
  set postCO(value) 
  {
    this.sclPostCOBehaviorSubject.next(value);
  };

  get postCO() 
  {
    return this.sclPostCOBehaviorSubject.getValue();
  } 
  
  

   @ViewChild("fixAdvertisement", { static: false }) srcElement;
  @ViewChild("hideSocialOnFooter", { static: false }) srcElement1;


  constructor(
    private renderer: Renderer2,
    private managePopupService: ManagePopupService,
    private blogModuleService: BlogModuleService,
    private modalService: NgbModal,
    private appCurrentStateService: AppCurrentStateService,
    public location: Location,
    public sanitizer: DomSanitizer,
    public router: Router,
    private meta: Meta,
    private title: Title,
    @Inject(DOCUMENT) private doc) { }

  ngOnInit() {
    if (this.appCurrentStateService.isBrowser()) {
     // this.shareUrl = window.location.href;
      this.jumpTagString = window.location.href.split('#')[1];
      /* this.blogModuleService.getFacebookShareCount(this.shareUrl)
        .subscribe(response => {
          // console.log("res123",response)
          //this.facebookCount = parseInt(response.share['share_count'])
        }); */
    }

    this.scheduleJumpTag(this.jumpTagString);

    this.sclPostCOBehaviorSubject
    .subscribe(x => {
      if (this.postCO) 
      {
        if (this.postCO.type === AppConstants.ARTICLE) {
          this.postTitleRoutingLink = "/";
          this.replyCommentCount = parseInt(this.postCO.totalComments) + parseInt(this.postCO.totalReplies);
          this.writeCommentStatus = "comment"
          this.textbackground = this.postCO.userCO.firstName.charAt(0).toUpperCase()
          this.bindArticleData();
        }
        else if (this.postCO.type === AppConstants.POLL) {
          this.postTitleRoutingLink = "/";
          this.replyCommentCount = parseInt(this.postCO.totalComments);
          this.textbackground = this.postCO.userCO.firstName.charAt(0).toUpperCase()
          this.writeCommentStatus = "poll";
          this.bindPollData();
        }
        else if (this.postCO.type === AppConstants.QUESTION) 
        {
          this.replyCommentCount = parseInt(this.postCO.totalComments);
          this.textbackground = this.postCO.userCO.firstName.charAt(0).toUpperCase()
          this.writeCommentStatus = "answer";
          if (this.postCO.sclPostQuestionCO.type === AppConstants.SUBJECTIVE) 
          {
            this.bindSubjectiveQuestionData();
          }
          else if (this.postCO.sclPostQuestionCO.type === AppConstants.MCQ) 
          {
            this.replyCommentCount = parseInt(this.postCO.totalComments);
            this.textbackground = this.postCO.userCO.firstName.charAt(0).toUpperCase()
            this.bindMCQData();
          }

          if(this.postCO.sclPostQuestionCO.questionStatus)
          {
            this.postTitleRoutingLink = "/";
          }
          else
          {
            this.postTitleRoutingLink = "/unanswered";
          }
        }
        else if (this.postCO.type === AppConstants.LABEL) {
          this.postTitleRoutingLink = "/";
          this.replyCommentCount = parseInt(this.postCO.totalComments);
          this.textbackground = this.postCO.userCO.firstName.charAt(0).toUpperCase()
          this.bindLabelData();
        }
        else if (this.postCO.type === AppConstants.QUIZ) {
          this.postTitleRoutingLink = "/";
          this.replyCommentCount = parseInt(this.postCO.totalComments);
          this.textbackground = this.postCO.userCO.firstName.charAt(0).toUpperCase()
          this.bindQuizData();
        }
       
        
        //like work
        if (this.postCO.hasUserLiked === 'true') {
          this.postLikeStatus = true;
        }
        else {
          this.postLikeStatus = false;
        }
      }
      if (this.router.url.includes('/unanswered/')) {
        this.meta.addTag({ name: 'robots', content: 'noindex, nofollow' });
      }
    });

    this.managePopupService.appPopupCO.subscribe(appPopupCO => {
      this.appPopupCO = appPopupCO;

      if (this.appCurrentStateService.isBrowser()) {
        if (this.appPopupCO.isAnyPopupOpen()) {
          this.renderer.addClass(document.body, 'modal-open');
        }
        else {
          this.renderer.removeClass(document.body, 'modal-open');
        }
      }
    });

    this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
      this.appCurrentState = appCurrentState;
  //    this.textBackgroundUserProfile=this.appCurrentState.userCO.firstName.charAt(0).toUpperCase();
      if(!this.appCurrentState.isUserLoggedIn || AppUtility.isEmpty(this.appCurrentState.userCO.firstName))
      {
          this.textBackgroundUserProfile=AppConstants.DEFAULT_FIRST_CHARACTER;
      }
      else{
          this.textBackgroundUserProfile=this.appCurrentState.userCO.firstName.charAt(0).toUpperCase();
      }
    });
    /* this.replyCommentCount = parseInt(this.postCO.totalComments) + parseInt(this.postCO.totalReplies);

    if (this.postCO.type === AppConstants.ARTICLE) 
    {
      this.writeCommentStatus = "comment"
      this.textbackground=this.postCO.userCO.firstName.charAt(0).toUpperCase()
      this.bindArticleData();
    }
    else if (this.postCO.type === AppConstants.POLL) {
      this.textbackground=this.postCO.userCO.firstName.charAt(0).toUpperCase()
      this.writeCommentStatus = "poll"
      this.bindPollData();
    }
    else if (this.postCO.type === AppConstants.QUESTION) 
    {
      this.textbackground=this.postCO.userCO.firstName.charAt(0).toUpperCase()
      this.writeCommentStatus = "answer"
      if (this.postCO.sclPostQuestionCO.type === AppConstants.SUBJECTIVE)
       {
        this.bindSubjectiveQuestionData();
       }
      else if (this.postCO.sclPostQuestionCO.type === AppConstants.MCQ) 
      {
        this.textbackground=this.postCO.userCO.firstName.charAt(0).toUpperCase()
        this.bindMCQData();
      }
    }
    else if (this.postCO.type === AppConstants.LABEL) 
    {
      this.textbackground=this.postCO.userCO.firstName.charAt(0).toUpperCase()
      this.bindLabelData();
    } */
    //this.postDescription ="\\sum_{i=1}^nx_i";
   // this.postTitle ="(x = {-b \pm \sqrt{b^2-4ac} \over 2a}\)"
    

    //this.advertisementScrollEvent();
    // this.onWindowScrollBottom();
    //this.hideSocialIconsOnFooter();
    this.isReadMore(this.postDescription)
    
  }

  // Sticky Advertisement Code Start
  advertisementScrollEvent() { // To stick advertisement
    if (this.appCurrentStateService.isBrowser()) {
      window.addEventListener("scroll", e => {
        if (window.pageYOffset > 1200) {
          this.srcElement.nativeElement.classList.add("stickyAdvertisement");
          this.srcElement.nativeElement.classList.add("animated");
          this.srcElement.nativeElement.classList.add("fadeInDown");
        } else {
          this.srcElement.nativeElement.classList.remove("stickyAdvertisement");
          this.srcElement.nativeElement.classList.remove("animated");
          this.srcElement.nativeElement.classList.remove("fadeInDown");
        }
      });
    }
  }

  onWindowScrollBottom() { // To close advertisement
    if (this.appCurrentStateService.isBrowser()) {
      window.addEventListener("scroll", e => {
        if ((window.innerHeight + window.scrollY - 180) >= document.body.offsetHeight) {
          this.srcElement.nativeElement.classList.remove("stickyAdvertisement");
        }
      })
    }
  };


  hideSocialIconsOnFooter() { // To hide social icon on footer
    if (this.appCurrentStateService.isBrowser()) {
      window.addEventListener("scroll", e => {
        if (
          window.innerHeight + window.scrollY - 180 >=
          document.body.offsetHeight
        ) {
          this.srcElement1.nativeElement.classList.add("d-none");
        } else {
          this.srcElement1.nativeElement.classList.remove("d-none");
        }
      });
    }
  }
  // Sticky Advertisement Code End

  bindArticleData() 
  {
    this.postType = this.postCO.type;
    //this.postTitle = this.postCO.sclPostArticlesCO.title;
   this.bindMathematicalEquation(this.postCO.sclPostArticlesCO.title);
    if (this.postCO.sclPostArticlesCO.shortDecription) 
    {
      this.postDescription = this.postCO.sclPostArticlesCO.shortDecription;
    }
   /*  if (this.postCO.hasUserLiked === 'true') {
      this.postLikeStatus = true;
    }
    else {
      this.postLikeStatus = false;
    } */
    this.userFunction = "Posted an";
  }

  bindPollData() {
    this.postType = this.postCO.type;
    //this.postTitle = this.postCO.sclPostPollCO.title;
    this.bindMathematicalEquation(this.postCO.sclPostPollCO.title);
    if (this.postCO.sclPostPollCO.description) 
    {
      this.postDescription = this.postCO.sclPostPollCO.description;
    }

    this.userFunction = "Created a";
    this.bindPollOption();
  }

  bindSubjectiveQuestionData() {

    this.postType = this.postCO.type;
    this.bindMathematicalEquation(this.postCO.sclPostQuestionCO.sclSubjectiveQuesCO.title);

   
    //this.postTitle = this.postCO.sclPostQuestionCO.sclSubjectiveQuesCO.title;

    if (this.postCO.sclPostQuestionCO.sclSubjectiveQuesCO.description) {
      this.postDescription = this.postCO.sclPostQuestionCO.sclSubjectiveQuesCO.description;
    }

    this.userFunction = "Asked a";
  }

  bindLabelData()
  {
    this.postType = this.postCO.sclPostLabelCO.labelName;
    //this.postTitle = this.postCO.sclPostLabelCO.title;
    this.bindMathematicalEquation(this.postCO.sclPostLabelCO.title);
    //this.bindLabelDescription()

    this.userFunction = "Posted in";
    if (this.postCO.sclPostLabelCO.shortDescription) 
    {
      this.postDescription = this.postCO.sclPostLabelCO.shortDescription;
    }
    else
    {
      this.postDescription = this.postCO.sclPostLabelCO.description;
    }

  }
  
  bindMCQData() 
  {
    this.postType = this.postCO.sclPostQuestionCO.type;
    //this.postTitle = this.postCO.sclPostQuestionCO.sclPostMcqCO.title;
    this.bindMathematicalEquation(this.postCO.sclPostQuestionCO.sclPostMcqCO.title);
    if (this.postCO.sclPostQuestionCO.sclPostMcqCO.description) 
    {
      this.postDescription = this.postCO.sclPostQuestionCO.sclPostMcqCO.description;
    }

    this.userFunction = "Asked an";
    this.bindMcqOption();
  }

  bindQuizData() 
  {
    this.postType = this.postCO.type;
    //this.postTitle = this.postCO.sclPostQuestionCO.sclPostMcqCO.title;
    this.bindMathematicalEquation(this.postCO.sclQuizCO.title);

    this.userFunction = "Posted in";
  }

  bindMcqOption()
   {
     
    for (let i = 0; i < this.postCO.sclPostQuestionCO.sclPostMcqCO.sclMcqOptionCOList.length; i++) 
    {
      for (let j = 0; j < this.postCO.sclPostQuestionCO.sclPostMcqCO.sclUserMcqOptionCOList.length; j++)
       {
        if (this.postCO.sclPostQuestionCO.sclPostMcqCO.sclUserMcqOptionCOList[j].mcqOptionId === this.postCO.sclPostQuestionCO.sclPostMcqCO.sclMcqOptionCOList[i].id) 
        {
          this.postCO.sclPostQuestionCO.sclPostMcqCO.sclMcqOptionCOList[i].isAnsweredOption = true;
        }
      }
    }
   
    if(this.postCO.sclPostQuestionCO.sclPostMcqCO.sclUserMcqOptionCOList
       && this.postCO.sclPostQuestionCO.sclPostMcqCO.sclUserMcqOptionCOList.length >0)
      {
        this.isMcqAnswered = true;
      }
      else
      {
        this.isMcqAnswered = false;;
      }
   
  }

  bindPollOption()
   {
    // console.log("hehe",JSON.stringify(this.postCO.sclPostPollCO));
    for (let i = 0; i < this.postCO.sclPostPollCO.sclPollOptionCOList.length; i++) 
    {
      if (this.postCO.sclPostPollCO.sclUserPollOptionCO) 
      {
        this.postCO.sclPostPollCO.sclPollOptionCOList[i].isAnsweredOption = true;
      }
      else 
      {
        this.postCO.sclPostPollCO.sclPollOptionCOList[i].isAnsweredOption = false;
      }
    }
  }


  likePost() {

    //this.postCO.hasUserLiked = true;
    if (this.postCO.type === AppConstants.ARTICLE) {
      if (this.appCurrentState.isUserLoggedIn) {
        this.postLikeStatus = true;
        let count = parseInt(this.postCO.totalLikes);
        count = count + 1;
        this.postCO.totalLikes = count.toString();
        let likePostApiCall: ApiCall = new ApiCall(ApiActions.likePost);
        likePostApiCall.addRequestParams('postId', this.postCO.id);

        this.blogModuleService.likePost(likePostApiCall).subscribe(responseValues => {
          //Do nothing

        });
      }
      else {
        this.openLoginPopUp(this.postCO.defaultCategoryId)
      }

    }
    else {
      this.openAppPopUp();
    }


  }

  unlikePost() {
    if (this.postCO.type === AppConstants.ARTICLE) {
      if (this.appCurrentState.isUserLoggedIn) {
        this.postLikeStatus = false;
        let count = parseInt(this.postCO.totalLikes);
        count = count - 1;
        this.postCO.totalLikes = count.toString();
        let unlikePostApiCall: ApiCall = new ApiCall(ApiActions.unlikePost);
        unlikePostApiCall.addRequestParams('postId', this.postCO.id);
        this.blogModuleService.unlikePost(unlikePostApiCall).subscribe(response => {
          //Do nothing

        });
      }
      else {
        this.openLoginPopUp(this.postCO.defaultCategoryId)
      }

    }
    else {
      this.openAppPopUp();
    }


  }

  openLoginPopUp(categoryId) {

    // this.managePopupService.showLoginPopUp(categoryId);
    localStorage.setItem(AppConstants.REGISTRATION_PROCESS, AppConstants.BLOG_ACTIVITY_REGISTRATION_PROCESS);
    this.managePopupService.showSignPopUp(categoryId,AppConstants.APP_PROMOTION);

    /*  if (!this.appPopupCO.isAnyPopupOpen()) {
       this.managePopupService.showAndroidAppPopUp();
     } */


  }

  openAppPopUp() {
    if (!this.appPopupCO.isAnyPopupOpen()) {
      this.managePopupService.showAndroidAppPopUp();
    }
  }

  public getFileSize(size: number): string {
    let sizeAct;
    let unit;
    if (size < 1000) {
      sizeAct = size;
      unit = "bytes";
    } else if (size < 1000 * 1000) {
      sizeAct = size / 1000;
      unit = "kb";
    } else if (size < 1000 * 1000 * 1000) {
      sizeAct = size / 1000 / 1000;
      unit = "mb";
    } else {
      sizeAct = size / 1000 / 1000 / 1000;
      unit = "gb";
    }
    return sizeAct + " " + unit;
  }


  redirectUrl() {
    if (this.postCO.type != 'ARTICLE') 
    {
      if (this.postCO.type == AppConstants.POLL) {
        if (this.postCO.slug) 
        {
          this.router.navigate(['/', this.postCO.slug]);
        }
        else {
          this.router.navigate([""]);
        }
        
      }
      else {
        if (this.postCO.sclPostQuestionCO.questionStatus) {
          if (this.postCO.slug) {
            this.router.navigate(['/', this.postCO.slug]);
          }
          else {
            this.router.navigate([""]);
          }
        }
        else {
          if (this.postCO.slug) {
            this.router.navigate(['/unanswered', this.postCO.slug]);
          }
          else {
            this.router.navigate([""]);
          }

        }
      }

    }
    else {
      if (this.postCO.slug) {
        this.router.navigate(['/', this.postCO.slug]);
      }
      else {
        this.router.navigate([""]);
      }

    }


  }

  public redirectToComment() {
    if (this.postCO.slug) {

      this.router.navigate(['/', this.postCO.slug],{fragment: 'commentId'});
    }
    else {
      this.router.navigate([""]);
    }

  }


  public getOrganisationName(): string {

    if (this.postCO.userCO.isAdmin === '1') {
      return "@" + this.postCO.userCO.organisationName;
    }
    else {
      return "";
    }
  }

  redirectUrlForViewAllComments() 
  {
    if (this.postCO.type != 'ARTICLE') 
    {
      if (this.postCO.type == AppConstants.POLL) 
      {
        if (this.postCO.slug) 
        {
          this.router.navigate(['/', this.postCO.slug],{fragment: 'commentId'});
        }
        else {
          this.router.navigate([""]);
        }
      }
      else {
        if (this.postCO.sclPostQuestionCO.questionStatus) {
          if (this.postCO.slug) {
            this.router.navigate(['/', this.postCO.slug],{fragment: 'commentId'});
          }
          else {
            this.router.navigate([""]);
          }
        }
        else {
          if (this.postCO.slug) {
            this.router.navigate(['/unanswered', this.postCO.slug],{fragment: 'commentId'});
          }
          else {
            this.router.navigate([""]);
          }

        }
      }

    }
    else 
    {
      if (this.postCO.slug)
       {
        this.router.navigate(['/', this.postCO.slug],{fragment: 'commentId'});
      }
      else 
      {
        this.router.navigate([""]);
      }

    }
  }

  


  public getClassForTextBackGround(type:string):String
  {
    if(type=="40")
    {
      return "user-profile-commonDesign userProfile-40 "+AppUtility.textBackgroundColor(this.textbackground)+" sm16";;
    }
    else
    {
      return "user-profile-commonDesign userProfile-48 "+AppUtility.textBackgroundColor(this.textBackgroundUserProfile)+" sm16";;
    }
      
  }

  public onUserNameClick()
  {
    return "www.fb.com";
    /* if(this.postCO.userCO && this.postCO.userCO.fullName().toLowerCase().includes("@eduncle"))
    {

    } */
  }

  public isUserAdmin():Boolean
  {
    if(this.postCO.userCO && this.postCO.userCO.userType && (this.postCO.userCO.userType.toLowerCase() === "admin" || this.postCO.userCO.userType.toLowerCase() === "moderator"))
    {
      return true;
    }
    else
    {
      return false;
    }
   /*  if(this.postCO.userCO && this.postCO.userCO.fullName().toLowerCase().includes("@eduncle"))
    {
      return true;
    }
    else
    {
      return false;
    } */
  }

  openPopUp()
  {
    if(this.appCurrentState.isUserLoggedIn)
    {
      this.openAppPopUp();
    }
    else
    {
      this.openLoginPopUp(this.postCO.defaultCategoryId);
    }
  }

  
  getInitialCommentList() {
 //   console.log("getInitialCommentList");
    let getPostInitialCommentListApiCall: ApiCall = new ApiCall(ApiActions.getPostInitialCommentList);
    getPostInitialCommentListApiCall.addRequestParams('postId', this.postCO.id);

    this.blogModuleService.getPostInitialCommentList(getPostInitialCommentListApiCall).subscribe(responseValues => {
      
      this.commentIdList = responseValues['sclPostCommentIdList'];
      this.commentCOArray = <SclPostCommentCO[]>responseValues['sclPostCommentCOList'];
      this.chunkSize = responseValues['chunkSize'];
      this.commentCOArray = deserialize<SclPostCommentCO[]>(SclPostCommentCO, this.commentCOArray);
      if (this.commentCOArray.length === this.commentIdList.length) {
        this.isAllCommentsLoaded = false;
      }
      else{
       // this.replyCommentCount =this.commentIdList.length-this.commentCOArray.length;
       this.loadMoreCount=this.commentIdList.length-this.commentCOArray.length;
        this.isAllCommentsLoaded = true;
      }
    });
  }

  loadMoreComments() 
  {
    if (this.commentCOArray.length <= this.commentIdList.length) 
    {
      let size = this.commentIdList.length - this.commentCOArray.length;
      this.loadMoreCount =size;
      if (size >= parseInt(this.chunkSize)) {
        size = parseInt(this.chunkSize);
      }
      else {
        this.isAllCommentsLoaded = false;
      }

      let tempList = new Array();

      let msize = size + this.commentCOArray.length;
      for (let i = this.commentCOArray.length; i < msize; i++) {
        tempList.push(this.commentIdList[i]);
      }
      let getCommentDetailListApiCall: ApiCall = new ApiCall(ApiActions.getCommentDetailList);
      getCommentDetailListApiCall.addRequestParams('commentIdList', tempList);

      this.blogModuleService.getCommentDetailList(getCommentDetailListApiCall).subscribe(response => {
        this.commentCOArray = this.commentCOArray.concat(response);
        /* this.count = 0;
        for (let comment of this.commentCOArray) {
          this.count = this.count + parseInt(comment.totalReply);
        }
        this.count = this.count + this.commentCOArray.length; */
      });
    }
  }

  canIReport(): boolean 
  {
    if (this.appCurrentState.isUserLoggedIn) 
    {
      if (this.postCO.userCO.id === this.appCurrentState.userCO.id) 
      {
        return false;
      }
      return true;
    }
    else 
    {
      return false;
    }
  }

  canINotifyWhenAnswered(): boolean 
  {
    if (this.appCurrentState.isUserLoggedIn) 
    {
      if (this.postCO.type === AppConstants.QUESTION) 
      {
        if (!this.postCO.sclPostQuestionCO.questionStatus && this.postCO.userCO.id != this.appCurrentState.userCO.id) 
        {
          return true;
        }
        else 
        {
          return false;
        }
      }
      else {
        return false;
      }
    }
    else
    {
      return false;
    }
  }

  bindMathematicalEquation(postTitle) 
  {
    if (this.appCurrentStateService.isBrowser() ) 
    {
       try{
        let id = "#abc" + this.postCO.id;
        AppUtility.MathJaxRendering(() => {
          const math = document.querySelector(id);
          math.innerHTML = postTitle;
          return math;
        });
       }
       catch(error)
       {
        this.postTitle = postTitle;
       }
    }
    else 
    {
      this.postTitle = postTitle;
    }
  }


 changeClassForShow()
 {  
   this.show=!this.show

    if(this.show)
    {
      this.showClass= "lg16 text-grey paraTextLines show-MorePara";
    }
    else
    {
      this.showClass=  "lg16 text-grey paraTextLines"
    }



 } 

 getVideoUrl(url:string):string
  {
    return AppUtility.DcpVideoUrl(url)
  }


  scheduleJumpTag(id: string) {
    setTimeout(() => {
        if (this.appCurrentStateService.isBrowser())
        {
            let elem: HTMLElement = document.getElementById(id);

            if(elem)
            {
                //elem.scrollIntoView()
                elem.scrollIntoView();
            }
        }

    }, 0);
}

  isReadMore(data: string) {
    this.dataLength = !(data.length > 250)
  }

  
  getFeaturedImage(postType:String):string
  {
      if(postType===AppConstants.ARTICLE)
      {
          return this.postCO.sclPostArticlesCO.featuredImage;
      }
      else if(postType===AppConstants.LABEL)
      {
        return this.postCO.sclPostLabelCO.featuredImage;
      }
      else if(postType===AppConstants.QUIZ)
      {
        //return this.postCO.sclQuizCO.featuredImage;
        if(!this.postCO.sclQuizCO.featuredImage)
        {
          return "https://thumbs.dreamstime.com/b/quiz-pop-website-test-isolated-human-wisdom-63221258.jpg";
        }
        else
        {
          return this.postCO.sclQuizCO.featuredImage;
        }
       
      }
     
  }

  findPostIsArticleOrQuestion():Boolean
  {
      if(this.postCO.type===AppConstants.ARTICLE)
      {
        return true;
      }
      else if(this.postCO.type===AppConstants.LABEL)
      {
          if(this.postCO.sclPostLabelCO.featuredImage && !this.postCO.sclVideoPostAttachmentsCO && this.postCO.sclPostLabelCO.viewType && this.postCO.sclPostLabelCO.viewType===AppConstants.ARTICLE)
          {
              return true;
          }
          else
          {
            return false;
          }
      }
      else
      {
        return false;
      }
  }
   public getResponsiveImage(imagePath:string):string {
    return AppUtility.getResponsiveImage(imagePath);
   }

   increaseFbShareCount()
   {

    if(!this.appCurrentState.isUserLoggedIn)
    {
      this.openLoginPopUp(this.postCO.defaultCategoryId);
    }
       
    /* let increaseFbShareCountApiCall: ApiCall = new ApiCall(ApiActions.scl_increaseFbShareCount);
    increaseFbShareCountApiCall.addRequestParams('postId', this.postCO.id);

    this.blogModuleService.increaseFbShareCount(increaseFbShareCountApiCall).subscribe(response => {
        let result = response;
        if(this.postCO.fbShareCount)
        {
            let updatedFbCOunt = +(this.postCO.fbShareCount) + 1;
            this.postCO.fbShareCount = updatedFbCOunt.toString();
        }
        else{
            this.postCO.fbShareCount = "1";
        }
    }); */
   }


}
