import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialTimelineComponent } from './social-timeline.component';

describe('SocialTimelineComponent', () => {
  let component: SocialTimelineComponent;
  let fixture: ComponentFixture<SocialTimelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialTimelineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
