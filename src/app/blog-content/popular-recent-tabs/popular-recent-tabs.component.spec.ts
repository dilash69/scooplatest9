import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopularRecentTabsComponent } from './popular-recent-tabs.component';

describe('PopularRecentTabsComponent', () => {
  let component: PopularRecentTabsComponent;
  let fixture: ComponentFixture<PopularRecentTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopularRecentTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopularRecentTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
