import { AppCurrentStateService } from './../../services/app.current.state.service';
import { SclPostCO } from './../../co/sclPostCO';
import { SclPostArticlesCO } from './../../co/sclPostArticlesCO';
import { BehaviorSubject} from 'rxjs';
import { CatCategoriesCO } from './../../co/categoryCO';
import { BlogModuleService } from './../../services/blog-module.service';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'app-popular-recent-tabs',
  templateUrl: './popular-recent-tabs.component.html',
  styleUrls: ['./popular-recent-tabs.component.css']
})
export class PopularRecentTabsComponent implements OnInit 
{

  public sclPostCOArray :SclPostCO[];
  public recentsclPostCOArray :SclPostCO[];
  
  @Input() defaultCategoryId: string;
  
  constructor(private blogModuleService : BlogModuleService,
    private appCurrentStateService:AppCurrentStateService) 
  { 

  }

  ngOnInit() 
  {
    if(this.defaultCategoryId)
    {
        let getMostPopularArticleListApiCall: ApiCall = new ApiCall(ApiActions.getMostPopularArticleList);
        getMostPopularArticleListApiCall.addRequestParams('categoryId', this.defaultCategoryId);

        this.blogModuleService.getMostPopularArticleList(getMostPopularArticleListApiCall).subscribe(response => 
        {
          this.sclPostCOArray = response;
        });

        let getRecentArticleListApiCall: ApiCall = new ApiCall(ApiActions.getRecentArticleList);
        getRecentArticleListApiCall.addRequestParams('categoryId', this.defaultCategoryId);
       
        this.blogModuleService.getRecentArticleList(getRecentArticleListApiCall).subscribe(response => 
        {
          this.recentsclPostCOArray = response;
        });
    }
    else
    {
        let getMostPopularGlobalArticleListApiCall: ApiCall = new ApiCall(ApiActions.getMostPopularGlobalArticleList);
        
        this.blogModuleService.getMostPopularGlobalArticleList(getMostPopularGlobalArticleListApiCall).subscribe(response => 
        {
          this.sclPostCOArray = response;
        });

        let getRecentGlobalArticleListApiCall: ApiCall = new ApiCall(ApiActions.getRecentGlobalArticleList);
       
        this.blogModuleService.getRecentGlobalArticleList(getRecentGlobalArticleListApiCall).subscribe(response => 
        {
          this.recentsclPostCOArray = response;
        });
    }
          
  }


  changePosition()
  {
    if(this.appCurrentStateService.isBrowser())
    {
      window.scrollTo(0,0);
    }
  }

}