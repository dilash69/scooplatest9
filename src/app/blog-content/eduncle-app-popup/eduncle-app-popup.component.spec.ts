import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EduncleAppPopupComponent } from './eduncle-app-popup.component';

describe('EduncleAppPopupComponent', () => {
  let component: EduncleAppPopupComponent;
  let fixture: ComponentFixture<EduncleAppPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EduncleAppPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EduncleAppPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
