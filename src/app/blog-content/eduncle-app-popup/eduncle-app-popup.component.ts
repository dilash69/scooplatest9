import { Component, OnInit } from '@angular/core';
import { ManagePopupService } from '../../services/manage-popup.service';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { BlogModuleService } from './../../services/blog-module.service';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { AppConstants } from './../../common/app.constants';
import { ActivatedRoute, Router } from '@angular/router';
import { UpdateCurrentCategoryDataService } from './../../services/update-current-category-data.service';
import { CatCategoriesCO } from '../../co/categoryCO';

@Component({
  selector: 'app-eduncle-app-popup',
  templateUrl: './eduncle-app-popup.component.html',
  styleUrls: ['./eduncle-app-popup.component.css']
})
export class EduncleAppPopupComponent implements OnInit {
  public currentCategoryName:string;
  public defaultTitle:string;
  public slug: string;
  public catCategoryCO:CatCategoriesCO;
  public mobileDownloadImagePath:string = "assets/images/promotion-app/iit-jam.png";

  /* constructor() { } */
  constructor(private managePopupService:ManagePopupService,
    private blogModuleService: BlogModuleService,
    private appCurrentStateService:AppCurrentStateService,
    private updateCurrentCategoryData: UpdateCurrentCategoryDataService,
    private router: Router) 
    {

    }


  ngOnInit() 
  {

    /* this.updateCurrentCategoryData.updateCurrentCategoryObservable.subscribe(catCategoryCO => 
      {
        if(catCategoryCO)
        {
          this.catCategoryCO = catCategoryCO;
          if(this.catCategoryCO.title)
          {
            this.currentCategoryName = this.catCategoryCO.title;
            this.currentCategoryName = this.currentCategoryName + " 2021 Exam?";
          }
          else
          {
            this.defaultTitle = "For Exam Preparation?";
          }
        }
        else{
          this.defaultTitle = "For Exam Preparation?";
        }
      }); */

      if(this.router.url.toLowerCase().includes("csir-net"))
      {//csir net
        this.mobileDownloadImagePath = "assets/images/promotion-app/csir-net.png"
        this.currentCategoryName =   "CSIR NET 2021 Exam?";
      }
      else if(this.router.url.toLowerCase().includes("iit-jam"))
      {//iit jam
        this.mobileDownloadImagePath = "assets/images/promotion-app/iit-jam.png"
        this.currentCategoryName =   "IIT JAM 2021 Exam?";
      } 
      else if(this.router.url === "/")
      {//home page
        this.mobileDownloadImagePath = "assets/images/promotion-app/ugc-net.png"
        this.currentCategoryName =   "Exam Preparation?";
      }
      else 
      {
        this.mobileDownloadImagePath = "assets/images/promotion-app/ugc-net.png"
        this.currentCategoryName =   "UGC NET 2021 Exam?";
      }



   

   /*  if(this.appCurrentStateService.isBrowser())
    {
      if(localStorage.getItem(AppConstants.currentCategoryName))
      {
        this.currentCategoryName = localStorage.getItem(AppConstants.currentCategoryName);
        this.currentCategoryName = this.currentCategoryName + " 2020 Exam?";
      }
      else
      {
        this.defaultTitle = "Your Exam Preparation?";
      }
      
    } */

  }


  dismiss() {
    this.managePopupService.closePopUp();
  } 

  addAppSubsciber(mobileNumber)
  {
    let saveAppSubscribersApiCall: ApiCall = new ApiCall(ApiActions.saveAppSubscribers);
    saveAppSubscribersApiCall.showLoader = true;
    saveAppSubscribersApiCall.addRequestParams('mobile', mobileNumber);
    saveAppSubscribersApiCall.addRequestParams('currentUrl',  window.location.href);
    /* if(localStorage.getItem("currentCategoryId"))
    {
      saveAppSubscribersApiCall.addRequestParams('categoryId',  localStorage.getItem("currentCategoryId"));
    } */
    if(this.catCategoryCO.id)
    {
      saveAppSubscribersApiCall.addRequestParams('categoryId',  this.catCategoryCO.id);
    }
    this.blogModuleService.saveAppSubscribers(saveAppSubscribersApiCall)
    .subscribe(responseValues =>
     {
       this.dismiss();
       this.showSuccessAlert();
     });
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

 showSuccessAlert()
  {
    this.appCurrentStateService.updateAlertMessage(AppConstants.linkSentMobile,"success");
  }
  
}
