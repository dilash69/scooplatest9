import { Component, OnInit } from '@angular/core';
import { ManagePopupService } from './../../services/manage-popup.service';

@Component({
  selector: 'app-confirm-unfollow',
  templateUrl: './confirm-unfollow.component.html',
  styleUrls: ['./confirm-unfollow.component.css']
})
export class ConfirmUnfollowComponent implements OnInit {

  constructor(private managePopupService : ManagePopupService) { }

  ngOnInit() {
  }

  dismiss()
  {
    this.managePopupService.closePopUp();

  }

}
