import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmUnfollowComponent } from './confirm-unfollow.component';

describe('ConfirmUnfollowComponent', () => {
  let component: ConfirmUnfollowComponent;
  let fixture: ComponentFixture<ConfirmUnfollowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmUnfollowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmUnfollowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
