import { CatCategoriesCO } from './../../co/categoryCO';
import { FilterService } from './../../services/filter.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-in-banner',
  templateUrl: './search-in-banner.component.html',
  styleUrls: ['./search-in-banner.component.css']
})
export class SearchInBannerComponent implements OnInit 
{

  public catCategoriesCOList: CatCategoriesCO[];
  public categoryTitle:string[]= new Array();

  constructor(private filterService: FilterService) { }
 
  ngOnInit() 
  { 
    this.filterService.catCategoriesCOList.subscribe(catCategoriesCOList=> 
        {
            this.catCategoriesCOList=catCategoriesCOList;
            for(let category of this.catCategoriesCOList )
            {
                this.categoryTitle.push(category.title);
            }
        });   
  }
}
