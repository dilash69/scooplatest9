import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchInBannerComponent } from './search-in-banner.component';

describe('SearchInBannerComponent', () => {
  let component: SearchInBannerComponent;
  let fixture: ComponentFixture<SearchInBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchInBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchInBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
