import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderBkComponent } from './header-bk.component';

describe('HeaderBkComponent', () => {
  let component: HeaderBkComponent;
  let fixture: ComponentFixture<HeaderBkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderBkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderBkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
