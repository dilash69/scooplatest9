import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header-bk',
  templateUrl: './header-bk.component.html',
  styleUrls: ['./header-bk.component.css']
})
export class HeaderBkComponent implements OnInit {

  // Header toggle condition default close
  public isCollapsed = true; 
 
  constructor() { }

  ngOnInit() {
  }

}
