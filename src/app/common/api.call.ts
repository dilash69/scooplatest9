import { HttpHeaders, HttpParams } from '@angular/common/http';
import { ApiParams } from './api.params';
import { AppUtility } from './app.utility';
export class ApiCall
{
   action: string;
   showLoader: boolean;
   isGetRequest: boolean = false;
   invalidateCache:boolean = false;
   additionalUrlParameter: string;
   params: ApiParams[];
   uniqueKey: string;

   constructor(action: string)
   {
        this.action = action;
   }

   addRequestParams(key: string, value: any)
   {
      let stringValue:string;      
      if((value instanceof String) || (typeof value === "string"))
      {
        stringValue = <string>value;
      }
      else
      {
        stringValue = JSON.stringify(value);
      }

      if(this.params)
      {
        this.params.push(new ApiParams(key, stringValue));
      }
      else
      {
        this.params = new Array(new ApiParams(key, stringValue));
      }
   }

   constructOptionsForGet()
   {
    let httpParams = new HttpParams()
    .set('APP_VERSION', "55")
    .set('APP_TYPE',  "CUSTOMER")
    .set('REQ_SOURCE', "WEB")
    .set('USER_PROFILE_TYPE', "STUDENT")
    .set('ACTION', this.action);

      for (var i = 0; i < this.params.length; i++)
      {
        let apiParams:ApiParams =   this.params[i] ;
        httpParams = httpParams.append(apiParams.key, apiParams.value);
      }

      const options =  { params: httpParams }
      return options;
   }

   constructPostBody(accessToken:string)
   {
      if(accessToken)
      {
        this.addRequestParams("ACCESS_TOKEN", accessToken);
      }

      this.addRequestParams("APP_VERSION", "55");//5
      this.addRequestParams("APP_TYPE", "CUSTOMER");
      this.addRequestParams("REQ_SOURCE", "WEB");//WEB
      this.addRequestParams("USER_PROFILE_TYPE","STUDENT")
      

      if(this.invalidateCache)
      {
        this.addRequestParams("invalidateCache","Y");
      }
      
      let obj = {"ACTION": this.action};
      for (var i = 0; i < this.params.length; i++)
      {
        let apiParams:ApiParams =   this.params[i] ;
        obj[apiParams.key] = apiParams.value;
      }

      return JSON.stringify(AppUtility.sort(obj));
   }

   calculateUniqueKey()
   {
     this.uniqueKey = this.action;
     for (let i = 0; i < this.params.length; i++)
     {
       let apiParams:ApiParams = this.params[i] ;
       this.uniqueKey = apiParams.key + "-" + apiParams.value;
     }
   }

   constructHeader(accessToken:string)
   {
/*     let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization':  accessToken
      })
    };

    return httpOptions; */

     if(accessToken)
     {
        let httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json'/* ,
            
            'Authorization':  accessToken */
          })
        };

        return httpOptions;
     }
     else
     {
        let httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json'
          })
        };

        return httpOptions;
     } 
   }
}