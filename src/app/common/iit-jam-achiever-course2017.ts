export class IitJamAchieverCourse2017
{
    public static getIitJamAchieverCourse(): string 
    {
        return '<h2 class=\'md21 sm18 xs16 topmargin2\'>'+
        '		<strong>Know what made the IIT JAM Toppers? score best and gain merit scores!</strong>'+
        '	</h2>'+
        '	<p class=\'md15 sm14 xs13 lh150\'> '+
        '         If you aim to score high in JAM 2019 Exam, join Eduncle for smart Preparations! Choose amongst the content rich- comprehensively designed study courses for all JAM Subjects.'+
        '    </p>'+
        '	<div class=\'row\' style=\'margin-right: -15px;margin-left: -15px;margin-top:10px;\'>'+
        '	  	<div class=\'col-md-12\' style=\'padding-right: 15px;padding-left: 15px;\'>'+
        '		   <div class=\'row\' style=\'border: 1px solid #c7cdd2;float:left\'>'+
        '			<div class=\'c-header\'>'+
        '				<div class=\'col-md-12\'>'+
        '					IIT JAM 2019 Exam Crackers Course'+
        '				</div>'+
        '			</div>	'+
        '			<div class=\'col-md-6 col-sm-6 col-xs-12\' style=\'padding: 0 !important\'>'+
        '				<div class=\'c-header-inner\'>'+
        '					<div class=\'col-md-12 text-center c-header-content\'>'+
        '						Course Highlights'+
        '					</div>'+
        '				</div>				'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					2100 + Practice Questions with Solutions'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					15 Topic wise Unit Solved Papers (USPs)'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					5 Volume Solved Papers (VSPs)'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					12 Full Length Model Solved Papers (MSPS)'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					4 Previous Year Solved Papers (2015, 2016, 2017 & 2018) (PSPs)'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Easy to Understand Format'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Prepared by Highly Experienced and Qualified Faulty Team'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Complete Syllabus Covered'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Result Oriented Approach'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Based on Latest pattern and changes</br>'+
        '				</div>'+
        '			</div>'+
        ''+
        '			<div class=\'col-md-6 col-sm-6 col-xs-12 mobile-top25\' style=\'border-left: 1px solid #c7cdd2;padding: 0 !important\'>'+
        '				<div class=\'c-header-inner\'>'+
        '					<div class=\'col-md-12 text-center c-header-content\'>'+
        '						Subjects'+
        '					</div>'+
        '				</div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Physics</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'https://goo.gl/cDYHmh\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        ''+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Chemistry</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'https://goo.gl/6doQcy\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        ''+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Mathematics</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'https://goo.gl/t76Skf\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        ''+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Mathematical Stats</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'https://goo.gl/RE2qVy\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        ''+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Biological Science</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'https://goo.gl/eRnQRe\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        ''+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Biotechnology</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'https://goo.gl/2JqgAa\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        ''+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Geology</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'https://goo.gl/VWmsvQ\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '			</div>'+
        ''+
        '			<div class=\'c-footer\'>'+
        '				<div class=\'col-md-12\'>'+
        '					<span class=\'c-click\'>For More IIT JAM Related Courses </span>'+
        '					<span class=\'c-click\'><a href=\'https://goo.gl/iJMMjQ\' target=\'_blank\'>Click Here</a></span>'+
        '				</div>'+
        '			</div>'+
        '		  </div>'+
        '	  	</div>'+
        '	</div>';
    }

}