import { HttpResponse } from "@angular/common/http";

export class ApiCacheCO
{
    key: string;
    value: any;

   constructor(key: string, value: any)
   {
        this.key = key;
        this.value = value;
   }
}