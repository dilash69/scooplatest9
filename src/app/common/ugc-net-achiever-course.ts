export class UgcNetAchieverCourse
{
   /*  public static getUgcNetAchieverCourse2016_17(): string {
        return '<h2 class=\'md21 sm18 xs16 w700 topmargin2\'>'+
        '		<strong>Know how to crack UGC NET November 2017 Exam with the best study material</strong>'+
        '	</h2>'+
        '	<p class=\'md15 sm14 xs13 lh150\'> '+
        '        Eduncle caters for 9 popular subjects for UGC NET Exam. Look out for your opted subject and take your exam prep to the next level!'+
        '    </p>'+
        '	<div class=\'row\' style=\'margin-right: -15px;margin-left: -15px;margin-top:10px;\'>'+
        '	  	<div class=\'col-md-12\' style=\'padding-right: 15px;padding-left: 15px;\'>'+
        '		  <div class=\'row\' style=\'border: 1px solid #c7cdd2;float:left\'>'+
        '			<div class=\'c-header col-md-12\'>'+
        '				<div class=\'col-md-12\'>'+
        '					UGC NET November 2017 Exam Achievers Course'+
        '				</div>'+
        '			</div>	'+
        '			<div class=\'col-md-6 col-sm-6 col-xs-12\' style=\'padding: 0 !important\'>'+
        '				<div class=\'c-header-inner\'>'+
        '					<div class=\'col-md-12 text-center c-header-content\'>'+
        '						Course Highlights'+
        '					</div>'+
        '				</div>				'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					3100+ Practice Questions with Solutions.'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Complete Prescribed Syllabus Covered.'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Easy to Understand Format.'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Highly Experienced and NET Qualified Faculty.'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Result Oriented Study Material.'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Complete Theory Covered for PAPER I, II, III respectively.'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Topic Wise Unit Solved Papers (USPs) [PAPER I, II, III respectively].'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Volume Solved Papers (VSPs) [PAPER I, II, III respectively].'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Model Solved Papers (MSPs) [PAPER I, II, III respectively].'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Previous Year Solved Papers (PSPs) [PAPER I, II, III respectively].'+
        '				</div>'+
        '			</div>'+
        '			<div class=\'col-md-6 col-sm-6 col-xs-12 mobile-top25\' style=\'border-left: 1px solid #c7cdd2;padding: 0 !important\'>'+
        '				<div class=\'c-header-inner\'>'+
        '					<div class=\'col-md-12 text-center c-header-content\'>'+
        '						Subjects'+
        '					</div>'+
        '				</div>   '+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Commerce</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugcnet-commerce\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Computer Science</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugcnet-computer-science\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Economics</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugcnet-economics\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Education</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugcnet-education\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>English</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugcnet-english\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>HRM</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugcnet-hrm\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Library Science</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugcnet-library-science\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Management</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugcnet-management\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Political Science</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugcnet-political-science\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '			</div>'+
        '			<div class=\'c-footer\'>'+
        '				<div class=\'col-md-12\'>'+
        '					<span class=\'c-click\'>For More UGC NET Related Courses </span> '+
        '					<span class=\'c-click\'><a href=\'http://bit.ly/ac-ugc-net\' target=\'_blank\'>Click Here</a></span>'+
        '				</div>'+
        '			</div>'+
        '		  </div>'+
        '		</div>'+
        '	</div>';
    } */

   /*  public static getUgcNetAchieverCourse2018_19():string {
        return '<h2 class=\'md21 sm18 xs16 w700 topmargin2\'>'+
        '		<strong>Know how to crack UGC NET 2018 Exam with the best study material</strong>'+
        '	</h2>'+
        '	<p class=\'md15 sm14 xs13 lh150\'> '+
        '        Eduncle caters for 9 popular subjects for UGC NET Exam. Look out for your opted subject and take your exam prep to the next level!'+
        '    </p>'+
        '	<div class=\'row\' style=\'margin-right: -15px;margin-left: -15px;margin-top:10px;\'>'+
        '	  	<div class=\'col-md-12\' style=\'padding-right: 15px;padding-left: 15px;\'>'+
        '		  <div class=\'row\' style=\'border: 1px solid #c7cdd2;float:left\'>'+
        '			<div class=\'c-header col-md-12\'>'+
        '				<div class=\'col-md-12\'>'+
        '					UGC NET 2018 Exam Achievers Course'+
        '				</div>'+
        '			</div>	'+
        '			<div class=\'col-md-6 col-sm-6 col-xs-12\' style=\'padding: 0 !important\'>'+
        '				<div class=\'c-header-inner\'>'+
        '					<div class=\'col-md-12 text-center c-header-content\'>'+
        '						Course Highlights'+
        '					</div>'+
        '				</div>				'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					3550 + Practice Questions with Solutions.'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					25 Units of Theory Covered for PAPER I & II respectively.'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					25 Topic Wise Unit Solved Papers (USPs) [PAPER I & II respectively].'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					10 Volume Solved Papers (VSPs) [PAPER I & II respectively].'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					7 Model Solved Papers (MSPs) [PAPER I & II respectively].'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					5 Previous Year Solved Papers (PSPs) [PAPER I & II respectively].'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Complete Prescribed Syllabus Covered.'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Easy to Understand Format.'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Highly Experienced and NET Qualified Faculty.'+
        '				</div>'+
        '				<div class=\'c-highlights-list pull-left\'>'+
        '					<i class=\'fa fa-check\' aria-hidden=\'true\'></i>  '+
        '					Result Oriented Study Material.'+
        '				</div>'+
        '			</div>'+
        '			<div class=\'col-md-6 col-sm-6 col-xs-12 mobile-top25\' style=\'border-left: 1px solid #c7cdd2;padding: 0 !important\'>'+
        '				<div class=\'c-header-inner\'>'+
        '					<div class=\'col-md-12 text-center c-header-content\'>'+
        '						Subjects'+
        '					</div>'+
        '				</div>   '+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>General Paper - 1</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugc-net-paper-1-achiever-course\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>  '+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>English</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugc-net-english-achiever-course\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Commerce</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugc-net-commerce-achiever-course\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Management</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugc-net-management-achiever-course\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Computer Science</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugc-net-computer-science-achiever-course\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Economics</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugc-net-economics-achiever-course\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Education</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugc-net-education-achiever-course\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>HRM</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugc-net-hrm-achiever-course\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Political Science</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugc-net-political-science-achiever-course\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Library Science</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugc-net-library-science-achiever-course\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '				<div class=\'clear-bottom-border\'> </div>'+
        '				<div class=\'c-course-section\'>'+
        '					<span class=\'c-course-name md18 sm16 xs15 pull-left lh200\'>Law</span>'+
        '					<span class=\'c-course-btn\'>'+
        '						<a href=\'http://bit.ly/ugc-net-law-achiever-course\' target=\'_blank\'>Buy Now</a>'+
        '					</span>'+
        '				</div>'+
        '			</div>'+
        '			<div class=\'c-footer\'>'+
        '				<div class=\'col-md-12\'>'+
        '					<span class=\'c-click\'>For More UGC NET Related Courses </span> '+
        '					<span class=\'c-click\'><a href=\'https://goo.gl/5JpMzL\' target=\'_blank\'>Click Here</a></span>'+
        '				</div>'+
        '			</div>'+
        '		  </div>'+
        '		</div>'+
        '	</div>';
    } */


    public static getUgcNetAchieverCourse2018_19():string 
    {
        return '<div class="row">'+
        '	<div class="col-md-12">'+
        '	  <div class="row" style="border: 1px solid #c7cdd2;float:left;">'+
        '		<div class="c-header col-md-12">'+
        '			<div class="col-md-12">'+
        '				UGC NET 2019 Exam Achievers Course'+
        '			</div>'+
        '		</div>	'+
        '		<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0 !important">'+
        '			<div class="c-header-inner">'+
        '				<div class="col-md-12 text-center c-header-content">'+
        '					Course Highlights'+
        '				</div>'+
        '			</div>				'+
        '			<div class="c-highlights-list pull-left">'+
        '				<i class="fa fa-check" aria-hidden="true"></i>  '+
        '				3550 + Practice Questions with Solutions.'+
        '			</div>'+
        '			<div class="c-highlights-list pull-left">'+
        '				<i class="fa fa-check" aria-hidden="true"></i>  '+
        '				25 Units of Theory Covered for PAPER I & II respectively.'+
        '			</div>'+
        '			<div class="c-highlights-list pull-left">'+
        '				<i class="fa fa-check" aria-hidden="true"></i>  '+
        '				25 Topic Wise Unit Solved Papers (USPs) [PAPER I & II respectively].'+
        '			</div>'+
        '			<div class="c-highlights-list pull-left">'+
        '				<i class="fa fa-check" aria-hidden="true"></i>  '+
        '				10 Volume Solved Papers (VSPs) [PAPER I & II respectively].'+
        '			</div>'+
        '			<div class="c-highlights-list pull-left">'+
        '				<i class="fa fa-check" aria-hidden="true"></i>  '+
        '				7 Model Solved Papers (MSPs) [PAPER I & II respectively].'+
        '			</div>'+
        '			<div class="c-highlights-list pull-left">'+
        '				<i class="fa fa-check" aria-hidden="true"></i>  '+
        '				5 Previous Year Solved Papers (PSPs) [PAPER I & II respectively].'+
        '			</div>'+
        '			<div class="c-highlights-list pull-left">'+
        '				<i class="fa fa-check" aria-hidden="true"></i>  '+
        '				Complete Prescribed Syllabus Covered.'+
        '			</div>'+
        '			<div class="c-highlights-list pull-left">'+
        '				<i class="fa fa-check" aria-hidden="true"></i>  '+
        '				Easy to Understand Format.'+
        '			</div>	'+
        '			<div class="c-highlights-list pull-left">'+
        '				<i class="fa fa-check" aria-hidden="true"></i>  '+
        '				Highly Experienced and NET Qualified Faculty.'+
        '			</div>	'+
        '			<div class="c-highlights-list pull-left">'+
        '				<i class="fa fa-check" aria-hidden="true"></i>  '+
        '				Result Oriented Study Material.'+
        '			</div>			'+
        '		</div>'+
        '		<div class="col-md-6 col-sm-6 col-xs-12 mobile-top25" style="border-left: 1px solid #c7cdd2;padding: 0 !important">'+
        '			<div class="c-header-inner">'+
        '				<div class="col-md-12 text-center c-header-content">'+
        '					Subjects'+
        '				</div>'+
        '			</div>   '+
        '			<div class="c-course-section">'+
        '				<span class="c-course-name md18 sm16 xs15 pull-left lh200">General Paper - 1</span>'+
        '				<span class="c-course-btn">'+
        '					<a href="http://bit.ly/ugc-net-paper-1-achiever-course" target="_blank">Buy Now</a>'+
        '				</span>'+
        '			</div>'+
        '			<div class="clear-bottom-border"> </div>'+
        '			<div class="c-course-section">'+
        '				<span class="c-course-name md18 sm16 xs15 pull-left lh200">English</span>'+
        '				<span class="c-course-btn">'+
        '					<a href="http://bit.ly/ugc-net-english-achiever-course" target="_blank">Buy Now</a>'+
        '				</span>'+
        '			</div>'+
        '			<div class="clear-bottom-border"> </div>'+
        '			<div class="c-course-section">'+
        '				<span class="c-course-name md18 sm16 xs15 pull-left lh200">Commerce</span>'+
        '				<span class="c-course-btn">'+
        '					<a href="http://bit.ly/ugc-net-commerce-achiever-course" target="_blank">Buy Now</a>'+
        ''+
        '				</span>'+
        '			</div>'+
        '			<div class="clear-bottom-border"> </div>'+
        '			<div class="c-course-section">'+
        '				<span class="c-course-name md18 sm16 xs15 pull-left lh200">Management</span>'+
        '				<span class="c-course-btn">'+
        '					<a href="http://bit.ly/ugc-net-management-achiever-course" target="_blank">Buy Now</a>'+
        '				</span>'+
        '			</div>'+
        '			<div class="clear-bottom-border"> </div>'+
        '			<div class="c-course-section">'+
        '				<span class="c-course-name md18 sm16 xs15 pull-left lh200">Computer Science</span>'+
        '				<span class="c-course-btn">'+
        '					<a href="http://bit.ly/ugc-net-computer-science-achiever-course" target="_blank">Buy Now</a>'+
        '				</span>'+
        '			</div>'+
        '			<div class="clear-bottom-border"> </div>'+
        '			<div class="c-course-section">'+
        '				<span class="c-course-name md18 sm16 xs15 pull-left lh200">Economics</span>'+
        '				<span class="c-course-btn">'+
        '					<a href="http://bit.ly/ugc-net-economics-achiever-course" target="_blank">Buy Now</a>'+
        '				</span>'+
        '			</div>'+
        '			<div class="clear-bottom-border"> </div>'+
        '			<div class="c-course-section">'+
        '				<span class="c-course-name md18 sm16 xs15 pull-left lh200">Education</span>'+
        '				<span class="c-course-btn">'+
        '					<a href="http://bit.ly/ugc-net-education-achiever-course" target="_blank">Buy Now</a>'+
        '				</span>'+
        '			</div>'+
        '			<div class="clear-bottom-border"> </div>'+
        '			<div class="c-course-section">'+
        '				<span class="c-course-name md18 sm16 xs15 pull-left lh200">HRM</span>'+
        '				<span class="c-course-btn">'+
        '					<a href="http://bit.ly/ugc-net-hrm-achiever-course" target="_blank">Buy Now</a>'+
        '				</span>'+
        '			</div>'+
        '			<div class="clear-bottom-border"> </div>'+
        '			<div class="c-course-section">'+
        '				<span class="c-course-name md18 sm16 xs15 pull-left lh200">Political Science</span>'+
        '				<span class="c-course-btn">'+
        '					<a href="http://bit.ly/ugc-net-political-science-achiever-course" target="_blank">Buy Now</a>'+
        '				</span>'+
        '			</div>'+
        '			<div class="clear-bottom-border"> </div>'+
        '			<div class="c-course-section">'+
        '				<span class="c-course-name md18 sm16 xs15 pull-left lh200">Library Science</span>'+
        '				<span class="c-course-btn">'+
        '					<a href="http://bit.ly/ugc-net-library-science-achiever-course" target="_blank">Buy Now</a>'+
        '				</span>'+
        '			</div> '+
        '			<div class="clear-bottom-border"> </div>'+
        '			<div class="c-course-section">'+
        '				<span class="c-course-name md18 sm16 xs15 pull-left lh200">Law</span>'+
        '				<span class="c-course-btn">'+
        '					<a href="http://bit.ly/ugc-net-law-achiever-course" target="_blank">Buy Now</a>'+
        '				</span>'+
        '			</div>'+
        '		</div>'+
        '		<div class="c-footer">'+
        '			<div class="col-md-12">'+
        '				For More UGC NET Related Courses  '+
        '				<span class="c-click"><a href="https://www.eduncle.com/store/search?sortby=sellhtl&cat_id=2&category=UGC+NET" target=\'_blank\'>Click Here</a></span>'+
        '			</div>'+
        '		</div>'+
        '	  </div>'+
        '	</div>'+
        '</div>'; 
    }
}