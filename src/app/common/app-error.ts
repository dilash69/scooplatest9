export class AppError
{
    constructor(originalError?: any)
    {
        if(originalError)
        {
            console.error(originalError);
        }
    }
}