import { environment } from "../../environments/environment";


export class AppSettings 
{
   // public static API_ENDPOINT = 'http://localhost:8080/api/fc';
    //public static FACEBOOK_APP_ID ="1198870903601779";
    public static FACEBOOK_APP_ID ="966340406727754";
    public static GOOGLE_APP_ID ="813497323672-29bcuti0tm2sn5oe2vtr4cqqk82hcf7e.apps.googleusercontent.com"; 
    //UAT
    public static API_DOMAIN = 'https://scoop.eduncletest.com';
    public static DCP_URL = 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js'; // live
    //public static DCP_URL = 'https://cdn.dotcompaltest.com/apps/engage/smart_engage/js/config-loader.js'; // test
    
    public static MAIN_WEBSITE_HOME = AppSettings.API_DOMAIN.replace("scoop", "www");
    public static SCOOP_WEBSITE_HOME = AppSettings.API_DOMAIN;
    public static upload_file_url =  AppSettings.API_DOMAIN + "/api/uploadFile";
    //public static isProductionMode = environment.production;
    public static isProductionMode = true;

    public static LOCAL_API_DOMAIN = 'http://localhost:8080';
    public static API_END_URL = '/api/fc';
    public static GET_API_END_URL = '/api/gc';
    public static API_ENDPOINT = AppSettings.API_DOMAIN + AppSettings.API_END_URL;
    public static LOCAL_API_ENDPOINT = 'http://localhost:8080' + AppSettings.API_END_URL;

    //public static API_ENDPOINT = 'http://localhost:8080/api/fc';
    //public static isProductionMode = environment.production;
  /*   public static isProductionMode = true;
    public static MAIN_WEBSITE_HOME ="https://www.eduncletest.com";
    public static SCOOP_WEBSITE_HOME ="https://scoop.eduncletest.com";
    public static upload_file_url = "https://scoop.eduncletest.com/api/uploadFile"; */
                                                                                                                                          
    public static ENABLE_TRANSFER_STATE:boolean = true; 

    public static RETRY_COUNT =3;


    //localhost
/*     public static API_ENDPOINT = 'https://www.sagturboinboxer.com/api/fc';
    public static isProductionMode = environment.production; */

    //eduncletest
/*      public static API_ENDPOINT = 'https://www.eduncletest.com/api/fc';  */


      //Testing URL
/*       public static MAIN_WEBSITE_HOME ="https://scoop.eduncletest.com";
      public static SCOOP_WEBSITE_HOME ="https://scoop.eduncletest.com"; */

      
      //Live URL
     /*  public static MAIN_WEBSITE_HOME ="https://eduncle.com";
      public static SCOOP_WEBSITE_HOME ="https://scoop.eduncle.com"; */

}