import { HttpResponse } from '@angular/common/http';
import { ApiCacheCO } from './api.cache.co';
export class ApiCacheMap
{
   chacheList: ApiCacheCO[];

   addApiInCache(key: string, value: any)
   {
      if(this.chacheList)
      {
        let isReplaced : boolean = false;
        for (var i = 0; i < this.chacheList.length; i++)
        {
          let apiCacheCO:ApiCacheCO = this.chacheList[i];
          if(apiCacheCO.key === key)
          {
              apiCacheCO.value = value;
              isReplaced = true;
              break;
          }
        }

        if(!isReplaced)
        {
          this.chacheList.push(new ApiCacheCO(key, value));
        }        
      }
      else
      {
        this.chacheList = new Array(new ApiCacheCO(key, value));
      }

     // console.log("added "+ key);
     // console.log("current cache "+ JSON.stringify(this.chacheList));
   }

   getCachedData(key: string)
   {
  //   console.log("searching cache for "+ key);
     if(this.chacheList)
     {
 //     console.log("cache size "+ this.chacheList.length);
 //     console.log("current cache "+ JSON.stringify(this.chacheList));
      for (var i = 0; i < this.chacheList.length; i++)
      {
        let apiCacheCO:ApiCacheCO = this.chacheList[i];
        if(apiCacheCO.key === key)
        {
            return apiCacheCO.value;
        }
      }
     }
   }

}