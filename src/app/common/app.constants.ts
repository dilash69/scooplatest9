import { AppSettings } from './app-settings';
import { CatCategoryAdSlidersCO } from "../co/catCategoryAdSlidersCO";
import { CatCategoriesCO } from "../co/categoryCO";

export class AppConstants
{
    public static ACCESS_TOKEN = 'ACCESS_TOKEN';
    public static DB_ACCESS_TOKEN = 'DB_ACCESS_TOKEN';
    public static APP_VERSION = 'APP_VERSION';
    public static APP_TYPE = 'APP_TYPE';
    public static REQ_SOURCE = 'REQ_SOURCE';
    public static ACTION = 'ACTION';
    public static PANEL_ARRAY:string[]=["All","Store","Scoop"];

    public static ALL = "All";
    public static STORE = "Store";
    public static SCOOP = "Scoop";

    public static YES = "Y";
    public static NO = "N";

    public static SUCCESS_REQUEST:string = "S";
    public static LOGGED_IN_USER_CO = 'LOGGED_IN_USER_CO';
    public static AFTER_POST = 3;

    public static Upload = "Upload";
    public static Uploading = "Uploading...";
    public static FOLLOW_CATEGORY = "FOLLOW_CATEGORY";
    public static SWITCH_CATEGORY = "SWITCH_CATEGORY";
    public static EDIT_CATEGORY = "EDIT_CATEGORY";
    public static SELECT_CATEGORY = "SELECT_CATEGORY";
    public static UNFOLLOW_CATEGORY = "UNFOLLOW_CATEGORY";
    

    public static NORMAL_REGISTRATION = "NORMAL_REGISTRATION";
    public static ENTRY_POPUP = "ENTRY_POPUP";

    public static RESUME_UPLOADED_PATH = "resumeUploadedPath";

    public static STEP_1_VALUE = 1;
    public static STEP_2_VALUE = 2;
    public static STEP_3_VALUE = 3;
    public static STEP_NONE_VALUE = 0;

    public static REGISTRATION_PROCESS = 'REGISTRATION_PROCESS';
    public static SIMPLE_REGISTRATION_PROCESS = 'SIMPLE_REGISTRATION';
    public static ENTRY_POPUP_REGISTRATION_PROCESS = 'ENTRY_POPUP';
    public static EXIT_POPUP_REGISTRATION_PROCESS = 'EXIT_POPUP';
    public static FOLLOW_CATEGORY_REGISTRATION_PROCESS = 'FOLLOW_CATEGORY';
    public static ADVERTISEMENT_REGISTRATION_PROCESS = 'ADVERTISEMENT';
    public static COMMENT_REGISTRATION_PROCESS = 'COMMENT_REGISTRATION';
    public static REPLY_REGISTRATION_PROCESS = 'REPLY_REGISTRATION';
    public static LIKE_REGISTRATION_PROCESS = 'LIKE_REGISTRATION';
    public static BLOG_ACTIVITY_REGISTRATION_PROCESS = 'BLOG_ACTIVITY_REGISTRATION_PROCESS';

    public static ENGLISH_MEDIUM_LANGUAGE_ID = "2";

    public static SCOOP_HOME_PAGE = "SCOOP_HOME_PAGE";
    public static SLUG_PAGE = "SLUG_PAGE";
    public static TAG_PAGE = "TAG_PAGE";
    public static AUTHOR_PAGE = "AUTHOR_PAGE";

    
    public static EXAM = "EXAM";
    public static STREAM = "STREAM";
    public static STAGE = "STAGE";
    public static SUBJECT = "SUBJECT";
    public static TOPIC = "TOPIC";
    public static YEAR = "YEAR";
    public static BOARD = "BOARD";
    public static MEDIUM_LANGUAGE = "MEDIUM_LANGUAGE";
    public static ZONE = "ZONE";

    public static iOS = "iOS";

    public static MEDIUM  = "MEDIUM LANGUAGE";

    public static Category  = "Select Category..";

    public static NONE = "NONE";
    public static CRITICAL_SERVICE_FAILED = "800012";

    
    
    
    // created for category profile dummy id
    // public static   CATEGORY_PROFILE_ID:string = "13";
    
    // created for user profile dummy id
    // public static USER_PROFILE_ID:string = "2";

    // created for user post id
    // public static USER_POST_ID:string = "39";

    // created for post comment reply
    // public static USER_POST_COMMENT_ID:string = "33";

    public static POST_LOAD_MORE_CHUNK_SIZE:number = 10;

    // [START] Added By Shakir
    public static MIN_PASSWORD_LENGTH = 7;
    public static OTP_VALID_TIME: number = 45;
    public static USERCO_AFTER_OTP_VERIFIED = "userCoAfterOtpVerified";
    /* public static TEMP_ACCESS_TOKEN = "TEMP_ACCESS_TOKEN"; */
    public static PASSWORD_CREATED_SUCCESSFULLY = "Password Created Successfully.";
    public static FREE_DOWNLOAD_PAGE_URL = AppSettings.MAIN_WEBSITE_HOME+'/member/student';

    public static REDIRECT_URL = 'https://www.eduncle.com/store/search?sortby=ltst';

    public static UNSAVED_COMMENT_CO: string = "unSavedCommentCO";

    public static DEFAULT_IMAGE_SQUARE = "assets/images/default_square.svg";
    public static DEFAULT_IMAGE_CIRCLE = "assets/images/default_circular.svg";
    

    // [START] Added By Shakir


    // ads


    /* public static horizontal_ads='<div class="examFeed-block" >'+
    '	<div class="block-detail">'+
    
    '		<div class="slider carousel-maxHeight" data-slick=\'{"slidesToShow": 1.5, "slidesToScroll": 1.5}\'>'+
    '			<div>'+
    'sliderImg'+
    '			</div>'+
    '		</div>'+
    '	</div>'+
    '</div>'; */

    /* '<ngb-carousel data-interval="1000" showNavigationArrows={{showNavigationArrows}} showNavigationIndicators={{showNavigationIndicators}}>'+
'             <ng-template ngbSlide *ngFor="let catAddSlider of catCategoryAdSlidersCOList">'+
'              <a href="{{catAddSlider?.redirectUrl}}">'+
'               <img alt="" src="{{catAddSlider?.imageName}}" alt="Random slide">'+
'             </a>  '+
'             </ng-template>'+
'         </ngb-carousel>' */

    public static horizontal_ads= '<div class="carousel slide imgWidth100" data-ride="carousel" id="eduncleAds" data-interval="1000">'+
    ''+
    '  <!-- Indicators -->'+
    // '  <ul class="carousel-indicators">'+
    // 'circleList'+
    // '  </ul>'+
    '  '+
    '  <!-- The slideshow -->'+
    '  <div class="carousel-inner">'+
    'sliderImg'+
    '  </div>'+
    '  '+
    '  <!-- Left and right controls -->'+
    '  <a class="carousel-control-prev" href="#eduncleAds" data-slide="prev">'+
    '    <span class="carousel-control-prev-icon"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>'+
    '  </a>'+
    '  <a class="carousel-control-next" href="#eduncleAds" data-slide="next">'+
    '    <span class="carousel-control-next-icon"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>'+
    '  </a>'+
    '</div>';


    public static POLL ="POLL";
    public static QUESTION ="QUESTION";
    public static ARTICLE ="ARTICLE";
    public static MCQ ="MCQ";
    public static SUBJECTIVE ="SUBJECTIVE";
    public static LABEL ="LABEL";
    public static QUIZ ="QUIZ";

    public static REGISTRATION ="REGISTRATION";
    public static FORGOT_PASSWORD ="FORGOT_PASSWORD";
    public static MOBILE_EMAIL ="MOBILE_EMAIL";
    public static LOGIN_RIGHT_PART ="LOGIN_RIGHT_PART";
    public static OTP ="OTP";

    public static CATEGORY ="CATEGORY";
    public static newAccessTokenRequired ="newAccessTokenRequired";
    public static dirtyList ="dirtyList";//Blocked_User_ID_List
    public static tutorFormFilled ="tutorFormFilled";
    public static param_redirect_url ="redirectUrl";
    public static COMMENT_BEST_ANSWER = "2";
    public static PENDING = "PENDING";
    public static SAVE_FOR_LATER = "SAVE_FOR_LATER";
    public static TUTOR_FORM_REJECTED = "REJECTED";
    public static APP_PROMOTION = "APP_PROMOTION";

    public static RESUME = "RESUME";
    public static START = "START";
    public static RE_ATTEMPT = "RE_ATTEMPT";
    public static MAX_ATTEMPT = "MAX_ATTEMPT";
    public static EXPIRED = "EXPIRED";
    

    public static A = "A";
    public static B = "B";
    public static C = "C";
    public static D = "D";
    public static E = "E";
    public static F = "F";
    public static G = "G";
    public static H = "H";
    public static I = "I";
    public static J = "J";
    public static K = "K";
    public static L = "L";
    public static M = "M";
    public static N = "N";
    public static O = "O";
    public static P = "P";
    public static Q = "Q";
    public static R = "R";
    public static S = "S";
    public static T = "T";
    public static U = "U";
    public static V = "V";
    public static W = "W";
    public static X = "X";
    public static Y = "Y";
    public static Z = "Z";
    public static DEFAULT_FIRST_CHARACTER = AppConstants.N;
    public static isPromotionBannerOpen: string = "isPromotionBannerOpen";
    public static linkSentMobile: string = "Link has been sent to your mobile.";
    public static currentCategoryName: string = "currentCategoryName";
    public static auto: string = "auto";
    public static autoBrokenLeadApiHitTime = 1000;

    public static TUTOR_REGISTRATION = "TUTOR_REGISTRATION";
    public static LOGIN = "LOGIN";
    public static CatUpdate = "CatUpdate";
    public static ProfileUpdate = "ProfileUpdate";
    
 }