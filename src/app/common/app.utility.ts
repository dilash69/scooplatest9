import { AppConstants } from './app.constants';
//import { AppConstants } from 'src/app/common/app.constants';
import { AppSettings } from "./app-settings";
import 'zone.js';
// import { AppConstants } from "./app.constants";

declare var MathJax: any;

export class AppUtility {
  public static siteUrl: string;
  /*  static log(object: any)
   {
     if(AppSettings.isProductionMode)
     {
        //this.siteUrl =  "http://localhost login url: /login?action=login";
        AppUtility.log(object);
     }
    //  else
    //  {
    //   this.siteUrl =  "https://www.edunce.com";
    //  }
   } */

  static log(message?: any, ...optionalParams: any[]): void {
    /* if(AppSettings.isProductionMode) */
    if (AppSettings.isProductionMode) {
      //this.siteUrl =  "http://localhost login url: /login?action=login";
      console.log(message);
    }
    //  else
    //  {
    //   this.siteUrl =  "https://www.edunce.com";
    //  }
  }



  static getTrimmedReadableString(value: string, trimLength: number) {
    if (value.length > trimLength) {
      return value.substring(0, (trimLength - 3)).concat('...');
    }
    else {
      return value;
    }
  }

  /*    static getTrimmedReadableString(value: string)
     {
       if(value.length > 100)
       {
        return value.substring(0,97).concat('...');
       }
     } */

  static getTimeStringFromMilliseconds(milliseconds: any): String {
    var date = new Date(milliseconds);

    return this.formatAMPM(date);
  }

  static getDateStringFromMilliseconds(milliseconds: any): string {
    var date = new Date(milliseconds);

    return this.monthNumToName(date.getMonth() + 1) + " " + date.getDate().toString() + ", " + date.getFullYear().toString();
  }

  static monthNumToName(monthnum) {
    var months = [
      'January', 'February', 'March', 'April', 'May',
      'June', 'July', 'August', 'September',
      'October', 'November', 'December'
    ];
    return months[monthnum - 1] || '';
  }

  static formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  static getBaseUrl() {
    if (Zone.current.get("originUrl")) {
      return Zone.current.get('originUrl');
    } else if (location) {
      return location.origin;
    } else {
      return 'something went wrong!';
    }
  }

  static getRandomBgColor() {
    let rand = Math.floor(Math.random() * 10);
    while (rand < 1) {
      rand = Math.floor(Math.random() * 10);
    }

    return rand.toString();
  }

  static sendToDashboard(): boolean {
    let data = localStorage.getItem(AppConstants.REGISTRATION_PROCESS);
    console.log('sendToDashboard : ' + data);

    if (data) {
      switch (data) {
        case AppConstants.ENTRY_POPUP_REGISTRATION_PROCESS:
          return true;

        case AppConstants.EXIT_POPUP_REGISTRATION_PROCESS:
          return true;

        case AppConstants.FOLLOW_CATEGORY_REGISTRATION_PROCESS:
          return false;

        case AppConstants.SIMPLE_REGISTRATION_PROCESS:
          return true;

        case AppConstants.COMMENT_REGISTRATION_PROCESS:
          return false;

        case AppConstants.REPLY_REGISTRATION_PROCESS:
          return false;

        case AppConstants.LIKE_REGISTRATION_PROCESS:
          return false;

        case AppConstants.ADVERTISEMENT_REGISTRATION_PROCESS:
          return true;

        case AppConstants.BLOG_ACTIVITY_REGISTRATION_PROCESS:
          return false;
      }
    }

    return false;
  }

static textBackgroundColor(alphNumber: String): string
{
  switch (alphNumber) {
    case AppConstants.A:
      return 'bgcolor1';
    case AppConstants.B:
      return 'bgcolor2';
    case AppConstants.C:
      return 'bgcolor3';
    case AppConstants.D:
      return 'bgcolor4';
    case AppConstants.E:
      return 'bgcolor5';
    case AppConstants.F:
      return 'bgcolor6';
    case AppConstants.G:
      return 'bgcolor7';
    case AppConstants.H:
      return 'bgcolor8';
    case AppConstants.I:
      return 'bgcolor9';
    case AppConstants.J:
      return 'bgcolor10';
    case AppConstants.K:
      return 'bgcolor11';
    case AppConstants.L:
      return 'bgcolor1';
    case AppConstants.M:
      return 'bgcolor13';
    case AppConstants.N:
      return 'bgcolor14';
    case AppConstants.O:
      return 'bgcolor15';
    case AppConstants.P:
      return 'bgcolor16';
    case AppConstants.Q:
      return 'bgcolor1';
    case AppConstants.R:
      return 'bgcolor2';
    case AppConstants.S:
      return 'bgcolor3';
    case AppConstants.T:
      return 'bgcolor4';
    case AppConstants.U:
      return 'bgcolor5';
    case AppConstants.V:
      return 'bgcolor6';
    case AppConstants.W:
      return 'bgcolor7';
    case AppConstants.X:
      return 'bgcolor8';
    case AppConstants.Y:
      return 'bgcolor9';
    case AppConstants.Z:
      return 'bgcolor10';
    default:
          return 'bgcolor1';

  }
}

static isEmpty(data: any): boolean
{
    if(data)
    {
      return false;
    }
    else
    {
      return true;
    }
}

public static getRequestMapFromUrl(url:string)
{
  let obj = {};
  if(url.indexOf('?') > 0)
  {
    let queryString:string = url.substring(url.indexOf('?') + 1);
    let keyValueParamsArray:string[] = queryString.split('&');

    for (var i = 0; i < keyValueParamsArray.length; i++)
    {
      let keyValueParam:string = keyValueParamsArray[i];
      let keyValueParams:string[] = keyValueParam.split('=');
      obj[keyValueParams[0]] = keyValueParams[1];
    }
  }

  return obj;
}

public static sort(unsortedList: any): any 
{
  const keys: string[] = Object.keys(unsortedList);
  const sortedKeys = keys.sort().reverse(); //reverse if you need or not
  const sortedList: any = {};
  sortedKeys.forEach(x => {
    sortedList[x] = unsortedList[x];
  });
  return sortedList;
}

public static MathJaxRendering(code) 
{
/*   setTimeout(() => {
    
      MathJax.startup.promise = MathJax.startup.promise.then(() => {code(); return MathJax.typesetPromise()})
      .catch((err) => console.log('Typeset failed: ' + err.message));
      
  }, 1000); */

  MathJax.startup.promise = MathJax.startup.promise.then(() => {code(); return MathJax.typesetPromise()})
  .catch((err) => console.log('Typeset failed: hehe ' + err.message));

  return MathJax.startup.promise;
}

public static DcpVideoUrl(url:string):string 
{ 
    let dcpUrl:string;
    if(url.includes('eduncle.com'))
    {
      dcpUrl='https://eduncle.dotcompal.com/video/embed/'+ url.split('video_id=')[1];
    }
    else
    {
      dcpUrl='https://dev.dotcompaltest.com/video/embed/'+ url.split('video_id=')[1];
    }
    return dcpUrl;
}

public static isScriptLoaded(src):boolean
{
    return document.querySelector('script[src="' + src + '"]') ? true : false;
}

public static downloadMathJs()
{
    if(!this.isScriptLoaded("https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js"))
    {
      const script = document.createElement('script');
      script.src = 'https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js';
      document.body.appendChild(script);
    }
}

public static getResponsiveImage(imagePath:string):string {
  let imageSizeSmall: string = "_small.";

  let extension: string = imagePath.substring(imagePath.lastIndexOf(".") + 1);
  if (extension.includes("jpg") || extension.includes("jpeg") || (extension.includes("JPG") || extension.includes("JPEG"))) {
    let newImageUrl: String = "";
    let imagePathSmall: String = imagePath.substring(0, imagePath.lastIndexOf("."));
    newImageUrl = (imagePathSmall + imageSizeSmall + extension);
    imagePath = newImageUrl + " 500w," + imagePath;
  
  }
  
    return imagePath;
  
  
}


}