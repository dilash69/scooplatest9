import { TestBed } from '@angular/core/testing';

import { TutorDetailService } from './tutor-detail.service';

describe('TutorDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TutorDetailService = TestBed.get(TutorDetailService);
    expect(service).toBeTruthy();
  });
});
