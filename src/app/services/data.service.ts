
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError as observableThrowError } from 'rxjs';
import { map } from 'rxjs/operators';
//import 'rxjs/add/operator/map'
/* import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw'; */
import { deserialize } from "serializer.ts/Serializer";
import { ResponseCO } from '../co/responseCO';
import { ServerInstructionCO } from '../co/server-instructionCO';
import { ApiCall } from '../common/api.call';
import { AppError } from '../common/app-error';
import { AppSettings } from '../common/app-settings';
import { AppConstants } from '../common/app.constants';
import { AppUtility } from '../common/app.utility';
import { ApiActions } from './../common/api.actions';
import { AppCurrentStateService } from './app.current.state.service';
import { ShowloaderService } from './showloader.service';

@Injectable()
export class DataService 
{
  constructor(private http: HttpClient, 
    private appCurrentStateService: AppCurrentStateService,
    private showloaderService: ShowloaderService) 
  {

  }

  hitApi(apiCall: ApiCall)
  {
    // AppUtility.log('apiCall : ' + JSON.stringify(apiCall));
    if(this.appCurrentStateService.stopApiCalls)
    {
        return;
    }

    let endPoint:string = AppSettings.API_DOMAIN;
    let accessToken:string ="";
    if(this.appCurrentStateService.isServer())
    {//nodeJS server
        //endPoint = endPoint.replace("https", "http");
        //apiCall.addRequestParams("NODE_SERVER", "YES");
        //endPoint = AppSettings.LOCAL_API_DOMAIN;
    }
    else
    {
     // apiCall.addRequestParams("CURRENT_URL",window.location.href);
      accessToken  = localStorage.getItem(AppConstants.ACCESS_TOKEN);
    }

    //console.log("endPoint " , endPoint);
    if(apiCall.showLoader)
    {
      this.showloaderService.updateShowLoader(apiCall.showLoader);
    }

    if(apiCall.isGetRequest)
    {
      endPoint = endPoint + AppSettings.GET_API_END_URL + "/" + apiCall.action;

      if(apiCall.additionalUrlParameter)
      {
        endPoint = endPoint + "/" + apiCall.additionalUrlParameter;
      }

      console.log('endPoint ', endPoint);
      return this.http.get(endPoint , apiCall.constructOptionsForGet())
      .pipe(map( (responseData) => {
   //     console.log("get request response ", responseData);
         return this.processApiResponse(apiCall, responseData);
      }))
      //.retry(AppSettings.RETRY_COUNT)
  /*     .shareReplay(1)
      .publishReplay(1)
      .refCount() */
      /* .catch(this.handleError) */
      ;
    }
    else
    {
      endPoint = endPoint + AppSettings.API_END_URL + "/" + apiCall.action;
    // endPoint = endPoint + AppSettings.API_END_URL;
   
      return this.http.post(endPoint, apiCall.constructPostBody(accessToken) , apiCall.constructHeader(accessToken))
      .pipe(map( (responseData) => {
         return this.processApiResponse(apiCall, responseData);
      }))
      //.retry(AppSettings.RETRY_COUNT)
  /*     .shareReplay(1)
      .publishReplay(1)
      .refCount() */
      /* .catch(this.handleError) */
      ;
    }
  }

  private processApiResponse(apiCall: ApiCall, responseData:any)
  {
    if(apiCall.action === ApiActions.createAndGetMobileOtp
      || apiCall.action === ApiActions.userRegistrationFrontEnd
      || apiCall.action === ApiActions.registerUser)
      {
        //console nothing
      }
      else
      {
        console.log('apiCall : ' + JSON.stringify(apiCall));
        console.log('Response before parsing :', responseData);
      }

      if(apiCall.showLoader)
      {
        this.showloaderService.updateShowLoader(false);
      }

      let jsonResponse = responseData;
      if(jsonResponse)
      {
        let responseCO:ResponseCO = <ResponseCO>jsonResponse;
        responseCO = deserialize<ResponseCO>(ResponseCO, responseCO);
          if(responseCO.serverResponseCode)
          {
            if(responseCO.serverResponseCode === AppConstants.SUCCESS_REQUEST)
            {
                if(responseCO.userResponseData)
                {
                  return responseCO.userResponseData;
                }
                else
                {
                  throw new Error('errorneous response userResponseData not present');
                }
            }
            else
            {//failled response    
              if(responseCO.serverInstructionCO && responseCO.serverInstructionCO.type === ServerInstructionCO.FORCE_LOGOUT_TYPE)
              {
                this.appCurrentStateService.logout();
              }
              else if(AppConstants.CRITICAL_SERVICE_FAILED === responseCO.serverMessage)
              {
                this.appCurrentStateService.criticalRequestFailed();
              }

              console.error(responseCO.serverMessage);
              throw new Error(responseCO.serverMessage);
            }
          }
          else
          {
            throw new Error('errorneous response serverResponseCode not present');
              //errorneous response
          }
      }
      else
      {
        throw new Error('Server side error');
      }
  }

  private handleError(error: any) 
  {
    AppUtility.log('Inside handleError');

    return observableThrowError(new AppError(error));

    /* if(this.appCurrentStateService.isBrowser())
    {
      if(error instanceof Response)
      {
        if (error.status === 400)
        return Observable.throw(new BadInput(error.json));
    
        if (error.status === 404)
          return Observable.throw(new NotFoundError());
        
        return Observable.throw(new AppError(error));
      }
      else 
      {
        return Observable.throw(new AppError(error));
      } 
    }
    else 
    {
      return Observable.throw(new AppError(error));
    }  */
  }
}

