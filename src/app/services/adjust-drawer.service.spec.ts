import { TestBed } from '@angular/core/testing';

import { AdjustDrawerService } from './adjust-drawer.service';

describe('AdjustDrawerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdjustDrawerService = TestBed.get(AdjustDrawerService);
    expect(service).toBeTruthy();
  });
});
