
import {map} from 'rxjs/operators';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { APP_ID, Inject, Injectable, PLATFORM_ID } from '@angular/core';

import { BehaviorSubject ,  Observable } from 'rxjs';
import { deserialize } from "serializer.ts/Serializer";
import { AccessCheckResponseCO } from '../co/access-check-response.co';
import { AppCurrentState } from '../co/app.current.state';
import { UserCO } from '../co/userCO';
import { AppConstants } from '../common/app.constants';
import { AppSettings } from './../common/app-settings';
import { PlainHttpDataService } from './plain-http-data.service';
import { AppUtility } from '../common/app.utility';
import { Router } from '@angular/router';
import { AlertMessageCO } from '../co/alertMessageCO';

@Injectable()
export class AppCurrentStateService
{
  private currentPage:string = "";
  stopApiCalls:boolean = false;
//  isNativeLoadingSupported:boolean = false;
  isLoggedInChecked:boolean = false;

  appCurrentState: Observable<AppCurrentState>;
  private _appCurrentState: BehaviorSubject<AppCurrentState>;
  private _alertMessage: BehaviorSubject<AlertMessageCO>;
  alertMessage: Observable<AlertMessageCO>
  
     /* constructor(private cmsAdminService:CmsAdminService)
   {
      let loggedInUserCurrentStateTemp:AppCurrentState = new AppCurrentState();;
      loggedInUserCurrentStateTemp.userCO = JSON.parse(localStorage.getItem(AppConstants.LOGGED_IN_USER_CO));

      if(loggedInUserCurrentStateTemp.userCO)
      {        
        loggedInUserCurrentStateTemp.isUserLoggedIn = true;
      }
      else
      {
        loggedInUserCurrentStateTemp.isUserLoggedIn = false;
        //window.location.href = AppConstants.sag_panel_login_url;
      }

      this._appCurrentState = <BehaviorSubject<AppCurrentState>>new BehaviorSubject(loggedInUserCurrentStateTemp);
      this.appCurrentState = this._appCurrentState.asObservable(); 
   }  */

  constructor(
          private plainHttpDataService:PlainHttpDataService,
          @Inject(PLATFORM_ID) private platformId: Object,
          @Inject(APP_ID) private appId: string,
          public router: Router
             )
   {   
      let loggedInUserCurrentStateTemp:AppCurrentState;
      if(isPlatformBrowser(platformId))
      {

        
/*         AppUtility.log('*********************************************BROWSER***************************************************');

        AppUtility.log('PRODUCTION MODE');    */
        
        this.clearLocalStorageAndPreserveImpData();
/* 
        if ('loading' in HTMLImageElement.prototype) 
        {
          console.log("loading is supported...")
          this.isNativeLoadingSupported = true;
        } 
        else 
        {
          console.log("loading is not supported...")
          this.isNativeLoadingSupported = false;
        } */

        // Dynamically import the LazySizes library
      /*   const script = document.createElement('script');
        script.src = 'https://cdnjs.cloudflare.com/ajax/libs/lazysizes/4.1.8/lazysizes.min.js';
        document.body.appendChild(script); */
        
        loggedInUserCurrentStateTemp = new AppCurrentState();
        loggedInUserCurrentStateTemp.isServerSideRendering = false;
        loggedInUserCurrentStateTemp.isUserLoggedIn = false;
        loggedInUserCurrentStateTemp.isBasicMandatoryInitializationsDone = true;
        //loggedInUserCurrentStateTemp.userCO = JSON.parse(localStorage.getItem(AppConstants.LOGGED_IN_USER_CO));
        //console.log("accessCheckResponseCO1");    

        this.getCurrentUserLoginDetails()
        .subscribe((accessCheckResponseCO: AccessCheckResponseCO) => 
        {
            this.isLoggedInChecked = true;
            if(accessCheckResponseCO != null && accessCheckResponseCO.isUserLogin === 'Y')
            {  
             // console.log("accessCheckResponseCO", accessCheckResponseCO);    
              
              loggedInUserCurrentStateTemp.isUserLoggedIn = true;
              localStorage.setItem(AppConstants.ACCESS_TOKEN, accessCheckResponseCO.access_token);
              localStorage.setItem(AppConstants.LOGGED_IN_USER_CO, JSON.stringify(accessCheckResponseCO.userCO));
              loggedInUserCurrentStateTemp.userCO = accessCheckResponseCO.userCO;
              loggedInUserCurrentStateTemp.isBasicMandatoryInitializationsDone = true;            
              this.updatedLoggedInUserCurrentState(loggedInUserCurrentStateTemp);

/*               if(accessCheckResponseCO.userCO.isCategoryRequired === '1')
              {
                window.location.href = AppSettings.MAIN_WEBSITE_HOME + "/email-otp-instruction?getdefaultcat";
              }
              else
              {
                loggedInUserCurrentStateTemp.isUserLoggedIn = true;
                localStorage.setItem(AppConstants.ACCESS_TOKEN, accessCheckResponseCO.access_token);
                localStorage.setItem(AppConstants.LOGGED_IN_USER_CO, JSON.stringify(accessCheckResponseCO.userCO));
                loggedInUserCurrentStateTemp.userCO = accessCheckResponseCO.userCO;
                loggedInUserCurrentStateTemp.isBasicMandatoryInitializationsDone = true;            
                this.updatedLoggedInUserCurrentState(loggedInUserCurrentStateTemp);
              } */ 
            }
            else
            {
              loggedInUserCurrentStateTemp.isUserLoggedIn = false;
<<<<<<< HEAD
 
              /*if(window.location.href.includes('localhost'))
=======
 /* 
              if(window.location.href.includes('localhost'))
>>>>>>> db833056505d4a04ed87fe0cc0c4ca00d87c4301
              {
                loggedInUserCurrentStateTemp.isUserLoggedIn = true;
                let userCO:UserCO = JSON.parse("{\"id\":\"2186563\",\"organizationId\":\"1\",\"leadFormId\":\"5\",\"defaultCategoryId\":\"2\",\"email\":\"gsgssgsgsg@gmail.com\",\"mobile\":\"9024866056\",\"firstName\":\"abhishek\",\"middleName\":null,\"lastName\":null,\"profileImage\":\"\",\"slug\":\"abhishek\",\"dob\":null,\"about\":null,\"isStudent\":\"1\",\"isTutor\":\"0\",\"lastLogin\":null,\"lastLoginIp\":\"122.15.17.44, 72.247.179.135\",\"facebookId\":null,\"googleId\":null,\"status\":\"1\",\"mobileNoVerified\":\"1\",\"createdBy\":\"1\",\"createdDate\":\"2019-02-08 09:42:49\",\"modifiedBy\":\"1560417\",\"modifiedDate\":\"2019-02-08 09:43:19\"}");
                userCO = deserialize<UserCO>(UserCO, userCO);
                localStorage.setItem(AppConstants.ACCESS_TOKEN, "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIyMTg2MDQzIiwiaWF0IjoxNjA0OTkxMTQzLCJpc3MiOiI2NzFhYzJmMDA2OWI1MDhkMjJlN2ZkYzE3YmY2N2EwMCJ9.S4gQ4-J4KU_022FBKxhjCsjKHV0ABreJ6fWIrFcivvE");
                localStorage.setItem(AppConstants.LOGGED_IN_USER_CO, JSON.stringify(userCO));
                loggedInUserCurrentStateTemp.userCO = userCO;
                loggedInUserCurrentStateTemp.isBasicMandatoryInitializationsDone = true;  
                console.log("after ng-check-login");          
                this.updatedLoggedInUserCurrentState(loggedInUserCurrentStateTemp);
<<<<<<< HEAD
              }       */ 
 
=======
              }        
  */
>>>>>>> db833056505d4a04ed87fe0cc0c4ca00d87c4301

            }
            loggedInUserCurrentStateTemp.isBasicMandatoryInitializationsDone = true;

            this.updatedLoggedInUserCurrentState(loggedInUserCurrentStateTemp);
        });

/*             if(loggedInUserCurrentStateTemp.userCO)
        {   
          AppUtility.log('user information found');  
          AppUtility.log(localStorage.getItem(AppConstants.ACCESS_TOKEN));  
          loggedInUserCurrentStateTemp.isUserLoggedIn = true;
          loggedInUserCurrentStateTemp.isBasicMandatoryInitializationsDone = true;
        }
        else
        {
          AppUtility.log('user information not found');  
          loggedInUserCurrentStateTemp.isUserLoggedIn = false;

          this.getCurrentUserLoginDetails()
          .subscribe((accessCheckResponseCO: AccessCheckResponseCO) => 
          {
              if(accessCheckResponseCO.isUserLogin === 'Y')
              {
                loggedInUserCurrentStateTemp.isUserLoggedIn = true;
                localStorage.setItem(AppConstants.ACCESS_TOKEN, accessCheckResponseCO.access_token);
                localStorage.setItem(AppConstants.LOGGED_IN_USER_CO, JSON.stringify(accessCheckResponseCO.userCO));
                loggedInUserCurrentStateTemp.userCO = accessCheckResponseCO.userCO;
                loggedInUserCurrentStateTemp.isBasicMandatoryInitializationsDone = true;
                this.updatedLoggedInUserCurrentState(loggedInUserCurrentStateTemp);
              }
              else
              {
                loggedInUserCurrentStateTemp.isUserLoggedIn = false;
              }

              loggedInUserCurrentStateTemp.isBasicMandatoryInitializationsDone = true;
          })
        } */
        
      }
      else
      {
        AppUtility.log('*********************************************SERVER***************************************************');
        loggedInUserCurrentStateTemp = new AppCurrentState();
        loggedInUserCurrentStateTemp.isUserLoggedIn = false;
        loggedInUserCurrentStateTemp.isServerSideRendering = true;
        loggedInUserCurrentStateTemp.isBasicMandatoryInitializationsDone = true;
        loggedInUserCurrentStateTemp.userCO  = new UserCO();
      }
    
    this._appCurrentState = <BehaviorSubject<AppCurrentState>>new BehaviorSubject(loggedInUserCurrentStateTemp);
    this.appCurrentState = this._appCurrentState.asObservable();
    
    this._alertMessage = <BehaviorSubject<AlertMessageCO>>new BehaviorSubject(new AlertMessageCO());
    this.alertMessage = this._alertMessage.asObservable();
   }

   getCurrentUserLoginDetails(): Observable<AccessCheckResponseCO> 
   {
       return this.plainHttpDataService.get(AppSettings.SCOOP_WEBSITE_HOME + "/auth/ng-check-login.php").pipe(
           map((accessCheckResponseCO: AccessCheckResponseCO) => 
           {
               let accessCheckResponseCOTemp = deserialize<AccessCheckResponseCO>(AccessCheckResponseCO, accessCheckResponseCO);
               return accessCheckResponseCOTemp;
           }))
   } 

    login(showDashboardPage:boolean, dbAccessToken:string, extraParam:string)
    {
      if(this.isBrowser())
      {
        AppUtility.log('AppCurrentStateService login : ');

/*         if(!redirectUrl)
        {
          redirectUrl = window.location.href;
        }  */
        
        let redirectUrl:string;
        if(showDashboardPage)  
        {
          redirectUrl = AppConstants.FREE_DOWNLOAD_PAGE_URL;
        }
        else
        {
          redirectUrl = window.location.href;
        }

/*         if(selectCategoryrequired)
        {              
          // redirectUrl = AppSettings.MAIN_WEBSITE_HOME+"/email-otp-instruction?getemail&redirect_url=" + encodeURIComponent(redirectUrl);
          if(!targetStep)
          {
            targetStep = "getemail";
          }
          redirectUrl = AppSettings.MAIN_WEBSITE_HOME+"/email-otp-instruction?"+ targetStep +"&redirect_url=" + encodeURIComponent(redirectUrl);
        } */

        if(extraParam)
        {
          redirectUrl = redirectUrl + extraParam;
        }

        // console.log(AppSettings.MAIN_WEBSITE_HOME+"/include/misutils/ng-login?access_token="+dbAccessToken+"&redirect_url=" + encodeURIComponent(redirectUrl))
        // alert(AppSettings.MAIN_WEBSITE_HOME+"/include/misutils/ng-login?access_token="+dbAccessToken+"&redirect_url=" + encodeURIComponent(redirectUrl));
      // console.log("yo",AppSettings.MAIN_WEBSITE_HOME+"/include/misutils/ng-login?access_token="+dbAccessToken+"&redirect_url=" + encodeURIComponent(redirectUrl))
        window.location.href = AppSettings.MAIN_WEBSITE_HOME+"/include/misutils/ng-login?access_token="+dbAccessToken+"&redirect_url=" + encodeURIComponent(redirectUrl);
      }
   } 

    logout()
    {
      if(this.isBrowser())
      {
        AppUtility.log('AppCurrentStateService logout :');
        localStorage.clear();
        let redirectUrl = window.location.href;
        window.location.href = AppSettings.MAIN_WEBSITE_HOME+"/include/misutils/ng-login?action=logout&redirect_url=" + encodeURIComponent(redirectUrl);
      }
    }
    
    criticalRequestFailed()
    {
      if(this.isServer())
      {
        this.stopApiCalls = true;
      //  console.log('stopping api calls');
      }
      else
      {
        AppUtility.log('AppCurrentStateService criticalRequestFailed :');

        if(AppConstants.SCOOP_HOME_PAGE === this.getCurrentPage())
        {
          window.location.href = AppSettings.MAIN_WEBSITE_HOME+"/member/student/";
        }
        else
        {
          this.router.navigate(['']);          
        }        
      }
    }

   updatedLoggedInUserCurrentState(appCurrentState: AppCurrentState)
   {
   // console.log('updatedLoggedInUserCurrentState : ', JSON.stringify(appCurrentState));
      this._appCurrentState.next(appCurrentState);
   }

   isServer():boolean
   {
     return isPlatformServer(this.platformId);
   }

   isBrowser():boolean
   {
     return isPlatformBrowser(this.platformId);
   }

   clearLocalStorageAndPreserveImpData()
   {
    let unSavedCommentCOString:string = localStorage.getItem(AppConstants.UNSAVED_COMMENT_CO); 
    let isTutorFormSubmitted =   localStorage.getItem(AppConstants.tutorFormFilled);
    //AppUtility.log("AppCurrentStateService unSavedCommentCOString",unSavedCommentCOString);  

    localStorage.clear();

    if(unSavedCommentCOString && unSavedCommentCOString.length > 10)
    {
      //console.log("saving unSavedCommentCOString");
      localStorage.setItem(AppConstants.UNSAVED_COMMENT_CO, unSavedCommentCOString); 
    }
    if(isTutorFormSubmitted && isTutorFormSubmitted.length > 0)
    {
      localStorage.setItem(AppConstants.tutorFormFilled, "1"); 
    }
   }

   getCurrentPage()
   {
     return this.currentPage;
   }

   setCurrentPage(currentPage:string)
   {
     this.currentPage = currentPage;
   }

   updateAlertMessage(message: string,alertType:string) 
   {
     let alertMessageTemp: AlertMessageCO = this._alertMessage.getValue();
     alertMessageTemp.message = message
     alertMessageTemp.type = alertType
     this._alertMessage.next(alertMessageTemp);
     setTimeout(() => alertMessageTemp.message = null, 2000);
   }
}