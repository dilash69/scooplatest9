import { TestBed, inject } from '@angular/core/testing';

import { LoadExitService } from './load-exit.service';

describe('LoadExitService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoadExitService]
    });
  });

  it('should be created', inject([LoadExitService], (service: LoadExitService) => {
    expect(service).toBeTruthy();
  }));
});
