import { TestBed, inject } from '@angular/core/testing';

import { CategoryCOService } from './category-co.service';

describe('CategoryCOService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CategoryCOService]
    });
  });

  it('should be created', inject([CategoryCOService], (service: CategoryCOService) => {
    expect(service).toBeTruthy();
  }));
});
