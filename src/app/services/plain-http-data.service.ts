
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
/* import { Observable, Subject, ReplaySubject, from, of, range } from 'rxjs';
import { map, filter, switchMap } from 'rxjs/operators'; */
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AppError } from '../common/app-error';
import { AppUtility } from '../common/app.utility';






@Injectable()
export class PlainHttpDataService 
{
  constructor(private http: HttpClient) 
  {

  }

  get(url:string)
  {
    return this.http.get(url,).pipe(
    map( (responseData) => {
       console.log('PlainHttpDataService Response before parsing :', responseData);
       return responseData;      
    }),
    //.retry(AppSettings.RETRY_COUNT)
    catchError(this.handleError),)
    ;

    // return this.http.post(url,httpOptions)
    // .map( (responseData) => {
    //    AppUtility.log('PlainHttpDataService Response before parsing :');
    //    AppUtility.log(responseData);
    //    return responseData;      
    // })
    // .retry(AppSettings.RETRY_COUNT)
    // .catch(this.handleError)
    // ;
  }

/*   jsonpGet(url)
  {
    return this.jsonp
    .request(url)
    .subscribe( (responseData) => {
      
      AppUtility.log(responseData.json());
      //return responseData;      
   })
  }

  jsonpGet1(url)
  {
    return this.jsonp.request(url)  
    .subscribe(res => {
      return res.json().results.map(item => {
        AppUtility.log("item",item)
      });
    });
} */



  post(url:string)
  {
  
    return this.http.post(url,
      [{"method":"pos.plusones.get", "id":"p", "params":{"nolog":true, "id":"http://blog.sriraman.in/", "source":"widget", "userId":"@viewer",  "groupId":"@self"    },"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]
      ).pipe(
    map( (responseData) => 
    {
       return responseData[0];      
    }),
    //.retry(AppSettings.RETRY_COUNT)
    catchError(this.handleError),)
    ;
  }

  private handleError(error: any) 
  {
    AppUtility.log('Inside PlainHttpDataService handleError');

    // return Observable.throw(new AppError(error));
    return throwError(new AppError(error));
  }
}

