import { TestBed, inject } from '@angular/core/testing';

import { LoadEntryService } from './load-entry.service';

describe('LoadEntryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoadEntryService]
    });
  });

  it('should be created', inject([LoadEntryService], (service: LoadEntryService) => {
    expect(service).toBeTruthy();
  }));
});
