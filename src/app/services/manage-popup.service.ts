import { Injectable } from '@angular/core';
import { json } from 'express';
import { AppPopupCO } from '../co/app.popup.co';
import { BehaviorSubject ,  Observable } from 'rxjs';
import { AppUtility } from '../common/app.utility';
import { SclPostAttachmentsCO } from '../co/SclPostAttachmentsCO';
import { AppConstants } from '../common/app.constants';
import { TriggerJoinExamBlockService } from './trigger-join-exam-block.service';

@Injectable({
  providedIn: 'root'
})
export class ManagePopupService {

  appPopupCO: Observable<AppPopupCO>;
  private _appPopupCO: BehaviorSubject<AppPopupCO>;

  constructor(private triggerJoinExamBlock: TriggerJoinExamBlockService) 
  {
    let appPopupCO:AppPopupCO = new AppPopupCO();
    this._appPopupCO = <BehaviorSubject<AppPopupCO>>new BehaviorSubject(appPopupCO);
    this.appPopupCO = this._appPopupCO.asObservable();   
  }

  //this.renderer.removeClass(document.body,  'modal-open');
  showLoginPopUp(defaultCategoryId) 
  {
/*     localStorage.removeItem("defaultCategoryId");
    if(defaultCategoryId)
    {
      localStorage.setItem("defaultCategoryId",defaultCategoryId);
    } */
    let appPopupCO:AppPopupCO = this._appPopupCO.getValue();
    appPopupCO.resetAll();
    appPopupCO.isOpenLoginPopUp = true;
    this.updatedAppPopupCO(appPopupCO);
  }

  showSignPopUp(defaultCategoryId, type) 
  {
    if (type === AppConstants.REGISTRATION) 
    {
      this.showSignUp();
    }
    else if (type === AppConstants.APP_PROMOTION) 
    {
      if (defaultCategoryId) 
      {
        this.showSignUp();
        /* if (defaultCategoryId === "2" || defaultCategoryId === "3" || defaultCategoryId === "4") 
        {
          this.showAndroidAppPopUp()
        }
        else 
        {
          this.showSignUp();
        } */
      }
      else
      {
        this.showSignUp();
      }
    }
    
  }

  
  showAndroidAppPopUp() 
  {
    let appPopupCO:AppPopupCO = this._appPopupCO.getValue();
    appPopupCO.resetAll();
    appPopupCO.isAndroidAppPopUpOpen = true;
    this.updatedAppPopupCO(appPopupCO);
   // this.triggerJoinExamBlock.triggerEvent(true);
  }


  showEntryPopUp() 
  {
    let appPopupCO:AppPopupCO = this._appPopupCO.getValue();
    if(!appPopupCO.isEntryPopUpShownOnce && appPopupCO.isEntryPopUpRequired)
    {
      appPopupCO.resetAll();
      appPopupCO.isEntryPopUpOpen = true;
      appPopupCO.isEntryPopUpShownOnce = true;
      this.updatedAppPopupCO(appPopupCO);
    }
  }

  showExitPopUp() 
  {
    let appPopupCO:AppPopupCO = this._appPopupCO.getValue();
    if(!appPopupCO.isExitPopUpShownOnce && !appPopupCO.isAnyPopupOpen() &&  appPopupCO.isExitPopUpRequired)
    {
      appPopupCO.resetAll();
      appPopupCO.isExitPopUpOpen = true;
      appPopupCO.isExitPopUpShownOnce = true;
      this.updatedAppPopupCO(appPopupCO);
    }
  }

  showReplyDeletePopUpOpen() 
  {
    let appPopupCO:AppPopupCO = this._appPopupCO.getValue();
    appPopupCO.resetAll();
    appPopupCO.isReplyDeletePopUpOpen = true;
    this.updatedAppPopupCO(appPopupCO);
  }

  showCommentDeletePopUpOpen() 
  {
    let appPopupCO:AppPopupCO = this._appPopupCO.getValue();
    appPopupCO.resetAll();
    appPopupCO.isCommentDeletePopUpOpen = true;
    this.updatedAppPopupCO(appPopupCO);
  }


  showSwitchCategoryPopUp()
  {
    let appPopupCO:AppPopupCO = this._appPopupCO.getValue();
    appPopupCO.resetAll();
    appPopupCO.isSwitchCategoryPopUpOpen = true;
    this.updatedAppPopupCO(appPopupCO);
  }


  showrequestACallBackPopUp() 
  {
    let appPopupCO:AppPopupCO = this._appPopupCO.getValue();
    appPopupCO.resetAll();
    appPopupCO.isRequestACallBackPopUpOpen = true;
    this.updatedAppPopupCO(appPopupCO);
  }

  showDenyUnfollowPopUp()
  {
    let appPopupCO:AppPopupCO = this._appPopupCO.getValue();
    appPopupCO.resetAll();
    appPopupCO.isDenyUnfollowPopUpOpen = true;
    this.updatedAppPopupCO(appPopupCO);
  }


  showConfirmUnfollowPopUp()
  {
    let appPopupCO:AppPopupCO = this._appPopupCO.getValue();
    appPopupCO.resetAll();
    appPopupCO.isConfirmUnFollowPopUpOpen = true;
    this.updatedAppPopupCO(appPopupCO);
  }


  showAskQuestionPopUp()
  {
    let appPopupCO:AppPopupCO = this._appPopupCO.getValue();
    appPopupCO.resetAll();
    appPopupCO.isAskQuestionPopUpOpen = true;
    this.updatedAppPopupCO(appPopupCO);
  }

  showUserQualificationPopUp()
  {
    let appPopupCO:AppPopupCO = this._appPopupCO.getValue();
    appPopupCO.resetAll();
    appPopupCO.isUserQualificationPopUpOpen = true;
    this.updatedAppPopupCO(appPopupCO);
  }

  showOtpPopUp()
  {
    let appPopupCO:AppPopupCO = this._appPopupCO.getValue();
    appPopupCO.resetAll();
    appPopupCO.isOtpPopUpOpen = true;
    this.updatedAppPopupCO(appPopupCO);
  }



  updatedAppPopupCO(appPopupCO: AppPopupCO)
  {
     //AppUtility.log('updatedAppPopupCO : ' + JSON.stringify(appPopupCO));
     this._appPopupCO.next(appPopupCO);
  }

  closePopUp()
  {
    let appPopupCO:AppPopupCO = this._appPopupCO.getValue();
    appPopupCO.resetAll();
    this.updatedAppPopupCO(appPopupCO);
  }

  showTutorSuccessPopUp()
  {
    let appPopupCO:AppPopupCO = this._appPopupCO.getValue();
    appPopupCO.resetAll();
    appPopupCO.isTutorSuccessPopUpOpen = true;
    this.updatedAppPopupCO(appPopupCO);
  }

  showImagePopUp(sclPostAttachmentsCOList:SclPostAttachmentsCO[],position:string)
  {
    let appPopupCO:AppPopupCO = this._appPopupCO.getValue();
    appPopupCO.resetAll();
    appPopupCO.sclPostAttachmentsCOList=sclPostAttachmentsCOList;
    appPopupCO.sclPostAttachmentPosition=position;
    appPopupCO.isImagePopUpOpen = true;
    this.updatedAppPopupCO(appPopupCO);
  }

  showSignUp() 
  {
    let appPopupCO: AppPopupCO = this._appPopupCO.getValue();
    appPopupCO.resetAll();
    appPopupCO.isSignUpPopUpOpen = true;
    this.updatedAppPopupCO(appPopupCO);
  }

/*   isAnyPopupOpen():boolean
  {
    AppUtility.log("=============="+JSON.stringify(this.openPopupCount));

    if(this.openPopupCount > 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  } */
}
