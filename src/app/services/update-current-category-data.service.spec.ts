import { TestBed } from '@angular/core/testing';

import { UpdateCurrentCategoryDataService } from './update-current-category-data.service';

describe('UpdateCurrentCategoryDataService', () => {
  let service: UpdateCurrentCategoryDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UpdateCurrentCategoryDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
