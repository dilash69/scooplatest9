import { Injectable } from '@angular/core';

import { BehaviorSubject ,  Observable } from 'rxjs';
import { ChangeCategoryStateCO } from '../co/changeCategoryStateCO';
import { ChangeCategoryUserSelectionCO } from '../co/changeCategoryUserSelectionCO';
import { AppConstants } from '../common/app.constants';
import { AppCurrentStateService } from './app.current.state.service';
import { CatCategoriesCO } from '../co/categoryCO';
import { AppUtility } from '../common/app.utility';

@Injectable({
  providedIn: 'root'
})
export class ChangeCategoryService 
{
  private _changeCategoryStateCO:BehaviorSubject<ChangeCategoryStateCO>;
  changeCategoryStateCO:Observable<ChangeCategoryStateCO>;

  private _changeCategoryUserSelectionCO:BehaviorSubject<ChangeCategoryUserSelectionCO>;
  changeCategoryUserSelectionCO:Observable<ChangeCategoryUserSelectionCO>;

  constructor(private appCurrentStateService:AppCurrentStateService) 
  {
    this._changeCategoryStateCO = new BehaviorSubject<ChangeCategoryStateCO>(this.initializeChangeCategoryStateCO());
    this.changeCategoryStateCO = this._changeCategoryStateCO.asObservable();

    this._changeCategoryUserSelectionCO = new BehaviorSubject<ChangeCategoryUserSelectionCO>(this.initializeChangeCategoryUserSelectionCO());
    this.changeCategoryUserSelectionCO = this._changeCategoryUserSelectionCO.asObservable();
  }

  initializeChangeCategoryUserSelectionCO()
  {
    let changeCategoryUserSelectionCOTemp:ChangeCategoryUserSelectionCO = new ChangeCategoryUserSelectionCO();    
    return changeCategoryUserSelectionCOTemp;
  }

  initializeChangeCategoryStateCO():ChangeCategoryStateCO
  {
    //localStorage.setItem(AppConstants.ACCESS_TOKEN,"eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxMDYxMTYxIiwiaWF0IjoxNTQ1MTk0MjQ3LCJpc3MiOiIyNzA5Y2JlZWQyZDQzNjFlMDI4ODYyYjY2NTAxOWRhZiJ9.z_n4HYmrupWUhFjHUy67pBFdzhgghFcknQLtRdyAPbg")
    //localStorage.setItem(AppConstants.ACCESS_TOKEN, "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxNDYyMTY1IiwiaWF0IjoxNTQ1MzIyOTU4LCJpc3MiOiIxMjM0NTYifQ.f-S-DCZx4Acjda5v_AWQQqwkkNEnqHv6mBAlxN5xrfw")
    let changeCategoryStateCOTemp:ChangeCategoryStateCO = new ChangeCategoryStateCO();
    changeCategoryStateCOTemp.currentStep = AppConstants.STEP_2_VALUE;

      if (this.appCurrentStateService.isBrowser()) 
      {
          if (localStorage.getItem(AppConstants.ACCESS_TOKEN)) 
          {
              changeCategoryStateCOTemp.isUserLoggedIn =  true;
          }
          else 
          {
              changeCategoryStateCOTemp.isUserLoggedIn = false;
          }
      }
      else 
      {
          changeCategoryStateCOTemp.isUserLoggedIn = false;
      }

    return changeCategoryStateCOTemp;
  }

  resetService()
  {
    this.updateChangeCategoryStateCO(this.initializeChangeCategoryStateCO());
    this.updateChangeCategoryUserSelectionCO(this.initializeChangeCategoryUserSelectionCO());
  }

  updateChangeCategoryStateCO(changeCategoryStateCO:ChangeCategoryStateCO) 
  {
    AppUtility.log('changeCategoryStateCO');
    AppUtility.log(changeCategoryStateCO);
    this._changeCategoryStateCO.next(changeCategoryStateCO)
  }

  updateChangeCategoryUserSelectionCO(changeCategoryUserSelectionCO:ChangeCategoryUserSelectionCO) 
  {
    AppUtility.log('changeCategoryUserSelectionCO');
    AppUtility.log(changeCategoryUserSelectionCO);
    this._changeCategoryUserSelectionCO.next(changeCategoryUserSelectionCO)
  }

   switchCategory(catCategoryCOList:CatCategoriesCO[])
  {
    let changeCategoryStateCOTemp:ChangeCategoryStateCO = this.initializeChangeCategoryStateCO();
    changeCategoryStateCOTemp.currentStep = AppConstants.STEP_3_VALUE;
    changeCategoryStateCOTemp.catCategoriesCOList = catCategoryCOList;
    changeCategoryStateCOTemp.purpose = AppConstants.SWITCH_CATEGORY;
    this.updateChangeCategoryStateCO(changeCategoryStateCOTemp);
  } 

  subscribeCategory()
  {
    //let changeCategoryStateCOTemp:ChangeCategoryStateCO = this.initializeChangeCategoryStateCO();
    let changeCategoryStateCOTemp:ChangeCategoryStateCO = this._changeCategoryStateCO.getValue();
    changeCategoryStateCOTemp.currentStep = AppConstants.STEP_1_VALUE;
    //changeCategoryStateCOTemp.catCategoriesCOList = catCategoryCOList;
    changeCategoryStateCOTemp.purpose = AppConstants.SELECT_CATEGORY;
    this.updateChangeCategoryStateCO(changeCategoryStateCOTemp);
  }
  
  unFollowCategory(catCategoryCO:CatCategoriesCO)
  {
    let changeCategoryStateCOTemp:ChangeCategoryStateCO = this._changeCategoryStateCO.getValue();
    changeCategoryStateCOTemp.currentStep = AppConstants.STEP_NONE_VALUE;
    changeCategoryStateCOTemp.purpose = AppConstants.UNFOLLOW_CATEGORY;
    this.updateChangeCategoryStateCO(changeCategoryStateCOTemp);

    let changeCategoryUserSelectionCOTemp:ChangeCategoryUserSelectionCO = this._changeCategoryUserSelectionCO.getValue();
    changeCategoryUserSelectionCOTemp.catCategoryCO = catCategoryCO;
    this.updateChangeCategoryUserSelectionCO(changeCategoryUserSelectionCOTemp);
  }

  followCategory(catCategoryCOList:CatCategoriesCO[], catCategoryCO:CatCategoriesCO)
  {
    let changeCategoryStateCOTemp:ChangeCategoryStateCO = this.initializeChangeCategoryStateCO();
    changeCategoryStateCOTemp.currentStep = AppConstants.STEP_2_VALUE;
    changeCategoryStateCOTemp.purpose = AppConstants.FOLLOW_CATEGORY;
    changeCategoryStateCOTemp.catCategoriesCOList = catCategoryCOList;
    this.updateChangeCategoryStateCO(changeCategoryStateCOTemp);

    let changeCategoryUserSelectionCOTemp:ChangeCategoryUserSelectionCO = this.initializeChangeCategoryUserSelectionCO();
    changeCategoryUserSelectionCOTemp.catCategoryCO = catCategoryCO;
    this.updateChangeCategoryUserSelectionCO(changeCategoryUserSelectionCOTemp);
  }

  setStepByPurpose(changeCategoryStateCO: ChangeCategoryStateCO)
  {
    if(AppConstants.FOLLOW_CATEGORY === changeCategoryStateCO.purpose)
    {
      changeCategoryStateCO.currentStep = AppConstants.STEP_2_VALUE;
    }
    else if(AppConstants.EDIT_CATEGORY === changeCategoryStateCO.purpose)
    {
      changeCategoryStateCO.currentStep = AppConstants.STEP_2_VALUE;
    }
    else if(AppConstants.SWITCH_CATEGORY === changeCategoryStateCO.purpose)
    {
      changeCategoryStateCO.currentStep = AppConstants.STEP_3_VALUE;
    }
    else if(AppConstants.SELECT_CATEGORY === changeCategoryStateCO.purpose)
    {
      changeCategoryStateCO.currentStep = AppConstants.STEP_1_VALUE;
    }
    else
    {
      console.log("no purpose"+ changeCategoryStateCO.purpose);
    }
  }
}
