import { Injectable } from '@angular/core';
//import { BehaviorSubject } from 'rxjs';
import {BehaviorSubject} from 'rxjs';
import { Step3CO } from '../co/step3CO';
import { CatObjectCategoryRelationCO } from '../co/catObjectCategoryRelationCO';

@Injectable({
  providedIn: 'root'
})
export class Step3Service {

  private step3BS = new BehaviorSubject<Step3CO>(null);
  step3Observable = this.step3BS.asObservable();

  private catObjectCategoryRelationCOListBS = new BehaviorSubject<CatObjectCategoryRelationCO[]>(null);
  catObjectCategoryRelationCOListObservable = this.catObjectCategoryRelationCOListBS.asObservable();

  constructor() {}

  updateStep3COObject(step3Observable) 
  {
    this.step3BS.next(step3Observable)
  }

  updateCatObjectCategoryRelationCOList(catObjectCategoryRelationCOListObservable) 
  {
    this.catObjectCategoryRelationCOListBS.next(catObjectCategoryRelationCOListObservable)
  }
}
