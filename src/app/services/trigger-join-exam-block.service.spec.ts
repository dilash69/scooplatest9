import { TestBed } from '@angular/core/testing';

import { TriggerJoinExamBlockService } from './trigger-join-exam-block.service';

describe('TriggerJoinExamBlockService', () => {
  let service: TriggerJoinExamBlockService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TriggerJoinExamBlockService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
