import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserCO } from '../co/userCO';

@Injectable({
  providedIn: 'root'
})
export class TutorDetailService {

  private tutorDetailBS = new BehaviorSubject<UserCO>(null);
  tutorDetailObservable = this.tutorDetailBS.asObservable();

  constructor() { }

  updateTutorDetail(tutorDetailObservable) {
    this.tutorDetailBS.next(tutorDetailObservable)
  }
}
