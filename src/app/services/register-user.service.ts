import { Injectable } from '@angular/core';
//import {  Observable } from 'rxjs';
import { Observable , BehaviorSubject} from 'rxjs';
import { UserCO } from '../co/userCO';
import { UserRegistrationDetailCO } from '../co/userRegistrationDetailCO';

@Injectable({
  providedIn: 'root'
})
export class RegisterUserService {

  message: Observable<any>
  otp: Observable<any>
  phone: Observable<any>
  isTutorRegistration: Observable<any>
  emailId: Observable<any>
  private _message: BehaviorSubject<any>;
  private _otp: BehaviorSubject<any>;
  private _phone: BehaviorSubject<any>;
  private _userInfo: BehaviorSubject<any>;
  private _isTutorRegistration: BehaviorSubject<any>;
  private _emailId: BehaviorSubject<any>;

  private userRegistrationDetailBS = new BehaviorSubject<UserRegistrationDetailCO>(null);
  userRegistrationDetailObservable = this.userRegistrationDetailBS.asObservable();

  
  constructor()
   {   
    this._otp = <BehaviorSubject<any>>new BehaviorSubject(this.otp);
    this.otp = this._otp.asObservable();  
    
    this._phone = <BehaviorSubject<any>>new BehaviorSubject(this.phone);
    this.phone = this._phone.asObservable();   

    this._message = <BehaviorSubject<any>>new BehaviorSubject(this.message);
    this.message = this._message.asObservable();

    this._isTutorRegistration = <BehaviorSubject<any>>new BehaviorSubject(this._isTutorRegistration);
    this.isTutorRegistration = this._isTutorRegistration.asObservable();

    this._emailId = <BehaviorSubject<any>>new BehaviorSubject(this.emailId);
    this.emailId = this._emailId.asObservable();
    
   }

    updatedUser(otp:any,phone:any, message:any,isTutorRegistration:any,emailId:any)
   {
      this._otp.next(otp);
      this._phone.next(phone);
      this._message.next(message);
      this._isTutorRegistration.next(isTutorRegistration);
      this._emailId.next(emailId);
   }  

   updatedUser1(userRegistrationDetailCO:any)
   {
      this.userRegistrationDetailBS.next(userRegistrationDetailCO);
   }




   
  //  resetSclPostCO()
  //  {
  //   let sclPostCO = new SclPostCO();
  //   sclPostCO.sclPostArticlesCO = new SclPostArticlesCO();
  //   sclPostCO.sclPostsSeoCO = new SclPostsSeoCO();
  //   sclPostCO.userCO = new UserCO();
  //   sclPostCO.cmsTagsCOList=new Array();
  //   sclPostCO.sclPostsSeoCO.keywordsList=new Array();
  //   this.updatedSclPostCO(sclPostCO);
  //  }
}
