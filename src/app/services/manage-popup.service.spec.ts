import { TestBed, inject } from '@angular/core/testing';

import { ManagePopupService } from './manage-popup.service';

describe('ManagePopupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ManagePopupService]
    });
  });

  it('should be created', inject([ManagePopupService], (service: ManagePopupService) => {
    expect(service).toBeTruthy();
  }));
});
