import { TestBed, inject } from '@angular/core/testing';

import { ShowloaderService } from './showloader.service';

describe('ShowloaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShowloaderService]
    });
  });

  it('should be created', inject([ShowloaderService], (service: ShowloaderService) => {
    expect(service).toBeTruthy();
  }));
});
