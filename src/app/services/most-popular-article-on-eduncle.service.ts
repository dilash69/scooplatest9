import { Injectable } from '@angular/core';

import { BehaviorSubject ,  Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class MostPopularArticleOnEduncleService {

  mostPopularOnEduncleArticleId: Observable<string[]>
  private _mostPopularOnEduncleArticleId: BehaviorSubject<string[]>;

   constructor()
   {   
    this.mostPopularOnEduncleArticleId = new BehaviorSubject<string[]>(null);
    let mostPopularOnEduncleArticleIdList:string[] = new Array() ;
    this._mostPopularOnEduncleArticleId = <BehaviorSubject<string[]>>new BehaviorSubject(mostPopularOnEduncleArticleIdList);

    this.mostPopularOnEduncleArticleId = this._mostPopularOnEduncleArticleId.asObservable();   
   }

   updateMostPopularArticleIdList(mostPopularOnEduncleArticleId: string[])
   {
      this._mostPopularOnEduncleArticleId.next(mostPopularOnEduncleArticleId);
   }

   resetCatCategoriesCOList()
   {
    this._mostPopularOnEduncleArticleId.next(null);
   }
}
