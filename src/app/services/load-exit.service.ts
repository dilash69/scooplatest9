import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadExitService
{

  private showHideBS = new BehaviorSubject<boolean>(null);
  showHideObservable = this.showHideBS.asObservable();

  constructor() {}

  showExitPopUp(showHideObservable) 
  {
    this.showHideBS.next(showHideObservable)
  }
}
