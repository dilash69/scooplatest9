import { CmsCategoryMetaDataCO } from './../co/categoryMetaDataCO';
import { CatCategoriesCO } from './../co/categoryCO';

import { BehaviorSubject ,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FilterService 
{
  catCategoriesCOList: Observable<CatCategoriesCO[]>
  private _catCategoriesCOList: BehaviorSubject<CatCategoriesCO[]>;
  searchText: Observable<string>;
  private _searchText: BehaviorSubject<string>;

   constructor()
   {   
    this.catCategoriesCOList = new BehaviorSubject<CatCategoriesCO[]>(null);
    let catCategoriesCOArray:CatCategoriesCO[] = new Array() ;
    this._catCategoriesCOList = <BehaviorSubject<CatCategoriesCO[]>>new BehaviorSubject(catCategoriesCOArray);

    this.searchText=new BehaviorSubject<string>(null);
    let variable='';
    this._searchText = <BehaviorSubject<string>>new BehaviorSubject(variable);
    

    this.catCategoriesCOList = this._catCategoriesCOList.asObservable();   
    this.searchText = this._searchText.asObservable();   
   }

   updatedCatCategoriesCOList(catCategoriesCOList: CatCategoriesCO[])
   {
      this._catCategoriesCOList.next(catCategoriesCOList);
   }

   updatedText(text: string)
   {
      this._searchText.next(text);
   }

   resetCatCategoriesCOList()
   {
    this._catCategoriesCOList.next(null);
   }
}
