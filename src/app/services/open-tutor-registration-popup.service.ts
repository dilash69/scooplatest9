import { Injectable } from '@angular/core';

import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OpenTutorRegistrationPopupService 
{
  private showHideBS = new BehaviorSubject<boolean>(null);
  showHideObservable = this.showHideBS.asObservable();

  constructor() {}

  openTutorPopUp(showHideObservable) 
  {
    this.showHideBS.next(showHideObservable)
  }
}
