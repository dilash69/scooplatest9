import { TestBed, inject } from '@angular/core/testing';

import { OpenTutorRegistrationPopupService } from './open-tutor-registration-popup.service';

describe('OpenTutorRegistrationPopupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OpenTutorRegistrationPopupService]
    });
  });

  it('should be created', inject([OpenTutorRegistrationPopupService], (service: OpenTutorRegistrationPopupService) => {
    expect(service).toBeTruthy();
  }));
});
