import { TestBed, inject } from '@angular/core/testing';

import { ChangeCategoryService } from './change-category.service';

describe('ChangeCategoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChangeCategoryService]
    });
  });

  it('should be created', inject([ChangeCategoryService], (service: ChangeCategoryService) => {
    expect(service).toBeTruthy();
  }));
});
