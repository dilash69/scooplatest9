import {Injectable, ɵConsole} from '@angular/core';
import {HttpClient, HttpRequest, HttpEvent} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppSettings } from '../common/app-settings';
import { AppConstants } from '../common/app.constants';

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {

  constructor(private http: HttpClient) { }

  pushFileToStorage(file: File): Observable<HttpEvent<{}>> 
  {
    
    const formdata: FormData = new FormData();
    formdata.append('uploadedfile', file);
    formdata.append('APP_VERSION', "55");
    formdata.append('REQ_SOURCE', "WEB");
    formdata.append('APP_TYPE', "CUSTOMER");
    formdata.append('USER_PROFILE_TYPE', "STUDENT");
    formdata.append("Access-Control-Allow-Origin", "true");
    if(localStorage.getItem(AppConstants.ACCESS_TOKEN))
    {
      formdata.append('ACCESS_TOKEN', localStorage.getItem(AppConstants.ACCESS_TOKEN));
    }
    else
    {
      formdata.append('ACCESS_TOKEN', "");
    }
    
    const req = new HttpRequest('POST', AppSettings.upload_file_url, formdata, 
    {
      reportProgress: true,
      responseType: 'text'
    }
    );
    return this.http.request(req);
  }
}
