import { CatCategoryFiltersCO } from './../co/catCategoryFiltersCO';
import { CatSubjectsCO } from './../co/catSubjectsCO';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { deserialize } from 'serializer.ts/Serializer';
import { CatObjectCategoryRelationCO } from '../co/catObjectCategoryRelationCO';
import { CmsTagsCO } from '../co/cmsTagCO';
import { FilterValueCO } from '../co/filterValueCO';
import { CatCategoryAdSlidersCO } from './../co/catCategoryAdSlidersCO';
import { CatCategoryQuickLinksCO } from './../co/catCategoryQuickLinksCO';
import { CatCategoriesCO } from './../co/categoryCO';
import { CatExamsCO } from './../co/catExamsCO';
import { SclPostCommentCO } from './../co/commentCO';
import { SclCommentReplyCO } from './../co/commentReplyCO';
import { DetailedCategoryForHomeCO } from './../co/detailedCategoryForHomeCO';
import { FacebookShareCO } from './../co/facebook-shareCO';
import { SclPostArticlesCO } from './../co/sclPostArticlesCO';
import { SclPostCO } from './../co/sclPostCO';
import { UserResponseData } from './../co/user-response-data';
import { UserCO } from './../co/userCO';
import { ApiCall } from './../common/api.call';
import { AppConstants } from './../common/app.constants';
import { DataService } from './data.service';
import { PlainHttpDataService } from './plain-http-data.service';
import { CoursesHeaderCO } from '../co/coursesHeaderCO';
import { CmnFeedbackQuestionsCO } from '../co/CmnFeedbackQuestionsCO';
import { UserFeedbackQuestionAnswers } from '../co/userFeedbackQuestionAnswers';
import { map } from 'rxjs/operators';
import { CatGradesCO } from '../co/catGradesCO';
import { FeedCO } from '../co/feedCO';

@Injectable({
    providedIn: 'root'
})
export class BlogModuleService {

    constructor(private dataSearvice: DataService,
                private plainHttpDataService: PlainHttpDataService) { }

    getCategoryList(apiCall: ApiCall): Observable<CatCategoriesCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let catCategoriesCO: CatCategoriesCO[] = userResponseData.responseValues['catCategoriesCOList'];
                    catCategoriesCO = deserialize<CatCategoriesCO[]>(CatCategoriesCO, catCategoriesCO);
                    return catCategoriesCO;
                }
                else 
                {
                    throw new Error('Unexpected user error');
                }
            }))
    }

    getAllCategories(apiCall: ApiCall): Observable<CatCategoriesCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let catCategoriesCO: CatCategoriesCO[] = userResponseData.responseValues['catCategoriesCOList'];
                    catCategoriesCO = deserialize<CatCategoriesCO[]>(CatCategoriesCO, catCategoriesCO);
                    return catCategoriesCO;
                }
                else 
                {
                    throw new Error('Unexpected user error');
                }
            }))
    }

    getTagPostArticleIdList(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                  
                    return userResponseData.responseValues;
                }
                else 
                {
                    throw new Error('Unexpected id list error');
                }
            }))
    }

    getCommentReplyByIdList(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                }
                else 
                {
                    throw new Error('Unexpected id list error');
                }
            }))
    }

    getMostPopularArticleList(apiCall: ApiCall): Observable<SclPostCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let sclPostCOList: SclPostCO[] = userResponseData.responseValues['sclPostCOList'];
                    sclPostCOList = deserialize<SclPostCO[]>(SclPostCO, sclPostCOList);
                    return sclPostCOList;
                }
                else 
                {
                    throw new Error('Unexpected article list error');
                }
            }))
    }

    getFilteredStream(apiCall: ApiCall): Observable<CatCategoriesCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let catCategoriesCOList: CatCategoriesCO[] = userResponseData.responseValues['catCategoriesCOList'];
                    catCategoriesCOList = deserialize<CatCategoriesCO[]>(CatCategoriesCO, catCategoriesCOList);
                    return catCategoriesCOList;
                }
                else 
                {
                    throw new Error('Unexpected article list error');
                }
            }))
    }


    getRecentArticleList(apiCall: ApiCall): Observable<SclPostCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let sclPostCOArray: SclPostCO[] = userResponseData.responseValues['sclPostCOList'];
                    sclPostCOArray = deserialize<SclPostCO[]>(SclPostCO, sclPostCOArray);
                    return sclPostCOArray;
                }
                else 
                {
                    throw new Error('Unexpected article list error');
                }
            }))
    }

    getCategoryDetail(apiCall: ApiCall): Observable<CatCategoriesCO> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let catCategoriesCO: CatCategoriesCO = userResponseData.responseValues['catCategoriesCO'];
                    catCategoriesCO = deserialize<CatCategoriesCO>(CatCategoriesCO, catCategoriesCO);
                    return catCategoriesCO;
                }
                else {
                    throw new Error('Unexpected article list error');
                }
            }))
    }

    getPostDetail(apiCall: ApiCall): Observable<SclPostCO> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let sclPostCO: SclPostCO = userResponseData.responseValues['sclPostCO'];
                    sclPostCO = deserialize<SclPostCO>(SclPostCO, sclPostCO);
                    return sclPostCO;
                }
                else 
                {
                    throw new Error('Unexpected user error');
                }
            }))
    }

    getPostDetailStatic(): Observable<SclPostCO> 
    {
        return this.plainHttpDataService.get("https://a.uguu.se/XNnpMYFYvWhA_getPostDetailResponse.txt")
            .pipe(map((sclPostCO: SclPostCO) => 
            {
                sclPostCO = deserialize<SclPostCO>(SclPostCO, sclPostCO);
                return sclPostCO;
            }))
    }

    getPostCOList(apiCall: ApiCall): Observable<SclPostCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let sclPostCO: SclPostCO[] = userResponseData.responseValues['sclPostCOList'];
                    sclPostCO = deserialize<SclPostCO[]>(SclPostCO, sclPostCO);
                    return sclPostCO;
                }
                else
                {
                    throw new Error('Unexpected user error');
                }
            }))
    }

    getSclPostCO(apiCall: ApiCall): Observable<SclPostCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return  userResponseData.responseValues;
                }
                else
                {
                    throw new Error('Unexpected user error');
                }
            }))
    }

    getMostAlsoLikeArticleListApiCall(apiCall: ApiCall): Observable<SclPostArticlesCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let sclPostArticlesCOArray: SclPostArticlesCO[] = userResponseData.responseValues['articleList'];
                    sclPostArticlesCOArray = deserialize<SclPostArticlesCO[]>(SclPostArticlesCO, sclPostArticlesCOArray);
                    return sclPostArticlesCOArray;
                }
                else 
                {
                    throw new Error('Unexpected user error');
                }
            }))
    }

    getTagDetailBySlug(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return  userResponseData.responseValues;
                    // let cmsTagsCO: CmsTagsCO = userResponseData.responseValues['cmsTagCO'];
                    // cmsTagsCO = deserialize<CmsTagsCO>(CmsTagsCO, cmsTagsCO);
                    // return cmsTagsCO;
                }
                else {
                    throw new Error('Unexpected tag detail error');
                }
            }))
    }

    getTagArticleIdList(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                }
                else 
                {
                    throw new Error('Unexpected id list error');
                }
            }))
    }

    getUserDetailbySlug(apiCall: ApiCall): Observable<UserCO> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                    // let userCO: UserCO = userResponseData.responseValues['userCO'];
                    // userCO = deserialize<UserCO>(UserCO, userCO);
                    // return userCO;
                }
                else {
                    throw new Error('Unexpected tag detail error');
                }
            }))
    }

    getAuthorArticleIdList(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                }
                else 
                {
                    throw new Error('Unexpected id list error');
                }
            }))
    }

    getPostInitialCommentList(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                }
                else 
                {
                    throw new Error('Unexpected id list error');
                }
            }))
    }

    deleteComment(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    
                }
                else {
                    throw new Error('comment not deleted error');
                }
            }))
    }

    likeComment(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST)
                {
                
                }
                else {
                    
                }
            }))
    }

    getSearchedKeyWords(apiCall: ApiCall): Observable<SclPostCommentCO>
     {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) =>
             {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                  
                    return userResponseData.responseValues;
                }
                else {
                    throw new Error('Unexpected id list error');
                }
            }))
    }


    writePostComment(apiCall: ApiCall): Observable<SclPostCommentCO> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    let sclPostCommentCO: SclPostCommentCO = userResponseData.responseValues['sclPostComment'];
                    sclPostCommentCO = deserialize<SclPostCommentCO>(SclPostCommentCO, sclPostCommentCO);
                    return sclPostCommentCO;
                }
                else {
                    throw new Error('Unexpected id list error');
                }
            }))
    }

    unlikeComment(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    
                }
                else {
                    
                }
            }))
    }

    likeReply(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {

                }
                else {
                    
                }
            }))
    }

    getCommentDetailList(apiCall: ApiCall): Observable<SclPostCommentCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let sclPostCommentCOList: SclPostCommentCO[] = userResponseData.responseValues['sclPostCommentCOList'];
                    sclPostCommentCOList = deserialize<SclPostCommentCO[]>(SclPostCommentCO, sclPostCommentCOList);
                    return sclPostCommentCOList;
                }
                else
                {
                    throw new Error('Unexpected user error');
                }
            }))
    }

    getAuthorPostCOList(apiCall: ApiCall): Observable<SclPostCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let sclPostCO: SclPostCO[] = userResponseData.responseValues['postDetail'];
                    sclPostCO = deserialize<SclPostCO[]>(SclPostCO, sclPostCO);
                    return sclPostCO;
                }
                else
                {
                    throw new Error('Unexpected user error');
                }
            }))
    }

    getCategoryPostIdList(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    return userResponseData.responseValues;
                } else {
                    throw new Error('Unexpected id list error');
                }
            }))
    }

    replyComment(apiCall: ApiCall): Observable<SclCommentReplyCO> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    let sclCommentReplyCO: SclCommentReplyCO = userResponseData.responseValues['sclCommentReply'];
                    sclCommentReplyCO = deserialize<SclCommentReplyCO>(SclCommentReplyCO, sclCommentReplyCO);
                    return sclCommentReplyCO;
                }
                else {
                    throw new Error('Unexpected id list error');
                }
            }))
    }

    unlikeReply(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    
                }
                else {
                    
                }
            }))
    }

    deleteReply(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    
                }
                else {
                    throw new Error('comment not deleted error');
                }
            }))
    }

    getCommentReplyIdList(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                  
                    return userResponseData.responseValues;
                }
                else 
                {
                    throw new Error('Unexpected id list error');
                }
            }))
    }

    getRecentGlobalArticleList(apiCall: ApiCall): Observable<SclPostCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let sclPostCOArray: SclPostCO[] = userResponseData.responseValues['sclPostCOList'];
                    sclPostCOArray = deserialize<SclPostCO[]>(SclPostCO, sclPostCOArray);
                    return sclPostCOArray;
                }
                else 
                {
                    throw new Error('Unexpected article list error');
                }
            }))
    }

    getMostPopularGlobalArticleList(apiCall: ApiCall): Observable<SclPostCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let sclPostCOArray: SclPostCO[] = userResponseData.responseValues['sclPostCOList'];
                    sclPostCOArray = deserialize<SclPostCO[]>(SclPostCO, sclPostCOArray);
                    return sclPostCOArray;
                }
                else 
                {
                    throw new Error('Unexpected article list error');
                }
            }))
    }

    getCategoryDetailByCategoryId(apiCall: ApiCall): Observable<CatCategoriesCO> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let catCategoriesCO: CatCategoriesCO = userResponseData.responseValues['catCategoriesCO'];
                    catCategoriesCO = deserialize<CatCategoriesCO>(CatCategoriesCO, catCategoriesCO);
                    return catCategoriesCO;
                }
                else {
                    throw new Error('Unexpected article list error');
                }
            }))
    }

    /**
     * Use this method/service to register an user and send an otp.
     * @author Shakir Ansari
     * @param apiCall 
     */
    registerUser(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                // if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                //     return userResponseData.responseValues['otp'];
                // } else {
                //     throw new Error("Unexpected registerUser error");
                // }
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                }
                else 
                {           
                    return userResponseData.userMessageList[0];
                }
            }));
    }

    /**
     * Use this method/service to resend an otp.
     * @author Shakir Ansari
     * @param apiCall 
     */
    createAndGetMobileOtp(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    return userResponseData.responseValues['otp'];
                } else {
                    throw new Error("Unexpected createAndGetMobileOtp error");
                }
            }));
    }

    getProductsCartCount(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues['cartCount'];
                } 
                else 
                {
                    throw new Error("Unexpected createAndGetMobileOtp error");
                }
            }));
    }

    /**
     * Use this method/service to verify an otp.
     * @author Shakir Ansari
     * @param apiCall 
     */
    verifiedOtp(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    //saving access token temp..
                    localStorage.setItem(AppConstants.DB_ACCESS_TOKEN, userResponseData.responseValues[AppConstants.DB_ACCESS_TOKEN]);

                    let userCO: UserCO = userResponseData.responseValues['userCO'];
                    userCO = deserialize<UserCO>(UserCO, userCO);
                    return userCO;
                } else {
                    throw new Error("Unexpected verifiedOtp error");
                }
            }));
    }
    userInfoPopUpShow(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                  
                    return userResponseData.responseValues;
                }
                else 
                {
                    throw new Error('Unexpected id list error');
                }
            }));
    }

    resendOtp(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                  
                    return userResponseData.responseValues;
                }
                else 
                {
                    throw new Error('Unexpected id list error');
                }
            }));
    }

    saveUserEducationInformations(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                  
                    return userResponseData.responseValues;
                }
                else 
                {
                    throw new Error('Unexpected id list error');
                }
            }));
    }

    /**
     * Use this method/service to create user password after opt process;
     * @author Shakir Ansari
     * @param apiCall 
     */
    createUserPassword(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {

                    if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                        return userResponseData.responseValues['message'];
                    }
                } else {
                    throw new Error("Unexpected createUserPassword error");
                }
            }));
    }

    followCategoryByUser(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                  
                    return userResponseData.responseValues;
                }
                else 
                {
                    throw new Error('Unexpected error in follow category');
                }
            }))
    }

    unFollowCategory(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                  
                    return userResponseData.responseValues;
                }
                else 
                {
                    throw new Error('Unexpected error in unfollow category');
                }
            }))
    }

    getCategoryAdSlider(apiCall: ApiCall): Observable<CatCategoryAdSlidersCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let CatCategoryAdSlidersCOList: CatCategoryAdSlidersCO[] = userResponseData.responseValues['catCategoryAdSlidersCOList'];
                    CatCategoryAdSlidersCOList = deserialize<CatCategoryAdSlidersCO[]>(CatCategoryAdSlidersCO, CatCategoryAdSlidersCOList);
                    return CatCategoryAdSlidersCOList;
                }
                else 
                {
                    throw new Error('Unexpected product error');
                }
            }))
    }

    getDetailedCategoriesForHome(apiCall: ApiCall): Observable<DetailedCategoryForHomeCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let detailedCategoryForHomeCOList: DetailedCategoryForHomeCO[] = userResponseData.responseValues['detailedCategoryForHomeCOList'];
                    detailedCategoryForHomeCOList = deserialize<DetailedCategoryForHomeCO[]>(DetailedCategoryForHomeCO, detailedCategoryForHomeCOList);
                    return detailedCategoryForHomeCOList;
                }
                else 
                {
                    throw new Error('Unexpected getDetailedCategoriesForHome error');
                }
            }))
    }

    getCategoryQuickLinks(apiCall: ApiCall): Observable<CatCategoryQuickLinksCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let catCategoryQuickLinksCOArray: CatCategoryQuickLinksCO[] = userResponseData.responseValues['catCategoryQuickLinksCOList'];
                    catCategoryQuickLinksCOArray = deserialize<CatCategoryQuickLinksCO[]>(CatCategoryQuickLinksCO, catCategoryQuickLinksCOArray);
                    return catCategoryQuickLinksCOArray;
                }
                else 
                {
                    throw new Error('Unexpected article list error');
                }
            }))
    }

    getTagAdSliderByTagId(apiCall: ApiCall): Observable<CatCategoryAdSlidersCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let CatCategoryAdSlidersCOList: CatCategoryAdSlidersCO[] = userResponseData.responseValues['catCategoryAdSlidersCOList'];
                    CatCategoryAdSlidersCOList = deserialize<CatCategoryAdSlidersCO[]>(CatCategoryAdSlidersCO, CatCategoryAdSlidersCOList);
                    return CatCategoryAdSlidersCOList;
                }
                else 
                {
                    throw new Error('Unexpected product error');
                }
            }))
    }

    increasePostViewCount(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                  
                    return userResponseData.responseValues;
                }
                else 
                {
                    throw new Error('Unexpected id list error');
                }
            }))
    }

    getYouMightAlsoLikePost(apiCall: ApiCall): Observable<SclPostCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let sclPostCOList: SclPostCO[] = userResponseData.responseValues['sclPostCOList'];
                    sclPostCOList = deserialize<SclPostCO[]>(SclPostCO, sclPostCOList);
                    return sclPostCOList;
                }
                else 
                {
                    throw new Error('Unexpected user error');
                }
            }))
    }

   

      
    getFacebookShareCount(shareUrl): Observable<FacebookShareCO> 
    {
        return this.plainHttpDataService.get("https://graph.facebook.com/?id="+encodeURIComponent(shareUrl))
            .pipe(map((facebookShareCO: FacebookShareCO) => 
            {
                return facebookShareCO;
            }))
    }

    getCompletePostDataBySlug(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                }
                else 
                {
                    throw new Error('Unexpected id list error');
                }
            }))
    }


    getObjectBySlug(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                }
                else 
                {
                    throw new Error('Unexpected id list error');
                }
            }))
    }

    getExamsOfAllCategories(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall).pipe(map((userResponseData: UserResponseData) => {
            if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                return userResponseData.responseValues; //['categoryExamCOList'];
            } else {
                throw new Error('Exception in getExamsOfAllCategories');
            }
        }));
    }

    getExamsByParticularCategoryId(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall).pipe(map((userResponseData: UserResponseData) => {
            if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                return userResponseData.responseValues; //['categoryExamCOList'];
            } else {
                throw new Error('Exception in getExamsOfAllCategories');
            }
        }));
    }

    login(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                }
                else 
                {
                    return userResponseData.userMessageList[0];
                    //throw new Error('Unexpected user error');
                }
            }))
    }

    userRegistrationFrontEnd(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                }
                else 
                {
                    return userResponseData.userMessageList[0];
                    //throw new Error('Unexpected user error');
                }
            }))
    }

    contactUsQueriesUser(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    localStorage.setItem(AppConstants.DB_ACCESS_TOKEN, userResponseData.responseValues[AppConstants.DB_ACCESS_TOKEN]);
                    return userResponseData.responseValues;
                }
                else 
                {
                    return userResponseData.userMessageList[0];
                    //throw new Error('Unexpected user error');
                }
            }))
    }

    forgotPassword(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                }
                else 
                {
                    return userResponseData.userMessageList[0];
                    //throw new Error('Unexpected user error');
                }
            }))
    }


    getCatExams(apiCall: ApiCall): Observable<any> 
    { 
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let catExamsCOList: CatExamsCO[] = userResponseData.responseValues['catExamsCOList'];
                    catExamsCOList = deserialize<CatExamsCO[]>(CatExamsCO, catExamsCOList);
                    return catExamsCOList;
                }
                else 
                {
                    throw new Error('Unexpected product error');
                }
            }))
    }


    tutorRegistration(apiCall: ApiCall): Observable<any> 
    { 
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    localStorage.setItem(AppConstants.DB_ACCESS_TOKEN, userResponseData.responseValues[AppConstants.DB_ACCESS_TOKEN]);
                    return userResponseData.responseValues;
                }
                else 
                {           
                    return userResponseData.userMessageList[0];
                }
            }))
    }

    getCategoryFiltersForCustomer(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                }
                else 
                {
                    throw new Error('Unexpected user error');
                }
            }))
    }

    getIndependentFilterValues(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    let filterValueCOList: FilterValueCO[] = userResponseData.responseValues['filterValueCOList'];
                    filterValueCOList = deserialize<FilterValueCO[]>(FilterValueCO, filterValueCOList);
                    return filterValueCOList;
                }
                else {
                    throw new Error('Unexpected Error');
                }
            }))
    }

    getDependentFilterValues(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    let filterValueCOList: FilterValueCO[] = userResponseData.responseValues['filterValueCOList'];
                    filterValueCOList = deserialize<FilterValueCO[]>(FilterValueCO, filterValueCOList);
                    return filterValueCOList;
                }
                else {
                    throw new Error('Unexpected Error');
                }
            }))
    }

likePost(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST)
                {
                
                }
                else {
                    
                }
            }))
    }

    socialUserRegistration(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST)
                {
                    return userResponseData.responseValues;
                }
                else {
                    
                }
            }))
    }


    unlikePost(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST)
                {
                
                }
                else {
                    
                }
            }))
    }

    updateUserCategoryRelationObject(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    return userResponseData.responseValues;
                }
                else {
                    throw new Error('Unexpected Error');
                }
            }))
    }

    updateDefaultCategoryIdOfUser(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    return userResponseData.responseValues;
                }
                else {
                    throw new Error('Unexpected Error');
                }
            }))
    }

    getCategoryRelationObjectByUser(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    let catCategoryCOList: CatCategoriesCO[] = userResponseData.responseValues['catCategoryCOList'];
                    catCategoryCOList = deserialize<CatCategoriesCO[]>(CatCategoriesCO, catCategoryCOList);
                    return catCategoryCOList;
                }
                else {
                    throw new Error('Unexpected Error');
                }
            }))
    }

    editCategorySelectionApiCall(apiCall: ApiCall): Observable<UserResponseData> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData;
                }
                else 
                {
                    throw new Error('Unexpected Error');
                }
            }))
    }

    deleteCategoryObjectRelationObject(apiCall: ApiCall): Observable<UserResponseData> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData;
                }
                else 
                {
                    throw new Error('Unexpected Error');
                }
            }))
    }

    getCoursesForHeader(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    let coursesHeaderCOList: CoursesHeaderCO[] = userResponseData.responseValues['coursesHeaderCOList'];
                    coursesHeaderCOList = deserialize<CoursesHeaderCO[]>(CoursesHeaderCO, coursesHeaderCOList);
                    return coursesHeaderCOList;
                }
                else {
                    throw new Error('Unexpected Error');
                }
            }))
    }

    updateFbShareCount(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST)
                {
                
                }
                else {
                    
                }
            }))
    }

    getExamsByCategoryId(apiCall: ApiCall): Observable<CatExamsCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let catExamsCOArray: CatExamsCO[] = userResponseData.responseValues['catExamsCOList'];
                    catExamsCOArray = deserialize<CatExamsCO[]>(CatExamsCO, catExamsCOArray);
                    return catExamsCOArray;
                }
                else 
                {
                    throw new Error('Unexpected user error');
                }
            }))
    }

    getSubjectByExamId(apiCall: ApiCall): Observable<CatSubjectsCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let catSubjectsCOArray: CatSubjectsCO[] = userResponseData.responseValues['catSubjectsCOList'];
                    catSubjectsCOArray = deserialize<CatSubjectsCO[]>(CatSubjectsCO, catSubjectsCOArray);
                    return catSubjectsCOArray;
                }
                else 
                {
                    throw new Error('Unexpected user error');
                }
            }))
    }



    getFeedbackQuestions(apiCall: ApiCall): Observable<CmnFeedbackQuestionsCO[]> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                   // return userResponseData;
                  
                    let cmnFeedbackQuestionsCOArray: CmnFeedbackQuestionsCO[] = userResponseData.responseValues['cmnFeedbackQuestionsCOList'];
                    cmnFeedbackQuestionsCOArray = deserialize<CmnFeedbackQuestionsCO[]>(CmnFeedbackQuestionsCO, cmnFeedbackQuestionsCOArray);
                    return cmnFeedbackQuestionsCOArray;
                }
                else 
                {
                    throw new Error('Unexpected Error');
                }
            }))
    }

    setUserFeedbackQuestionAnswers(apiCall: ApiCall): Observable<UserResponseData> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                }
                else 
                {
                    throw new Error('Unexpected Error');
                }
            }))
    }


    getCategoryFiltersForAdmin(apiCall: ApiCall): Observable<CatCategoryFiltersCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let catCategoryFiltersCOList: CatCategoryFiltersCO[] = userResponseData.responseValues['catCategoryFiltersCOList'];
                    catCategoryFiltersCOList = deserialize<CatCategoryFiltersCO[]>(CatCategoryFiltersCO, catCategoryFiltersCOList);
                    return catCategoryFiltersCOList;
                }
                else 
                {
                    throw new Error('Unexpected user error');
                }
            }))
    }


   /*  userFeedbackQuestionAnswersCOList(apiCall: ApiCall): Observable<UserFeedbackQuestionAnswers[]> {
        return this.dataSearvice.hitApi(apiCall)
            .map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let UserFeedbackQuestionAnswersArray: UserFeedbackQuestionAnswers[] = userResponseData.responseValues['catSubjectsCOList'];
                    UserFeedbackQuestionAnswersArray = deserialize<UserFeedbackQuestionAnswers[]>(UserFeedbackQuestionAnswers, UserFeedbackQuestionAnswersArray);
                    return UserFeedbackQuestionAnswersArray;
                }
                else 
                {
                    throw new Error('Unexpected Error');
                }
            })
    } */
   
    userPostLike(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let postLikeStatus = userResponseData.responseValues.hasUserLiked;
                    return postLikeStatus;
                }
                else 
                {
                    throw new Error('Unexpected user error');
                }
            }))
    }

    userCommentLike(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let commentLikeStatus = userResponseData.responseValues.hasUserLiked;
                    return commentLikeStatus;
                }
                else 
                {
                    throw new Error('Unexpected user error');
                }
            }))
    }

    getMostPopularPosts(apiCall: ApiCall): Observable<any> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    return userResponseData.responseValues;
                } else {
                    throw new Error('Unexpected id list error');
                }
            }))
    }

    checkMobileEmailAvailablity(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                return userResponseData;
               /*  if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                }
                else 
                {
                    return userResponseData.userMessageList[0];
                } */
            }))
    }

    getCatGrades(apiCall: ApiCall): Observable<CatGradesCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let CatGradesCOList: CatGradesCO[] = userResponseData.responseValues['catGradesCOList'];
                    CatGradesCOList = deserialize<CatGradesCO[]>(CatGradesCO, CatGradesCOList);
                    return CatGradesCOList;
                }
                else 
                {
                    throw new Error('Unexpected product error');
                }
            }))
    }

    getFilterValuesByGradeId(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                }
            }))
    }

    generateOtp(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                return userResponseData;
            }))
    }

    verifyOtp(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                return userResponseData;
            }))
    }

    scl_userRegistrationFrontEnd(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    //saving access token temp..
                    localStorage.setItem(AppConstants.DB_ACCESS_TOKEN, userResponseData.responseValues[AppConstants.DB_ACCESS_TOKEN]);
                   /*  return userResponseData.responseValues[AppConstants.ACCESS_TOKEN]

                    let userCO: UserCO = userResponseData.responseValues['userCO'];
                    userCO = deserialize<UserCO>(UserCO, userCO); */
                    return userResponseData; 
                } else {
                    throw new Error("Unexpected verifiedOtp error");
                }
            }));
    }

    
    regenerateOtp(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                return userResponseData;
            }))
    }

    getSclPostsByPostShareIdList(apiCall: ApiCall): Observable<SclPostCO[]> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let sclPostCO: SclPostCO[] = userResponseData.responseValues['sclPostCOList'];
                    sclPostCO = deserialize<SclPostCO[]>(SclPostCO, sclPostCO);
                    return sclPostCO;
                }
                else
                {
                    throw new Error('Unexpected user error');
                }
            }))
    }

    getUserConfiguration(apiCall: ApiCall): Observable<UserResponseData> {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData;
                }
                else 
                {
                    throw new Error('Unexpected Error');
                }
            }))
    }

    getUserfeedPosts(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    return userResponseData.responseValues;
                } else {
                    throw new Error('Unexpected id list error');
                }
            }))
    }

    getUserCurrentStateInfo(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                return userResponseData;
            }))
    }

    forgotPasswordScoop(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                return userResponseData;
            }))
    }

    verifyOtpForgotPassword(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) {
                    //saving access token temp..
                    localStorage.setItem(AppConstants.DB_ACCESS_TOKEN, userResponseData.responseValues[AppConstants.DB_ACCESS_TOKEN]);
                
                    return userResponseData; 
                } else {
                    return userResponseData; 
                }
            }));
    }

    hasUserLikedPost(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                } 
                else {
                    throw new Error('Unexpected id list error');
                }
            }))
    }

    getLoggedOutFeeds(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                } 
                else {
                    throw new Error('Unexpected id list error');
                }
            }))
    }

    getSclPostsByFeedCOIdList(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    let feedCOList: FeedCO[] = userResponseData.responseValues['feedCOList'];
                    feedCOList = deserialize<FeedCO[]>(FeedCO, feedCOList);
                    return feedCOList;
                }
                else
                {
                    throw new Error('Unexpected user error');
                }
            }))
    }

    getStudentFeeds(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                } 
                else {
                    throw new Error('Unexpected id list error');
                }
            }))
    }

    saveAppSubscribers(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                } 
                else {
                    throw new Error('Unexpected error');
                }
            }))
    }

    userRegistrationNew(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                return userResponseData;
            }))
    }

    verifyOtpNew(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                return userResponseData;
            }))
    }

    contactUsQueriesUserNew(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                return userResponseData;
            }))
    }

    saveUserBrokenLeads(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                return userResponseData;
            }))
    }

    increaseFbShareCount(apiCall: ApiCall): Observable<any> 
    {
        return this.dataSearvice.hitApi(apiCall)
            .pipe(map((userResponseData: UserResponseData) => 
            {
                if (userResponseData.userResponseCode === AppConstants.SUCCESS_REQUEST) 
                {
                    return userResponseData.responseValues;
                } 
                else {
                    throw new Error('Unexpected error');
                }
            }))
    }

    
}
