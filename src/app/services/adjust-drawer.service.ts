import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdjustDrawerService {

  private showHideBS = new BehaviorSubject<any>(null);
  showHideObservable = this.showHideBS.asObservable();

  constructor() {}

  updateData(showHideObservable) {
    this.showHideBS.next(showHideObservable)
  }
}
