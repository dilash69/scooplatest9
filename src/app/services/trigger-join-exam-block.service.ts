import { Injectable } from '@angular/core';
import { BehaviorSubject ,  Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TriggerJoinExamBlockService {

  private triggerBS = new BehaviorSubject<boolean>(null);
  triggerObservable = this.triggerBS.asObservable();

  constructor() {}

  triggerEvent(triggerObservable) 
  {
    this.triggerBS.next(triggerObservable)
  }
}
