import { TestBed, inject } from '@angular/core/testing';

import { MostPopularArticleOnEduncleService } from './most-popular-article-on-eduncle.service';

describe('MostPopularArticleOnEduncleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MostPopularArticleOnEduncleService]
    });
  });

  it('should be created', inject([MostPopularArticleOnEduncleService], (service: MostPopularArticleOnEduncleService) => {
    expect(service).toBeTruthy();
  }));
});
