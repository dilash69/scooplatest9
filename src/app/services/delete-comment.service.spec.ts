import { TestBed, inject } from '@angular/core/testing';

import { DeleteCommentService } from './delete-comment.service';

describe('DeleteCommentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeleteCommentService]
    });
  });

  it('should be created', inject([DeleteCommentService], (service: DeleteCommentService) => {
    expect(service).toBeTruthy();
  }));
});
