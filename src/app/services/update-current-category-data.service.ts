import { Injectable } from '@angular/core';
import { BehaviorSubject ,  Observable } from 'rxjs';
import { CatCategoriesCO } from '../co/categoryCO';

@Injectable({
  providedIn: 'root'
})
export class UpdateCurrentCategoryDataService {

  private updateCurrentCategoryBS = new BehaviorSubject<CatCategoriesCO>(null);
  updateCurrentCategoryObservable = this.updateCurrentCategoryBS.asObservable();

  constructor() {}

  updateCurrentCategory(updateCurrentCategoryObservable) 
  {
    this.updateCurrentCategoryBS.next(updateCurrentCategoryObservable)
  }
}
