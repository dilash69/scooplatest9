import { CatCategoriesCO } from './../co/categoryCO';
import { Injectable } from '@angular/core';
import { BehaviorSubject ,  Observable } from 'rxjs';
import { AppUtility } from '../common/app.utility';

@Injectable({
  providedIn: 'root'
})
export class CategoryCOService {

  categoryCO: Observable<CatCategoriesCO>
  private _categoryCO: BehaviorSubject<CatCategoriesCO>;

   constructor()
   {   
    this.categoryCO = new BehaviorSubject<CatCategoriesCO>(null);
    let categoryCO:CatCategoriesCO;
    this._categoryCO = <BehaviorSubject<CatCategoriesCO>>new BehaviorSubject(categoryCO);

    this.categoryCO = this._categoryCO.asObservable();   
   }

   updatecategoryCO(categoryCO: CatCategoriesCO)
   {
     AppUtility.log(categoryCO);
      this._categoryCO.next(categoryCO);
     
   }

   resetcategoryCO()
   {
    this._categoryCO.next(null);
   }
}
