import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadEntryService {

  private showHideEntryBS = new BehaviorSubject<boolean>(null);
  showHideObservable = this.showHideEntryBS.asObservable();

  constructor() {}

}
