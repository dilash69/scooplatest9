import { TestBed, inject } from '@angular/core/testing';

import { PlainHttpDataService } from './plain-http-data.service';

describe('PlainHttpDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlainHttpDataService]
    });
  });

  it('should be created', inject([PlainHttpDataService], (service: PlainHttpDataService) => {
    expect(service).toBeTruthy();
  }));
});
