import { OnInit, Directive, ViewContainerRef, TemplateRef, Inject, PLATFORM_ID } from "@angular/core";
import { isPlatformServer } from "@angular/common";
import { AppConstants } from "../common/app.constants";
import { AppCurrentStateService } from "../services/app.current.state.service";
import { AppCurrentState } from "../co/app.current.state";

@Directive({
    selector: '[renderIfUserLoggedIn]'
})

//https://blog.angular-university.io/angular-universal/
export class RenderIfUserLoggedInDirective implements OnInit 
{
    public appCurrentState : AppCurrentState = new AppCurrentState();

    constructor(
        private viewContainer: ViewContainerRef,
        private templateRef: TemplateRef<any>,
        private appCurrentStateService: AppCurrentStateService) {
        }
          
    ngOnInit() 
    {
        this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => 
        {
            this.appCurrentState = appCurrentState;
            
            if(this.appCurrentState)
            {
                if(this.appCurrentState.isUserLoggedIn)
                {
                    this.viewContainer.createEmbeddedView(this.templateRef);
                }
                else
                {
                    this.viewContainer.clear();
                //    window.location.href = AppConstants.sag_panel_login_url;
                }
            }
            else
            {
                this.viewContainer.clear();
            //    window.location.href = AppConstants.sag_panel_login_url;
            }            
        });
    }
}