import {Directive, Input, HostBinding, ElementRef, HostListener} from '@angular/core'
@Directive({
  selector: 'img[imageFallBack]'
})
  
 export class ImagePreloadDirective {
  @Input() imageFallBack: string;
  constructor(private el: ElementRef) { }

  @HostListener('error')
  loadFallBackOnError() {
    const element:HTMLImageElement=<HTMLImageElement>this.el.nativeElement
    element.src=this.imageFallBack || 'https://via.placeholder.com/150';
  }
  
  }