import { DOCUMENT, PlatformLocation } from '@angular/common';
import { Component, ElementRef, Inject, Input, OnInit, ViewChild, HostListener } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject } from 'rxjs';
import { CatCategoriesCO } from './../../co/categoryCO';
import { SclPostCO } from './../../co/sclPostCO';
import { AppUtility } from './../../common/app.utility';
import { AppCurrentStateService } from './../../services/app.current.state.service';
import { BlogModuleService } from './../../services/blog-module.service';


@Component({
    selector: 'app-single-blog',
    templateUrl: './single-blog.component.html',
    styleUrls: ['./single-blog.component.css'],
    providers: [NgbModalConfig, NgbModal]
})
export class SingleBlogComponent implements OnInit 
{
    @ViewChild("fixedHeader, fixAdvertisement",{static: false}) srcElement;
    @ViewChild("hideSocialOnFooter",{static: false}) srcElement1;
    @ViewChild("fixJoinExam",{static: false}) srcElement2;

    private sclPostCOBehaviorSubject = new BehaviorSubject<SclPostCO>(null);
    public slug: string;
    public sclPostCO: SclPostCO;
    public catCategoryCO: CatCategoriesCO = new CatCategoriesCO();
    public errorMessage:string;
    public showPassword:boolean=false;
    public changeClass:string="fa fa-eye toogle-icon";
    public changeType:string="password";
    public pageUrl:string;
    public showVerticalAds: boolean = false;
    public showFooter: boolean = false;

    @Input()
    set sclPostCObject(value) 
    {
        this.sclPostCOBehaviorSubject.next(value);
    };

    get sclPostCObject() 
    {
        return this.sclPostCOBehaviorSubject.getValue();
    }

    constructor(
        private blogModuleService: BlogModuleService,
        private route: ActivatedRoute,
        private meta: Meta,
        private title: Title,
        @Inject(DOCUMENT) private doc,
        private platformLocation: PlatformLocation,
        private appCurrentStateService: AppCurrentStateService,
        config: NgbModalConfig, private modalService: NgbModal,
        private router: Router) 
        {
            config.backdrop = 'static';
            config.keyboard = false;
            platformLocation.onPopState(() => {
                    this.closeProtectedPopupOnBack();
            });
        }

    ngOnInit() 
    {
        if (this.appCurrentStateService.isBrowser()) 
        {
            window.scrollTo(0, 0);
            this.pageUrl = window.location.href;

            
        }
        this.sclPostCOBehaviorSubject
            .subscribe(x => {
                if (this.sclPostCObject) 
                {
                    if(this.sclPostCObject.visibility==="2")
                    {
                        this.openModal.nativeElement.click();
                    }
                    this.catCategoryCO.id = this.sclPostCObject.defaultCategoryId;
                    if (this.sclPostCObject.sclPostsSeoCO.metaTitle) 
                    {
                        //this.title.setTitle(this.sclPostCObject.sclPostsSeoCO.metaTitle.replace(/&amp;/g, '&'));
                        this.title.setTitle(this.sclPostCObject.sclPostsSeoCO.metaTitle);
                        this.meta.addTags([
                            { name: 'description', content: this.sclPostCObject.sclPostsSeoCO.metaDescription },
                            { name: 'keywords', content: JSON.parse(this.sclPostCObject.sclPostsSeoCO.metaKeyword) },
                            {property: 'og:title', content:  this.sclPostCObject.sclPostsSeoCO.metaTitle},
                            {property: 'og:type', content:"article"},
                            {property: 'og:image', content:this.sclPostCObject.sclPostArticlesCO.featuredImage},
                            {property: 'og:url', content:this.sclPostCObject.sclPostArticlesCO.pageUrl},
                            {property: 'og:site_name', content:"EduScoop"}
                        ]);
                    }
                    else
                    {
                        AppUtility.log("single blog component setting title else");
                    }
                    let link: HTMLLinkElement = this.doc.createElement('link');
                    link.setAttribute('rel', 'canonical');
                    this.doc.head.appendChild(link);
                    if(this.sclPostCObject.sclPostArticlesCO.canonicalUrl)
                    {
                        link.setAttribute('href', this.sclPostCObject.sclPostArticlesCO.canonicalUrl);
                    }

/*                     if (this.appCurrentStateService.isBrowser()) 
                    { 
                        let increasePostViewCountApiCall: ApiCall = new ApiCall(ApiActions.increasePostViewCount);
                        increasePostViewCountApiCall.addRequestParams('postId', this.sclPostCObject.id);

                        this.blogModuleService.increasePostViewCount(increasePostViewCountApiCall).subscribe(response => {
                            
                        });
                    } */
                }
            });

        this.hideElementsOnScroll();
        // this.advertisementScrollEvent();
        // this.onWindowScrollBottom();
        // this.hideSocialIconsOnFooter();
    }

    // STICK ELEMENTS ON SCROLL CODE START
    @HostListener('window:scroll', ['$event'])
    hideElementsOnScroll() { // To hide social icon on footer
        if (this.appCurrentStateService.isBrowser())
        {
            if (window.scrollY >= (document.body.scrollHeight - 1600)) {
                this.srcElement1.nativeElement.classList.add("d-none");
            }
            else
            {
                this.srcElement1.nativeElement.classList.remove("d-none");
            }
        

            // STICK JOIN EXAM BLOCK
            if(this.srcElement2)
            {
                if (window.pageYOffset > 800) {
                    this.srcElement2.nativeElement.classList.add("stickyJoinExam");
                    this.srcElement2.nativeElement.classList.add("animated");
                    this.srcElement2.nativeElement.classList.add("fadeInDown");
                } 
                else {
                    this.srcElement2.nativeElement.classList.remove("stickyJoinExam");
                    this.srcElement2.nativeElement.classList.remove("animated");
                    this.srcElement2.nativeElement.classList.remove("fadeInDown");
                }


                if (window.scrollY >= (document.body.scrollHeight - 1800)) {
                    this.srcElement2.nativeElement.classList.remove("stickyJoinExam");
                    this.srcElement2.nativeElement.classList.remove("animated");
                    this.srcElement2.nativeElement.classList.remove("fadeInDown");
                }
            }
        }
    }
    // STICK ELEMENTS ON SCROLL CODE END



    // Sticky Advertisement Code Start
    // advertisementScrollEvent() { // To stick advertisement
    //     if (this.appCurrentStateService.isBrowser())
    //     {
    //         window.addEventListener("scroll", e => {
    //             if (window.pageYOffset > 1200) {
    //                 this.srcElement.nativeElement.classList.add("stickyAdvertisement");
    //                 this.srcElement.nativeElement.classList.add("animated");
    //                 this.srcElement.nativeElement.classList.add("fadeInDown");
    //             } else {
    //                 this.srcElement.nativeElement.classList.remove("stickyAdvertisement");
    //                 this.srcElement.nativeElement.classList.remove("animated");
    //                 this.srcElement.nativeElement.classList.remove("fadeInDown");
    //             }
    //         });
    //     }
    // }

    // onWindowScrollBottom() { // To close advertisement
    //     if (this.appCurrentStateService.isBrowser())
    //     {
    //         window.addEventListener("scroll", e => {
    //             if ((window.innerHeight + window.scrollY - 180) >= document.body.offsetHeight) {
    //                 this.srcElement.nativeElement.classList.remove("stickyAdvertisement");
    //             }
    //         })
    //     }
    // };


    // hideSocialIconsOnFooter() { // To hide social icon on footer
    //     if (this.appCurrentStateService.isBrowser())
    //     {
    //         window.addEventListener("scroll", e => {
    //             if (
    //               window.innerHeight + window.scrollY - 180 >=
    //               document.body.offsetHeight
    //             ) {
    //               this.srcElement1.nativeElement.classList.add("d-none");
    //             }else{
    //                 this.srcElement1.nativeElement.classList.remove("d-none");
    //             }
    //         });
    //     }
    // }
    // Sticky Advertisement Code End

    open(content) 
    {
        this.modalService.open(content);
    }

    protectedForm(passwordObj: JSON, content)
    {
        let password = passwordObj['password'];
        if(password === this.sclPostCObject.sclPostArticlesCO.postPassword)
        {
            this.modalService.dismissAll(content);
        }
        else
        {
            this.errorMessage = "Invalid Password";
        }
    }
    closeProtectedPopup(content)
    {
        this.modalService.dismissAll(content);
        this.router.navigate(['']);
    }

    closeProtectedPopupOnBack()
    {
        this.modalService.dismissAll();
    }

    toggle()
    {
        if(this.showPassword)
        {
            this.showPassword=false;
            this.changeClass="fa fa-eye toogle-icon";
            this.changeType="password";
        }
        else
        {
            this.showPassword=true;
            this.changeClass="fa toogle-icon icon-show icon-hide fa-eye-slash";
            this.changeType="text";
        }
    }

    // toggle1()
    // {
    //     this.showPassword=false;
    // }


}