
import { DOCUMENT } from "@angular/common";
import { Component, Inject, Input, OnInit, Renderer2, ViewChild, HostListener } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { deserialize } from 'serializer.ts/Serializer';
import { AppCurrentState } from '../../co/app.current.state';
import { AppCurrentStateService } from '../../services/app.current.state.service';
import { ManagePopupService } from '../../services/manage-popup.service';
import { AppPopupCO } from './../../co/app.popup.co';
import { CatCategoriesCO } from './../../co/categoryCO';
import { FeedFilterCO } from './../../co/feedFilterCO';
import { PostShareIdCO } from './../../co/postShareIdCO';
import { SclPostCO } from './../../co/sclPostCO';
import { ApiActions } from './../../common/api.actions';
import { ApiCall } from './../../common/api.call';
import { AppConstants } from './../../common/app.constants';
import { BlogModuleService } from './../../services/blog-module.service';
import { FeedIdCO } from './../../co/feedIdCO';
import { FeedCO } from './../../co/feedCO';

@Component({
    selector: 'app-single-category',
    templateUrl: './single-category.component.html',
    styleUrls: ['./single-category.component.css']
})
export class SingleCategoryComponent implements OnInit {
    @ViewChild("fixAdvertisement",{static: false}) srcElement;
    @ViewChild("fixQuickLinks",{static: false}) srcElement1;
    @ViewChild("fixJoinExam",{static: false}) srcElement2;
    @ViewChild("hideSocialOnFooter",{static: false}) srcElement3;
    @ViewChild("footerHeight",{static: false}) srcElement4;
    //@ViewChild("footer",{static: false}) footer;
    private catCategoriesCOBehaviorSubject = new BehaviorSubject<CatCategoriesCO>(null);

    public postIdList: String[];
    public sclPostCOArray: SclPostCO[] = new Array();
    public chunkSize: number;
    public load = false;
    public noPost = false;
    public catCategoryCO: CatCategoriesCO;
    public sclPostCO: SclPostCO = new SclPostCO();
    public pageUrl: string;
    public appCurrentState: AppCurrentState = new AppCurrentState();
    public index:number;
    //public postShareIdCOList:PostShareIdCO[]= new Array();
   // public postShareIdCOList:FeedIdCO[]= new Array();
    public feedCOList:FeedCO[]= new Array();
    public feedIdCOList:FeedIdCO[]= new Array();
    public feedFilterCO:FeedFilterCO = new FeedFilterCO();
    public appPopupCO: AppPopupCO = new AppPopupCO();
    //@Output() defaultCategoryId = new EventEmitter();

    @Input()
    set catCategoriesCO(value) 
    {
        this.catCategoriesCOBehaviorSubject.next(value);
    };

    get catCategoriesCO() 
    {
        return this.catCategoriesCOBehaviorSubject.getValue();
    }


    constructor(private blogModuleService: BlogModuleService,
        private appCurrentStateService: AppCurrentStateService,
        private managePopupService: ManagePopupService,
        private renderer: Renderer2,
        private meta: Meta,
        private title: Title,
        private route: ActivatedRoute,
        @Inject(DOCUMENT) private doc) 
        {
        }

    ngOnInit() 
    {
        this.managePopupService.appPopupCO.subscribe(appPopupCO => {
            this.appPopupCO = appPopupCO;
      
            if (this.appCurrentStateService.isBrowser()) 
            {
                if (this.appPopupCO.isAnyPopupOpen()) 
                {
                    this.renderer.addClass(document.body, 'modal-open');
                }
                else 
                {
                    this.renderer.removeClass(document.body, 'modal-open');
                }
            }
        });

        if (this.appCurrentStateService.isBrowser()) 
        {
            window.scrollTo(0, 0);
        }
        if (this.appCurrentStateService.isBrowser()) 
        {
            this.pageUrl = window.location.href;
        }
        this.appCurrentStateService.appCurrentState.subscribe(appCurrentState => {
            this.appCurrentState = appCurrentState;

            /* if(this.appCurrentStateService.isLoggedInChecked && this.appCurrentState.isUserLoggedIn)
            {
                this.getUserFeedPosts();
            } */
        });

        this.catCategoriesCOBehaviorSubject
            .subscribe(x => {
                if (this.catCategoriesCO) 
                {
                   this.getUserFeedPosts();
                    /*  let getCategoryPostIdListApiCall: ApiCall = new ApiCall(ApiActions.getCategoryPostIdList);
                    getCategoryPostIdListApiCall.addRequestParams('categoryId', this.catCategoriesCO.id);

                    this.blogModuleService.getCategoryPostIdList(getCategoryPostIdListApiCall)
                        .subscribe(responseValues => {
                            this.postIdList = responseValues['categoryPostIdList'];

                            this.sclPostCOArray = <SclPostCO[]>responseValues['sclPostCOList'];
                            this.sclPostCOArray = deserialize<SclPostCO[]>(SclPostCO, this.sclPostCOArray);
                            this.chunkSize = responseValues['chunkSize'];

                            if (this.sclPostCOArray.length == 0 || this.postIdList.length == 0) 
                            {
                                this.noPost = true;
                            } else if (this.sclPostCOArray.length < this.postIdList.length) 
                            {
                                this.load = true;
                            }
                        });   */

                    if (this.catCategoriesCO.cmsCategoryMetaDataCO.metaTitle)
                    {
                        this.title.setTitle(this.catCategoriesCO.cmsCategoryMetaDataCO.metaTitle);
                        this.meta.addTags([
                            { name: 'description', content: this.catCategoriesCO.cmsCategoryMetaDataCO.metaDescription },
                            { name: 'keywords', content: this.catCategoriesCO.cmsCategoryMetaDataCO.metaKeywords },
                            { property: 'og:title', content: this.catCategoriesCO.cmsCategoryMetaDataCO.metaTitle },
                            { property: 'og:image', content: this.catCategoriesCO.displayLogo },
                            { property: 'og:url', content: this.catCategoriesCO.pageUrl },
                            { property: 'og:site_name', content: "EduScoop" }
                        ]);
                    }
                    let link: HTMLLinkElement = this.doc.createElement('link');
                    link.setAttribute('rel', 'canonical');
                    this.doc.head.appendChild(link);
                    link.setAttribute('href', this.catCategoriesCO.canonicalUrl);
                }
            });


        
        // FUNCTION CALLS FOR STICKY METHODS
        this.stickElementsOnScroll();
        //this.advertisementScrollEvent();
        //this.onWindowScrollBottom();
        //this.hideSocialIconsOnFooter()

       /*  var limit = Math.max( document.body.scrollHeight, document.body.offsetHeight, 
            document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight );

            console.log("limit",limit) */

        if(this.catCategoriesCO.isUserJoin)
        {
            
           // this.quickLinksScrollEvent();
            //this.onWindowScrollBottomQuickLinks();
        }
        else
        {
            //this.joinExamScrollEvent();
            //this.onWindowScrollBottomJoinExam();
        }
    }

    // STICK ELEMENTS ON SCROLL CODE START
    @HostListener('window:scroll', ['$event'])
    stickElementsOnScroll() { // Method for sticky advertisement
        if(this.appCurrentStateService.isBrowser())
        {
            if (window.pageYOffset > 900) {
                this.srcElement.nativeElement.classList.add("stickyAdvertisement");
                this.srcElement.nativeElement.classList.add("animated");
                this.srcElement.nativeElement.classList.add("fadeInDown");
            } 
            else {
                this.srcElement.nativeElement.classList.remove("stickyAdvertisement");
                this.srcElement.nativeElement.classList.remove("animated");
                this.srcElement.nativeElement.classList.remove("fadeInDown");
            }

            // REMOVE ADVERTISEMENT WHEN FOOTER START SHOWING
            if (window.scrollY >= (document.body.scrollHeight - 1800)) {
                this.srcElement.nativeElement.classList.remove("stickyAdvertisement");
                this.srcElement.nativeElement.classList.remove("animated");
                this.srcElement.nativeElement.classList.remove("fadeInDown");
            }

            // SHOW AND HIDE SOCIAL SHARE ICONS
            if (window.scrollY >= (document.body.scrollHeight - 1700)) 
            {
                this.srcElement3.nativeElement.classList.add("d-none");
            }
            else{
                this.srcElement3.nativeElement.classList.remove("d-none");
            }


            // STICK JOIN EXAM BLOCK
            if(this.srcElement2)
            {
                if (window.pageYOffset > 800) {
                    this.srcElement2.nativeElement.classList.add("stickyJoinExam");
                    this.srcElement2.nativeElement.classList.add("animated");
                    this.srcElement2.nativeElement.classList.add("fadeInDown");
                } 
                else {
                    this.srcElement2.nativeElement.classList.remove("stickyJoinExam");
                    this.srcElement2.nativeElement.classList.remove("animated");
                    this.srcElement2.nativeElement.classList.remove("fadeInDown");
                }

                // REMOVE JOIN EXAM BEFORE FOOTER
                if (window.scrollY >= (document.body.scrollHeight - 1800)) {
                    this.srcElement2.nativeElement.classList.remove("stickyJoinExam");
                    this.srcElement2.nativeElement.classList.remove("animated");
                    this.srcElement2.nativeElement.classList.remove("fadeInDown");
                }
            }
        }
    }
    // STICK ELEMENTS ON SCROLL CODE END










    // STICKY ADVERTISEMENT, QUICK LINKS AND CATEGORY FOLLOW CODE START
    //  advertisementScrollEvent() { // Method for sticky advertisement
    //     if(this.appCurrentStateService.isBrowser())
    //     {
    //         window.addEventListener("scroll", e => {
    //             if (window.pageYOffset > 900) {
    //                 this.srcElement.nativeElement.classList.add("stickyAdvertisement");
    //                 this.srcElement.nativeElement.classList.add("animated");
    //                 this.srcElement.nativeElement.classList.add("fadeInDown");
    //             } else {
    //                 this.srcElement.nativeElement.classList.remove("stickyAdvertisement");
    //                 this.srcElement.nativeElement.classList.remove("animated");
    //                 this.srcElement.nativeElement.classList.remove("fadeInDown");
    //             }
    //         });
    //     }
    // }  
/* 
     @HostListener('window:scroll', ['$event'])
    advertisementScrollEvent() { // Method for sticky advertisement
        if(this.appCurrentStateService.isBrowser())
        {
            window.addEventListener("scroll", e => {
            console.log("footer",window.pageYOffset)
                if (window.pageYOffset > 900 && window.pageYOffset < 8026 - this.footer.nativeElement.scrollHeight) {
                    this.srcElement.nativeElement.classList.add("stickyAdvertisement");
                    this.srcElement.nativeElement.classList.add("animated");
                    this.srcElement.nativeElement.classList.add("fadeInDown");
                } 
                else {
                    this.srcElement.nativeElement.classList.remove("stickyAdvertisement");
                    this.srcElement.nativeElement.classList.remove("animated");
                    this.srcElement.nativeElement.classList.remove("fadeInDown");
                }
                
            });
        }
    }  */

    // onWindowScrollBottom() { // Method to get height of footer and minus from doc height
    //     if(this.appCurrentStateService.isBrowser())
    //     {
    //         window.addEventListener("scroll", e => {
    //             if ((window.innerHeight + window.scrollY - 180) >= document.body.clientHeight) {
    //                 this.srcElement.nativeElement.classList.remove("stickyAdvertisement");
    //             }
    //         })
    //     }
    // };

    quickLinksScrollEvent() { // Method for sticky quick links
        if(this.appCurrentStateService.isBrowser())
        {
            window.addEventListener("scroll", e => {
                if (window.pageYOffset > 100) {
                    this.srcElement1.nativeElement.classList.add("stickyQuickLinks");
                    this.srcElement1.nativeElement.classList.add("animated");
                    this.srcElement1.nativeElement.classList.add("fadeInDown");
                } else {
                    this.srcElement1.nativeElement.classList.remove("stickyQuickLinks");
                    this.srcElement1.nativeElement.classList.remove("animated");
                    this.srcElement1.nativeElement.classList.remove("fadeInDown");
                }
            });
        }
        
    }

    // onWindowScrollBottomQuickLinks() {  // Method to get height of footer and minus from doc height
    //     if(this.appCurrentStateService.isBrowser())
    //     {
    //         window.addEventListener("scroll", e => {
    //             if ((window.innerHeight + window.scrollY - 50) >= document.body.offsetHeight) {
    //                 this.srcElement1.nativeElement.classList.remove("stickyQuickLinks");
    //             }
    //         })
    //     }
    // };

    joinExamScrollEvent() { // Method for sticky join exams
        if(this.appCurrentStateService.isBrowser())
        {
            window.addEventListener("scroll", e => 
            {
                if(this.srcElement2)
                {
                    if (window.pageYOffset > 800) {
                        this.srcElement2.nativeElement.classList.add("stickyJoinExam");
                        this.srcElement2.nativeElement.classList.add("animated");
                        this.srcElement2.nativeElement.classList.add("fadeInDown");
                    } else {
                        this.srcElement2.nativeElement.classList.remove("stickyJoinExam");
                        this.srcElement2.nativeElement.classList.remove("animated");
                        this.srcElement2.nativeElement.classList.remove("fadeInDown");
                    }
                }
            });
        }
    }

    // onWindowScrollBottomJoinExam() {  // Method to get height of footer and minus from doc height
    //     if(this.appCurrentStateService.isBrowser())
    //     {
    //         window.addEventListener("scroll", e => {
    //             if ((window.innerHeight + window.scrollY - 150) >= document.body.offsetHeight) {
    //                 this.srcElement2.nativeElement.classList.remove("stickyJoinExam");
    //             }
    //         })
    //     }
    // };


    // hideSocialIconsOnFooter() { // To hide social icon on footer
    //     if(this.appCurrentStateService.isBrowser())
    //     {
    //         window.addEventListener("scroll", e => {
    //             if (
    //               window.innerHeight + window.scrollY - 180 >=
    //               document.body.offsetHeight
    //             ) {
    //               this.srcElement3.nativeElement.classList.add("d-none");
    //             }else{
    //                 this.srcElement3.nativeElement.classList.remove("d-none");
    //             }
    //         });
    //     }
    // }
    // STICKY ADVERTISEMENT, QUICK LINKS AND CATEGORY FOLLOW CODE END

    getPosition(): number 
    {
        let postCount: number = this.sclPostCOArray.length; // input
        let AFTER_POST: number = 3;
        let afterPost = AppConstants.AFTER_POST

        if (postCount <= afterPost) {
            let temp: number = postCount / afterPost;
            return (temp < 1) ? 1 : temp;
        }  
        return afterPost;
    }

    public loadMoreLogic()
    {
        let tempIdList:FeedIdCO[]= new Array();
        tempIdList = [];
        for(let i =this.index;i<this.index + (+this.chunkSize); i++)
        {
            if(i<this.feedIdCOList.length)
            {
                tempIdList.push(this.feedIdCOList[i]);
            }
        }

        this.index = this.index + this.chunkSize;
       
       // console.log("temp",tempIdList);
        if(tempIdList.length>0)
        {
            let getSclPostsByFeedCOIdListApiCall: ApiCall = new ApiCall(ApiActions.getSclPostsByFeedCOIdList);
            getSclPostsByFeedCOIdListApiCall.addRequestParams('feedIdCOList', tempIdList);
            getSclPostsByFeedCOIdListApiCall.addRequestParams('loadMoreCounter', "1");
    
                this.blogModuleService.getSclPostsByFeedCOIdList(getSclPostsByFeedCOIdListApiCall).subscribe(response => {
                    //
                    let tempFeedCOList:FeedCO[] = response;
                  //  console.log("tempFeedCOList",tempFeedCOList)
                    for(let i = 0;i<tempFeedCOList.length;i++)
                    {
                        if(tempFeedCOList[i].sclPostCO)
                        {
                            if(this.appCurrentState.isUserLoggedIn)
                            {
                                if(!tempFeedCOList[i].sclPostCO.userCO.isBlockedUser())
                                {
                                    this.sclPostCOArray.push(tempFeedCOList[i].sclPostCO);
                                }
                            }
                            else
                            {
                                this.sclPostCOArray.push(tempFeedCOList[i].sclPostCO);
                            }
                        }
                    }
                });
        }
    }

    public getUserFeedPosts1()
    {
     /*    if(this.appCurrentState.isUserLoggedIn)
        {
            this.sclPostCO.defaultCategoryId = this.catCategoriesCO.id;
            let getUserfeedPostsApiCall: ApiCall = new ApiCall(ApiActions.getUserfeedPosts);
            getUserfeedPostsApiCall.addRequestParams('categoryId', this.catCategoriesCO.id);
            if(this.feedFilterCO.feedFilterTypeCOList.length>0)
            {
                getUserfeedPostsApiCall.addRequestParams('feedFilterCO', this.feedFilterCO);
            }
            
            this.blogModuleService.getUserfeedPosts(getUserfeedPostsApiCall)
            .subscribe(responseValues => {
                this.postShareIdCOList = <PostShareIdCO[]>responseValues['postShareIdCOList'];
                this.postShareIdCOList = deserialize<PostShareIdCO[]>(PostShareIdCO, this.postShareIdCOList);

                let sclPostCOAllList:SclPostCO[] = new Array();
                sclPostCOAllList = <SclPostCO[]>responseValues['sclPostCOList'];
                sclPostCOAllList = deserialize<SclPostCO[]>(SclPostCO, sclPostCOAllList);

                this.feedFilterCO = <FeedFilterCO>responseValues['feedFilterCO'];
                this.feedFilterCO = deserialize<FeedFilterCO>(FeedFilterCO, this.feedFilterCO);

                if(this.appCurrentStateService.isBrowser && localStorage.getItem(AppConstants.dirtyList))
                {
                    if (sclPostCOAllList) 
                    {
                        this.sclPostCOArray = new Array();
                        for (let i = 0; i < sclPostCOAllList.length; i++) 
                        {
                            if (!sclPostCOAllList[i].userCO.isBlockedUser()) 
                            {
                                this.sclPostCOArray.push(sclPostCOAllList[i]);
                            }
                        }
                    }
                    else
                    {
                        this.noPost = true;
                    }
                }
                else
                {
                    this.sclPostCOArray = <SclPostCO[]>responseValues['sclPostCOList'];
                    this.sclPostCOArray = deserialize<SclPostCO[]>(SclPostCO, this.sclPostCOArray);
                }

                this.chunkSize = parseInt(responseValues['chunkSize']);
                this.index = this.chunkSize;
                

                if(!this.sclPostCOArray)
                {
                    this.noPost = true;
                }
                else if(this.sclPostCOArray && this.sclPostCOArray.length == 0)
                {
                    this.noPost = true;
                }
                else
                {
                    this.noPost = false;
                }
            });  
        } 
        else
        { */
           /*  this.sclPostCO.defaultCategoryId = this.catCategoriesCO.id;
            let getMostPopularPostsApiCall: ApiCall = new ApiCall(ApiActions.getMostPopularPosts);
            getMostPopularPostsApiCall.addRequestParams('categoryId', this.catCategoriesCO.id);
            this.blogModuleService.getMostPopularPosts(getMostPopularPostsApiCall)
            .subscribe(responseValues => {
                this.postShareIdCOList = <PostShareIdCO[]>responseValues['postShareIdCOList'];
                this.postShareIdCOList = deserialize<PostShareIdCO[]>(PostShareIdCO, this.postShareIdCOList);

                this.sclPostCOArray = <SclPostCO[]>responseValues['postCOList'];
                this.sclPostCOArray = deserialize<SclPostCO[]>(SclPostCO, this.sclPostCOArray);
                this.chunkSize = parseInt(responseValues['chunkSize']);

                this.index = this.chunkSize;

                if(!this.sclPostCOArray)
                {
                    this.noPost = true;
                }
                else if(this.sclPostCOArray && this.sclPostCOArray.length==0)
                {
                    this.noPost = true;
                }
                else
                {
                    this.noPost = false;
                }
            });   */
        /* } */
    }

    getUserFeedPosts()
    {
        if(this.appCurrentState.isUserLoggedIn)
        {
            this.getStudentFeeds();
        }
        else
        {
            this.getloggedOutUserFeeds();
        }
    }

    getSelectedFilterFeeds(id:string)
    {
        for(let i =0;i<this.feedFilterCO.feedFilterTypeCOList.length;i++)
        {
            if(this.feedFilterCO.feedFilterTypeCOList[i].id===id)
            {
                this.feedFilterCO.feedFilterTypeCOList[i].isSelected = true;
            }
            else
            {
                this.feedFilterCO.feedFilterTypeCOList[i].isSelected = false;
            }
        }
        this.sclPostCOArray =[];
        this.feedIdCOList = [];
        this.getUserFeedPosts()
    }


    openLoginPopUp(categoryId) 
    {
        localStorage.setItem(AppConstants.REGISTRATION_PROCESS, AppConstants.LIKE_REGISTRATION_PROCESS);
        this.managePopupService.showSignPopUp(categoryId,AppConstants.APP_PROMOTION);
    }
  
    openAppPopUp() 
    {
      if (!this.appPopupCO.isAnyPopupOpen()) {
        this.managePopupService.showAndroidAppPopUp();
      } 
    }

    getloggedOutUserFeeds()
    {
        let getLoggedOutFeedsApiCall: ApiCall = new ApiCall(ApiActions.getLoggedOutFeeds);
            getLoggedOutFeedsApiCall.addRequestParams('categoryId', this.catCategoriesCO.id);
            if(this.feedFilterCO.feedFilterTypeCOList.length>0)
            {
                getLoggedOutFeedsApiCall.addRequestParams('feedFilterCO', this.feedFilterCO);
            }
            this.blogModuleService.getUserfeedPosts(getLoggedOutFeedsApiCall)
            .subscribe(responseValues => {

                this.feedFilterCO = <FeedFilterCO>responseValues['feedFilterCO'];
                this.feedFilterCO = deserialize<FeedFilterCO>(FeedFilterCO, this.feedFilterCO);

                this.feedCOList = <FeedCO[]>responseValues['feedCOList'];
                this.feedCOList = deserialize<FeedCO[]>(FeedCO, this.feedCOList);

                this.sclPostCOArray =[];

                if (this.feedCOList && this.feedCOList.length > 0) 
                {
                    this.noPost = false;
                    this.feedIdCOList = <FeedIdCO[]>responseValues['feedIdCOList'];
                    this.feedIdCOList = deserialize<FeedIdCO[]>(FeedIdCO, this.feedIdCOList);

                    this.chunkSize = parseInt(responseValues['chunkSize']);

                    for (let i = 0; i < this.feedCOList.length; i++) 
                    {
                        if (this.feedCOList[i].sclPostCO) 
                        {
                            this.feedCOList[i].sclPostCO.separateVideoAttachmentCO();
                            this.sclPostCOArray.push(this.feedCOList[i].sclPostCO)
                        }
                    }

                    this.index = this.chunkSize;
                }
                else
                {
                    this.noPost = true;
                }
            }); 
    }

    getStudentFeeds()
    {
        let getStudentFeedsApiCall: ApiCall = new ApiCall(ApiActions.getStudentFeeds);
        getStudentFeedsApiCall.addRequestParams('categoryId', this.catCategoriesCO.id);
            if(this.feedFilterCO.feedFilterTypeCOList.length>0)
            {
                getStudentFeedsApiCall.addRequestParams('feedFilterCO', this.feedFilterCO);
            }
            this.blogModuleService.getStudentFeeds(getStudentFeedsApiCall)
            .subscribe(responseValues => {

                this.feedFilterCO = <FeedFilterCO>responseValues['feedFilterCO'];
                this.feedFilterCO = deserialize<FeedFilterCO>(FeedFilterCO, this.feedFilterCO);

                this.feedCOList = <FeedCO[]>responseValues['feedCOList'];
                this.feedCOList = deserialize<FeedCO[]>(FeedCO, this.feedCOList);

                this.sclPostCOArray =[];

                if(this.feedCOList && this.feedCOList.length >0)
                {
                    this.noPost = false;
                    this.feedIdCOList = <FeedIdCO[]>responseValues['feedIdCOList'];
                    this.feedIdCOList = deserialize<FeedIdCO[]>(FeedIdCO, this.feedIdCOList);
    
                    this.chunkSize = parseInt(responseValues['chunkSize']);
    
                    for(let i = 0;i<this.feedCOList.length;i++)
                    {
                        if(this.feedCOList[i].sclPostCO && !this.feedCOList[i].sclPostCO.userCO.isBlockedUser())
                        {
                            this.feedCOList[i].sclPostCO.separateVideoAttachmentCO();
                            this.sclPostCOArray.push(this.feedCOList[i].sclPostCO)
                        }
                    }
                     
                    this.index = this.chunkSize;
                }
                else
                {
                    this.noPost = true;
                }
            }); 
    }


}